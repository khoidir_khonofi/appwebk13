<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BackendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
       'Aset/bower_components/bootstrap/dist/css/bootstrap.min.css',
         'Aset/bower_components/font-awesome/css/font-awesome.min.css',
         'Aset/bower_components/Ionicons/css/ionicons.min.css',
         'Aset/dist/css/AdminLTE.min.css',
          'Aset/dist/css/blue.css',
          //'Aset/dist/css/grafik.css',
         'Aset/dist/css/skins/_all-skins.min.css',
         'Aset/bower_components/morris.js/morris.css',
         'Aset/bower_components/jvectormap/jquery-jvectormap.css',
         'Aset/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
         'Aset/bower_components/bootstrap-daterangepicker/daterangepicker.css',
         'Aset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',

            ];
    public $js = [
      // 'Aset/bower_components/jquery/dist/jquery.min.js',
        //'Aset/bower_components/jquery/dist/jquery.js',
        'Aset/bower_components/jquery-ui/jquery-ui.min.js',
        'Aset/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'Aset/bower_components/raphael/raphael.min.js',
        //'Aset/bower_components/morris.js/morris.min.js',
        'Aset/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
        'Aset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'Aset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'Aset/bower_components/jquery-knob/dist/jquery.knob.min.js',
        'Aset/bower_components/moment/min/moment.min.js',
        'Aset/bower_components/chart.js/Chart.js',
        'Aset/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        'Aset/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'Aset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'Aset/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'Aset/bower_components/fastclick/lib/fastclick.js',
        'Aset/dist/js/adminlte.min.js',
       'Aset/dist/js/pages/dashboard.js',
        'Aset/dist/js/demo.js',
        'Aset/dist/js/grafik.js',
        'Aset/dist/js/modal.js',
        //'Aset/dist/js/jquery-2.2.1.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
