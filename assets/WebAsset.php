<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WebAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'WebAsset/css/font-awesome.min.css',
        'WebAsset/css/style.minified.css',
        'WebAsset/css/main.css',
        'WebAsset/css/style.html',

            ];
    public $js = [
        'WebAsset/js/vendor/jquery-2.2.4.min.js',
        'WebAsset/js/scripts.minified.js',
        'WebAsset/js/main.js',
        'WebAsset/js/jquery-1.10.2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
