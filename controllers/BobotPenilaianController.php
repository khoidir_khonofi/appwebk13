<?php

namespace app\controllers;

use Yii;
use app\models\BobotPenilaian;
use app\models\BobotPenilaianSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Kelas;
use app\models\Mengajar;
/**
 * BobotPenilaianController implements the CRUD actions for BobotPenilaian model.
 */
class BobotPenilaianController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }
    /**
     * Lists all BobotPenilaian models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new BobotPenilaianSearch(['id_sekolah' => $sekolah->id_sekolah]);
            
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new BobotPenilaianSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new BobotPenilaianSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new BobotPenilaianSearch();
            }
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BobotPenilaian model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BobotPenilaian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BobotPenilaian();

        if ($model->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $cekbobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->one();
            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            if ($cekbobot) {
                return "<script>alert('Bobot kelas ".$cekbobot->kelas->tingkat_kelas.''.$cekbobot->kelas->nama." sudah ada');
                                window.location = 'index.php?r=bobot-penilaian';
                        </script>";
            }else{
                 $model->save();
            }
           
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BobotPenilaian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BobotPenilaian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BobotPenilaian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BobotPenilaian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BobotPenilaian::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
