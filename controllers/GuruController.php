<?php

namespace app\controllers;

use Yii;
use app\models\Guru;
use app\models\GuruSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Users;
use app\models\Siswa;
use app\models\Sekolah;
use yii\web\UploadedFile;
/**
 * GuruController implements the CRUD actions for Guru model.
 */
class GuruController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
  public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all Guru models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            
            $user = User::find()->where(['id_user' => $sekolah['id_user']])->one();
                if($user){
                    $searchModel = new GuruSearch(['id_sekolah' => $sekolah['id_sekolah']]);
                }else{
                    $searchModel = new GuruSearch(['id_user' => Yii::$app->user->identity->id_user]);
                }
         }elseif(Yii::$app->user->identity->role=="Guru"){
            $searchModel = new GuruSearch(['id_user' => Yii::$app->user->identity->id_user]);
         }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new GuruSearch(['id_guru' => $siswa->id_guru]);
         }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new GuruSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionLihatfoto($id)
    {
       
        $model = Guru::find()->where(['id_guru' => $id])->one();

        return $this->renderAjax('lihatfoto', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Guru model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Guru model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Guru();
       
        if ($model->load(Yii::$app->request->post())) {
            $cekuser = Guru::find()->where(['id_user' => $model->id_user])->one();
            if ($cekuser) {
                return "<script>
                    alert('user sudah dipakai');
                    window.location= 'index.php?r=guru';
                </script>";
            }else{
                $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
                $model->id_sekolah = $sekolah->id_sekolah;

                $tmp = UploadedFile::getInstance($model, 'fotoguru');
                if ($tmp != NULL) {
                    $model->foto = $tmp->baseName.'.'.$tmp->extension;
                }
                
                $model->save();
                if ($tmp != NULL) {
                    $tmp->saveAs('foto_guru/'.$model->foto);
                }
                
                
                 Yii::$app->getSession()->setFlash(
                        'success','Data saved!');
               
                    return $this->redirect(['index']);
          }  
        }else{
                return $this->renderAjax('_form', [
                'model' => $model,
          
        ]);
    }
    }

    /**
     * Updates an existing Guru model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$user = User::find()->where(['id_user' => $id])->one();
        if ($model->load(Yii::$app->request->post())){
            $tmp = UploadedFile::getInstance($model, 'fotoguru');
            if ($tmp != NULL) {
                $model->foto = $tmp->baseName.'.'.$tmp->extension;
            }
            
            $model->save();
            if ($tmp != NULL) {
                $tmp->saveAs('foto_guru/'.$model->foto);
            }

             Yii::$app->getSession()->setFlash(
                    'success','Data saved!');

            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Guru model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Guru model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Guru the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Guru::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
