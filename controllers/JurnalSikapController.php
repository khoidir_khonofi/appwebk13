<?php

namespace app\controllers;

use Yii;
use app\models\JurnalSikap;
use app\models\JurnalSikapSearch;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Semester;
use app\models\SikapSosial;
use app\models\SikapSpiritual;
use app\models\Guru;
use app\models\Siswa;

/**
 * JurnalSikapController implements the CRUD actions for JurnalSikap model.
 */
class JurnalSikapController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all JurnalSikap models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->where(['id_siswa' => $siswa->id_siswa]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
            
        return $this->render('index', [
            'searchModel' => new JurnalSikapSearch(),
            'dataProvider' => $provider, 
        ]);
    }

    public function actionSearch()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $guru->id_sekolah]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = JurnalSikapSearch::find()->where(['id_siswa' => $siswa->id_siswa]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
        ]);
            
        return $this->renderAjax('_search', [
            'model' => new JurnalSikapSearch(),
            'dataProvider' => $provider, 
        ]);
    }

    /**
     * Displays a single JurnalSikap model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JurnalSikap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JurnalSikap();

        if ($model->load(Yii::$app->request->post())) {
            $cek = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->AndWhere(['sikap' => $model->sikap])->one();
            if ($cek) {
                return  "<script>alert('Sikap ".$model->sikap." atas nama ".$model->siswa->nama." telah dibuat'); window.location = '".Yii::$app->urlManager->createUrl(['jurnal-sikap'])."';</script>";
            }else{
                 if (Yii::$app->user->identity->role=="Guru") {
                    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $model->id_sekolah = $guru->id_sekolah;
                 }else{
                    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $model->id_sekolah = $sekolah->id_sekolah;
                }
                $model->save();
                return $this->redirect(['index']);
            }
           
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing JurnalSikap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JurnalSikap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JurnalSikap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JurnalSikap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JurnalSikap::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSiswa($id)
    {
        $jumlah=Siswa::find()->where(['id_kelas'=>$id])->count();
        $kec=Siswa::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_siswa;?>"><?php echo $ke->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

    public function actionSemester($id)
    {
        $jumlah=Siswa::find()->where(['id_siswa'=>$id])->count();
        $kec=Siswa::find()->where(['id_siswa'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_semester;?>"><?php echo $ke->semester->semester;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

    public function actionSikap($sikap)
    {
        if ($sikap == "Sosial") {?>
            
                 <option value="Jujur">Jujur</option>
                 <option value="Disiplin">Disiplin</option>
                 <option value="Tanggung Jawab">Tanggung Jawab</option>
                 <option value="Santun">Santun</option>
                 <option value="Peduli">Peduli</option>
                 <option value="Percaya Diri">Percaya Diri</option>

        <?php }else{ ?>
            
                <option value="Ketaatan Beribadah">Ketaatan Beribadah</option>
                <option value="Berprilaku Syukur">Berprilaku Syukur</option>
                <option value="Berdoa sebelum dan sesudah melakukan kegiatan">Berdoa sebelum dan sesudah melakukan kegiatan</option>
                <option value="Toleransi dalam beragama">Toleransi dalam beragama</option>
       <?php }
    }
}
