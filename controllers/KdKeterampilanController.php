<?php

namespace app\controllers;

use Yii;
use app\models\KdKeterampilan;
use app\models\KdKeterampilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KdKeterampilanController implements the CRUD actions for KdKeterampilan model.
 */
class KdKeterampilanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KdKeterampilan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KdKeterampilanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KdKeterampilan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KdKeterampilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KdKeterampilan();

        if ($model->load(Yii::$app->request->post())) {
             $mapel = KdKeterampilan::find()->where(['mata_pelajaran' => $model->mata_pelajaran, 'kelas' => $model->kelas, 'no_kd' => $model->no_kd])->one();
            if ($mapel) {
                return "gagal";
            }else{
                $model->save();
                return $this->redirect(['kd-keterampilan/create']);
            }
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAmbil()
    {
        $model = new KdKeterampilan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kd]);
        }

        return $this->renderAjax('ambil', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KdKeterampilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kd]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KdKeterampilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KdKeterampilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KdKeterampilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KdKeterampilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
