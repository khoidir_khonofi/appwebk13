<?php

namespace app\controllers;

use Yii;
use app\models\Kelas;
use app\models\KelasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Siswa;
use yii\filters\VerbFilter;
use app\models\Semester;

/**
 * KelasController implements the CRUD actions for Kelas model.
 */
class KelasController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all Kelas models.
     * @return mixed
     */
    public function actionIndex()
    {
        

        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' =>  Yii::$app->user->identity->id_user])->one();
            $searchModel = new KelasSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KelasSearch(['id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KelasSearch(['id_kelas' => $siswa->id_kelas]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kelas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kelas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kelas();

        if ($model->load(Yii::$app->request->post())) {
            $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model->id_sekolah = $sekolah->id_sekolah;
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Kelas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kelas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kelas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kelas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kelas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     public function actionSemester($id)
    {
        //$model = Kelas::find()->where(['id_kelas' => $id])->one();
        $jumlah=Semester::find()->where(['id_semester'=>$id])->count();
        $kec=Semester::find()->where(['id_semester'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->tahun_ajaran;?>"><?php echo $ke->tahun_ajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
