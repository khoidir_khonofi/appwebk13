<?php

namespace app\controllers;

use Yii;
use app\models\KkmSatuanPendidikan;
use app\models\KkmSatuanPendidikanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Siswa;
use app\models\Mengajar;
use app\models\Guru;
use app\models\Kelas;
/**
 * KkmSatuanPendidikanController implements the CRUD actions for KkmSatuanPendidikan model.
 */
class KkmSatuanPendidikanController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all KkmSatuanPendidikan models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KkmSatuanPendidikanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new KkmSatuanPendidikanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new KkmSatuanPendidikanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new KkmSatuanPendidikanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KkmSatuanPendidikanSearch(['id_kelas' => $siswa->id_kelas]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KkmSatuanPendidikan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionInterval($id)
    {
        $model = KkmSatuanPendidikan::find()->where(['id_kkm' => $id])->one();
        return $this->renderAjax('interval',[
            'model' => $model,
        ]);
    }

    /**
     * Creates a new KkmSatuanPendidikan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KkmSatuanPendidikan();
        if ($model->load(Yii::$app->request->post())) {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $cek = KkmSatuanPendidikan::find()->where(['id_kelas' => $model->id_kelas])->one();
            if ($cek) {
                return "<script>alert('KKM Kelas ".$model->kelas->tingkat_kelas." ".$model->kelas->nama." sudah ada'); window.location='index.php?r=kkm-satuan-pendidikan';</script>";
            }else{

                if (Yii::$app->user->identity->role=="Guru") {
                    $model->id_sekolah = $guru->id_sekolah;
                }else{
                    $model->id_sekolah = $sekolah->id_sekolah;
                }
                $model->save();
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KkmSatuanPendidikan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KkmSatuanPendidikan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KkmSatuanPendidikan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KkmSatuanPendidikan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KkmSatuanPendidikan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
