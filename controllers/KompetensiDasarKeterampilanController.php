<?php

namespace app\controllers;

use Yii;
use app\models\KompetensiDasarKeterampilan;
use app\models\KompetensiDasarKeterampilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Guru;
use app\models\Siswa;
use app\models\KdKeterampilan;
use app\models\MataPelajaran;
use app\models\Mengajar;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\SdNilaiHarianKeterampilan;
use app\models\SmpNilaiHarianKeterampilan;
use app\models\SmaNilaiHarianKeterampilan;
/**
 * KompetensiDasarKeterampilanController implements the CRUD actions for KompetensiDasarKeterampilan model.
 */
class KompetensiDasarKeterampilanController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }
    /**
     * Lists all KompetensiDasarKeterampilan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();

        if(Yii::$app->user->identity->role=="Administrator"){
            $searchModel = new KompetensiDasarKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new KompetensiDasarKeterampilanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new KompetensiDasarKeterampilanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new KompetensiDasarKeterampilanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KompetensiDasarKeterampilanSearch(['id_kelas' => $siswa->id_kelas]);
        }
       
      
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new KompetensiDasarKeterampilanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionLihatkd($id)
    {
       
        $searchModel = new KompetensiDasarKeterampilanSearch(['id_mapel' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHapus()
    {
        if ($row = Yii::$app->request->post('hapus')) {
            foreach ($row as $key => $value) {
                $sd = SdNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $value])->one();
                $smp = SmpNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $value])->one();
                $sma = SmaNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $value])->one();
                if ($sd || $smp || $sma) {
                    return "<script>alert('data masih dipakai');
                        window.location = 'index.php?r=kompetensi-dasar-keterampilan';</script>";
                }else{
                	if(Yii::$app->user->identity->role == 'Administrator' || Yii::$app->user->identity->role == 'Administrator'){
                		$model = KompetensiDasarKeterampilan::findOne($value)->delete();
                	}else{
                		return "<script>alert('Gagal... Anda bukan Administrator');
                        window.location = 'index.php?r=kompetensi-dasar-keterampilan';</script>";
                	}
                    
                }
            }
            
        }else{
             return "<script>alert('Belum Dipilih');
                    window.location='index.php?r=kompetensi-dasar-keterampilan';</script>";
        }
        return $this->redirect(['index']);

    }

    /**
     * Displays a single KompetensiDasarKeterampilan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionLihatkade($id)
    {
        $model = KdKeterampilan::find()->where(['kelas' => $id])->groupBy(['mata_pelajaran'])->all();
        return $this->renderAjax('lihatkade', [
            'model' => $model,
        ]);
    }

    public function actionAmbil()
    {
        $kd = new KompetensiDasarKeterampilan();
        if ($kd->load(Yii::$app->request->post())) {           
            $model = KdKeterampilan::find()->all();
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $cekmapel = KompetensiDasarKeterampilan::find()->where(['id_mapel' => $kd->id_mapel, 'id_kelas' => $kd->id_kelas])->one();
            if ($cekmapel) {
                return "<script>alert('KD mata pelajaran ".$kd->mapel->nama_mata_pelajaran." kelas ".$kd->kelas->tingkat_kelas."".$kd->kelas->nama." sudah tersedia');   window.location='index.php?r=kompetensi-dasar-keterampilan';</script>";
            }else{
                foreach ($model as $key => $value) {
                    if ($value->kelas == $kd->kelas->tingkat_kelas AND $value->mata_pelajaran == $kd->mapel->nama_mata_pelajaran) {
                        $newkd = new KompetensiDasarKeterampilan();
                        if (Yii::$app->user->identity->role=="Administrator") {
                            $newkd->id_sekolah = $sekolah->id_sekolah;
                        }else{
                            $newkd->id_sekolah = $guru->id_sekolah;
                        }
                        $newkd->id_kelas = $kd->id_kelas;
                        $newkd->id_mapel = $kd->id_mapel;
                        $newkd->judul = $value->judul;
                        $newkd->no_kd = $value->no_kd;
                        $newkd->save();
                    }else{
                        echo "gagal";
                    }
                }
            }
            return $this->redirect(['index']);
        }
        return $this->renderAjax('ambil', [
            'kd' => $kd
        ]);
    }

    /**
     * Creates a new KompetensiDasarKeterampilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KompetensiDasarKeterampilan();
        $mapel = new MataPelajaran();

        if ($model->load(Yii::$app->request->post()) OR $mapel->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();

            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
                $model->id_kelas = $kelas->id_kelas;
            }
            
            $model->save();
            $cekkd = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekkd) {
                $kd = KompetensiDasarKeterampilan::find()
                    ->where(['id_mapel' => $cekkd->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                    $row = count($kd);
                    $hitung += $value['kkm'];
                    $hasil = $hitung/$row;
                }
                $cekkd->kkm_keterampilan = round($hasil);
                $cekkd->save();
            }else{
                $mapel->kkm_keterampilan = $model->kkm;
                $mapel->save();

            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

     public function actionTambahkd($id)
    {
        $model = new KompetensiDasarKeterampilan();
        $mapel = new MataPelajaran();
        if ($model->load(Yii::$app->request->post()) OR $mapel->load(Yii::$app->request->post())) {
            $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $mapel = MataPelajaran::find()->where(['id_mapel' => $id])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            
            $model->id_mapel = $mapel->id_mapel;
            $model->id_kelas = $mapel->id_kelas;
            $model->save();
             $cekkd = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekkd) {
                $kd = KompetensiDasarKeterampilan::find()
                    ->where(['id_mapel' => $cekkd->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                    $row = count($kd);
                    $hitung += $value['kkm'];
                    $hasil = $hitung/$row;
                }
                $cekkd->kkm_keterampilan = round($hasil);
                $cekkd->save();
            }else{
                $mapel->kkm_keterampilan = $model->kkm;
                $mapel->save();

            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('tambahkd', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KompetensiDasarKeterampilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mapel = new MataPelajaran();
        if ($model->load(Yii::$app->request->post()) OR $mapel->load(Yii::$app->request->post())) {
            $model->save();
            $cekkd = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekkd) {
                $kd = KompetensiDasarKeterampilan::find()
                    ->where(['id_mapel' => $cekkd->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                    $row = count($kd);
                    $hitung += $value['kkm'];
                    $hasil = $hitung/$row;
                }
                $cekkd->kkm_keterampilan = round($hasil);
                $cekkd->save();
            }else{
                $mapel->kkm_keterampilan = $model->kkm;
                $mapel->save();

            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KompetensiDasarKeterampilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $sd = SdNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $id])->one();
        $smp = SmpNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $id])->one();
        $sma = SmaNilaiHarianKeterampilan::find()->where(['id_kd_keterampilan' => $id])->one();
        if ($sd || $smp || $sma) {
            return "<script>alert('data masih dipakai');
                    window.location = 'index.php?r=kompetensi-dasar-keterampilan';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the KompetensiDasarKeterampilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KompetensiDasarKeterampilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KompetensiDasarKeterampilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMapel($id)
    {
        $jumlah=MataPelajaran::find()->where(['id_kelas'=>$id])->count();
        $kec=MataPelajaran::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_mapel;?>"><?php echo $ke->nama_mata_pelajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
