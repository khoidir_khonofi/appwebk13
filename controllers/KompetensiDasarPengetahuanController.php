<?php

namespace app\controllers;

use Yii;
use app\models\KompetensiDasarPengetahuan;
use app\models\KompetensiDasarPengetahuanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Guru;
use app\models\Mengajar;
use app\models\Sekolah;
use app\models\KdPengetahuan;
use app\models\Kelas;
use app\models\Siswa;
use app\models\MataPelajaran;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\SmaNilaiHarianPengetahuan;
/**
 * KompetensiDasarPengetahuanController implements the CRUD actions for KompetensiDasarPengetahuan model.
 */
class KompetensiDasarPengetahuanController extends Controller
{
        public $layout = "backend.php";

    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }
    /**
     * Lists all KompetensiDasarPengetahuan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

        if(Yii::$app->user->identity->role=="Administrator"){
            $searchModel = new KompetensiDasarPengetahuanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new KompetensiDasarPengetahuanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new KompetensiDasarPengetahuanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new KompetensiDasarPengetahuanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new KompetensiDasarPengetahuanSearch(['id_kelas' => $siswa->id_kelas]);
        }
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new KompetensiDasarPengetahuanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHapus()
    {
        if ($row = Yii::$app->request->post('hapus')) {
            foreach ($row as $key => $value) {
                $sd = SdNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $value])->one();
                $smp = SmpNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $value])->one();
                $sma = SmaNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $value])->one();
                if ($sd || $smp || $sma) {
                    return "<script>alert('data masih dipakai');
                        window.location = 'index.php?r=kompetensi-dasar-pengetahuan';</script>";
                }else{
                    $model = KompetensiDasarPengetahuan::findOne($value)->delete();
                }
            }
        }else{
             return "<script>alert('Belum Dipilih');
                    window.location='index.php?r=kompetensi-dasar-pengetahuan';</script>";
        }
        return $this->redirect(['index']);

    }

     public function actionLihatkade($id)
    {
        $model = KdPengetahuan::find()->where(['kelas' => $id])->groupBy(['mata_pelajaran'])->all();
        return $this->renderAjax('lihatkade', [
            'model' => $model,
        ]);
    }

    public function actionLihatkd($id)
    {
       
        $searchModel = new KompetensiDasarPengetahuanSearch(['id_mapel' => $id]);
        
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KompetensiDasarPengetahuan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

     public function actionAmbil()
    {
        
        $kd = new KompetensiDasarPengetahuan();
        if ($kd->load(Yii::$app->request->post())) {           
            $model = KdPengetahuan::find()->all();
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $cekmapel = KompetensiDasarPengetahuan::find()->where(['id_mapel' => $kd->id_mapel, 'id_kelas' => $kd->id_kelas])->one();
            if ($cekmapel) {
                return "<script>alert('KD mata pelajaran ".$kd->mapel->nama_mata_pelajaran."  kelas ".$kd->kelas->tingkat_kelas."".$kd->kelas->nama." sudah tersedia'); window.location='index.php?r=kompetensi-dasar-pengetahuan';</script>";
            }else{
                foreach ($model as $key => $value) {
                    if ($value->kelas == $kd->kelas->tingkat_kelas AND $value->mata_pelajaran == $kd->mapel->nama_mata_pelajaran) {
                        $newkd = new KompetensiDasarPengetahuan();
                        if (Yii::$app->user->identity->role=="Administrator") {
                            $newkd->id_sekolah = $sekolah->id_sekolah;
                        }else{
                            $newkd->id_sekolah = $guru->id_sekolah;
                        }
                        
                        $newkd->id_kelas = $kd->id_kelas;
                        $newkd->id_mapel = $kd->id_mapel;
                        $newkd->judul = $value->judul;
                        $newkd->no_kd = $value->no_kd;
                        $newkd->save();
                    }else{
                        echo "gagal";
                    }
                }
            }
           
            return $this->redirect(['index']);
        }

        return $this->renderAjax('ambil', [
            'kd' => $kd
        ]);
    }

    /**
     * Creates a new KompetensiDasarPengetahuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $model = new KompetensiDasarPengetahuan();
      $mapel = new MataPelajaran();

      if ($model->load(Yii::$app->request->post()) OR $mapel->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();

            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
                $model->id_kelas = $kelas->id_kelas;
            }
            
            $model->save();

            $cekmapel = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekmapel) {
                $kd = KompetensiDasarPengetahuan::find()
                    ->where(['id_mapel' => $cekmapel->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                  $row = count($kd);
                  $hitung += $value['kkm'];
                  $hasil = $hitung/$row;
                }
                $cekmapel->kkm_pengetahuan = round($hasil);
                $cekmapel->save();
            }else{
                $mapel->kkm_pengetahuan = $model->kkm;
                $mapel->save();
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    public function actionTambahkd($id)
    {
    $model = new KompetensiDasarPengetahuan();
    $mapel = new MataPelajaran();
        if ($model->load(Yii::$app->request->post()) OR $mapel->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $mapel = MataPelajaran::find()->where(['id_mapel' => $id])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            $model->id_kelas = $mapel->id_kelas;
            $model->id_mapel = $mapel->id_mapel;
            
            $model->save();
             $cekmapel = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekmapel) {
                $kd = KompetensiDasarPengetahuan::find()
                    ->where(['id_mapel' => $cekmapel->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                  $row = count($kd);
                  $hitung += $value['kkm'];
                  $hasil = $hitung/$row;
                }
                $cekmapel->kkm_pengetahuan = round($hasil);
                $cekmapel->save();
            }else{
                $mapel->kkm_pengetahuan = $model->kkm;
                $mapel->save();
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('tambahkd', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KompetensiDasarPengetahuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mapel = new MataPelajaran();
        if ($model->load(Yii::$app->request->post()) OR  $mapel->load(Yii::$app->request->post())) {
            $model->save();
            $cekmapel = MataPelajaran::find()
                    ->where(['id_mapel' => $model->id_mapel])
                    ->one();
            if ($cekmapel) {
                $kd = KompetensiDasarPengetahuan::find()
                    ->where(['id_mapel' => $cekmapel->id_mapel])
                    ->all();
                $hitung = 0;
                foreach ($kd as $key => $value) {
                  $row = count($kd);
                  $hitung += $value['kkm'];
                  $hasil = $hitung/$row;
                }
                $cekmapel->kkm_pengetahuan = round($hasil);
                $cekmapel->save();
            }else{
                $mapel->kkm_pengetahuan = $model->kkm;
                $mapel->save();
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KompetensiDasarPengetahuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $sd = SdNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $id])->one();
        $smp = SmpNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $id])->one();
        $sma = SmaNilaiHarianPengetahuan::find()->where(['id_kd_pengetahuan' => $id])->one();
        if ($sd || $smp || $sma) {
            return "<script>alert('data masih dipakai');
                        window.location = 'index.php?r=kompetensi-dasar-pengetahuan';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the KompetensiDasarPengetahuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KompetensiDasarPengetahuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KompetensiDasarPengetahuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMapel($id)
    {
        $jumlah=MataPelajaran::find()->where(['id_kelas'=>$id])->count();
        $kec=MataPelajaran::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_mapel;?>"><?php echo $ke->nama_mata_pelajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
