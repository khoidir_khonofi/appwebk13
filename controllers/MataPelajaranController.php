<?php

namespace app\controllers;

use Yii;
use app\models\MataPelajaran;
use app\models\MataPelajaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Siswa;
use app\models\Mengajar;
use app\models\Kelas;
use app\models\Sekolah;
use app\models\Guru;
use app\models\KompetensiDasarKeterampilan;
use app\models\KompetensiDasarPengetahuan;
/**
 * MataPelajaranController implements the CRUD actions for MataPelajaran model.
 */
class MataPelajaranController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],           
        ];
    }

    /**
     * Lists all MataPelajaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        
            if(Yii::$app->user->identity->role=="Administrator"){
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new MataPelajaranSearch(['id_sekolah' => $sekolah->id_sekolah]);
            }elseif (Yii::$app->user->identity->role=="Guru") {
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new MataPelajaranSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new MataPelajaranSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new MataPelajaranSearch();
                }
            }else{
                $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new MataPelajaranSearch(['id_kelas' => $siswa->id_kelas]);
            }

       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionDeleteall()
    {
            if ($row = Yii::$app->request->post('choi')) {
                foreach ($row as $value) {
                    if(Yii::$app->user->identity->role == 'Administrator' || Yii::$app->user->identity->role == 'Administrator'){
                        $ktr = KompetensiDasarKeterampilan::find()->where(['id_mapel' => $value])->one();
                        $peng = KompetensiDasarPengetahuan::find()->where(['id_mapel' => $value])->one();
                        if ($ktr || $peng) {
                            return "<script>alert('data masih dipakai');
                                    window.location='index.php?r=mata-pelajaran';</script>";
                        }else{
                            $model = MataPelajaran::findOne($value)->delete();
                        }
                    }else{
                        return "<script>alert('Gagal.. Anda bukan Administrator');
                                    window.location='index.php?r=mata-pelajaran';</script>";
                    }
                    
                }
            }else{
                 return "<script>alert('Belum Dipilih');
                    window.location='index.php?r=mata-pelajaran';</script>";
            }
            return $this->redirect(['index']);
        
    }

    public function actionSearch()
    {
        
            if(Yii::$app->user->identity->role=="Administrator"){
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new MataPelajaranSearch(['id_sekolah' => $sekolah->id_sekolah]);
            }elseif (Yii::$app->user->identity->role=="Guru") {
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new MataPelajaranSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new MataPelajaranSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new MataPelajaranSearch();
                }
            }else{
                $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new MataPelajaranSearch(['id_kelas' => $siswa->id_kelas]);
            }

       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MataPelajaran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MataPelajaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MataPelajaran();

        if ($model->load(Yii::$app->request->post()) ) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
            foreach ($kelas as $key => $value) {
               
                    if ($model->kelas->tingkat_kelas == $value->tingkat_kelas) {
                        if($sekolah->jenjang_pendidikan != "SMA"){
                            $newmapel = new MataPelajaran();
                            $newmapel->id_kelas = $value->id_kelas;
                            $newmapel->id_sekolah = $sekolah->id_sekolah;
                            $newmapel->nama_mata_pelajaran = $model->nama_mata_pelajaran;
                            $newmapel->tahun_ajaran = $value->tahun_ajaran;
                            $newmapel->kelompok = $model->kelompok;
                            $newmapel->save();
                        }else{
                            $newmapel = new MataPelajaran();
                            $newmapel->id_kelas = $value->id_kelas;
                            $newmapel->id_sekolah = $sekolah->id_sekolah;
                            $newmapel->nama_mata_pelajaran = $model->nama_mata_pelajaran;
                            $newmapel->tahun_ajaran = $value->tahun_ajaran;
                            $newmapel->jurusan = $model->jurusan;
                            $newmapel->kelompok = $model->kelompok;
                            $newmapel->save();
                        }
                    }
                    
            }
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MataPelajaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = MataPelajaran::findone(['id_mapel' =>$id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MataPelajaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $kdktr = KompetensiDasarKeterampilan::find()->where(['id_mapel' => $id])->one();
        $kdpeng = KompetensiDasarPengetahuan::find()->where(['id_mapel' => $id])->one();
        $mengajar = Mengajar::find()->where(['id_mapel' => $id])->one();
        if($kdktr || $mengajar){
            return "<script>alert('data masih dipakai pada KD keterampilan');
            window.location='index.php?r=mata-pelajaran';</script>";
        }elseif($kdpeng || $mengajar){
            return "<script>alert('data masih dipakai pada KD pengetahuan');
            window.location='index.php?r=mata-pelajaran';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the MataPelajaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MataPelajaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MataPelajaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionThnajaran($id)
    {
        //$jumlah=Kelas::find()->where(['id_kelas'=>$id])->count();
        $kec=Kelas::find()->where(['id_kelas'=>$id])->orderBy(['id_kelas' => SORT_DESC])->all();
            if($kec>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->tahun_ajaran;?>"><?php echo $ke->tahun_ajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
