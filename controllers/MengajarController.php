<?php

namespace app\controllers;

use Yii;
use app\models\Mengajar;
use app\models\MengajarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Siswa;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Semester;
use app\models\MataPelajaran;
use app\models\SiswaSearch;
/**
 * MengajarController implements the CRUD actions for Mengajar model.
 */
class MengajarController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all Mengajar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new MengajarSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }else{
            $searchModel = new MengajarSearch(['id_guru' => $guru->id_guru]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new MengajarSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }else{
            $searchModel = new MengajarSearch(['id_guru' => $guru->id_guru]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mengajar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionLihatfoto($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        return $this->renderAjax('lihatfoto', [
            'model' => $model,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new SiswaSearch(['id_kelas' => $id, 'status' => 'Aktif']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('nilaisiswa', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Mengajar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mengajar();

        if ($model->load(Yii::$app->request->post())) {
            $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model->id_sekolah = $sekolah->id_sekolah;
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Mengajar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Mengajar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mengajar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mengajar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mengajar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMapel($id)
    {
        //$jumlah=MataPelajaran::find()->where(['id_kelas'=>$id])->count();
        $mapel = MataPelajaran::find()->where(['id_kelas'=>$id])->orderBy(['id_mapel' => SORT_DESC])->all();
            if($mapel>0){
                foreach($mapel as $ke) { ?>

                    <option value="<?php echo $ke->id_mapel;?>"><?php echo $ke->nama_mata_pelajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
    public function actionTahun($id)
    {
        $jumlah=Semester::find()->where(['id_semester'=>$id])->count();
        $kec=Semester::find()->where(['id_semester'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->tahun_ajaran;?>"><?php echo $ke->tahun_ajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
