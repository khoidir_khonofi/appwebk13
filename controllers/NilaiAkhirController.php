<?php

namespace app\controllers;

use Yii;
use app\models\NilaiAkhir;
use app\models\NilaiAkhirSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Siswa;
use kartik\mpdf\Pdf;
use app\models\SikapAkhir;
use app\models\Kehadiran;
use app\models\Prestasi;
use app\models\Esktrakurikuler;
use app\models\Saran;
use app\models\KondisiKesehatan;
use app\models\TinggiBadan;
use app\models\KkmSatuanPendidikan;
use app\models\NilaiKdKeterampilan;
use app\models\Guru;
use app\models\Kelas;
use app\models\NilaiKdPengetahuan;
use app\models\Mengajar;
use app\models\NilaiAkhirKkmSatuanPendidikan;
/**
 * NilaiAkhirController implements the CRUD actions for NilaiAkhir model.
 */
class NilaiAkhirController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all NilaiAkhir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new NilaiAkhirSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new NilaiAkhirSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new NilaiAkhirSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new NilaiAkhirSearch();
                }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new NilaiAkhirSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new NilaiAkhirSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new NilaiAkhirSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new NilaiAkhirSearch();
                }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new NilaiAkhirSearch(['id_siswa' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPilihsiswa()
    {
        return $this->render('pilihsiswa');
    }
    public function actionPilih()
    {
        return $this->render('pilih');
    }
    public function actionPrintraport($id_siswa)
    {
        $modelA = NilaiAkhir::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'A'])->all();
        $modelB = NilaiAkhir::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'B'])->all();
        $modelC = NilaiAkhir::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'C'])->all();

        $siswa = Siswa::find()->where(['id_siswa' => $id_siswa])->all();
        $pesertadidik = Siswa::find()->where(['id_siswa' => $id_siswa])->one();
        $sekolah = Sekolah::find()->where(['id_sekolah' => $pesertadidik->id_sekolah])->all();
        $nilaisikap = SikapAkhir::find()->where(['id_siswa' => $id_siswa])->all();
        $ekskul = Esktrakurikuler::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $kehadiran = Kehadiran::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $prestasi = Prestasi::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $saran = Saran::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $kondisikesehatan = KondisiKesehatan::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $tinggibadan = TinggiBadan::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();

        $content = $this->renderPartial('raport',[
            'modelA' => $modelA,
            'modelB' => $modelB,
            'modelC' => $modelC,
            'sekolah' => $sekolah,
            'siswa' => $siswa,
            'nilaisikap' => $nilaisikap,
            'pesertadidik' => $pesertadidik,
            'ekskul' => $ekskul,
            'kehadiran' => $kehadiran,
            'saran' => $saran,
            'kondisikesehatan' => $kondisikesehatan,
            'prestasi' => $prestasi,
            'tinggibadan' => $tinggibadan,
        ]);
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            'filename' => 'test.pdf',
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // 'marginLeft' => 5, 'marginTop' => 5, 'marginRight' => 5, 'marginBottom' => 5,
            'defaultFont' => 'Calibri',
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Krajee Report Header'], 
                'SetFooter' => ['raport sd'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        // return the pdf output as per the destination setting
        return $pdf->render();
       
        
    }

    public function actionKkmsatuanpendidikan()
    {
        $searchModel = new NilaiAkhirSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('kkm_satuan_pendidikan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionRangking()
    {
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new NilaiAkhirSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new NilaiAkhirSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new NilaiAkhirSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new NilaiAkhirSearch();
                }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('rangking', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

  
    /**
     * Displays a single NilaiAkhir model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDeleteall()
    {
        if (Yii::$app->user->identity->role == 'Administrator' || Yii::$app->user->identity->role == 'Guru') {
            if ($row = Yii::$app->request->post('hapus')) {
                foreach ($row as $key => $value) {
                    $model = NilaiAkhir::findOne($value)->delete();
            }

            }else{
                return "<script>alert('data belum dipilih'); window.location='index.php?r=nilai-akhir';</script>";
            }
        }else{
            return "<script>alert('Gagal...'); window.location='index.php?r=nilai-akhir';</script>";
        }
        
        return $this->redirect(['index']);
    }

  

    /**
     * Creates a new NilaiAkhir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NilaiAkhir();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NilaiAkhir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NilaiAkhir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiAkhir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiAkhir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiAkhir::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
