<?php

namespace app\controllers;

use Yii;
use app\models\NilaiAkhirKkmSatuanPendidikan;
use app\models\NilaiAkhirKkmSatuanPendidikanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use yii\filters\VerbFilter;
use app\models\NilaiAkhir;
use app\models\KkmSatuanPendidikan;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdPengetahuan;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Mengajar;
use app\models\Siswa;
use app\models\SikapAkhir;
use app\models\Esktrakurikuler;
use app\models\Prestasi;
use app\models\TinggiBadan;
use app\models\Saran;
use app\models\KondisiKesehatan;
use app\models\Kehadiran;
use app\models\Kelas;
use kartik\mpdf\Pdf;
/**
 * NilaiAkhirKkmSatuanPendidikanController implements the CRUD actions for NilaiAkhirKkmSatuanPendidikan model.
 */
class NilaiAkhirKkmSatuanPendidikanController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all NilaiAkhirKkmSatuanPendidikan models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch();
                }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                
                if ($kelas) {
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_kelas' => $kelas->id_kelas]);
                }elseif ($mengajar){
                    $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_mapel' => $mapel->id_mapel]);
                }else{
                    $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch();
                }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiAkhirKkmSatuanPendidikanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintraportkkmsatuanpendidikan($id_siswa)
    {
        $modelA = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'A'])->all();
        $modelB = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'B'])->all();
        $modelC = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $id_siswa])->joinWith('mapel')->where(['id_siswa' => $id_siswa, 'kelompok' => 'C'])->all();
        $test = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $id_siswa])->one();



        $siswa = Siswa::find()->where(['id_siswa' => $id_siswa])->all();
        $pesertadidik = Siswa::find()->where(['id_siswa' => $id_siswa])->one();
        $sekolah = Sekolah::find()->where(['id_sekolah' => $pesertadidik->id_sekolah])->all();
        $nilaisikap = SikapAkhir::find()->where(['id_siswa' => $id_siswa])->all();
        $ekskul = Esktrakurikuler::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $kehadiran = Kehadiran::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $prestasi = Prestasi::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $saran = Saran::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $kondisikesehatan = KondisiKesehatan::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $tinggibadan = TinggiBadan::find()->where(['id_siswa' => $id_siswa, 'id_kelas' => $pesertadidik->id_kelas, 'id_semester' => $pesertadidik->id_semester])->all();
        $kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $pesertadidik->id_kelas])->one();

        $content = $this->renderPartial('raport',[
            'modelA' => $modelA,
            'modelB' => $modelB,
            'modelC' => $modelC,
            'sekolah' => $sekolah,
            'siswa' => $siswa,
            'nilaisikap' => $nilaisikap,
            'pesertadidik' => $pesertadidik,
            'ekskul' => $ekskul,
            'kehadiran' => $kehadiran,
            'test' => $test,
            'saran' => $saran,
            'kondisikesehatan' => $kondisikesehatan,
            'prestasi' => $prestasi,
            'tinggibadan' => $tinggibadan,
            'kkm' => $kkm,
        ]);
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Krajee Report Header'], 
                'SetFooter' => ['raport sd'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        // return the pdf output as per the destination setting
        return $pdf->render();
       
        
    }

    /**
     * Displays a single NilaiAkhirKkmSatuanPendidikan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPilihsiswa()
    {
        return $this->render('pilihsiswa');
    }

    /**
     * Creates a new NilaiAkhirKkmSatuanPendidikan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $nilai = NilaiAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilai = NilaiAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }
        
                
        foreach ($nilai as $key => $nilaiakhir) {
            $model = new NilaiAkhirKkmSatuanPendidikan();
                
            $kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $nilaiakhir->id_kelas])->one();
            $cek = NilaiAkhirKkmSatuanPendidikan::find()
                    ->where(['id_siswa' => $nilaiakhir->id_siswa, 'id_semester' => $nilaiakhir->id_semester])
                    ->andWhere(['id_mapel' => $nilaiakhir->id_mapel])
                    ->one();
            if($cek){
                $cek->id_sekolah = $nilaiakhir->id_sekolah;
                $cek->id_kelas = $nilaiakhir->id_kelas;
                $cek->id_siswa = $nilaiakhir->id_siswa;
                $cek->id_semester = $nilaiakhir->id_semester;
                $cek->id_mapel = $nilaiakhir->id_mapel;
                $cek->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $cek->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $cek->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                $interval = (100-$kkm->kkm)/3;
                $a = 100-$interval;
                $b = $a-$interval;
                $c = $b-$interval;
                $d = $c-$interval;
                    if ($cek->nilai_pengetahuan >= $a) {
                        $ket = "A";
                    }elseif ($cek->nilai_pengetahuan >= $b) {
                        $ket = "B";
                    }elseif ($cek->nilai_pengetahuan >= $kkm->kkm) {
                        $ket = "C";
                    }else{
                        $ket = "D";
                    }
                $cek->predikat_pengetahuan = $ket;
                    if ($cek->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($cek->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($cek->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $cek->predikat_keterampilan = $ktk;
                $tinggi = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nilai_kd' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nilai_kd' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->all();
                $row = count($nilai);
                if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($cek->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }

                        }else {
                            if($row == 1){
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                        $cek->deskripsi_pengetahuan = $x;
                        $cek->save();
                    }
                
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                     if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($cek->predikat_keterampilan == "A") {
                           
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                    $cek->deskripsi_keterampilan = $ket;
                    $cek->save();
                    }  
                
                
                $cek->save();
                    

            }else{
                $model->id_sekolah = $nilaiakhir->id_sekolah;
                $model->id_kelas = $nilaiakhir->id_kelas;
                $model->id_siswa = $nilaiakhir->id_siswa;
                $model->id_semester = $nilaiakhir->id_semester;
                $model->id_mapel = $nilaiakhir->id_mapel;
                $model->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $model->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $model->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                //$kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $model->id_kelas])->one();
                    $interval = (100-$kkm->kkm)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    $d = $c-$interval;
                        if ($model->nilai_pengetahuan >= $a) {
                            $ket = "A";
                        }elseif ($model->nilai_pengetahuan >= $b) {
                            $ket = "B";
                        }elseif ($model->nilai_pengetahuan >= $kkm->kkm) {
                            $ket = "C";
                        }else{
                            $ket = "D";
                        }
                $model->predikat_pengetahuan = $ket;
                    if ($model->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($model->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($model->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $model->predikat_keterampilan = $ktk;
                    $tinggi = NilaiKdPengetahuan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nilai_kd' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nilai_kd' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuan::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
                    if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($model->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }else{
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                    $model->deskripsi_pengetahuan = $x;
                    $model->save();
                    }
                        
                    
                    
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                    if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($model->predikat_keterampilan == "A") {
                            
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                        $model->deskripsi_keterampilan = $ket;
                        $model->save();
                    }
                        
                    
                    
                $model->save();
                
            }
        }return $this->redirect(['index']);
    }

    public function actionCreatesekolahmenengah()
    {

        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $nilai = NilaiAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilai = NilaiAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }
        
                
        foreach ($nilai as $key => $nilaiakhir) {
            $model = new NilaiAkhirKkmSatuanPendidikan();
                
            $kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $nilaiakhir->id_kelas])->one();
            $cek = NilaiAkhirKkmSatuanPendidikan::find()
                    ->where(['id_siswa' => $nilaiakhir->id_siswa, 'id_semester' => $nilaiakhir->id_semester])
                    ->andWhere(['id_mapel' => $nilaiakhir->id_mapel])
                    ->one();
            if($cek){
                $cek->id_sekolah = $nilaiakhir->id_sekolah;
                $cek->id_kelas = $nilaiakhir->id_kelas;
                $cek->id_siswa = $nilaiakhir->id_siswa;
                $cek->id_semester = $nilaiakhir->id_semester;
                $cek->id_mapel = $nilaiakhir->id_mapel;
                $cek->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $cek->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $cek->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                $interval = (100-$kkm->kkm)/3;
                $a = 100-$interval;
                $b = $a-$interval;
                $c = $b-$interval;
                $d = $c-$interval;
                    if ($cek->nilai_pengetahuan >= $a) {
                        $ket = "A";
                    }elseif ($cek->nilai_pengetahuan >= $b) {
                        $ket = "B";
                    }elseif ($cek->nilai_pengetahuan >= $kkm->kkm) {
                        $ket = "C";
                    }else{
                        $ket = "D";
                    }
                $cek->predikat_pengetahuan = $ket;
                    if ($cek->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($cek->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($cek->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $cek->predikat_keterampilan = $ktk;
                $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nph' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nph' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->all();
                $row = count($nilai);
                if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($cek->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }

                        }else {
                            if($row == 1){
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                        $cek->deskripsi_pengetahuan = $x;
                        $cek->save();
                    }
                
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                     if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($cek->predikat_keterampilan == "A") {
                           
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                    $cek->deskripsi_keterampilan = $ket;
                    $cek->save();
                    }  
                
                
                $cek->save();
                    

            }else{
                $model->id_sekolah = $nilaiakhir->id_sekolah;
                $model->id_kelas = $nilaiakhir->id_kelas;
                $model->id_siswa = $nilaiakhir->id_siswa;
                $model->id_semester = $nilaiakhir->id_semester;
                $model->id_mapel = $nilaiakhir->id_mapel;
                $model->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $model->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $model->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                //$kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $model->id_kelas])->one();
                    $interval = (100-$kkm->kkm)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    $d = $c-$interval;
                        if ($model->nilai_pengetahuan >= $a) {
                            $ket = "A";
                        }elseif ($model->nilai_pengetahuan >= $b) {
                            $ket = "B";
                        }elseif ($model->nilai_pengetahuan >= $kkm->kkm) {
                            $ket = "C";
                        }else{
                            $ket = "D";
                        }
                $model->predikat_pengetahuan = $ket;
                    if ($model->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($model->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($model->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $model->predikat_keterampilan = $ktk;
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
                    if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($model->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }else{
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                    $model->deskripsi_pengetahuan = $x;
                    $model->save();
                    }
                        
                    
                    
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                    if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($model->predikat_keterampilan == "A") {
                            
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                        $model->deskripsi_keterampilan = $ket;
                        $model->save();
                    }
                        
                    
                    
                $model->save();
                
            }
        }return $this->redirect(['index']);
    }

    /**
     * Updates an existing NilaiAkhirKkmSatuanPendidikan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NilaiAkhirKkmSatuanPendidikan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiAkhirKkmSatuanPendidikan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiAkhirKkmSatuanPendidikan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiAkhirKkmSatuanPendidikan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
