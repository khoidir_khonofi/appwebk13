<?php

namespace app\controllers;

use Yii;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdKeterampilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Mengajar;
use app\models\Sekolah;
use app\models\Siswa;
use app\models\NilaiAkhir;
use app\models\BobotPenilaianSearch;
use app\models\Guru;
use app\models\BobotPenilaian;
use app\models\MataPelajaran;
use app\models\Kelas;


/**
 * NilaiKdKeterampilanController implements the CRUD actions for NilaiKdKeterampilan model.
 */
class NilaiKdKeterampilanController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all NilaiKdKeterampilan models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdKeterampilanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdKeterampilanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdKeterampilanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdKeterampilanSearch(['id_siswa' => $siswa->id_siswa]);
        }

       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdKeterampilanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdKeterampilanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdKeterampilanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdKeterampilanSearch(['id_siswa' => $siswa->id_siswa]);
        }

       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       
        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatnilai($id)
    {
        $searchModel = new NilaiKdKeterampilanSearch(['id_siswa' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NilaiKdKeterampilan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NilaiKdKeterampilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NilaiKdKeterampilan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai_kd_keterampilan]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NilaiKdKeterampilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->post()) OR $nilaiakhir->load(Yii::$app->request->post())) {
            
            $model->save();

            $ceknilaikd = NilaiAkhir::find()
                            ->where(['id_mapel' => $model->id_mapel])
                            ->andWhere(['id_siswa' => $model->id_siswa])
                            ->one();
            
            if ($ceknilaikd) {
                $nilai = NilaiKdKeterampilan::find()
                        ->where(['id_mapel' => $ceknilaikd->id_mapel])
                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                        ->all();
                $hitung = 0;
                foreach ($nilai as $key => $value) {
                    $row = count($nilai);
                    $hitung += $value['skor'];
                    $result = $hitung/$row;
                    $hasil = round($result);
                }
                $ceknilaikd->nilai_keterampilan = $hasil;

                $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                $tinggi = NilaiKdKeterampilan::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->all();
                $row = count($nilai);

                $interval = (100-$mapel->kkm_keterampilan)/3;
                    $a = 100 - $interval;
                    $b = $a - $interval;
                    $c = $b - $interval;
                    if ($ceknilaikd->nilai_keterampilan >= $a) {
                        $pre = "A";
                    }elseif($ceknilaikd->nilai_keterampilan >= $b){
                        $pre = "B";
                    }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $ceknilaikd->predikat_keterampilan = $pre;



                            if ($ceknilaikd->predikat_keterampilan == 'A') {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }elseif ($ceknilaikd->predikat_keterampilan == 'B') {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            
                            }elseif ($ceknilaikd->predikat_keterampilan == 'C') {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }else{
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                            }
                $ceknilaikd->deskripsi_keterampilan = $ket;
                $ceknilaikd->save();

            }else{
                $nilaiakhir->id_nilai_kd = $model->id_nilai_kd_keterampilan;
                $nilaiakhir->id_sekolah = $model->id_sekolah;
                $nilaiakhir->id_siswa = $model->id_siswa;
                $nilaiakhir->id_kelas = $model->id_kelas;
                $nilaiakhir->id_mapel = $model->id_mapel;
                $nilaiakhir->id_semester = $model->id_semester;
                $nilaiakhir->nilai_keterampilan = round($model->skor);
                $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                $tinggi = NilaiKdKeterampilan::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$model->id_mapel])
                                ->andWhere(['id_siswa' => $model->id_siswa])
                                ->orderBy(['skor' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$model->id_mapel])
                                ->andWhere(['id_siswa' => $model->id_siswa])
                                ->orderBy(['skor' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$model->id_mapel])
                                ->andWhere(['id_siswa' => $model->id_siswa])
                                ->all();
                $row = count($nilai);

                $interval = (100-$mapel->kkm_keterampilan)/3;
                    $a = 100 - $interval;
                    $b = $a - $interval;
                    $c = $b - $interval;
                    if ($nilaiakhir->nilai_keterampilan >= $a) {
                        $pre = "A";
                    }elseif($nilaiakhir->nilai_keterampilan >= $b){
                        $pre = "B";
                    }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $nilaiakhir->predikat_keterampilan = $pre;



                            if ($nilaiakhir->nilai_keterampilan > $mapel->kkm_keterampilan) {
                                if ($row == 1) {
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }elseif ($nilaiakhir->nilai_keterampilan == $mapel->kkm_keterampilan) {
                                if ($row == 1) {
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            
                            }else{
                                if ($row == 1) {
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }
                $nilaiakhir->deskripsi_keterampilan = $ket;
                $nilaiakhir->save();
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NilaiKdKeterampilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $nilaiakhir = NilaiAkhir::find()->where(['id_nilai_kd' => $id])->one();
        if ($nilaiakhir) {
            return "<script>
                    alert('Data masih ada di nilai akhir');
                    window.location = 'index.php?r=nilai-kd-keterampilan';
                    </script>";
        }else{
            $this->findModel($id)->delete();
        }
        

        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiKdKeterampilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiKdKeterampilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiKdKeterampilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
