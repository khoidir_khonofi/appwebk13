<?php

namespace app\controllers;

use Yii;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengahSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\BobotPenilaian;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiAkhir;
use app\models\User;
use app\models\Kelas;
use app\models\Siswa;
use app\models\Mengajar;
use app\models\Sekolah;
use app\models\Guru;
use app\models\MataPelajaran;
use yii\filters\VerbFilter;

/**
 * NilaiKdPengetahuanAkhirSekolahMenengahController implements the CRUD actions for NilaiKdPengetahuanAkhirSekolahMenengah model.
*/
class NilaiKdPengetahuanAkhirSekolahMenengahController extends Controller
{
    public $layout = 'backend.php';
    /**
     * {@inheritdoc}
     */
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all NilaiKdPengetahuanAkhirSekolahMenengah models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new NilaiKdPengetahuanAkhirSekolahMenengahSearch(['id_siswa' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NilaiKdPengetahuanAkhirSekolahMenengah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NilaiKdPengetahuanAkhirSekolahMenengah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NilaiKdPengetahuanAkhirSekolahMenengah();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai_kd_pengetahuan_akhir]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NilaiKdPengetahuanAkhirSekolahMenengah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->post()) OR $nilaiakhir->load(Yii::$app->request->post())) {
            $bobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->all();
            if($bobot){
                foreach ($bobot as $key => $value) {
                    $total = $value['bobot_nph'] + $value['bobot_npts'] + $value['bobot_npas'];
                    $nilaikd =  ($value->bobot_nph * $model->nilai_kd)+
                                ($value->bobot_npts * $model->npts)+
                                ($value->bobot_npas * $model->npas);
                    $hasil = $nilaikd/$total;
                }
               
            }else{
                $hasil = ($model->nilai_kd + $model->npts + $model->npas)/3;
            }
            $model->nilai_akhir_kd = round($hasil);
            $model->save();

            $ceknilaiakhir = NilaiAkhir::find()
                            ->where([
                                'id_siswa' => $model->id_siswa,
                                'id_semester' => $model->id_semester,
                                'id_mapel' => $model->id_mapel,
                            ])
                            ->one();
            

            if ($ceknilaiakhir) {
                $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                        ->where([
                            'id_mapel' => $ceknilaiakhir->id_mapel, 
                            'id_semester' => $ceknilaiakhir->id_semester, 
                            'id_siswa' => $ceknilaiakhir->id_siswa
                        ])
                        ->all();
                $hitung = 0;
                foreach ($nilai as $key => $value) {
                    $row = count($nilai);
                    $hitung += $value['nilai_akhir_kd'];
                    $result = $hitung/$row;
                    $hasil = round($result);
                }
                $ceknilaiakhir->nilai_pengetahuan = $hasil;

                $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaiakhir->id_mapel])->one();
                $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                ->orderBy(['nph' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                ->orderBy(['nph' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                ->all();
                $row = count($nilai);

                $interval = (100-$mapel->kkm_pengetahuan)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    if ($ceknilaiakhir->nilai_pengetahuan >= $a) {
                        $pre = "A";
                    }elseif ($ceknilaiakhir->nilai_pengetahuan >= $b) {
                        $pre = "B";
                    }elseif ($ceknilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $ceknilaiakhir->predikat_pengetahuan = $pre;

                        if ($ceknilaiakhir->predikat_pengetahuan == "A") {
                            if ($row == 1) {
                                $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            

                        }elseif ($ceknilaiakhir->predikat_pengetahuan == "B") {
                            if ($row == 1) {
                                $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            
                        }elseif ($ceknilaiakhir->predikat_pengetahuan == "C"){
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            
                        }else{
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                $ceknilaiakhir->deskripsi_pengetahuan = $x;

                $ceknilaiakhir->save();
            }else{
                $nilaiakhir->id_nilai_kd = $model->id_nilai_kd_pengetahuan_akhir;
                $nilaiakhir->id_sekolah = $model->id_sekolah;
                $nilaiakhir->id_siswa = $model->id_siswa;
                $nilaiakhir->id_kelas = $model->id_kelas;
                $nilaiakhir->id_mapel = $model->id_mapel;
                $nilaiakhir->id_semester = $model->id_semester;
                $nilaiakhir->nilai_pengetahuan = round($model->nilai_akhir_kd);
                $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                ->orderBy(['nph' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                ->orderBy(['nph' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                ->all();
                $row = count($nilai);

                $interval = (100-$mapel->kkm_pengetahuan)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    if ($nilaiakhir->nilai_pengetahuan >= $a) {
                        $pre = "A";
                    }elseif ($nilaiakhir->nilai_pengetahuan >= $b) {
                        $pre = "B";
                    }elseif ($nilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $nilaiakhir->predikat_pengetahuan = $pre;

                        if ($nilaiakhir->predikat_pengetahuan == "A") {
                            if ($row == 1) {
                                $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            

                        }elseif ($nilaiakhir->predikat_pengetahuan == "B") {
                            if ($row == 1) {
                                $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            
                        }elseif ($nilaiakhir->predikat_pengetahuan == "C"){
                            if($row == 1){
                                $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                            
                        }else {
                            if($row == 1){
                                $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                $nilaiakhir->deskripsi_pengetahuan = $x;
                $nilaiakhir->save();
                }
            
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NilaiKdPengetahuanAkhirSekolahMenengah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $nilai = NilaiAkhir::find()->where(['id_nilai_kd' => $id])->one();
        if ($nilai) {
            return "<script>alert('data masih dipakai di nilai akhir');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])."';
                    </script>";
        }else{
            $this->findModel($id)->delete();
        }
        
        return $this->redirect(['index']);
    }

    public function actionHapus()
    {
        if ($row = Yii::$app->request->post('hapussemua')) {
            foreach ($row as $key => $value) {
                $kd = NilaiAkhir::find()->where(['id_nilai_kd' => $value])->one();
                if ($kd) {
                    return "<script>alert('data masih dipakai di nilai akhir');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])."';
                            </script>";
                }
                $model = NilaiKdPengetahuanAkhirSekolahMenengah::findOne($value)->delete();
            }
        }else{
            return "<script>alert('data belum dipilih');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])."';
                </script>";
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiKdPengetahuanAkhirSekolahMenengah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiKdPengetahuanAkhirSekolahMenengah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiKdPengetahuanAkhirSekolahMenengah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
