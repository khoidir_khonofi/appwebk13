<?php

namespace app\controllers;

use Yii;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanSekolahMenengahSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Kelas;
use app\models\Siswa;
use app\models\Mengajar;
use app\models\Guru;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use app\models\Sekolah;

/**
 * NilaiKdPengetahuanSekolahMenengahController implements the CRUD actions for NilaiKdPengetahuanSekolahMenengah model.
 */
class NilaiKdPengetahuanSekolahMenengahController extends Controller
{
    public $layout = 'backend.php';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all NilaiKdPengetahuanSekolahMenengah models.
     * @return mixed
     */
    public function actionIndex()
    {
       if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionLihatnilai($id)
    {
        $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_siswa' => $id]);
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
       if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new NilaiKdPengetahuanSekolahMenengahSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    

    /**
     * Displays a single NilaiKdPengetahuanSekolahMenengah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

     public function actionHapus()
    {
        if ($row = Yii::$app->request->post('hapussemua')) {
            foreach ($row as $key => $value) {
                $kd = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_nilai_pengetahuan_sekolah_menengah' => $value])->one();
                if ($kd) {
                    return "<script>alert('data masih dipakai di nilai kd pengetahuan akhir');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])."';
                </script>";
                }
                $model = NilaiKdPengetahuanSekolahMenengah::findOne($value)->delete();
            }
        }else{
            return "<script>alert('data belum dipilih');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])."';
                </script>";
        }
        return $this->redirect(['index']);
    }

    /**
     * Creates a new NilaiKdPengetahuanSekolahMenengah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NilaiKdPengetahuanSekolahMenengah();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai_pengetahuan_sekolah_menengah]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NilaiKdPengetahuanSekolahMenengah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai_pengetahuan_sekolah_menengah]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NilaiKdPengetahuanSekolahMenengah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $kdakhir = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_nilai_pengetahuan_sekolah_menengah' => $id])->one();
        if ($kdakhir) {
            return "<script>alert('data masih dipakai di nilai kd pengetahuan akhir');
                            window.location = '".Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])."';
                </script>";
        }else{
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiKdPengetahuanSekolahMenengah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiKdPengetahuanSekolahMenengah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiKdPengetahuanSekolahMenengah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
