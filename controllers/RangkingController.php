<?php

namespace app\controllers;

use Yii;
use app\models\Rangking;
use app\models\RangkingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Siswa;
use app\models\Guru;
use app\models\Mengajar;
use app\models\Kelas;
use app\models\NilaiAkhir;
use app\models\NilaiAkhirKkmSatuanPendidikan;
/**
 * RangkingController implements the CRUD actions for Rangking model.
 */
class RangkingController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all Rangking models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RangkingSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new RangkingSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new RangkingSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new RangkingSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RangkingSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RangkingSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new RangkingSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new RangkingSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new RangkingSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RangkingSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rangking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rangking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $nilaiakhir = NilaiAkhir::find()
            ->where([
                'id_sekolah' => $sekolah->id_sekolah
            ])
            ->all();
        }elseif (Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilaiakhir = NilaiAkhir::find()
            ->where([
                'id_kelas' => $kelas->id_kelas,
                'id_semester' => $kelas->id_semester,
            ])
            ->groupBy(['id_siswa'])
            ->all();
            // echo "<pre>";
            // print_r($nilaiakhir);
            // die();
        }
           
            foreach ($nilaiakhir as $key => $nilai) {
                
                    $newmodel = new Rangking();
                    $ceksiswa = Rangking::find()
                        ->where([
                            'id_siswa' => $nilai->id_siswa, 
                            'id_semester' => $nilai->id_semester
                        ])
                        ->one();
                    if ($ceksiswa) {
                        $x = NilaiAkhir::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'id_semester' => $ceksiswa->id_semester])->all();
                        $hitp = 0;
                        $hitk = 0;
                        foreach ($x as $key => $value) {
                            $row = count($x);
                            $hitp += $value->nilai_pengetahuan;
                            $hasilp = $hitp/$row;
                            $hitk += $value->nilai_keterampilan;
                            $hasilk = $hitk/$row;
                            $hasilakhir = $hasilk+$hasilp;
                        }
                        $ceksiswa->total = $hasilakhir;
                        $juara = Rangking::find()->where(['id_kelas' => $ceksiswa->id_kelas, 'id_semester' => $ceksiswa->id_semester])->all();
                        $no = 1;
                        foreach ($juara as $key => $value) {
                            $ceksiswa->rangking = $no++;
                        }
                        // echo "<pre>";
                        // print_r($juara);
                        // die();
                        $ceksiswa->save();
                    }else{
                        $newmodel->id_sekolah = $nilai->id_sekolah;
                        $newmodel->id_kelas = $nilai->id_kelas;
                        $newmodel->id_siswa = $nilai->id_siswa;
                        $newmodel->id_semester = $nilai->id_semester;
                        $y = NilaiAkhir::find()->where(['id_siswa' => $nilai->id_siswa, 'id_semester' => $nilai->id_semester])->all();
                        $hitp = 0;
                        $hitk = 0;
                        foreach ($y as $key => $value) {
                            $row = count($y);
                            $hitp += $value->nilai_pengetahuan;
                            $hasilp = $hitp/$row;
                            $hitk += $value->nilai_keterampilan;
                            $hasilk = $hitk/$row;
                            $hasilakhir = $hasilk + $hasilp;
                        }
                        $newmodel->nilai_rata_rata_keterampilan = $hasilk;
                        $newmodel->nilai_rata_rata_pengetahuan = $hasilp;
                        $newmodel->total = $hasilk + $hasilp;
                        $juara = Rangking::find()->where(['id_siswa' => $nilai->id_siswa, 'id_semester' => $nilai->id_semester])->all();
                        $no = 1;
                        foreach ($juara as $key => $value) {
                            $newmodel->rangking = $no++;
                        }

                        $newmodel->save();
                    }
            }
        
        return $this->redirect(['index']);
    }

    public function actionRangking()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $nilaiakhir = NilaiAkhirKkmSatuanPendidikan::find()
            ->where([
                'id_sekolah' => $sekolah->id_sekolah
            ])
            ->all();
        }elseif (Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilaiakhir = NilaiAkhirKkmSatuanPendidikan::find()
            ->where([
                'id_kelas' => $kelas->id_kelas,
                'id_semester' => $kelas->id_semester,
            ])
            ->groupBy(['id_siswa'])
            ->all();
            // echo "<pre>";
            // print_r($nilaiakhir);
            // die();
        }
           
            foreach ($nilaiakhir as $key => $nilai) {
                
                    $newmodel = new Rangking();
                    $ceksiswa = Rangking::find()
                        ->where([
                            'id_siswa' => $nilai->id_siswa, 
                            'id_semester' => $nilai->id_semester
                        ])
                        ->one();
                    if ($ceksiswa) {
                        $x = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'id_semester' => $ceksiswa->id_semester])->all();
                        $hitp = 0;
                        $hitk = 0;
                        foreach ($x as $key => $value) {
                            $row = count($x);
                            $hitp += $value->nilai_pengetahuan;
                            $hasilp = $hitp/$row;
                            $hitk += $value->nilai_keterampilan;
                            $hasilk = $hitk/$row;
                            $hasilakhir = $hasilk+$hasilp;
                        }
                        $ceksiswa->total = $hasilakhir;
                        
                        
                        // echo "<pre>";
                        // print_r($juara);
                        // die();
                        $ceksiswa->save();
                    }else{
                        $newmodel->id_sekolah = $nilai->id_sekolah;
                        $newmodel->id_kelas = $nilai->id_kelas;
                        $newmodel->id_siswa = $nilai->id_siswa;
                        $newmodel->id_semester = $nilai->id_semester;
                        $y = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $nilai->id_siswa, 'id_semester' => $nilai->id_semester])->all();
                        $hitp = 0;
                        $hitk = 0;
                        foreach ($y as $key => $value) {
                            $row = count($y);
                            $hitp += $value->nilai_pengetahuan;
                            $hasilp = $hitp/$row;
                            $hitk += $value->nilai_keterampilan;
                            $hasilk = $hitk/$row;
                            $hasilakhir = $hasilk + $hasilp;
                        }
                        $newmodel->nilai_rata_rata_keterampilan = $hasilk;
                        $newmodel->nilai_rata_rata_pengetahuan = $hasilp;
                        $newmodel->total = $hasilk + $hasilp;
                        $juara = Rangking::find()->where(['id_siswa' => $nilai->id_siswa, 'id_semester' => $nilai->id_semester])->all();
                        $no = 1;
                        foreach ($juara as $key => $value) {
                            $newmodel->rangking = $no++;
                        }

                        $newmodel->save();
                    }
            }
        
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Rangking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_rangking]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Rangking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rangking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rangking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rangking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
