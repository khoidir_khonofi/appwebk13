<?php

namespace app\controllers;

use Yii;
use app\models\RombonganBelajar;
use app\models\RombonganBelajarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Guru;
use app\models\Sekolah;
use app\models\Semester;
use app\models\SiswaSearch;
use app\models\Kelas;
use app\models\Siswa;
use app\models\KelasSearch;
/**
 * RombonganBelajarController implements the CRUD actions for RombonganBelajar model.
 */
class RombonganBelajarController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all RombonganBelajar models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new RombonganBelajarSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSudahlulus()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' =>  Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_kelas' => $siswa->id_kelas]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('lulus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new RombonganBelajarSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatkelas($id)
    {
        $searchModel = new RombonganBelajarSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    // public function actionSudahlulus()
    // {

    //     $searchModel = SiswaSear
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('lulus', [
    //         'model' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single RombonganBelajar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RombonganBelajar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RombonganBelajar();

        if ($model->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' =>Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            if(Yii::$app->user->identity->role=="Administrator"){
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RombonganBelajar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    public function actionAksiNaikKelas(){
        $selection=(array)Yii::$app->request->post('selection');
        if($selection){
            $id_kelas = $_POST['id_kelas'];
        
            foreach ($selection as $key => $value) {
                $model =  new RombonganBelajar();
                $rombel = RombonganBelajar::find()->where(['id_rombel' => $value])->one();
                RombonganBelajar::UpdateAll(['status' => 'Tidak Aktif'],['id_siswa' => $rombel->id_siswa, 'status' => 'Aktif']);
                $kelas = Kelas::find()->where(['id_kelas' => $id_kelas])->one();
                $model->id_sekolah = $rombel->id_sekolah;
                $model->id_kelas = $id_kelas;
                $model->id_siswa = $rombel->id_siswa;
                $model->id_semester = $kelas->id_semester;
                $model->id_guru = $kelas->id_guru;
                $model->tahun_ajaran = $kelas->tahun_ajaran;
                $model->status = "Aktif";
                $model->save();
                $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
                $siswa->id_kelas = $model->id_kelas;
                $siswa->id_semester = $model->id_semester;
                $siswa->id_guru = $model->id_guru;
                $siswa->status = 'Aktif';
                $siswa->save();
            }
        }
        else{
            return "<script>alert('Data Belum Dipilih');
                    window.location = 'index.php?r=rombongan-belajar';
            </script>";
        }
         return $this->redirect(['index']);
    }

    public function actionChoi()
    {
        if ($row = Yii::$app->request->post('test')) {
            foreach ($row as $key => $value) {
                $model =  new RombonganBelajar();
                $rombel = RombonganBelajar::find()->where(['id_rombel' => $value])->one();
                RombonganBelajar::UpdateAll(['status' => 'Tidak Aktif'],['id_siswa' => $rombel->id_siswa, 'status' => 'Aktif']);
                $kelas = Kelas::find()->where(['id_kelas' => $rombel->id_kelas])->one();
                $model->id_sekolah = $rombel->id_sekolah;
                $model->id_kelas = $kelas->id_kelas;
                $model->id_siswa = $rombel->id_siswa;
                $model->id_semester = $kelas->id_semester;
                $model->id_guru = $kelas->id_guru;
                $model->tahun_ajaran = $kelas->tahun_ajaran;
                $model->status = 'Lulus';
                
                $model->save();
                $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
                $siswa->id_kelas = $model->id_kelas;
                $siswa->id_semester = $model->id_semester;
                $siswa->id_guru = $model->id_guru;
                $siswa->status = 'Tidak Aktif';
                
                if ($siswa->save()) {
                    return "berhasil";
                }else{
                    return "gagal";
                }
            }
        }else{
            return "<script>alert('Data Belum Dipilih');
                    window.location = 'index.php?r=rombongan-belajar/sudahlulus';
            </script>";
        }
        return $this->redirect(['sudahlulus']);
    }

   

    /**
     * Deletes an existing RombonganBelajar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RombonganBelajar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RombonganBelajar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RombonganBelajar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSemester($id)
    {
        $jumlah=Kelas::find()->where(['id_kelas'=>$id])->count();
        $kec=Kelas::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_semester;?>"><?php echo $ke->semester->semester;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

  
}
