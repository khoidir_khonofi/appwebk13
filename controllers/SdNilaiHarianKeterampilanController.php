<?php

namespace app\controllers;

use Yii;
use app\models\SdNilaiHarianKeterampilan;
use app\models\SdNilaiHarianKeterampilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Siswa;
use app\models\Guru;
use app\models\User;
use app\models\TemaKeterampilan;
use app\models\Mengajar;
use app\models\TeknikPenilaianKeterampilan;
use app\models\Kelas;
use app\models\Sekolah;
use app\models\KompetensiDasarKeterampilan;
use app\models\NilaiKdKeterampilan;
use app\models\MataPelajaran;
use app\models\NilaiAkhir;
/**
 * SdNilaiHarianKeterampilanController implements the CRUD actions for SdNilaiHarianKeterampilan model.
 */
class SdNilaiHarianKeterampilanController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all SdNilaiHarianKeterampilan models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SdNilaiHarianKeterampilanSearch();
            }
        }else{
           $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianKeterampilanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SdNilaiHarianKeterampilanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianKeterampilanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

   

    public function actionLihatnilaisiswaktr($id){
        $searchModel = new SdNilaiHarianKeterampilanSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SdNilaiHarianKeterampilan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

   

    /**
     * Creates a new SdNilaiHarianKeterampilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SdNilaiHarianKeterampilan();
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->post()) OR $nilai->load(Yii::$app->request->post()) OR $nilaiakhir->load(Yii::$app->request->post())) {
            $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
            $mengajar = Mengajar::find()->where(['id_mapel' => $model->id_mapel])->one();

            if(Yii::$app->user->identity->role=="Administrator"){
                $model->id_sekolah = $sekolah->id_sekolah;
                $model->created_by = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
                $model->created_by = $guru->id_guru;
            }
            
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            $model->save();

            $cekkd = NilaiKdKeterampilan::find()
            ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan, 'id_semester' => $model->id_semester])
            ->andWhere(['id_siswa' => $model->id_siswa])
            ->one();

            if ($cekkd) {

                $cek = SdNilaiHarianKeterampilan::find()
                ->select("MAX(nilai) as nilai_max")
                ->where([
                    'id_siswa'=> $cekkd->id_siswa,
                    'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan,
                    'id_semester' => $model->id_semester
                ])
                ->groupBy(['jenis_penilaian'])
                ->all();

                $rata_rata=0;
                foreach ($cek as $key => $value) {
                    $rata_rata += $value->nilai_max;
                }
                $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                $cekkd->skor = round($rata_rata);
                $cekkd->save();

              
            }else{
                $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                $nilai->id_sekolah = $model->id_sekolah;
                $nilai->id_kelas = $model->id_kelas;
                $nilai->id_siswa = $model->id_siswa;
                $nilai->id_mapel = $model->id_mapel;
                $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                $nilai->id_semester = $model->id_semester;
                $nilai->skor = $model->nilai;
                $nilai->save();
            }

                $ceknilaikd = NilaiAkhir::find()
                    ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                    ->andWhere(['id_siswa' => $model->id_siswa])
                    ->one();
    
                if ($ceknilaikd) {
                    $x = NilaiKdKeterampilan::find()
                            ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                            ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                            ->all();
                    $hitung = 0;
                    foreach ($x as $key => $value) {
                        $row = count($x);
                        $hitung += $value['skor'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaikd->nilai_keterampilan = $hasil;
        
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->all();
                    $row = count($nilai);
        
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($ceknilaikd->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($ceknilaikd->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaikd->predikat_keterampilan = $pre;
        
                                if ($ceknilaikd->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif($ceknilaikd->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                    $ceknilaikd->deskripsi_keterampilan = $ket;
                    $ceknilaikd->save();
    
                }else{
                    $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                    $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                    $nilaiakhir->id_siswa = $nilai->id_siswa;
                    $nilaiakhir->id_kelas = $nilai->id_kelas;
                    $nilaiakhir->id_mapel = $nilai->id_mapel;
                    $nilaiakhir->id_semester = $nilai->id_semester;
                    $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
    
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($nilaiakhir->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($nilaiakhir->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_keterampilan = $pre;
    
    
    
                                if ($nilaiakhir->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif($nilaiakhir->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                            $nilaiakhir->deskripsi_keterampilan = $ket;
                            $nilaiakhir->save();
                }
                return $this->redirect(['index']);
            }

        return $this->renderAjax('tambahnilai', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing SdNilaiHarianKeterampilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();

        if ($model->load(Yii::$app->request->post())  OR $nilai->load(Yii::$app->request->post())  OR $nilaiakhir->load(Yii::$app->request->post())) {
            $model->save();

            $cekkd = NilaiKdKeterampilan::find()
            ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan])
            ->andWhere(['id_siswa' => $model->id_siswa])
            ->one();

            if ($cekkd) {
                $cek = SdNilaiHarianKeterampilan::find()
                ->select("MAX(nilai) as nilai_max")
                ->where([
                    'id_siswa'=> $cekkd->id_siswa,
                    'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan
                ])
                ->groupBy(['jenis_penilaian'])
                ->all();

                $rata_rata=0;
                foreach ($cek as $key => $value) {
                    $rata_rata += $value->nilai_max;
                }
                $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                $cekkd->skor = round($rata_rata);
                $cekkd->save();

            }else{
                $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                $nilai->id_sekolah = $model->id_sekolah;
                $nilai->id_kelas = $model->id_kelas;
                $nilai->id_siswa = $model->id_siswa;
                $nilai->id_mapel = $model->id_mapel;
                $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                $nilai->id_semester = $model->id_semester;
                $nilai->skor = $model->nilai;
                $nilai->save();
            }
            $ceknilaikd = NilaiAkhir::find()
                    ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                    ->andWhere(['id_siswa' => $model->id_siswa])
                    ->one();
    
                if ($ceknilaikd) {
                $x = NilaiKdKeterampilan::find()
                        ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                        ->all();
                $hitung = 0;
                foreach ($x as $key => $value) {
                    $row = count($x);
                    $hitung += $value['skor'];
                    $result = $hitung/$row;
                    $hasil = round($result);
                }
                $ceknilaikd->nilai_keterampilan = $hasil;
    
                $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                $tinggi = NilaiKdKeterampilan::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->all();
                $row = count($nilai);
    
                $interval = (100-$mapel->kkm_keterampilan)/3;
                    $a = 100 - $interval;
                    $b = $a - $interval;
                    $c = $b - $interval;
                    if ($ceknilaikd->nilai_keterampilan >= $a) {
                        $pre = "A";
                    }elseif($ceknilaikd->nilai_keterampilan >= $b){
                        $pre = "B";
                    }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $ceknilaikd->predikat_keterampilan = $pre;
    
                            if ($ceknilaikd->predikat_keterampilan == "A") {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            
                            }elseif ( $ceknilaikd->predikat_keterampilan == "C"){
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }else{
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                            }
                $ceknilaikd->deskripsi_keterampilan = $ket;
                $ceknilaikd->save();
    
                }else{
                    $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                    $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                    $nilaiakhir->id_siswa = $nilai->id_siswa;
                    $nilaiakhir->id_kelas = $nilai->id_kelas;
                    $nilaiakhir->id_mapel = $nilai->id_mapel;
                    $nilaiakhir->id_semester = $nilai->id_semester;
                    $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
    
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($nilaiakhir->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($nilaiakhir->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_keterampilan = $pre;
    
    
    
                                if ($nilaiakhir->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($nilaiakhir->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                            $nilaiakhir->deskripsi_keterampilan = $ket;
                            $nilaiakhir->save();
                }
                return $this->redirect(['index']);
            }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SdNilaiHarianKeterampilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = NilaiKdKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        if ($model) {
            return "<script>alert('ups!! perikasa nilai kd keterampilan');
                    window.location='index.php?r=sd-nilai-harian-keterampilan';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        

        return $this->redirect(['index']);
    }

    /**
     * Finds the SdNilaiHarianKeterampilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SdNilaiHarianKeterampilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SdNilaiHarianKeterampilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionKdktr($id)
    {
        $jumlah=KompetensiDasarKeterampilan::find()->where(['id_mapel'=>$id])->count();
        $kec=KompetensiDasarKeterampilan::find()->where(['id_mapel'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kd_keterampilan;?>"><?php echo $ke->no_kd;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

     public function actionTeknik($id)
    {
        $jumlah=TeknikPenilaianKeterampilan::find()->where(['id_tema'=>$id])->count();
        $kec=TeknikPenilaianKeterampilan::find()->where(['id_tema'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id;?>"><?php echo $ke->jenis_penilaian;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

      public function actionTema($id)
    {
        $jumlah=TemaKeterampilan::find()->where(['id_kd'=>$id])->count();
        $kec=TemaKeterampilan::find()->where(['id_kd'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_tema;?>"><?php echo $ke->tema;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

}
