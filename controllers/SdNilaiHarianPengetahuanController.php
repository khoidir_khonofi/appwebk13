<?php

namespace app\controllers;

use Yii;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SdNilaiHarianPengetahuanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Guru;
use app\models\Siswa;
use app\models\Sekolah;
use kartik\mpdf\Pdf;
use app\models\KompetensiDasarPengetahuan;
use app\models\Subtema;
use app\models\Tema;
use app\models\Mengajar;
use app\models\NilaiKdPengetahuan;
use app\models\Kelas;
use app\models\NilaiKdPengetahuanAkhir;
/**
 * SdNilaiHarianPengetahuanController implements the CRUD actions for SdNilaiHarianPengetahuan model.
 */
class SdNilaiHarianPengetahuanController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all SdNilaiHarianPengetahuan models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianPengetahuanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SdNilaiHarianPengetahuanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianPengetahuanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianPengetahuanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SdNilaiHarianPengetahuanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SdNilaiHarianPengetahuanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatnilaisiswapeng($id)
    {
        $searchModel = new SdNilaiHarianPengetahuanSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SdNilaiHarianPengetahuan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPrint()
    {
        $model = SdNilaiHarianPengetahuan::find()->groupBy(['id_siswa'])->all();
        $content = $this->render('raport',[
            'model' => $model
        ]);
        foreach ($model as $key => $value) {
        
        
            $value = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE, 
                // A4 paper format
                'format' => Pdf::FORMAT_A4, 
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT, 
                // stream to browser inline
                'destination' => Pdf::DEST_DOWNLOAD,
                // your html content input
                'content' => $content,  

                //'filename' => uniqid().'.pdf',
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}', 
                 // set mPDF properties on the fly
                'options' => ['title' => 'Krajee Report Title'],
                // 'marginLeft' => 5, 'marginTop' => 5, 'marginRight' => 5, 'marginBottom' => 5,
                'defaultFont' => 'Calibri',
                 // call mPDF methods on the fly
                'methods' => [ 
                    'SetHeader'=>['Krajee Report Header'], 
                    'SetFooter' => ['raport sd'],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
           
            // return the pdf output as per the destination setting
            return $value->render();
        }
    }

    

    /**
     * Creates a new SdNilaiHarianPengetahuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SdNilaiHarianPengetahuan();
        $nilai = new NilaiKdPengetahuan();
        if ($model->load(Yii::$app->request->post()) OR $nilai->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();

            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            $model->save();


            $cekkd = NilaiKdPengetahuan::find()
                        ->where([
                            'id_kd_pengetahuan' => $model->id_kd_pengetahuan,
                            'id_semester' => $model->id_semester    
                        ])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SdNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save(); 
                    }
                    return $this->redirect(['index']);
        }


        return $this->renderAjax('tambahnilai', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing SdNilaiHarianPengetahuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nilai = new NilaiKdPengetahuan();
        if ($model->load(Yii::$app->request->post()) OR $nilai->load(Yii::$app->request->post())) {
           
            $model->save();
            $cekkd = NilaiKdPengetahuan::find()
                        ->where([
                            'id_kd_pengetahuan' => $model->id_kd_pengetahuan,
                            'id_semester' => $model->id_semester    
                        ])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SdNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save(); 
                    }
                    return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SdNilaiHarianPengetahuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = NilaiKdPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        if ($model) {
            return "<script>
                        alert('ups!! perikasa nilai kd pengetahuan');
                        window.location='index.php?r=sd-nilai-harian-pengetahuan';
                    </script>";
        }else{
            $this->findModel($id)->delete();

        return $this->redirect(['index']);
        }
        
    }

    public function actionHapus()
    {
        if ($row = Yii::$app->request->post('hapus')) {
            foreach ($row as $key => $value) {
                $nilaikd = NilaiKdPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $value])->one();
                if ($nilaikd) {
                    return "<script>
                        alert('ups!! perikasa nilai kd pengetahuan');
                        window.location='".Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])."';
                    </script>";
                }else{
                    $model = SdNilaiHarianPengetahuan::findOne($value)->delete();
                    if ($model) {
                        return "<script>
                        alert('Data Terhapus');
                        window.location='".Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])."';
                    </script>";
                    }
                }
                
            }
        }else{
             return "<script>
                        alert('Data belum dipilih');
                        window.location='".Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])."';
                    </script>";
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the SdNilaiHarianPengetahuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SdNilaiHarianPengetahuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SdNilaiHarianPengetahuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionKdktr($id)
    {
        $jumlah=KompetensiDasarPengetahuan::find()->where(['id_mapel'=>$id])->count();
        $kec=KompetensiDasarPengetahuan::find()->where(['id_mapel'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kd_pengetahuan;?>"><?php echo $ke->no_kd;?></option>
            <?php }
            }else {
                echo "<option>----</option>";
            }
    }

     public function actionSubtema($id)
    {
        $jumlah=Subtema::find()->where(['id_tema'=>$id])->count();
        $kec=Subtema::find()->where(['id_tema'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_subtema;?>"><?php echo $ke->subtema;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

      public function actionTema($id)
    {
        $jumlah=Tema::find()->where(['id_kd'=>$id])->count();
        $kec=Tema::find()->where(['id_kd'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_tema;?>"><?php echo $ke->tema;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

}
