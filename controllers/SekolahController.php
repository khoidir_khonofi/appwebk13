<?php

namespace app\controllers;

use Yii;
use app\models\Sekolah;
use app\models\SekolahSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Guru;
use app\models\Siswa;
use app\models\User;
use yii\web\UploadedFile;
/**
 * SekolahController implements the CRUD actions for Sekolah model.
 */
class SekolahController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all Sekolah models.
     * @return mixed
     */
    public function actionIndex()
    {
       return $this->render('index');
    }

    /**
     * Displays a single Sekolah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sekolah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Sekolah();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_user = Yii::$app->user->identity->id_user;
            $tmp = UploadedFile::getInstance($model, 'fotosekolah');
            if ($tmp != NULL) {
                $model->foto = $tmp->baseName.'.'.$tmp->extension;
            }
            
            $model->save();
            if ($tmp != NULL) {
                $tmp->saveAs('foto_sekolah/'.$model->foto);
            }
            
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sekolah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        if(Yii::$app->user->identity->role == 'Administrator'){
            if ($model->load(Yii::$app->request->post())) {
                $tmp = UploadedFile::getInstance($model, 'fotosekolah');
                if ($tmp != NULL) {
                    $model->foto = $tmp->baseName.'.'.$tmp->extension;
                }

                $model->save();
                if ($tmp != NULL) {
                    $tmp->saveAs('foto_sekolah/'.$model->foto);
                }
            return $this->redirect(['index']);
            }
        }else{
            return "<script>alert('Gagal... Anda bukan Administrator'); window.location = '".Yii::$app->urlManager->createUrl(['sekolah'])."'</script>";
        }
        
        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sekolah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Sekolah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sekolah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    protected function findModel($id)
    {
        if (($model = Sekolah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
