<?php

namespace app\controllers;

use Yii;
use app\models\SikapAkhir;
use app\models\SikapAkhirSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Kelas;
use app\models\SikapSosial;
use app\models\SikapSpiritual;
use app\models\Siswa;
use yii\data\ActiveDataProvider;
use app\models\JurnalSikap;
/**
 * SikapAkhirController implements the CRUD actions for SikapAkhir model.
 */
class SikapAkhirController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all SikapAkhir models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapAkhir::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapAkhir::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapAkhir::find()->where(['id_siswa' => $siswa->id_siswa]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
            
        return $this->render('index', [
            'searchModel' => new SikapSosial(),
            'dataProvider' => $provider, 
        ]);
    }

    public function actionSearch()
    {
        
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SikapAkhirSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_guru' => $guru->id_guru])->one();
            $searchModel = new SikapAkhirSearch(['id_sekolah' => $guru->id_sekolah]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SikapAkhirSearch(['id_siswa' => $siswa->id_siswa]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new SikapAkhirSearch(['id_siswa' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SikapAkhir model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionHapus()
    {
            if ($row = Yii::$app->request->post('del')) {
                foreach ($row as $key => $value) {
                    if (Yii::$app->user->identity->role == 'Administrator' || Yii::$app->user->identity->role == 'Guru') {
                        $model = SikapAkhir::findOne($value)->delete();
                    }else{
                        return "<script>alert('Gagal...');
                    window.location='index.php?r=sikap-akhir';</script>";
                    }
                }
            }else{
                return "<script>alert('Belum Dipilih');
                    window.location='index.php?r=sikap-akhir';</script>";
            }
            return $this->redirect(['index']);
        
    }

    public function actionLoad($id)
    {
            $model = SikapAkhir::find()->where(['id_siswa'=>$id])->one();
            if ($model->load(Yii::$app->request->post())) {
               
                $cekjurnal = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->one();
                if (!$cekjurnal) {
                    return "<script>alert('Siswa tidak ada di jurnal sikap'); window.location='".Yii::$app->urlManager->createUrl(['sikap-akhir'])."';</script>";
                }else{
                    $ceksiswa = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
                    if ($ceksiswa) {
                            $jurnal = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa])->one();
                            $sosial = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'sikap' => 'Sosial'])->one();
                            $spiritual = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'sikap' => 'Spiritual'])->one();
                            $siswa = Siswa::find()->where(['id_siswa' => $ceksiswa->id_siswa])->one();
                            $ceksiswa->id_siswa = $jurnal->id_siswa;
                            $ceksiswa->id_kelas = $siswa->id_kelas;
                            $ceksiswa->id_semester = $siswa->id_semester;
                            $ceksiswa->id_sekolah = $siswa->id_sekolah;
                            $ceksiswa->keterangan_sosial = $sosial->catatan_perilaku;
                            $ceksiswa->keterangan_spiritual = $spiritual->catatan_perilaku;
                            $ceksiswa->save();
                    }else{
                            $jurnal = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->all();
                            $newmodel = new SikapAkhir();
                            foreach ($jurnal as $key => $value) {
                                $siswa = Siswa::find()->where(['id_siswa' => $value->id_siswa])->one();
                                $sosial = JurnalSikap::find()->where(['id_siswa' => $siswa->id_siswa, 'sikap' => 'Sosial'])->one();
                                $spiritual = JurnalSikap::find()->where(['id_siswa' => $siswa->id_siswa, 'sikap' => 'Spiritual'])->one();
                                $newmodel->id_siswa = $value->id_siswa;
                                $newmodel->id_kelas = $siswa->id_kelas;
                                $newmodel->id_semester = $siswa->id_semester;
                                $newmodel->id_sekolah = $siswa->id_sekolah;
                                $newmodel->keterangan_sosial = $sosial->catatan_perilaku;
                                $newmodel->keterangan_spiritual = $spiritual->catatan_perilaku;
                                $newmodel->save();
                            }
                    }
                }
                
                return $this->redirect(['index']);
            }
        return $this->renderAjax('load',[
            'model' => $model
        ]);
    }

    /**
     * Creates a new SikapAkhir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SikapAkhir();

        if ($model->load(Yii::$app->request->post())) {
            $ceksiswa = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa])->one();
            if ($ceksiswa) {
                return "<script>alert('siswa atas nama ".$model->siswa->nama." sudah ada'); window.location='index.php?r=sikap-akhir';</script>";
            }else{
                if(Yii::$app->user->identity->role=="Administrator"){
                    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $model->id_sekolah = $sekolah->id_sekolah;
                }else{
                    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $model->id_sekolah = $guru->id_sekolah;
                }
                $model->save();
                return $this->redirect(['index']);
            }
            
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SikapAkhir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SikapAkhir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SikapAkhir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SikapAkhir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SikapAkhir::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
