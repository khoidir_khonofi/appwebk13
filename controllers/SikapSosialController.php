<?php

namespace app\controllers;

use Yii;
use app\models\SikapSosial;
use app\models\SikapSosialSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Mengajar;
use app\models\Siswa;
use app\models\Kelas;
use app\models\SikapAkhir;
use yii\data\ActiveDataProvider;
use app\models\Semester;
/**
 * SikapSosialController implements the CRUD actions for SikapSosial model.
 */
class SikapSosialController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }
    /**
     * Lists all SikapSosial models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapSosial::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapSosial::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = SikapSosial::find()->where(['id_siswa' => $siswa->id_siswa]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
            
        return $this->render('index', [
            'searchModel' => new SikapSosial(),
            'dataProvider' => $provider, 
        ]);
    }

    public function actionLihatsikap($id)
    {
        $searchModel = new SikapSosialSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SikapSosial model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionTambah()
    {
        $model = new SikapSosial();
        $sikapakhir = new SikapAkhir();
        if ($model->load(Yii::$app->request->post()) OR $sikapakhir->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $ceksikap = SikapSosial::find()->where(['id_siswa' => $model->id_siswa, 'butir_sikap' => $model->butir_sikap])->one();
            if ($ceksikap) {
                return "<script>alert('butir sikap ".$ceksikap->butir_sikap." atas nama ". $ceksikap->siswa->nama." sudah ada'); window.location='index.php?r=sikap-sosial';</script>";
            }else{
                if(Yii::$app->user->identity->role=="Administrator"){
                    $model->id_sekolah = $sekolah->id_sekolah;
                }else{
                    $model->id_sekolah = $guru->id_sekolah;
                }
                $model->save();
                $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
                if ($cek) {
                    $tinggi = SikapSosial::find()
                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                            ->orderBy(['nilai' => SORT_DESC])
                            ->one();
                    $rendah = SikapSosial::find()
                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                            ->orderBy(['nilai' => SORT_ASC])
                            ->one();
                    if ($cek->keterangan_sosial == NULL) {
                        if ($tinggi->nilai >= 90) {
                            $ket = "Sangat baik dalam ".$model->butir_sikap;
                        }else{
                            $ket = "Baik dalam ".$model->butir_sikap;
                        }
                        
                    }else{
                        if ($tinggi->nilai >= 90) {
                            $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                        }else{
                            $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                        }
                        
                    }
                    $cek->keterangan_sosial = $ket;
                    $cek->save();
                }else{
                    $sikapakhir->id_sekolah  = $model->id_sekolah;
                    $sikapakhir->id_siswa = $model->id_siswa;
                    $sikapakhir->id_kelas = $model->id_kelas;
                    $sikapakhir->id_semester = $model->id_semester;
                    $sikapakhir->id_sikap_sosial = $model->id_sikap_sosial;
                    if ($model->nilai >= 90) {
                        $sikapakhir->keterangan_sosial = "Sangat Baik dalam ".$model->butir_sikap;
                    }else{
                        $sikapakhir->keterangan_sosial = "Baik dalam ".$model->butir_sikap;
                    }
                    
                    $sikapakhir->save();
                }
            }
            
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form',[
            'model' => $model
        ]);
    }

    /**
     * Creates a new SikapSosial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new SikapSosial();
        $sikapakhir = new SikapAkhir();
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        if ($model->load(Yii::$app->request->post()) OR $sikapakhir->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $ceksikap = SikapSosial::find()->where(['id_siswa' => $id, 'butir_sikap' => $model->butir_sikap])->one();
            if ($ceksikap) {
                return "<script>alert('butir sikap ".$ceksikap->butir_sikap." atas nama ". $ceksikap->siswa->nama." sudah ada'); window.location='index.php?r=sikap-sosial';</script>";
            }else{
                if(Yii::$app->user->identity->role=="Administrator"){
                    $model->id_sekolah = $sekolah->id_sekolah;
                }else{
                    $model->id_sekolah = $guru->id_sekolah;
                }
                $model->id_siswa = $siswa->id_siswa;
                $model->id_kelas = $siswa->id_kelas;
                $model->id_semester = $siswa->id_semester;
                 $model->save();
    
                $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
                if ($cek) {
                    $tinggi = SikapSosial::find()
                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                            ->orderBy(['nilai' => SORT_DESC])
                            ->one();
                    $rendah = SikapSosial::find()
                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                            ->orderBy(['nilai' => SORT_ASC])
                            ->one();
                    if ($cek->keterangan_sosial == NULL) {
                        if ($tinggi->nilai >= 90) {
                            $ket = "Sangat baik dalam ".$model->butir_sikap;
                        }else{
                            $ket = "Baik dalam ".$model->butir_sikap;
                        }
                        
                    }else{
                        if ($tinggi->nilai >= 90) {
                            $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                        }else{
                            $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                        }
                        
                    }
                    $cek->keterangan_sosial = $ket;
                    $cek->save();
                }else{
                    $tes = SikapSosial::find()
                            ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
                            ->one();
                    $sikapakhir->id_sekolah = $model->id_sekolah;
                    $sikapakhir->id_siswa = $model->id_siswa;
                    $sikapakhir->id_kelas = $model->id_kelas;
                    $sikapakhir->id_semester = $model->id_semester;
                    $sikapakhir->id_sikap_sosial = $model->id_sikap_sosial;
                    if ($tes->nilai >= 90) {
                        $sikapakhir->keterangan_sosial = "Sangat Baik dalam ".$tes->butir_sikap;
                    }else{
                        $sikapakhir->keterangan_sosial = "Baik dalam ".$tes->butir_sikap;
                    }
                    
                    $sikapakhir->save();
                }
            }
            

            return $this->redirect(['index']);
        }

        return $this->renderAjax('nilaisikap', [
            'model' => $model,
            'siswa' => $siswa,
        ]);
    }

    /**
     * Updates an existing SikapSosial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sikapakhir = new SikapAkhir();

        if ($model->load(Yii::$app->request->post()) OR $sikapakhir->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
            if(Yii::$app->user->identity->role=="Administrator"){
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            $model->save();

            $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
            if ($cek) {
                $tinggi = SikapSosial::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_DESC])
                        ->one();
                $rendah = SikapSosial::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_ASC])
                        ->one();
                if ($cek->keterangan_sosial == NULL) {
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$model->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$model->butir_sikap;
                    }
                    
                }else{
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }
                    
                }
                
                $cek->keterangan_sosial = $ket;
                $cek->save();
            }else{
                $tes = SikapSosial::find()
                        ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
                        ->one();
                $sikapakhir->id_sekolah = $model->id_sekolah;
                $sikapakhir->id_siswa = $model->id_siswa;
                $sikapakhir->id_kelas = $model->id_kelas;
                $sikapakhir->id_semester = $model->id_semester;
                $sikapakhir->id_sikap_sosial = $model->id_sikap_sosial;
                if ($tes->nilai >= 90) {
                    $sikapakhir->keterangan_sosial = "Sangat Baik dalam ".$tes->butir_sikap;
                }else{
                    $sikapakhir->keterangan_sosial = "Baik dalam ".$tes->butir_sikap;
                }
                $sikapakhir->save();
            }

            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SikapSosial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SikapSosial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SikapSosial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SikapSosial::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSemester($id)
    {
        $jumlah=Siswa::find()->where(['id_siswa'=>$id])->count();
        $kec=Siswa::find()->where(['id_siswa'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_semester;?>"><?php echo $ke->semester->semester;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

    public function actionKelas($id)
    {
        $jumlah=Siswa::find()->where(['id_kelas'=>$id])->count();
        $kec=Siswa::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_siswa;?>"><?php echo $ke->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
