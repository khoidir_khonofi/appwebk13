<?php

namespace app\controllers;

use Yii;
use app\models\Siswa;
use app\models\SiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Tema;
use app\models\MataPelajaran;
use yii\web\UploadedFile;
use app\models\Kelas;
use app\models\RombonganBelajar;
use yii\helpers\Url;
use app\models\KompetensiDasarKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\User;
use app\models\NilaiKdKeterampilan;
use app\models\Saran;
use app\models\NilaiKdPengetahuan;
use app\models\SmpNilaiHarianPengetahuan;
/**
 * SiswaController implements the CRUD actions for Siswa model.
 */
class SiswaController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */

    public $enableCsrfValidation = false;
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all Siswa models.
     * @return mixed
     */
    public function actionIndex()
    {
            if(Yii::$app->user->identity->role=="Administrator"){
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                if($user){
                    $searchModel = new SiswaSearch(['id_sekolah' => $sekolah->id_sekolah, 'status' => 'Aktif']);
                }else{
                    $searchModel = new SiswaSearch(['id_user' => Yii::$app->user->identity->id_user, 'status' => 'Aktif']);
                }
            }elseif(Yii::$app->user->identity->role=="Guru"){
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new SiswaSearch(['id_guru' => $guru->id_guru]);
            }else{
                $searchModel = new SiswaSearch(['id_user' => Yii::$app->user->identity->id_user, 'status' => 'Aktif']);
            }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionSearch()
    {
       
            if(Yii::$app->user->identity->role=="Administrator"){
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                if($user){
                    $searchModel = new SiswaSearch(['id_sekolah' => $sekolah->id_sekolah]);
                }else{
                    $searchModel = new SiswaSearch(['id_user' => Yii::$app->user->identity->id_user]);
                }
            }elseif(Yii::$app->user->identity->role=="Guru"){
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $searchModel = new SiswaSearch(['id_guru' => $guru->id_guru]);
            }else{
                $searchModel = new SiswaSearch(['id_user' => Yii::$app->user->identity->id_user]);
            }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatsiswa($id)
    {
        $searchModel = new SiswaSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
       
    }

    public function actionGrafik($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        $nilai = NilaiKdPengetahuan::find()->where(['id_siswa' => $model->id_siswa])->all();
        return $this->render('grafik_pengetahuan',[
            'model' => $model,
            'nilai' => $nilai,
        ]);
    }

    public function actionGrafikktr($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        $nilai = NilaiKdKeterampilan::find()->where(['id_siswa' => $model->id_siswa])->all();
        return $this->render('grafik_keterampilan',[
            'model' => $model,
            'nilai' => $nilai,
        ]);
    }

    public function actionRekapnilai()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $searchModel = new SiswaSearch(['id_guru' => $kelas->id_guru]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_siswa' => $siswa->id_siswa]);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('rekapnilai', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetailsiswa($id)
    {
        $searchModel = new SiswaSearch(['id_siswa' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSiswaview($id)
    {
        $searchModel = new SiswaSearch(['id_user' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRekapnilaiketerampilan($id,$id_mapel)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();

        $nilai = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->groupBy(['id_kd_keterampilan'])->all();

        $nilaitema = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->groupBy(['id_tema'])->all();

        $tema = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->groupBy(['id_mapel'])->all();
        $nilaith = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->all();
        $kd = KompetensiDasarKeterampilan::find()->all();
        foreach ($nilai as $key => $kade) {
            $jenispenilaian = SdNilaiHarianKeterampilan::find()
                            ->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])
                            ->andWhere(['id_tema' => $kade->id_tema])
                            ->groupBy(['jenis_penilaian'])
                            ->all();
        }
        $jenis = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->groupBy(['jenis_penilaian'])->all();

        foreach ($nilai as $key => $value) {
        $nilaikd = NilaiKdKeterampilan::find()->where(['id_siswa' => $id, 'id_mapel' => $id_mapel])->all();
        }

        return $this->render('rekapnilaiketerampilan', [
            'nilai' => $nilai,
            'nilaith' => $nilaith,
            'nilaitema' => $nilaitema,
            'jenispenilaian' => $jenispenilaian,
            'model' => $model,
            'jenis' => $jenis,
            'tema' => $tema,
            //'mapel' => $mapel,
            //'nilai' => $nilai,
        ]);
    }
    public function actionPilihmapel($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        return $this->renderAjax('pilihmapel', [
            'model' => $model,
        ]);
    }

    public function actionPilihmapelsekolahmenengah($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        return $this->renderAjax('pilihmapelsekolahmenengah', [
            'model' => $model,
        ]);
    }

    public function actionRekapnilaipengetahuansekolahmenengah($id, $id_mapel){
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        $th = SmpNilaiHarianPengetahuan::find()->where(['id_siswa' => $id,'id_mapel' => $id_mapel])->groupBy(['nama_penilaian'])->all();
        $nilai = SmpNilaiHarianPengetahuan::find()->where(['id_siswa' => $id,'id_mapel' => $id_mapel])->groupBy(['id_kd_pengetahuan'])->all();

        return $this->render('rekapnilaipengetahuansekolahmenengah',[
            'model' => $model,
            'nilai' => $nilai,
            'th' => $th,
        ]);
    }


   

    /**
     * Displays a single Siswa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Siswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Siswa();
        $rombel = new RombonganBelajar();
        if ($model->load(Yii::$app->request->post()) OR $rombel->load(Yii::$app->request->post())) {
            $cekuser = Siswa::find()->where(['id_user' => $model->id_user])->all();
            if ($cekuser) {
                return "<script>
                            alert('user sudah dipakai');
                            window.location= 'index.php?r=siswa';
                        </script>";
            }else{
                
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

                
                if (Yii::$app->user->identity->role=="Guru") {
                    $model->id_guru = $guru->id_guru;
                    $model->id_sekolah = $guru->id_sekolah;
                }else{
                    $model->id_sekolah = $sekolah->id_sekolah;

                }
                $tmp = UploadedFile::getInstance($model, 'fotosiswa');
                if ($tmp != NULL) {
                    $model->foto = $tmp->baseName.'.'.$tmp->extension;
                }
                
               
                $model->save();
                if ($tmp != NULL) {
                    $tmp->saveAs('foto_siswa/'.$model->foto);
                }
                

                $ceksiswa = RombonganBelajar::find()->where(['id_siswa' => $model->id_siswa])->one();
                if ($ceksiswa) {
                    $rombel->id_siswa = $model->id_siswa;
                }else{
                    $smt = Kelas::find()->where(['id_kelas' => $model->id_kelas])->one();
                    $rombel->id_sekolah = $model->id_sekolah;
                    $rombel->id_kelas = $model->id_kelas;
                    $rombel->id_semester = $model->id_semester;
                    $rombel->tahun_ajaran = $smt->tahun_ajaran;
                    $rombel->id_siswa = $model->id_siswa;
                    $rombel->id_guru = $model->id_guru;
                    $rombel->save();
                }

                return $this->redirect(['index']);
            }
        }else{

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }
    }

    /**
     * Updates an existing Siswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rombel = new RombonganBelajar();
        if ($model->load(Yii::$app->request->post()) OR $rombel->load(Yii::$app->request->post())) {
             $tmp = UploadedFile::getInstance($model, 'fotosiswa');
                if ($tmp != NULL) {
                    $model->foto = $tmp->baseName.'.'.$tmp->extension;
                }
                $model->save();
                if ($tmp != NULL) {
                    $tmp->saveAs('foto_siswa/'.$model->foto);
                }
                $smt = Kelas::find()->where(['id_kelas' => $model->id_kelas])->one();
                $cek = RombonganBelajar::find()->where(['id_siswa' => $model->id_siswa])->one();
                if ($cek) {
                    $cek->id_sekolah = $model->id_sekolah;
                    $cek->id_kelas = $model->id_kelas;
                    $cek->id_semester = $model->id_semester;
                    $cek->tahun_ajaran = $smt->tahun_ajaran;
                    $cek->id_siswa = $model->id_siswa;
                    $cek->id_guru = $model->id_guru;
                    $cek->save();
                }else{
                    $rombel->id_sekolah = $model->id_sekolah;
                    $rombel->id_kelas = $model->id_kelas;
                    $rombel->id_semester = $model->id_semester;
                    $rombel->tahun_ajaran = $smt->tahun_ajaran;
                    $rombel->id_siswa = $model->id_siswa;
                    $rombel->id_guru = $model->id_guru;
                    $rombel->save();
                }
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Siswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Siswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Siswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Siswa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionKelas($id)
    {
        $jumlah=Kelas::find()->where(['id_guru'=>$id])->count();
        $kec=Kelas::find()->where(['id_guru'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kelas;?>"><?php echo $ke->tingkat_kelas." ".$ke->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
      public function actionSemester($id)
    {
        $jumlah=Kelas::find()->where(['id_kelas'=>$id])->count();
        $kec=Kelas::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_semester;?>"><?php echo $ke->semester->semester;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
