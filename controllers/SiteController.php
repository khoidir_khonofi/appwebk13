<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\Guru;
use app\models\User;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\Siswa;

class SiteController extends Controller
{
    public $layout = "backend.php";
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        }else{
            return $this->render('/web/index');
        }
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
       
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->render('index');
        }

        //$model->password = '';
        return $this->renderPartial('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->HomeUrl.'?r=web');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRegister()
    {

        $model = new Users();
        $guru = new Guru();
        $siswa = new Siswa();
        $sekolah = new Sekolah();

        if ($model->load(Yii::$app->request->post()) AND $guru->load(Yii::$app->request->post()) AND $siswa->load(Yii::$app->request->post()) AND $sekolah->load(Yii::$app->request->post())) {
            $cekusername = Users::find()->where(['username' => $model->username])->one();
            if($cekusername){
                return
                "<script>
                    alert('Username Telah Digunakan, coba username yang lain');
                    window.location = 'site/register';
                </script>";
            }else{

            
                if($model->role == "Administrator"){
                    $npsn = Sekolah::find()->where(['npsn' => $sekolah->npsn])->one();
                    if($npsn){
                        return "<script>
                                    alert('Npsn sudah terdaftar');
                                    window.location= 'site/register';
                                </script>";
                    }else{
                        $model->authKey = Yii::$app->security->generateRandomString();
                        $model->accessToken = "accessToken";
                        $model->status = "Aktif";
                        $model->save();
                        $sekolah->id_user = $model->id_user;
                        $sekolah->save();
                        return 
                            "<script>
                                alert('Pendaftaran Berhasil');
                                window.location = 'site/login';
                            </script>";
                    }

                }elseif($model->role=="Guru"){
                    $model->authKey = Yii::$app->security->generateRandomString();
                    $model->accessToken = "accessToken";
                    $model->status = "Tidak Aktif";
                    $model->save();
                    $guru->id_user = $model->id_user;
                    $guru->save();
                    return 
                        "<script>
                            alert('Pendaftaran Berhasil dan tunggu persetujuan dari admin:)');
                            window.location = 'site/login';
                        </script>";

                }else{
                    $model->authKey = Yii::$app->security->generateRandomString();
                    $model->accessToken = "accessToken";
                    $model->status = "Tidak Aktif";
                    $model->save();
                    $siswa->id_user = $model->id_user;
                    $siswa->save();
                    return 
                        "<script>
                            alert('Pendaftaran Berhasil dan tunggu persetujuan dari admin:)');
                            window.location = 'site/login';
                        </script>";
                }
            }
            
                
        }
           
        
        return $this->renderPartial('register',[
            'model' => $model,
            'guru' => $guru,
            'siswa' => $siswa,
            'sekolah' => $sekolah,
           

        ]);
    }

    public function actionProfil()
    {
        $model = User::find()->all();
            return $this->renderAjax('profil',[
                'model' => $model,
            ]);
    }

   

    public function actionKelas($id)
    {
        $jumlah=Kelas::find()->where(['id_sekolah'=>$id])->andWhere(['status' => 'Aktif'])->count();
        $kec=Kelas::find()->where(['id_sekolah'=>$id])->andWhere(['status' => 'Aktif'])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kelas;?>"><?php echo $ke->tingkat_kelas." ".$ke->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

     public function actionGuru($id)
    {
        //$model = Kelas::find()->where(['id_kelas' => $id])->one();
        $jumlah=Kelas::find()->where(['id_kelas'=>$id])->andWhere(['status' => 'Aktif'])->count();
        $kec=Kelas::find()->where(['id_kelas'=>$id])->andWhere(['status' => 'Aktif'])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_guru;?>"><?php echo $ke->guru->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }


    
   
}
