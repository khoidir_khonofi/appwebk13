<?php

namespace app\controllers;

use Yii;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianPengetahuanSearch;
use yii\web\Controller;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Guru;
use app\models\Kelas;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\KompetensiDasarPengetahuan;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdPengetahuanAkhir;
use app\models\Mengajar;
/**
/**
 * SmpNilaiHarianPengetahuanController implements the CRUD actions for SmpNilaiHarianPengetahuan model.
 */
class SmpNilaiHarianPengetahuanController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all SmpNilaiHarianPengetahuan models.
     * @return mixed
     */
    public function actionIndex()
    {
         
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SmpNilaiHarianPengetahuanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
         
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }elseif (Yii::$app->user->identity->role=="Guru") {
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_kelas' => $kelas->id_kelas]);
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_mapel' => $mapel->id_mapel]);
            }else{
                $searchModel = new SmpNilaiHarianPengetahuanSearch();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_siswa' => $siswa->id_siswa]);
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihatnilaisiswapeng($id)
    {
        $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_kelas' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmpNilaiHarianPengetahuan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SmpNilaiHarianPengetahuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SmpNilaiHarianPengetahuan();
        $nilai = new NilaiKdPengetahuanSekolahMenengah();
        $nilaikdakhir = new NilaiKdPengetahuanAkhirSekolahMenengah();
        if ($model->load(Yii::$app->request->post()) OR $nilai->load(Yii::$app->request->post()) OR $nilaikdakhir->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();

            if(Yii::$app->user->identity->role=="Administrator"){
                $model->id_sekolah = $sekolah->id_sekolah;

            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            // $model->save();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    $cekkd = NilaiKdPengetahuanSekolahMenengah::find()
                        ->where([
                            'id_kd_pengetahuan' => $model->id_kd_pengetahuan, 
                            'id_semester' => $model->id_semester
                        ])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SmpNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save(); 
                    }

                     $ceknilaikd = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                        ->where([
                            'id_siswa' => $model->id_siswa, 
                            'id_semester' => $model->id_semester
                        ])->andWhere([
                            'id_mapel' => $model->id_mapel
                        ])
                        ->one();
                    if ($ceknilaikd) {
                        $hitungnilaikd = NilaiKdPengetahuanSekolahMenengah::find()
                            ->where([
                                'id_siswa' => $ceknilaikd->id_siswa, 
                                'id_semester' => $ceknilaikd->id_semester
                            ])
                            ->andWhere([
                                'id_mapel' => $ceknilaikd->id_mapel
                            ])
                            ->all();
                        $hitung = 0;
                        foreach ($hitungnilaikd as $key => $value) {
                            $row = count($hitungnilaikd);
                            $hitung += $value['nph'];
                            $result = $hitung/$row;
                            $hasil = round($result);
                        }
                        $ceknilaikd->nilai_kd = $hasil;
                        $ceknilaikd->save();

                    }else{
                        $nilaikdakhir->id_nilai_pengetahuan_sekolah_menengah = $nilai->id_nilai_pengetahuan_sekolah_menengah;
                        $nilaikdakhir->id_sekolah = $nilai->id_sekolah;
                        $nilaikdakhir->id_siswa = $nilai->id_siswa;
                        $nilaikdakhir->id_kelas = $nilai->id_kelas;
                        $nilaikdakhir->id_mapel = $nilai->id_mapel;
                        $nilaikdakhir->id_semester = $nilai->id_semester;
                        $nilaikdakhir->nilai_kd = $nilai->nph;
                        $nilaikdakhir->save();
                    }
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                Yii::error($e->getMessage());
            }

            $transaction->rollBack();
        }

        return $this->renderAjax('tambahnilai', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SmpNilaiHarianPengetahuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nilai = new NilaiKdPengetahuanSekolahMenengah();
        $nilaikdakhir = new NilaiKdPengetahuanAkhirSekolahMenengah();
        if ($model->load(Yii::$app->request->post()) OR $nilai->load(Yii::$app->request->post()) OR $nilaikdakhir->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    $cekkd = NilaiKdPengetahuanSekolahMenengah::find()
                        ->where(['id_kd_pengetahuan' => $model->id_kd_pengetahuan,'id_semester' => $model->id_semester])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SmpNilaiHarianPengetahuan::find()
                                ->where(['id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,'id_semester' => $cekkd->id_semester])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save(); 
                    }

                     $ceknilaikd = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                        ->where([
                            'id_siswa' => $model->id_siswa, 
                            'id_semester' => $model->id_semester
                        ])->andWhere([
                            'id_mapel' => $model->id_mapel
                        ])
                        ->one();
                    if ($ceknilaikd) {
                        $hitungnilaikd = NilaiKdPengetahuanSekolahMenengah::find()
                            ->where([
                                'id_siswa' => $ceknilaikd->id_siswa, 
                                'id_semester' => $ceknilaikd->id_semester
                            ])
                            ->andWhere([
                                'id_mapel' => $ceknilaikd->id_mapel
                            ])
                            ->all();
                        $hitung = 0;
                        foreach ($hitungnilaikd as $key => $value) {
                            $row = count($hitungnilaikd);
                            $hitung += $value['nph'];
                            $result = $hitung/$row;
                            $hasil = round($result);
                        }
                        $ceknilaikd->nilai_kd = $hasil;
                        $ceknilaikd->save();

                    }else{
                        $nilaikdakhir->id_nilai_pengetahuan_sekolah_menengah = $nilai->id_nilai_pengetahuan_sekolah_menengah;
                        $nilaikdakhir->id_sekolah = $nilai->id_sekolah;
                        $nilaikdakhir->id_siswa = $nilai->id_siswa;
                        $nilaikdakhir->id_kelas = $nilai->id_kelas;
                        $nilaikdakhir->id_mapel = $nilai->id_mapel;
                        $nilaikdakhir->id_semester = $nilai->id_semester;
                        $nilaikdakhir->nilai_kd = $nilai->nph;
                        $nilaikdakhir->save();
                    }

                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                Yii::error($e->getMessage());
            }

            $transaction->rollBack();

           
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SmpNilaiHarianPengetahuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        if ($model) {
            return "<script>alert('ups!!! periksa nilai kd pengetahuan');
                        window.location='index.php?r=smp-nilai-harian-pengetahuan';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        

        return $this->redirect(['index']);
    }

    /**
     * Finds the SmpNilaiHarianPengetahuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmpNilaiHarianPengetahuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmpNilaiHarianPengetahuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     public function actionKdktr($id)
    {
        $jumlah=KompetensiDasarPengetahuan::find()->where(['id_mapel'=>$id])->count();
        $kec=KompetensiDasarPengetahuan::find()->where(['id_mapel'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kd_pengetahuan;?>"><?php echo $ke->no_kd;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
