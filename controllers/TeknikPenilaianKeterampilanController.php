<?php

namespace app\controllers;

use Yii;
use app\models\TeknikPenilaianKeterampilan;
use app\models\TeknikPenilaianKeterampilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\Guru;
use app\models\TemaKeterampilan;
use app\models\MataPelajaran;
use yii\filters\VerbFilter;
use app\models\KompetensiDasarKeterampilan;

/**
 * TeknikPenilaianKeterampilanController implements the CRUD actions for TeknikPenilaianKeterampilan model.
 */
class TeknikPenilaianKeterampilanController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
      public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all TeknikPenilaianKeterampilan models.
     * @return mixed
     */
    public function actionIndex()
    {
         $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
        $guru = Guru::find()->where(['id_user' => $user->id_user])->one();

        if (Yii::$app->user->identity->role=="Administrator") {
            $searchModel = new TeknikPenilaianKeterampilanSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }else{
            $searchModel = new TeknikPenilaianKeterampilanSearch(['id_sekolah' => $guru->id_sekolah]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLihat($id)
    {
        $searchModel = new TeknikPenilaianKeterampilanSearch(['id_tema' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TeknikPenilaianKeterampilan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TeknikPenilaianKeterampilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeknikPenilaianKeterampilan();

        if ($model->load(Yii::$app->request->post())) {

            if (Yii::$app->user->identity->role=="Administrator") {
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();    
                $model->id_sekolah = $guru->id_sekolah;
                $model->id_kelas = $kelas->id_kelas;
            }
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    public function actionTambahsubtema($id)
    {
        $model = new TeknikPenilaianKeterampilan();

        if ($model->load(Yii::$app->request->post())) {
            $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

            if (Yii::$app->user->identity->role=="Administrator") {
                $model->id_sekolah = $sekolah->id_sekolah;
            }else{
                $model->id_sekolah = $guru->id_sekolah;
            }
            $tema = TemaKeterampilan::find()->where(['id_tema' => $id])->one();
            $model->id_tema = $tema->id_tema;
            $model->id_kelas = $tema->id_kelas;
            $model->id_mapel = $tema->id_mapel;
            $model->id_kd = $tema->id_kd;
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->renderAjax('tambahsubtema', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TeknikPenilaianKeterampilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TeknikPenilaianKeterampilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TeknikPenilaianKeterampilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeknikPenilaianKeterampilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeknikPenilaianKeterampilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


     public function actionMapel($id)
    {
        $jumlah=MataPelajaran::find()->where(['id_kelas'=>$id])->count();
        $kec=MataPelajaran::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_mapel;?>"><?php echo $ke->nama_mata_pelajaran;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

    public function actionKdktr($id)
    {
        $jumlah=KompetensiDasarKeterampilan::find()->where(['id_mapel'=>$id])->count();
        $kec=KompetensiDasarKeterampilan::find()->where(['id_mapel'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_kd_keterampilan;?>"><?php echo $ke->no_kd;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }

     public function actionTema($id)
    {
        $jumlah=TemaKeterampilan::find()->where(['id_kd'=>$id])->count();
        $kec=TemaKeterampilan::find()->where(['id_kd'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_tema;?>"><?php echo $ke->tema;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
