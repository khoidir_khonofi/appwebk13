<?php

namespace app\controllers;

use Yii;
use app\models\Telegram;
use app\models\TelegramSearch;
use app\models\Kelas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\SdNilaiHarianPengetahuanSearch;
use app\models\SmaNilaiHarianPengetahuanSearch;
use app\models\SmpNilaiHarianPengetahuanSearch;
use app\models\SdNilaiHarianKeterampilanSearch;
use app\models\SmpNilaiHarianKeterampilanSearch;
use app\models\SmaNilaiHarianKeterampilanSearch;
use app\models\Sekolah;
use app\models\SiswaSearch;
use app\models\Guru;
use app\models\Siswa;
use kartik\mpdf\Pdf;
use  yii\helpers\Url;
/**
 * TelegramController implements the CRUD actions for Telegram model.
 */
class TelegramController extends Controller
{
    public $layout = "backend.php";
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    /**
     * Lists all Telegram models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_guru' => $guru->id_guru]);
        }
             
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionSearch()
    {
        if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_sekolah' => $sekolah->id_sekolah]);
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $searchModel = new SiswaSearch(['id_guru' => $guru->id_guru]);
        }
             
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('_search', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telegram model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionKirim()
    {
        $model = Siswa::find()->one();
        return $this->renderAjax('kirim',[
            'model' => $model
        ]);
    }

    public function actionEditid($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->renderAjax('edit',[
            'model' => $model 
        ]);
    }

    public function actionBulan()
    {
        return $this->renderAjax('bulan');
    }

    public function actionTelegrampengetahuan($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        if (Yii::$app->user->identity->role=='Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->one();
            if($sekolah->jenjang_pendidikan == 'SMA') {
                $searchModel = new SmaNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }elseif ($sekolah->jenjang_pendidikan == 'SMP') {
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }else{
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            if($guru->tblsekolah->jenjang_pendidikan == 'SMA') {
                $searchModel = new SmaNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }elseif ($guru->tblsekolah->jenjang_pendidikan == 'SMP') {
                $searchModel = new SmpNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }else{
                $searchModel = new SdNilaiHarianPengetahuanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }
        }
            
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            $content = $this->renderPartial('nilaisiswa',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model
            ]);


           $options = [
		        // set to use core fonts only
		        'mode' => Pdf::MODE_CORE, 
		        // A4 paper format
		        'format' => Pdf::FORMAT_A4, 
		        // portrait orientation
		        'orientation' => Pdf::ORIENT_PORTRAIT, 
		        // stream to browser inline
		        'destination' => Pdf::DEST_FILE,
		        'filename' => Yii::getAlias('@webroot/document/'.$model->nis.'.pdf'),

		        // your html content input
		        'content' => $content,  
		        // format content from your own css file if needed or use the
		        // enhanced bootstrap css built by Krajee for mPDF formatting 
		        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
		        // any css to be embedded if required
		        'cssInline' => '.kv-heading-1{font-size:18px}', 
		         // set mPDF properties on the fly
		        'options' => ['title' => 'Krajee Report Title'],
		         // call mPDF methods on the fly
		        'methods' => [ 
		            'SetHeader'=>['Krajee Report Header'], 
		            'SetFooter'=>['{PAGENO}'],
		        ]
		    ]; 

           $file = new Pdf($options);
           $file->render();

	        try{
		   	Yii::$app->telegram->sendDocument([
                'chat_id' => $model->chat_id_telegram,
                'document' => Yii::getAlias('@webroot/document/'.$model->nis.'.pdf'),
                'caption' => 'Nilai harian pengetahuan '.$model->nama,
            ]);

		   } catch(\Exception $e){
		   }

	       $options['destination']  = PDF::DEST_BROWSER;
	       unset($options['filename']);	
           $pdf = new Pdf($options);

 	    return $pdf->render(); 
    }

    public function actionTelegramketerampilan($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        if (Yii::$app->user->identity->role=='Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->one();
            if($sekolah->jenjang_pendidikan == 'SMA') {
                $searchModel = new SmaNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }elseif ($sekolah->jenjang_pendidikan == 'SMP') {
                $searchModel = new SmpNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }else{
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }
        }else{
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            if($guru->tblsekolah->jenjang_pendidikan == 'SMA') {
                $searchModel = new SmaNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }elseif ($guru->tblsekolah->jenjang_pendidikan == 'SMP') {
                $searchModel = new SmpNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }else{
                $searchModel = new SdNilaiHarianKeterampilanSearch(['id_siswa' => $model->id_siswa, 'id_kelas' => $kelas->id_kelas]);
            }
        }
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            $content = $this->renderPartial('nilaisiswaketerampilan',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model
            ]);
            $options = [
		        // set to use core fonts only
		        'mode' => Pdf::MODE_CORE, 
		        // A4 paper format
		        'format' => Pdf::FORMAT_A4, 
		        // portrait orientation
		        'orientation' => Pdf::ORIENT_PORTRAIT, 
		        // stream to browser inline
		        'destination' => Pdf::DEST_FILE,
		        'filename' => Yii::getAlias('@webroot/document/'.$model->nis.'.pdf'),

		        // your html content input
		        'content' => $content,  
		        // format content from your own css file if needed or use the
		        // enhanced bootstrap css built by Krajee for mPDF formatting 
		        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
		        // any css to be embedded if required
		        'cssInline' => '.kv-heading-1{font-size:18px}', 
		         // set mPDF properties on the fly
		        'options' => ['title' => 'Krajee Report Title'],
		         // call mPDF methods on the fly
		        'methods' => [ 
		            'SetHeader'=>['Krajee Report Header'], 
		            'SetFooter'=>['{PAGENO}'],
		        ]
		    ]; 

           $file = new Pdf($options);
           $file->render();

	        try{
		   	Yii::$app->telegram->sendDocument([
                'chat_id' => $model->chat_id_telegram,
                'document' => Yii::getAlias('@webroot/document/'.$model->nis.'.pdf'),
                'caption' => 'Nilai harian keterampilan '.$model->nama,
            ]);

		   } catch(\Exception $e){
		   }

	       $options['destination']  = PDF::DEST_BROWSER;
	       unset($options['filename']);	
           $pdf = new Pdf($options);

 	    return $pdf->render();
    }


   
    /**
     * Creates a new Telegram model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Telegram();

        if ($model->load(Yii::$app->request->post())) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model->id_sekolah = $sekolah->id_sekolah;
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Telegram model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Telegram model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telegram model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Telegram the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Telegram::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    

     public function actionSiswa($id)
    {
        $jumlah=Siswa::find()->where(['id_kelas'=>$id])->count();
        $kec=Siswa::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_siswa;?>"><?php echo $ke->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
