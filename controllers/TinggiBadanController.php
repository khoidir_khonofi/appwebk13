<?php

namespace app\controllers;

use Yii;
use app\models\TinggiBadan;
use app\models\TinggiBadanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Kelas;
use yii\data\ActiveDataProvider;
use app\models\Tema;
use app\models\Guru;
use app\models\Siswa;
/**
 * TinggiBadanController implements the CRUD actions for TinggiBadan model.
 */
class TinggiBadanController extends Controller
{
     public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
      public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all TinggiBadan models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = TinggiBadan::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah]);
        }elseif(Yii::$app->user->identity->role=="Guru"){
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = TinggiBadan::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $guru->id_sekolah]);
        }else{
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = TinggiBadan::find()->where(['id_siswa' => $siswa->id_siswa]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
            
        return $this->render('index', [
            'searchModel' => new TinggiBadan(),
            'dataProvider' => $provider, 
        ]);
    }

    /**
     * Displays a single TinggiBadan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TinggiBadan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TinggiBadan();

         if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->user->identity->role=="Guru") {
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $model->id_sekolah = $guru->id_sekolah;
                $model->id_kelas = $kelas->id_kelas;
                $model->id_semester = $kelas->id_semester;
            }else{
                $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $model->id_sekolah = $sekolah->id_sekolah;
            }
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TinggiBadan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TinggiBadan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TinggiBadan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TinggiBadan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TinggiBadan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
