<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\Guru;
use app\models\GuruSearch;
use app\models\User;
use app\models\Siswa;
use yii\data\ActiveDataProvider;
/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public $layout = "backend.php";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => ['index','create','update','view'],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }


    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {        
        if(Yii::$app->user->identity->role=="Administrator"){
            $sekolah = Sekolah::findOne(['id_user' => Yii::$app->user->identity->id_user]);

            $model = Users::find()->joinWith('siswa')->where(['siswa.id_sekolah' => $sekolah->id_sekolah])->joinWith('guru')->where(['guru.id_sekolah' => $sekolah->id_sekolah])->orderBy(['id_user' => SORT_DESC]);     
             
        }elseif(Yii::$app->user->identity->role== "Guru"){
            $guru = Guru::findOne(['id_user' => Yii::$app->user->identity->id_user]);
            $model = Users::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru])->orderBy(['id_user' => SORT_DESC]); 
        }else{
            $model = User::find()->where(['id_user' => Yii::$app->user->identity->id_user]);
        }
        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
            
        return $this->render('index', [
            'searchModel' => new User(),
            'dataProvider' => $provider, 
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_search',[
            'model' => $searchModel,
            'dataProvider' => $dataProvider, 
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionLihat($id)
    {
        return $this->render('lihat', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $guru = new Guru();
        $siswa = new Siswa();

        if ($model->load(Yii::$app->request->post()) || 
            $guru->load(Yii::$app->request->post())){

            $idsekolah = Sekolah::find()
                    ->where(['id_user' => Yii::$app->user->identity->id_user])
                    ->one();
            $cekusername = Users::find()
                    ->where([
                        'username' => $model->username
                    ])
                    ->one();
            if($cekusername){
                return 
                "<script>
                    alert('Username telah digunakan, coba username lain');
                    window.location = 'index.php?r=users';
                </script>";
            }else{
                if(Yii::$app->user->identity->role == "Administrator"){
                    if($model->role == "Guru"){
                            $model->authKey = Yii::$app->security->generateRandomString();
                            $model->accessToken = "accessToken";
                            $model->status = "Aktif";

                            if($model->save()){
                                $guru->id_user = $model->id_user;
                                $guru->id_sekolah = $idsekolah->id_sekolah;
                                $guru->save();
                            }else{
                                echo "error";
                            }
                    }else{
                            $model->authKey = Yii::$app->security->generateRandomString();
                            $model->accessToken = "accessToken";
                            $model->status = "Aktif";
                            if ($model->save()) {
                                $siswa->load(Yii::$app->request->post());
                                $siswa->id_user = $model->id_user;
                                $siswa->id_sekolah = $idsekolah->id_sekolah;
                                if ($siswa->save()) {
                                    echo "berhasil";
                                }else{
                                    echo "gagal maning";
                                }
                            }else{
                                echo "error siswa";
                            }
                    }   
                         
                   
                }else{
                    
                            $guru = Guru::find()
                                    ->where(['id_user' => Yii::$app->user->identity->id_user])
                                    ->one();
                            $kelas = Kelas::find()
                                    ->where(['id_guru' => $guru->id_guru])
                                    ->one();
                            $model->authKey = Yii::$app->security->generateRandomString();
                            $model->accessToken = "accessToken";
                            $model->status = "Aktif";
                            $model->role = "Siswa";
                            if($model->save()){
                                $siswa->id_user = $model->id_user;
                                $siswa->id_kelas = $kelas->id_kelas;
                                $siswa->id_semester = $kelas->id_semester;
                                $siswa->id_guru = $guru->id_guru;
                                $siswa->id_sekolah = $guru->id_sekolah;
                                $siswa->save();
                                $transaction->commit();
                            }
                        
                }
                return $this->redirect(['index']);
               
            }
           // return $this->redirect(['site/login']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
            'guru'=> $guru,
            'siswa' => $siswa,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $guru = Guru::find()->where(['id_user' => $model->id_user])->one();
        $siswa = new Siswa();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['index']);
        }
        
        return $this->renderAjax('edit', [
            'model' => $model,
            'guru' => $guru,
            'siswa'=> $siswa,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $ceksiswa = Siswa::find()->where(['id_user' => $id])->one();
        if ($ceksiswa) {
            return "<script>alert('Hapus siswa terlebih dahulu'); window.location='index.php?r=users';</script>";
        }else{
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAktif($id)
    {
        $model = Users::find()->where(['id_user' => $id])->one();
        $siswa = Siswa::find()->where(['id_user' => $id])->one();
        $model->status = "Aktif";
        $siswa->status = 'Aktif';
        $model->save();
        $siswa->save();
        return $this->redirect(['index']);
    }

    public function actionTidakaktif($id)
    {
        $model = Users::find()->where(['id_user' => $id])->one();
        $siswa = Siswa::find()->where(['id_user' => $id])->one();
        $model->status = "Tidak Aktif";
        $siswa->status = 'Tidak Aktif';
        $model->save();
        $siswa->save();
        return $this->redirect(['index']);
    }

    public function actionGuru($id)
    {
        //$model = Kelas::find()->where(['id_kelas' => $id])->one();
        $jumlah=Kelas::find()->where(['id_kelas'=>$id])->count();
        $kec=Kelas::find()->where(['id_kelas'=>$id])->all();
            if($jumlah>0){
                foreach($kec as $ke) { ?>

                    <option value="<?php echo $ke->id_guru;?>"><?php echo $ke->guru->nama;?></option>
            <?php }
            }else {
                echo "<option>---</option>";
            }
    }
}
