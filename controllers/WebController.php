<?php

namespace app\controllers;

use Yii;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SdNilaiHarianPengetahuanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
 * SdNilaiHarianPengetahuanController implements the CRUD actions for SdNilaiHarianPengetahuan model.
 */
class WebController extends Controller
{

	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'only' => [''],
                        'rules' => [
                            // allow authenticated users
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            // everything else is denied
                        ],
                    ],            
        ];
    }

    public function actionIndex()
    {
    	$this->layout = 'main.php';
    	return $this->render('index');
    }

}