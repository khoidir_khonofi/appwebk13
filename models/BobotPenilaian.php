<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bobot_penilaian".
 *
 * @property int $id
 * @property int $id_sekolah
 * @property int $bobot_nph
 * @property int $bobot_npts
 * @property int $bobot_npas
 */
class BobotPenilaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bobot_penilaian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'bobot_nph', 'bobot_npts', 'bobot_npas','id_kelas'], 'required'],
            [['id_sekolah', 'bobot_nph', 'bobot_npts', 'bobot_npas','id_kelas'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' =>  'Id Kelas',
            'bobot_nph' => 'Bobot Nph',
            'bobot_npts' => 'Bobot Npts',
            'bobot_npas' => 'Bobot Npas',
        ];
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
