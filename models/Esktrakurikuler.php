<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "esktrakurikuler".
 *
 * @property int $id_ekstrakurikuler
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $kegiatan_ekstrakurikuler
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 */
class Esktrakurikuler extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esktrakurikuler';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'kegiatan_ekstrakurikuler'], 'required'],
            [['id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['kegiatan_ekstrakurikuler'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ekstrakurikuler' => 'Id Ekstrakurikuler',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'kegiatan_ekstrakurikuler' => 'Kegiatan Ekstrakurikuler',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
