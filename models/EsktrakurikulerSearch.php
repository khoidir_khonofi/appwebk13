<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Esktrakurikuler;

/**
 * EsktrakurikulerSearch represents the model behind the search form of `app\models\Esktrakurikuler`.
 */
class EsktrakurikulerSearch extends Esktrakurikuler
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ekstrakurikuler', 'id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['kegiatan_ekstrakurikuler', 'keterangan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Esktrakurikuler::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_ekstrakurikuler' => $this->id_ekstrakurikuler,
            'id_sekolah' => $this->id_sekolah,
            'id_siswa' => $this->id_siswa,
            'id_kelas' => $this->id_kelas,
            'id_semester' => $this->id_semester,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kegiatan_ekstrakurikuler', $this->kegiatan_ekstrakurikuler])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
