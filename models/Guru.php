<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guru".
 *
 * @property int $id_guru
 * @property string $nip
 * @property string $nama
 * @property string $alamat
 * @property int $jabatan
 */
class Guru extends \yii\db\ActiveRecord
{
    public $fotoguru;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guru';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['jenis_kelamin'], 'safe'],
            [['alamat'], 'string'],
            [['id_user','id_sekolah'], 'integer'],
            [['nip', 'nama','jabatan','foto'], 'string', 'max' => 200],
            [['fotoguru'],'file','skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_guru' => 'Id Guru',
            'id_user' => 'Id User',
            'id_sekolah' => 'Id Sekolah',
            'nip' => 'Nip/Nik',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'foto' => 'Foto',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jabatan' => 'Jabatan',
        ];
    }
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
    public function getTblsekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
}
