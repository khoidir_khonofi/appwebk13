<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jurnal_sikap".
 *
 * @property int $id_jurnal_sikap
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property int $sikap
 * @property string $catatan_perilaku
 * @property int $butir_sikap
 * @property string $penilaian
 * @property string $tindak_lanjut
 * @property string $created_at
 * @property string $updated_at
 */
class JurnalSikap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurnal_sikap';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'sikap', 'catatan_perilaku', 'butir_sikap', 'penilaian'], 'required'],
            [['id_sekolah', 'id_siswa', 'id_kelas', 'id_semester'], 'integer'],
            [['catatan_perilaku', 'penilaian', 'tindak_lanjut', 'sikap', 'butir_sikap'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jurnal_sikap' => 'Id Jurnal Sikap',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'id_siswa' => 'Id Siswa',
            'sikap' => 'Sikap',
            'catatan_perilaku' => 'Catatan Perilaku',
            'butir_sikap' => 'Butir Sikap',
            'penilaian' => 'Penilaian',
            'tindak_lanjut' => 'Tindak Lanjut',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getTblsekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
