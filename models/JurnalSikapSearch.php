<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JurnalSikap;

/**
 * JurnalSikapSearch represents the model behind the search form of `app\models\JurnalSikap`.
 */
class JurnalSikapSearch extends JurnalSikap
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jurnal_sikap', 'id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['catatan_perilaku', 'penilaian', 'tindak_lanjut', 'created_at', 'updated_at', 'sikap', 'butir_sikap'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JurnalSikap::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jurnal_sikap' => $this->id_jurnal_sikap,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_semester' => $this->id_semester,
            'id_siswa' => $this->id_siswa,
            'sikap' => $this->sikap,
            'butir_sikap' => $this->butir_sikap,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'catatan_perilaku', $this->catatan_perilaku])
            ->andFilterWhere(['like', 'penilaian', $this->penilaian])
            ->andFilterWhere(['like', 'tindak_lanjut', $this->tindak_lanjut]);

        return $dataProvider;
    }
}
