<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_keterampilan".
 *
 * @property int $id_kd
 * @property int $kelas
 * @property string $no_kd
 * @property string $judul
 * @property string $mata_pelajaran
 */
class KdKeterampilan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelas', 'no_kd', 'judul', 'mata_pelajaran'], 'required'],
            [['kelas'], 'integer'],
            [['judul'], 'string'],
            [['no_kd'], 'string', 'max' => 20],
            [['mata_pelajaran'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kd' => 'Id Kd',
            'kelas' => 'Kelas',
            'no_kd' => 'No Kd',
            'judul' => 'Judul',
            'mata_pelajaran' => 'Mata Pelajaran',
        ];
    }
}
