<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KdPengetahuan;

/**
 * KdPengetahuanSearch represents the model behind the search form of `app\models\KdPengetahuan`.
 */
class KdPengetahuanSearch extends KdPengetahuan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd', 'kelas'], 'integer'],
            [['no_kd', 'judul', 'mata_pelajaran'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KdPengetahuan::find()->orderBy(['id_kd' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kd' => $this->id_kd,
            'kelas' => $this->kelas,
        ]);

        $query->andFilterWhere(['like', 'no_kd', $this->no_kd])
            ->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mata_pelajaran', $this->mata_pelajaran]);

        return $dataProvider;
    }
}
