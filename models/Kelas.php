<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelas".
 *
 * @property int $id_kelas
 * @property int $id_guru
 * @property int $id_semester
 * @property int $id_sekolah
 * @property string $tingkat_kelas
 * @property string $nama
 * @property string $tahun_ajaran
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Guru $guru
 * @property Semester $semester
 * @property Sekolah $sekolah
 * @property KompetensiDasarKeterampilan[] $kompetensiDasarKeterampilans
 * @property KompetensiDasarPengetahuan[] $kompetensiDasarPengetahuans
 * @property MataPelajaran[] $mataPelajarans
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_guru', 'id_semester', 'id_sekolah', 'tingkat_kelas', 'nama', 'tahun_ajaran'], 'required'],
            [['id_guru', 'id_semester', 'id_sekolah'], 'integer'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            [['tingkat_kelas', 'nama'], 'string', 'max' => 200],
            [['tahun_ajaran'], 'string', 'max' => 15],
            [['id_guru'], 'exist', 'skipOnError' => true, 'targetClass' => Guru::className(), 'targetAttribute' => ['id_guru' => 'id_guru']],
            [['id_semester'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['id_semester' => 'id_semester']],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kelas' => 'Id Kelas',
            'id_guru' => 'Id Guru',
            'id_semester' => 'Id Semester',
            'id_sekolah' => 'Id Sekolah',
            'tingkat_kelas' => 'Tingkat Kelas',
            'nama' => 'Nama',
            'tahun_ajaran' => 'Tahun Ajaran',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Guru]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuru()
    {
        return $this->hasOne(Guru::className(), ['id_guru' => 'id_guru']);
    }

    /**
     * Gets query for [[Semester]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[KompetensiDasarKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarKeterampilans()
    {
        return $this->hasMany(KompetensiDasarKeterampilan::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[KompetensiDasarPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarPengetahuans()
    {
        return $this->hasMany(KompetensiDasarPengetahuan::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[MataPelajarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMataPelajarans()
    {
        return $this->hasMany(MataPelajaran::className(), ['id_kelas' => 'id_kelas']);
    }
}
