<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kkm_satuan_pendidikan".
 *
 * @property int $id_kkm
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $kkm
 * @property string $created_at
 * @property string $updated_at
 */
class KkmSatuanPendidikan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kkm_satuan_pendidikan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'kkm'], 'required'],
            [['id_sekolah', 'id_kelas', 'kkm'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kkm' => 'Id Kkm',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'kkm' => 'Kkm',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
     public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
