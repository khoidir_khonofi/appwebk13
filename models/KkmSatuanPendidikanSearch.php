<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KkmSatuanPendidikan;

/**
 * KkmSatuanPendidikanSearch represents the model behind the search form of `app\models\KkmSatuanPendidikan`.
 */
class KkmSatuanPendidikanSearch extends KkmSatuanPendidikan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kkm', 'id_sekolah', 'id_kelas', 'kkm'], 'integer'],
            [['created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KkmSatuanPendidikan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kkm' => $this->id_kkm,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'kkm' => $this->kkm,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
