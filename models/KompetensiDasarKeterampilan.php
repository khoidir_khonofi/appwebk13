<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kompetensi_dasar_keterampilan".
 *
 * @property int $id_kd_keterampilan
 * @property int $id_sekolah
 * @property int $id_semester
 * @property int $id_kelas
 * @property int $id_mapel
 * @property string $no_kd
 * @property string $judul
 * @property float $kkm
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Kelas $kelas
 * @property Sekolah $sekolah
 * @property MataPelajaran $mapel
 * @property Semester $semester
 * @property SdNilaiHarianKeterampilan[] $sdNilaiHarianKeterampilans
 * @property SmaNilaiHarianKeterampilan[] $smaNilaiHarianKeterampilans
 * @property SmpNilaiHarianKeterampilan[] $smpNilaiHarianKeterampilans
 */
class KompetensiDasarKeterampilan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kompetensi_dasar_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah',  'id_kelas', 'id_mapel', 'no_kd', 'judul'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel'], 'integer'],
            [['judul'], 'string'],
            [['kkm'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_kd'], 'string', 'max' => 20],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
            [['id_mapel'], 'exist', 'skipOnError' => true, 'targetClass' => MataPelajaran::className(), 'targetAttribute' => ['id_mapel' => 'id_mapel']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kd_keterampilan' => 'Id Kd Keterampilan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'no_kd' => 'No Kd',
            'judul' => 'Judul',
            'kkm' => 'Kkm',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

    /**
     * Gets query for [[Mapel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }

    /**
     * Gets query for [[Semester]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[SdNilaiHarianKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianKeterampilans()
    {
        return $this->hasMany(SdNilaiHarianKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }

    /**
     * Gets query for [[SmaNilaiHarianKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmaNilaiHarianKeterampilans()
    {
        return $this->hasMany(SmaNilaiHarianKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }

    /**
     * Gets query for [[SmpNilaiHarianKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmpNilaiHarianKeterampilans()
    {
        return $this->hasMany(SmpNilaiHarianKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }
}
