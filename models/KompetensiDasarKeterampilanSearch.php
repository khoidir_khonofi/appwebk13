<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KompetensiDasarKeterampilan;

/**
 * KompetensiDasarKeterampilanSearch represents the model behind the search form of `app\models\KompetensiDasarKeterampilan`.
 */
class KompetensiDasarKeterampilanSearch extends KompetensiDasarKeterampilan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd_keterampilan', 'id_sekolah', 'id_kelas', 'id_mapel'], 'integer'],
            [['no_kd', 'judul', 'created_at', 'updated_at'], 'safe'],
            [['kkm'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KompetensiDasarKeterampilan::find()->orderBy(['id_kd_keterampilan' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kd_keterampilan' => $this->id_kd_keterampilan,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_mapel' => $this->id_mapel,
            'kkm' => $this->kkm,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'no_kd', $this->no_kd])
            ->andFilterWhere(['like', 'judul', $this->judul]);

        return $dataProvider;
    }
}
