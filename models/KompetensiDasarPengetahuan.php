<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kompetensi_dasar_pengetahuan".
 *
 * @property int $id_kd_pengetahuan
 * @property int $id_sekolah
 * @property int $id_semester
 * @property int $id_kelas
 * @property int $id_mapel
 * @property string $no_kd
 * @property string $judul
 * @property float $kkm
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Kelas $kelas
 * @property Sekolah $sekolah
 * @property MataPelajaran $mapel
 * @property Semester $semester
 * @property SdNilaiHarianPengetahuan[] $sdNilaiHarianPengetahuans
 * @property SmaNilaiHarianPengetahuan[] $smaNilaiHarianPengetahuans
 * @property SmpNilaiHarianPengetahuan[] $smpNilaiHarianPengetahuans
 */
class KompetensiDasarPengetahuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kompetensi_dasar_pengetahuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_mapel', 'no_kd', 'judul'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel'], 'integer'],
            [['judul'], 'string'],
            [['kkm'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_kd'], 'string', 'max' => 20],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
            [['id_mapel'], 'exist', 'skipOnError' => true, 'targetClass' => MataPelajaran::className(), 'targetAttribute' => ['id_mapel' => 'id_mapel']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kd_pengetahuan' => 'Id Kd Pengetahuan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'no_kd' => 'No Kd',
            'judul' => 'Judul',
            'kkm' => 'Kkm',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

    /**
     * Gets query for [[Mapel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }

    /**
     * Gets query for [[Semester]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[SdNilaiHarianPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianPengetahuans()
    {
        return $this->hasMany(SdNilaiHarianPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }

    /**
     * Gets query for [[SmaNilaiHarianPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmaNilaiHarianPengetahuans()
    {
        return $this->hasMany(SmaNilaiHarianPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }

    /**
     * Gets query for [[SmpNilaiHarianPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmpNilaiHarianPengetahuans()
    {
        return $this->hasMany(SmpNilaiHarianPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }
}
