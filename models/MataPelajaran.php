<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mata_pelajaran".
 *
 * @property int $id_mapel
 * @property int $id_semester
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property string $nama_mata_pelajaran
 * @property string $tahun_ajaran
 * @property string $created_at
 * @property string $updated_at
 *
 * @property KompetensiDasarKeterampilan[] $kompetensiDasarKeterampilans
 * @property KompetensiDasarPengetahuan[] $kompetensiDasarPengetahuans
 * @property Sekolah $sekolah
 * @property Kelas $kelas
 * @property Semester $semester
 */
class MataPelajaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mata_pelajaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah','nama_mata_pelajaran', 'tahun_ajaran'], 'required'],
            [[ 'id_sekolah', 'id_kelas'], 'integer'],
            [['kkm_pengetahuan', 'kkm_keterampilan'],'number'],
            [['created_at', 'updated_at', 'kelompok'], 'safe'],
            [['nama_mata_pelajaran','jurusan'], 'string', 'max' => 200],
            [['tahun_ajaran'], 'string', 'max' => 15],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mapel' => 'Id Mapel',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'nama_mata_pelajaran' => 'Nama Mata Pelajaran',
            'jurusan' => 'Jurusan',
            'kelompok' => 'Kelompok',
            'kkm_pengetahuan' => 'KKM Pengetahuan',
            'kkm_keterampilan' => 'KKM Keterampilan',
            'tahun_ajaran' => 'Tahun Ajaran',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[KompetensiDasarKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarKeterampilans()
    {
        return $this->hasMany(KompetensiDasarKeterampilan::className(), ['id_mapel' => 'id_mapel']);
    }

    /**
     * Gets query for [[KompetensiDasarPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarPengetahuans()
    {
        return $this->hasMany(KompetensiDasarPengetahuan::className(), ['id_mapel' => 'id_mapel']);
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[Semester]].
     *
     * @return \yii\db\ActiveQuery
     */

}
