<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MataPelajaran;

/**
 * MataPelajaranSearch represents the model behind the search form of `app\models\MataPelajaran`.
 */
class MataPelajaranSearch extends MataPelajaran
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mapel','id_sekolah', 'id_kelas'], 'integer'],
            [['nama_mata_pelajaran', 'tahun_ajaran', 'created_at', 'updated_at','kkm_pengetahuan','kkm_keterampilan','jurusan', 'kelompok'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MataPelajaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mapel' => $this->id_mapel,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'jurusan' =>$this->jurusan,
            'kelompok' => $this->kelompok,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_mata_pelajaran', $this->nama_mata_pelajaran])
         ->andFilterWhere(['like', 'kkm_keterampilan', $this->kkm_keterampilan])
         ->andFilterWhere(['like', 'kkm_pengetahuan', $this->kkm_pengetahuan])
            ->andFilterWhere(['like', 'tahun_ajaran', $this->tahun_ajaran]);

        return $dataProvider;
    }
}
