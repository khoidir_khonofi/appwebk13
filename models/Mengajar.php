<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mengajar".
 *
 * @property int $id_mengajar
 * @property int $id_kelas
 * @property int $id_guru
 * @property int $id_mapel
 * @property int $id_semester
 * @property int $tahun_ajaran
 * @property string $created_at
 * @property string $updated_at
 */
class Mengajar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mengajar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'id_guru', 'id_mapel',    'id_sekolah','id_kelas'], 'required'],
            [['id_kelas', 'id_guru', 'id_mapel','id_sekolah'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mengajar' => 'Id Mengajar',
            'id_kelas' => 'Id Kelas',
            'id_guru' => 'Id Guru',
            'id_mapel' => 'Id Mapel',
            'status' => 'status',
            'id_sekolah' => 'Id Sekolah',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

     public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

     public function getGuru()
    {
        return $this->hasOne(Guru::className(), ['id_guru' => 'id_guru']);
    }

     public function getMatapelajaran()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }

     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }

     public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
