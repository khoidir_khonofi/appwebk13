<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mengajar;

/**
 * MengajarSearch represents the model behind the search form of `app\models\Mengajar`.
 */
class MengajarSearch extends Mengajar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mengajar', 'id_guru', 'id_mapel','id_kelas','id_sekolah'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mengajar::find()->orderBy(['id_mengajar' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mengajar' => $this->id_mengajar,
            'id_kelas' => $this->id_kelas,
            'id_guru' => $this->id_guru,
            'id_mapel' => $this->id_mapel,
            'status' => $this->status,
            'id_sekolah' => $this->id_sekolah,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
