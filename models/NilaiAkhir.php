<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai_akhir".
 *
 * @property int $id_nilai
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property int $id_mapel
 * @property int $id_semester
 * @property int $id_nilai_kd_keterampilan
 * @property int $id_nilai_kd_pengetahuan
 * @property string $predikat
 * @property string $deskripsi
 * @property string $created_at
 * @property string $updated_at
 *
 * @property NilaiKdKeterampilan $nilaiKdKeterampilan
 * @property NilaiKdPengetahuan $nilaiKdPengetahuan
 */
class NilaiAkhir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai_akhir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'id_mapel', 'id_semester', 'id_nilai_kd'], 'required'],
            [['id_sekolah', 'id_siswa', 'id_mapel', 'id_semester', 'id_nilai_kd','id_kelas'], 'integer'],
            [['nilai_pengetahuan', 'nilai_keterampilan'], 'number'],
            [['predikat_pengetahuan','predikat_keterampilan', 'deskripsi_pengetahuan','deskripsi_keterampilan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai' => 'Id Nilai',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'id_semester' => 'Id Semester',
            'id_nilai_kd' => 'Id Nilai Kd ',
            'nilai_keterampilan' => 'Nilai Keterampilan',
            'nilai_pengetahuan' => 'Nilai Pengetahuan',
            'predikat_pengetahuan' => 'Predikat Pengetahuan',
            'predikat_keterampilan' => 'Predikat Keterampilan',
            'deskripsi_pengetahuan' => 'Deskripsi Pengetahuan',
            'deskripsi_keterampilan' => 'Deskripsi Keterampilan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[NilaiKdKeterampilan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNilaikdketerampilan()
    {
        return $this->hasOne(NilaiKdKeterampilan::className(), ['id_nilai_kd' => 'id_nilai_kd_keterampilan']);
    }

    /**
     * Gets query for [[NilaiKdPengetahuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNilaikdpengetahuan()
    {
        return $this->hasOne(NilaiKdPengetahuan::className(), ['id_nilai_kd' => 'id_nilai_kd_pengetahuan']);
    }

     public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
     public function getKdpengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }
     public function getKdketerampilan()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }
}
