<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai_akhir_kkm_satuan_pendidikan".
 *
 * @property int $id_nilai_akhir
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property int $id_kelas
 * @property int $id_semester
 * @property int $id_mapel
 * @property int $id_nilai_kd
 * @property int $nilai_pengetahuan
 * @property int $nilai_keterampilan
 * @property string $predikat_pengetahuan
 * @property string $predikat_keterampilan
 * @property string $deskripsi_pengetahuan
 * @property string $deskripsi_keterampilan
 * @property string $created_at
 * @property string $updated_at
 */
class NilaiAkhirKkmSatuanPendidikan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai_akhir_kkm_satuan_pendidikan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'id_kelas', 'id_semester', 'id_mapel'], 'required'],
            [['id_sekolah', 'id_siswa', 'id_kelas', 'id_semester', 'id_mapel', 'id_nilai_kd', 'nilai_pengetahuan', 'nilai_keterampilan'], 'integer'],
            [['predikat_pengetahuan', 'predikat_keterampilan', 'deskripsi_pengetahuan', 'deskripsi_keterampilan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_akhir' => 'Id Nilai Akhir',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'id_mapel' => 'Id Mapel',
            'id_nilai_kd' => 'Id Nilai Kd',
            'nilai_pengetahuan' => 'Nilai Pengetahuan',
            'nilai_keterampilan' => 'Nilai Keterampilan',
            'predikat_pengetahuan' => 'Predikat Pengetahuan',
            'predikat_keterampilan' => 'Predikat Keterampilan',
            'deskripsi_pengetahuan' => 'Deskripsi Pengetahuan',
            'deskripsi_keterampilan' => 'Deskripsi Keterampilan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKdpengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }
     public function getKdketerampilan()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }
    public function getNilaikdketerampilan()
    {
        return $this->hasOne(NilaiKdKeterampilan::className(), ['id_nilai_kd' => 'id_nilai_kd_keterampilan']);
    }

    /**
     * Gets query for [[NilaiKdPengetahuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNilaikdpengetahuan()
    {
        return $this->hasOne(NilaiKdPengetahuan::className(), ['id_nilai_kd' => 'id_nilai_kd_pengetahuan']);
    }
}
