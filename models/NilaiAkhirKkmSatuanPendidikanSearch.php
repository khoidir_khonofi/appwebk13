<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NilaiAkhirKkmSatuanPendidikan;

/**
 * NilaiAkhirKkmSatuanPendidikanSearch represents the model behind the search form of `app\models\NilaiAkhirKkmSatuanPendidikan`.
 */
class NilaiAkhirKkmSatuanPendidikanSearch extends NilaiAkhirKkmSatuanPendidikan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_akhir', 'id_sekolah', 'id_siswa', 'id_kelas', 'id_semester', 'id_mapel', 'id_nilai_kd', 'nilai_pengetahuan', 'nilai_keterampilan'], 'integer'],
            [['predikat_pengetahuan', 'predikat_keterampilan', 'deskripsi_pengetahuan', 'deskripsi_keterampilan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NilaiAkhirKkmSatuanPendidikan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_akhir' => $this->id_nilai_akhir,
            'id_sekolah' => $this->id_sekolah,
            'id_siswa' => $this->id_siswa,
            'id_kelas' => $this->id_kelas,
            'id_semester' => $this->id_semester,
            'id_mapel' => $this->id_mapel,
            'id_nilai_kd' => $this->id_nilai_kd,
            'nilai_pengetahuan' => $this->nilai_pengetahuan,
            'nilai_keterampilan' => $this->nilai_keterampilan,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'predikat_pengetahuan', $this->predikat_pengetahuan])
            ->andFilterWhere(['like', 'predikat_keterampilan', $this->predikat_keterampilan])
            ->andFilterWhere(['like', 'deskripsi_pengetahuan', $this->deskripsi_pengetahuan])
            ->andFilterWhere(['like', 'deskripsi_keterampilan', $this->deskripsi_keterampilan]);

        return $dataProvider;
    }
}
