<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NilaiKdKeterampilan;

/**
 * NilaiKdKeterampilanSearch represents the model behind the search form of `app\models\NilaiKdKeterampilan`.
 */
class NilaiKdKeterampilanSearch extends NilaiKdKeterampilan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_kd_keterampilan', 'id_nilai_harian_keterampilan', 'id_sekolah', 'id_siswa', 'id_mapel', 'id_kd_keterampilan', 'id_semester','id_kelas'], 'integer'],
            [['skor'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NilaiKdKeterampilan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_kd_keterampilan' => $this->id_nilai_kd_keterampilan,
            'id_nilai_harian_keterampilan' => $this->id_nilai_harian_keterampilan,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_mapel' => $this->id_mapel,
            'id_kd_keterampilan' => $this->id_kd_keterampilan,
            'id_semester' => $this->id_semester,
            'skor' => $this->skor,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
