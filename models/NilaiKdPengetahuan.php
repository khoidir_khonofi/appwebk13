<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai_kd_pengetahuan".
 *
 * @property int $id_nilai_kd_pengetahuan
 * @property int $id_nilai_harian_pengetahuan
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property int $id_mapel
 * @property int $id_semester
 * @property float $nph
 * @property float $npts
 * @property float $npas
 * @property int $nilai_kd
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SdNilaiHarianPengetahuan $nilaiHarianPengetahuan
 */
class NilaiKdPengetahuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai_kd_pengetahuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'id_mapel', 'id_semester', 'id_kd_pengetahuan'], 'required'],
            [['id_nilai_harian_pengetahuan', 'id_sekolah', 'id_siswa', 'id_mapel', 'id_semester','id_kd_pengetahuan','id_kelas'], 'integer'],
            [['nilai_kd','nph','npts','npas'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_kd_pengetahuan' => 'Id Nilai Kd Pengetahuan',
            'id_nilai_harian_pengetahuan' => 'Id Nilai Harian Pengetahuan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_mapel' => 'Id Mapel',
            'id_kd_pengetahuan' => 'Id KD Pengetahuan',
            'id_semester' => 'Id Semester',
            'nph' => 'Nph',
            'npts' => 'Npts',
            'npas' => 'Npas',
            'nilai_kd' => 'Nilai KD',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[NilaiHarianPengetahuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNilaiHarianPengetahuan()
    {
        return $this->hasOne(SdNilaiHarianPengetahuan::className(), ['id_nilai_harian_pengetahuan' => 'id_nilai_harian_pengetahuan']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKdpengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }

}
