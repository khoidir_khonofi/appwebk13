<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai_kd_pengetahuan_akhir_sekolah_menengah".
 *
 * @property int $id_nilai_kd_pengetahuan_akhir
 * @property int $id_nilai_kd_pengetahuan
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_siswa
 * @property int $id_mapel
 * @property int $id_semester
 * @property int $nilai_kd
 * @property int $npts
 * @property int $npas
 * @property string $created_at
 * @property string $updated_at
 */
class NilaiKdPengetahuanAkhirSekolahMenengah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai_kd_pengetahuan_akhir_sekolah_menengah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'nilai_kd'], 'required'],
            [['id_nilai_pengetahuan_sekolah_menengah', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel', 'id_semester', 'nilai_kd', 'npts', 'npas', 'nilai_akhir_kd'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_kd_pengetahuan_akhir' => 'Id Nilai Kd Pengetahuan Akhir',
            'id_nilai_pengetahuan_sekolah_menengah' => 'Id Nilai Kd Pengetahuan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_mapel' => 'Id Mapel',
            'id_semester' => 'Id Semester',
            'nilai_kd' => 'Nilai Kd',
            'npts' => 'Npts',
            'npas' => 'Npas',
            'nilai_akhir_kd' => 'Nilai Akir KD',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getNilaikd()
    {
        return $this->hasOne(NilaiKdPengetahuanSekolahMenengah::className(), ['id_nilai_pengetahuan_sekolah_menengah' => 'id_nilai_pengetahuan_sekolah_menengah']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
   
}
