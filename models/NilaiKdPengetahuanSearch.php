<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NilaiKdPengetahuan;

/**
 * NilaiKdPengetahuanSearch represents the model behind the search form of `app\models\NilaiKdPengetahuan`.
 */
class NilaiKdPengetahuanSearch extends NilaiKdPengetahuan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_kd_pengetahuan', 'id_nilai_harian_pengetahuan', 'id_sekolah', 'id_siswa', 'id_mapel', 'id_semester','id_kd_pengetahuan','id_kelas'], 'integer'],
            [['nilai_kd','nph','npts','npas'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NilaiKdPengetahuan::find()->orderBy(['id_nilai_kd_pengetahuan' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_kd_pengetahuan' => $this->id_nilai_kd_pengetahuan,
            'id_nilai_harian_pengetahuan' => $this->id_nilai_harian_pengetahuan,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_mapel' => $this->id_mapel,
            'id_kd_pengetahuan' => $this->id_kd_pengetahuan,
            'id_semester' => $this->id_semester,
            'nph' => $this->nph,
            'npts' => $this->npts,
            'npas' => $this->npas,
            'nilai_kd' => $this->nilai_kd,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
