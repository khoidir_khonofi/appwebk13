<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai_kd_pengetahuan_sekolah_menengah".
 *
 * @property int $id_nilai_pengetahuan_sekolah_menengah
 * @property int $id_nilai_harian_pengetahuan
 * @property int $id_kd_pengetahuan
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_siswa
 * @property int $id_mapel
 * @property int $id_semester
 * @property int $nph
 * @property string $created_at
 * @property string $updated_at
 */
class NilaiKdPengetahuanSekolahMenengah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai_kd_pengetahuan_sekolah_menengah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_harian_pengetahuan', 'id_kd_pengetahuan', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel', 'id_semester', 'nph'], 'required'],
            [['id_nilai_harian_pengetahuan', 'id_kd_pengetahuan', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel', 'id_semester', 'nph'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_pengetahuan_sekolah_menengah' => 'Id Nilai Pengetahuan Sekolah Menengah',
            'id_nilai_harian_pengetahuan' => 'Id Nilai Harian Pengetahuan',
            'id_kd_pengetahuan' => 'Id Kd Pengetahuan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_mapel' => 'Id Mapel',
            'id_semester' => 'Id Semester',
            'nph' => 'Nph',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getNilaiHarianPengetahuan()
    {
        return $this->hasOne(SmaNilaiHarianPengetahuan::className(), ['id_nilai_harian_pengetahuan' => 'id_nilai_harian_pengetahuan']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKdpengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }
}
