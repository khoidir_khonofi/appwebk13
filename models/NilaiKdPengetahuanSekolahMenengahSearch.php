<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NilaiKdPengetahuanSekolahMenengah;

/**
 * NilaiKdPengetahuanSekolahMenengahSearch represents the model behind the search form of `app\models\NilaiKdPengetahuanSekolahMenengah`.
 */
class NilaiKdPengetahuanSekolahMenengahSearch extends NilaiKdPengetahuanSekolahMenengah
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_pengetahuan_sekolah_menengah', 'id_nilai_harian_pengetahuan', 'id_kd_pengetahuan', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel', 'id_semester', 'nph'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NilaiKdPengetahuanSekolahMenengah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_pengetahuan_sekolah_menengah' => $this->id_nilai_pengetahuan_sekolah_menengah,
            'id_nilai_harian_pengetahuan' => $this->id_nilai_harian_pengetahuan,
            'id_kd_pengetahuan' => $this->id_kd_pengetahuan,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_mapel' => $this->id_mapel,
            'id_semester' => $this->id_semester,
            'nph' => $this->nph,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
