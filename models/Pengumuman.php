<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengumuman".
 *
 * @property int $id_pengumuman
 * @property int $id_sekolah
 * @property string $isi
 * @property string $gambar
 * @property string $created_at
 * @property string $updated_at
 */
class Pengumuman extends \yii\db\ActiveRecord
{
    public $gambarpengumuman;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengumuman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'isi'], 'required'],
            [['id_sekolah','post_by'], 'integer'],
            [['isi'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['gambar'], 'string', 'max' => 200],
            [['gambarpengumuman'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pengumuman' => 'Id Pengumuman',
            'id_sekolah' => 'Id Sekolah',
            'isi' => 'Isi',
            'gambar' => 'Gambar',
            'post_by' => 'Post By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
     public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
}
