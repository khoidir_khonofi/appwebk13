<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestasi".
 *
 * @property int $id_prestasi
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $prestasi
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 */
class Prestasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prestasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'prestasi'], 'required'],
            [['id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['prestasi'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prestasi' => 'Id Prestasi',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'prestasi' => 'Prestasi',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
