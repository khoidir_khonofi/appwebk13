<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rangking".
 *
 * @property int $id_rangking
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_siswa
 * @property int $id_semester
 * @property float $nilai_rata_rata
 * @property int $rangking
 */
class Rangking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rangking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_siswa', 'id_semester'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_siswa', 'id_semester', 'rangking'], 'integer'],
            [['nilai_rata_rata_pengetahuan', 'nilai_rata_rata_keterampilan', 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_rangking' => 'Id Rangking',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'nilai_rata_rata_keterampilan' => 'Nilai Rata2 Keterampilan',
            'nilai_rata_rata_pengetahuan' => 'Nilai rata2 Pengetahuan',
            'total' => 'Total',
            'id_siswa' => 'Id Siswa',
            'id_semester' => 'Id Semester',
            'nilai_rata_rata' => 'Nilai Rata Rata',
            'rangking' => 'Rangking',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
