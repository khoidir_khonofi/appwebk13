<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rangking;

/**
 * RangkingSearch represents the model behind the search form of `app\models\Rangking`.
 */
class RangkingSearch extends Rangking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_rangking', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_semester', 'rangking'], 'integer'],
            [['nilai_rata_rata_pengetahuan', 'nilai_rata_rata_keterampilan','total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rangking::find()->orderBy(['total' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rangking' => $this->id_rangking,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_semester' => $this->id_semester,
            'nilai_rata_rata_pengetahuan' => $this->nilai_rata_rata_pengetahuan,
            'nilai_rata_rata_keterampilan' => $this->nilai_rata_rata_keterampilan,
            'total' => $this->total,
            'rangking' => $this->rangking,
        ]);

        return $dataProvider;
    }
}
