<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rombongan_belajar".
 *
 * @property int $id_rombel
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_semester
 * @property int $tahun_ajaran
 * @property int $id_siswa
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Sekolah $sekolah
 * @property Kelas $kelas
 * @property Siswa $siswa
 * @property Semester $semester
 */
class RombonganBelajar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rombongan_belajar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'id_kelas'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_semester', 'tahun_ajaran', 'id_siswa','id_guru'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            [['id_siswa'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['id_siswa' => 'id_siswa']],
            [['id_semester'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['id_semester' => 'id_semester']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_rombel' => 'Id Rombel',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'tahun_ajaran' => 'Tahun Ajaran',
            'id_guru' => 'Id Guru',
            'id_siswa' => 'Id Siswa',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * Gets query for [[Siswa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }

    /**
     * Gets query for [[Semester]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getGuru()
    {
        return $this->hasOne(Guru::className(), ['id_guru' => 'id_guru']);
    }
}
