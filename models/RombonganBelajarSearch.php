<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RombonganBelajar;

/**
 * RombonganBelajarSearch represents the model behind the search form of `app\models\RombonganBelajar`.
 */
class RombonganBelajarSearch extends RombonganBelajar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_rombel', 'id_sekolah', 'id_kelas', 'id_semester', 'tahun_ajaran', 'id_siswa','id_guru'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RombonganBelajar::find()->orderBy(['id_rombel' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rombel' => $this->id_rombel,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_semester' => $this->id_semester,
            'tahun_ajaran' => $this->tahun_ajaran,
            'id_guru' => $this->id_guru,
            'id_siswa' => $this->id_siswa,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
