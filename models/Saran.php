<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "saran".
 *
 * @property int $id_saran
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $saran
 * @property string $created_at
 * @property string $updated_at
 */
class Saran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'saran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'saran'], 'required'],
            [['id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['saran'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_saran' => 'Id Saran',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'saran' => 'Saran',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
