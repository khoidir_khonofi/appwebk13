<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sd_nilai_harian_keterampilan".
 *
 * @property int $id_nilai_harian_keterampilan
 * @property int $id_kd_keterampilan
 * @property int $id_sekolah
 * @property int $id_semester
 * @property string $tema
 * @property float $nilai
 * @property string $created_at
 * @property string $updated_at
 *
 * @property KompetensiDasarKeterampilan $kdKeterampilan
 * @property SdNilaiKdKeterampilan[] $sdNilaiKdKeterampilans
 */
class SdNilaiHarianKeterampilan extends \yii\db\ActiveRecord
{
    public $nilai_max;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sd_nilai_harian_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd_keterampilan', 'id_sekolah', 'id_semester', 'nilai','id_siswa','jenis_penilaian','id_mapel'], 'required'],
            [['id_kd_keterampilan', 'id_kelas','id_sekolah', 'id_semester','id_siswa','id_mapel','id_tema','created_by'], 'integer'],
            [['nilai'], 'number'],
            [['created_at', 'updated_at', 'jenis_penilaian'], 'safe'],
            [['id_kd_keterampilan'], 'exist', 'skipOnError' => true, 'targetClass' => KompetensiDasarKeterampilan::className(), 'targetAttribute' => ['id_kd_keterampilan' => 'id_kd_keterampilan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_harian_keterampilan' => 'Id Nilai Harian Keterampilan',
            'id_kd_keterampilan' => 'Id Kd Keterampilan',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'id_mapel' => 'Id Mapel',
            'id_siswa' => 'Id Siswa',
            'id_tema' => 'Id Tema',
            'jenis_penilaian' => 'Jenis Penilaian',
            'nilai' => 'Nilai',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[KdKeterampilan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKdKeterampilan()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }

    /**
     * Gets query for [[SdNilaiKdKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiKdKeterampilans()
    {
        return $this->hasMany(SdNilaiKdKeterampilan::className(), ['id_nilai_harian_keterampilan' => 'id_nilai_harian_keterampilan']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
