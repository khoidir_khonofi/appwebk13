<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sd_nilai_harian_pengetahuan".
 *
 * @property int $id_nilai_harian_pengetahuan
 * @property int $id_kd_pengetahuan
 * @property int $id_semester
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $tema
 * @property float $nilai
 * @property string $created_at
 * @property string $updated_at
 *
 * @property KompetensiDasarPengetahuan $kdPengetahuan
 * @property SdNilaiKdPengetahuan[] $sdNilaiKdPengetahuans
 */
class SdNilaiHarianPengetahuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sd_nilai_harian_pengetahuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd_pengetahuan', 'id_semester', 'id_sekolah', 'id_mapel','id_siswa', 'nilai'], 'required'],
            [['id_kd_pengetahuan', 'id_semester', 'id_sekolah', 'id_siswa', 'id_mapel','id_tema','id_subtema','id_kelas'], 'integer'],
            [['nama_penilaian'], 'string', 'max' => 200],
            [['nilai'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_harian_pengetahuan' => 'Id Nilai Harian Pengetahuan',
            'id_kd_pengetahuan' => 'Id Kd Pengetahuan',
            'id_semester' => 'Id Semester',
            'id_mapel' => 'Id Mapel',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_tema' => 'Id Tema',
            'id_subtema' => 'Id Subtema',
            'nama_penilaian' => 'Nama Penilaian',
            'nilai' => 'Nilai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[KdPengetahuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKdPengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }

    /**
     * Gets query for [[SdNilaiKdPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiKdPengetahuans()
    {
        return $this->hasMany(SdNilaiKdPengetahuan::className(), ['id_nilai_harian_pengetahuan' => 'id_nilai_harian_pengetahuan']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
      public function getTema()
    {
        return $this->hasOne(Tema::className(), ['id_tema' => 'id_tema']);
    }
      public function getSubtema()
    {
        return $this->hasOne(Subtema::className(), ['id_subtema' => 'id_subtema']);
    }
     public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
