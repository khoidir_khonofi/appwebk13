<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sekolah".
 *
 * @property int $id_sekolah
 * @property string $nama_sekolah
 * @property string $nama_kepala_sekolah
 * @property string $jenjang_pendidikan
 * @property string $alamat
 * @property string $kelurahan
 * @property string $kecamatan
 * @property string $kabupaten/kota
 * @property string $provinsi
 * @property string $telepon
 */
class Sekolah extends \yii\db\ActiveRecord
{
    public $fotosekolah;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sekolah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['jenjang_pendidikan', 'alamat', 'kelurahan'], 'string'],
            [['id_user'], 'integer'],
            [['nama_sekolah', 'nama_kepala_sekolah','foto', 'kecamatan','npsn', 'kabupaten', 'provinsi','nip_kepsek'], 'string', 'max' => 200],
            [['telepon'], 'string', 'max' => 20],
            [['fotosekolah'], 'file','skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_sekolah' => 'Id Sekolah',
            'id_user' => 'Id User',
            'nama_sekolah' => 'Nama Sekolah',
            'nama_kepala_sekolah' => 'Nama Kepala Sekolah',
            'nip_kepsek' => 'Nip Kepsek',
            'jenjang_pendidikan' => 'Jenjang Pendidikan',
            'npsn' => 'NPSN',
            'alamat' => 'Alamat',
            'foto' => 'Foto',
            'kelurahan' => 'Kelurahan',
            'kecamatan' => 'Kecamatan',
            'kabupaten' => 'Kabupaten',
            'provinsi' => 'Provinsi',
            'telepon' => 'Telepon',
        ];
    }
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
