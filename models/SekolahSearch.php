<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sekolah;

/**
 * SekolahSearch represents the model behind the search form of `app\models\Sekolah`.
 */
class SekolahSearch extends Sekolah
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah','id_user'], 'integer'],
            [['nama_sekolah', 'nama_kepala_sekolah', 'jenjang_pendidikan', 'npsn','alamat', 'kelurahan', 'kecamatan', 'kabupaten', 'provinsi', 'telepon','foto','nip_kepsek'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sekolah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_sekolah' => $this->id_sekolah,
        ]);

        $query->andFilterWhere(['like', 'nama_sekolah', $this->nama_sekolah])
            ->andFilterWhere(['like', 'nama_kepala_sekolah', $this->nama_kepala_sekolah])
            ->andFilterWhere(['like', 'nip_kepsek', $this->nip_kepsek])
            ->andFilterWhere(['like', 'jenjang_pendidikan', $this->jenjang_pendidikan])
            ->andFilterWhere(['like', 'npsn', $this->npsn])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'id_user', $this->id_user])  
            ->andFilterWhere(['like', 'kelurahan', $this->kelurahan])
            ->andFilterWhere(['like', 'kecamatan', $this->kecamatan])
            ->andFilterWhere(['like', 'kabupaten/kota', $this->kabupaten])
            ->andFilterWhere(['like', 'provinsi', $this->provinsi])
            ->andFilterWhere(['like', 'telepon', $this->telepon]);

        return $dataProvider;
    }
}
