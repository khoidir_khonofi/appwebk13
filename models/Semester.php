<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester".
 *
 * @property int $id_semester
 * @property int $id_sekolah
 * @property string $tahun_ajaran
 * @property int $semester
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Kelas[] $kelas
 * @property KompetensiDasarKeterampilan[] $kompetensiDasarKeterampilans
 * @property KompetensiDasarPengetahuan[] $kompetensiDasarPengetahuans
 * @property MataPelajaran[] $mataPelajarans
 * @property Sekolah $sekolah
 */
class Semester extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semester';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'tahun_ajaran', 'semester', 'tanggal_mulai', 'tanggal_selesai','periode_aktif'], 'required'],
            [['id_sekolah', 'semester'], 'integer'],
            [['tanggal_mulai', 'tanggal_selesai', 'created_at', 'updated_at','periode_aktif'], 'safe'],
            [['tahun_ajaran'], 'string', 'max' => 15],
            [['id_sekolah'], 'exist', 'skipOnError' => true, 'targetClass' => Sekolah::className(), 'targetAttribute' => ['id_sekolah' => 'id_sekolah']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_semester' => 'Id Semester',
            'id_sekolah' => 'Id Sekolah',
            'tahun_ajaran' => 'Tahun Ajaran',
            'semester' => 'Semester',
            'periode_aktif' => 'Periode Aktif',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[KompetensiDasarKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarKeterampilans()
    {
        return $this->hasMany(KompetensiDasarKeterampilan::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[KompetensiDasarPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKompetensiDasarPengetahuans()
    {
        return $this->hasMany(KompetensiDasarPengetahuan::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[MataPelajarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMataPelajarans()
    {
        return $this->hasMany(MataPelajaran::className(), ['id_semester' => 'id_semester']);
    }

    /**
     * Gets query for [[Sekolah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
}
