<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sikap_akhir".
 *
 * @property int $id_sikap
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property int $id_sikap_sosial
 * @property int $id_sikap_spiritual
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 */
class SikapAkhir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sikap_akhir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa'], 'required'],
            [['id_sekolah', 'id_siswa', 'id_sikap_sosial', 'id_sikap_spiritual', 'id_semester', 'id_kelas'], 'integer'],
            [['keterangan_sosial', 'keterangan_spiritual'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_sikap' => 'Id Sikap',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_semester' => 'Id Semester',
            'id_sikap_sosial' => 'Id Sikap Sosial',
            'id_sikap_spiritual' => 'Id Sikap Spiritual',
            'keterangan_sosial' => 'Keterangan Sosial',
            'keterangan_spiritual' => 'Keterangan Spiritual',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getSosial()
    {
        return $this->hasOne(SikapSosial::className(), ['id_sikap_sosial' => 'id_sikap_sosial']);
    }
    public function getSpiritual()
    {
        return $this->hasOne(SikapSpiritual::className(), ['id_sikap_spiritual' => 'id_sikap_spiritual']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
