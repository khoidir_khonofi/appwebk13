<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SikapAkhir;

/**
 * SikapAkhirSearch represents the model behind the search form of `app\models\SikapAkhir`.
 */
class SikapAkhirSearch extends SikapAkhir
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sikap', 'id_sekolah', 'id_siswa', 'id_sikap_sosial', 'id_sikap_spiritual','id_semester'], 'integer'],
            [['keterangan_sosial', 'keterangan_spiritual', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SikapAkhir::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_sikap' => $this->id_sikap,
            'id_sekolah' => $this->id_sekolah,
            'id_siswa' => $this->id_siswa,
            'id_semester' => $this->id_semester,
            'id_sikap_sosial' => $this->id_sikap_sosial,
            'id_sikap_spiritual' => $this->id_sikap_spiritual,
            'keterangan_spiritual' => $this->keterangan_spiritual,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'keterangan_sosial', $this->keterangan_sosial]);

        return $dataProvider;
    }
}
