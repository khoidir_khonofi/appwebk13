<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sikap_spiritual".
 *
 * @property int $id_sikap_spiritual
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $butir_sikap
 * @property int $nilai
 * @property string $created_at
 * @property string $updated_at
 */
class SikapSpiritual extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sikap_spiritual';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'butir_sikap', 'nilai'], 'required'],
            [['id_sekolah', 'id_siswa', 'nilai', 'id_semester', 'id_kelas'], 'integer'],
            [['butir_sikap'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_sikap_spiritual' => 'Id Sikap Spiritual',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'butir_sikap' => 'Butir Sikap',
            'nilai' => 'Nilai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
