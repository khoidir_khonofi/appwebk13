<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siswa".
 *
 * @property int $id_siswa
 * @property int $id_sekolah
 * @property int $id_guru
 * @property string $nis
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $agama
 * @property string $alamat
 * @property string $nama_ayah
 * @property string $nama_ibu
 * @property string $pekerjaan_ayah
 * @property string $pekerjaan_ibu
 * @property string $created_at
 * @property string $updated_at
 */
class Siswa extends \yii\db\ActiveRecord
{
    public $fotosiswa;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_sekolah', 'id_guru','id_user', 'id_kelas', 'id_semester'], 'integer'],
            [['tanggal_lahir', 'created_at', 'updated_at','status'], 'safe'],
            [['jenis_kelamin', 'agama', 'alamat'], 'string'],
            [['nis', 'nama', 'tempat_lahir','foto','nama_ayah', 'nama_ibu', 'pekerjaan_ayah', 'pekerjaan_ibu','no_hp_orang_tua', 'chat_id_telegram'], 'string', 'max' => 200],
            [['fotosiswa'],'file','skipOnEmpty' => true, 'extensions' => 'gif, png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_siswa' => 'Id Siswa',
            'id_user' => 'Id User',
            'id_sekolah' => 'Id Sekolah',
            'id_guru' => 'Id Guru',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'nis' => 'Nis',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'agama' => 'Agama',
            'foto' => 'Foto',
            'alamat' => 'Alamat',
            'nama_ayah' => 'Nama Ayah',
            'nama_ibu' => 'Nama Ibu',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'no_hp_orang_tua' => 'No Hp Orang Tua',
            'chat_id_telegram' => 'Chat ID Telegram',
            'status'  => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

     public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
    public function getTblsekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
    public function getTblguru()
    {
        return $this->hasOne(Guru::className(), ['id_guru' => 'id_guru']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
