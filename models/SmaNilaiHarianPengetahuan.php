<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sma_nilai_harian_pengetahuan".
 *
 * @property int $id_nilai_harian_pengetahuan
 * @property int $id_kd_pengetahuan
 * @property int $id_semester
 * @property int $id_sekolah
 * @property string $nama
 * @property float $nilai
 * @property string $created_at
 * @property string $updated_at
 *
 * @property KompetensiDasarPengetahuan $kdPengetahuan
 */
class SmaNilaiHarianPengetahuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sma_nilai_harian_pengetahuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd_pengetahuan', 'id_semester', 'id_sekolah', 'nama_penilaian', 'nilai'], 'required'],
            [['id_kd_pengetahuan', 'id_semester', 'id_sekolah','id_siswa','id_kelas','id_mapel'], 'integer'],
            [['nilai'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_penilaian'], 'string', 'max' => 200],
            [['id_kd_pengetahuan'], 'exist', 'skipOnError' => true, 'targetClass' => KompetensiDasarPengetahuan::className(), 'targetAttribute' => ['id_kd_pengetahuan' => 'id_kd_pengetahuan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_harian_pengetahuan' => 'Id Nilai Harian Pengetahuan',
            'id_kd_pengetahuan' => 'Id Kd Pengetahuan',
            'id_semester' => 'Id Semester',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_mapel' => 'Id Mapel',
            'nama_penilaian' => 'Nama',
            'nilai' => 'Nilai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[KdPengetahuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKdPengetahuan()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd_pengetahuan']);
    }
     public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
}
