<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "smp_nilai_harian_keterampilan".
 *
 * @property int $id_nilai_harian_keterampilan
 * @property int $id_kd_keterampilan
 * @property int $id_semester
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_siswa
 * @property int $id_mapel
 * @property string $nama_penilaian
 * @property float $nilai
 * @property string $created_at
 * @property string $updated_at
 *
 * @property NilaiKdKeterampilan[] $nilaiKdKeterampilans
 * @property KompetensiDasarKeterampilan $kdKeterampilan
 * @property SmpNilaiKdKeterampilan[] $smpNilaiKdKeterampilans
 */
class SmpNilaiHarianKeterampilan extends \yii\db\ActiveRecord
{
    public $nilai_max;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'smp_nilai_harian_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kd_keterampilan', 'id_semester', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel', 'nama_penilaian', 'nilai'], 'required'],
            [['id_kd_keterampilan', 'id_semester', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel'], 'integer'],
            [['nilai'], 'number'],
            [['created_at', 'updated_at', 'nama_penilaian'], 'safe'],
            [['id_kd_keterampilan'], 'exist', 'skipOnError' => true, 'targetClass' => KompetensiDasarKeterampilan::className(), 'targetAttribute' => ['id_kd_keterampilan' => 'id_kd_keterampilan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nilai_harian_keterampilan' => 'Id Nilai Harian Keterampilan',
            'id_kd_keterampilan' => 'Id Kd Keterampilan',
            'id_semester' => 'Id Semester',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_siswa' => 'Id Siswa',
            'id_mapel' => 'Id Mapel',
            'nama_penilaian' => 'Nama Penilaian',
            'nilai' => 'Nilai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[NilaiKdKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNilaiKdKeterampilans()
    {
        return $this->hasMany(NilaiKdKeterampilan::className(), ['id_nilai_harian_keterampilan' => 'id_nilai_harian_keterampilan']);
    }

    /**
     * Gets query for [[KdKeterampilan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKdKeterampilan()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd_keterampilan']);
    }

    /**
     * Gets query for [[SmpNilaiKdKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmpNilaiKdKeterampilans()
    {
        return $this->hasMany(SmpNilaiKdKeterampilan::className(), ['id_nilai_harian_keterampilan' => 'id_nilai_harian_keterampilan']);
    }

     
     public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }

    public function getTeknik()
    {
        return $this->hasOne(TeknikPenilaianKeterampilan::className(), ['id' => 'jenis_penilaian']);
    }
    public function getTemaktr()
    {
        return $this->hasOne(TemaKeterampilan::className(), ['id_tema' => 'id_tema']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
}
