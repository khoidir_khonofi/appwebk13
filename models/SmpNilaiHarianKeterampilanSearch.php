<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmpNilaiHarianKeterampilan;

/**
 * SmpNilaiHarianKeterampilanSearch represents the model behind the search form of `app\models\SmpNilaiHarianKeterampilan`.
 */
class SmpNilaiHarianKeterampilanSearch extends SmpNilaiHarianKeterampilan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_harian_keterampilan', 'id_kd_keterampilan', 'id_semester', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel'], 'integer'],
            [['created_at', 'updated_at', 'nama_penilaian'], 'safe'],
            [['nilai'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmpNilaiHarianKeterampilan::find()->orderBy(['id_nilai_harian_keterampilan' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_harian_keterampilan' => $this->id_nilai_harian_keterampilan,
            'id_kd_keterampilan' => $this->id_kd_keterampilan,
            'id_semester' => $this->id_semester,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_mapel' => $this->id_mapel,
            'nilai' => $this->nilai,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_penilaian', $this->nama_penilaian]);

        return $dataProvider;
    }
}
