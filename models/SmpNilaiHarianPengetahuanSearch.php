<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmpNilaiHarianPengetahuan;

/**
 * SmpNilaiHarianPengetahuanSearch represents the model behind the search form of `app\models\SmpNilaiHarianPengetahuan`.
 */
class SmpNilaiHarianPengetahuanSearch extends SmpNilaiHarianPengetahuan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nilai_harian_pengetahuan', 'id_kd_pengetahuan', 'id_semester', 'id_sekolah', 'id_kelas', 'id_siswa', 'id_mapel'], 'integer'],
            [['nama_penilaian', 'created_at', 'updated_at'], 'safe'],
            [['nilai'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmpNilaiHarianPengetahuan::find()->orderBy(['id_nilai_harian_pengetahuan' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai_harian_pengetahuan' => $this->id_nilai_harian_pengetahuan,
            'id_kd_pengetahuan' => $this->id_kd_pengetahuan,
            'id_semester' => $this->id_semester,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_siswa' => $this->id_siswa,
            'id_mapel' => $this->id_mapel,
            'nilai' => $this->nilai,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_penilaian', $this->nama_penilaian]);

        return $dataProvider;
    }
}
