<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subtema".
 *
 * @property int $id_subtema
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_mapel
 * @property int $id_kd
 * @property int $id_tema
 * @property string $subtema
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SdNilaiHarianPengetahuan[] $sdNilaiHarianPengetahuans
 * @property Tema $tema
 */
class Subtema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subtema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'id_tema', 'subtema'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'id_tema'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subtema'], 'string', 'max' => 200],
            [['id_tema'], 'exist', 'skipOnError' => true, 'targetClass' => Tema::className(), 'targetAttribute' => ['id_tema' => 'id_tema']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_subtema' => 'Id Subtema',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'id_kd' => 'Id Kd',
            'id_tema' => 'Id Tema',
            'subtema' => 'Subtema',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[SdNilaiHarianPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianPengetahuans()
    {
        return $this->hasMany(SdNilaiHarianPengetahuan::className(), ['id_subtema' => 'id_subtema']);
    }

    /**
     * Gets query for [[Tema]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTema()
    {
        return $this->hasOne(Tema::className(), ['id_tema' => 'id_tema']);
    }
     public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getKd()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd']);
    }
}
