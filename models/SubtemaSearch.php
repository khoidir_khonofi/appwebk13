<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subtema;

/**
 * SubtemaSearch represents the model behind the search form of `app\models\Subtema`.
 */
class SubtemaSearch extends Subtema
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_subtema', 'id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'id_tema'], 'integer'],
            [['subtema', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subtema::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_subtema' => $this->id_subtema,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_mapel' => $this->id_mapel,
            'id_kd' => $this->id_kd,
            'id_tema' => $this->id_tema,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'subtema', $this->subtema]);

        return $dataProvider;
    }
}
