<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "teknik_penilaian_keterampilan".
 *
 * @property int $id
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_mapel
 * @property int $id_kd
 * @property string $jenis_penilaian
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SdNilaiHarianKeterampilan[] $sdNilaiHarianKeterampilans
 */
class TeknikPenilaianKeterampilan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teknik_penilaian_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'jenis_penilaian','id_tema'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd','id_tema'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['jenis_penilaian'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'id_kd' => 'Id Kd',
            'id_tema' => 'Id Tema',
            'jenis_penilaian' => 'Jenis Penilaian',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[SdNilaiHarianKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianKeterampilans()
    {
        return $this->hasMany(SdNilaiHarianKeterampilan::className(), ['jenis_penilaian' => 'id']);
    }
    public function getTemaktr()
    {
        return $this->hasOne(TemaKeterampilan::className(), ['id_tema' => 'id_tema']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getKd()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd']);
    }
}
