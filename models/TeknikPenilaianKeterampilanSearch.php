<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeknikPenilaianKeterampilan;

/**
 * TeknikPenilaianKeterampilanSearch represents the model behind the search form of `app\models\TeknikPenilaianKeterampilan`.
 */
class TeknikPenilaianKeterampilanSearch extends TeknikPenilaianKeterampilan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_sekolah', 'id_kelas', 'id_mapel', 'id_kd','id_tema'], 'integer'],
            [['jenis_penilaian', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeknikPenilaianKeterampilan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_sekolah' => $this->id_sekolah,
            'id_kelas' => $this->id_kelas,
            'id_mapel' => $this->id_mapel,
            'id_kd' => $this->id_kd,
            'id_tema' => $this->id_tema,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'jenis_penilaian', $this->jenis_penilaian]);

        return $dataProvider;
    }
}
