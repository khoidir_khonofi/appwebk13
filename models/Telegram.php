<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram".
 *
 * @property int $id_telegram
 * @property int $id_sekolah
 * @property string $api_telegram
 * @property string $created_at
 * @property string $updated_at
 */
class Telegram extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telegram';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'api_telegram'], 'required'],
            [['id_telegram', 'id_sekolah'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['api_telegram'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_telegram' => 'Id Telegram',
            'id_sekolah' => 'Id Sekolah',
            'api_telegram' => 'Api Telegram',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
     public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_sekolah' => 'id_sekolah']);
    }
}
