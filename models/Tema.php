<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tema".
 *
 * @property int $id_tema
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_mapel
 * @property int $id_kd
 * @property string $tema
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SdNilaiHarianPengetahuan[] $sdNilaiHarianPengetahuans
 * @property Subtema[] $subtemas
 */
class Tema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'tema'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd'], 'integer'],
            [['created_at', 'updated_at', 'judul'], 'safe'],
            [['tema'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tema' => 'Id Tema',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'id_kd' => 'Id Kd',
            'tema' => 'Tema',
            'judul' => 'Judul',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[SdNilaiHarianPengetahuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianPengetahuans()
    {
        return $this->hasMany(SdNilaiHarianPengetahuan::className(), ['id_tema' => 'id_tema']);
    }

    /**
     * Gets query for [[Subtemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubtemas()
    {
        return $this->hasMany(Subtema::className(), ['id_tema' => 'id_tema']);
    }
     public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
     public function getKd()
    {
        return $this->hasOne(KompetensiDasarPengetahuan::className(), ['id_kd_pengetahuan' => 'id_kd']);
    }

}
