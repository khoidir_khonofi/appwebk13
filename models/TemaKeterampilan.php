<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tema_keterampilan".
 *
 * @property int $id_tema
 * @property int $id_sekolah
 * @property int $id_kelas
 * @property int $id_mapel
 * @property int $id_kd
 * @property string $tema
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SdNilaiHarianKeterampilan[] $sdNilaiHarianKeterampilans
 * @property KompetensiDasarKeterampilan $kd
 */
class TemaKeterampilan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tema_keterampilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd', 'tema'], 'required'],
            [['id_sekolah', 'id_kelas', 'id_mapel', 'id_kd'], 'integer'],
            [['created_at', 'updated_at', 'judul'], 'safe'],
            [['tema'], 'string', 'max' => 200],
            [['id_kd'], 'exist', 'skipOnError' => true, 'targetClass' => KompetensiDasarKeterampilan::className(), 'targetAttribute' => ['id_kd' => 'id_kd_keterampilan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tema' => 'Id Tema',
            'id_sekolah' => 'Id Sekolah',
            'id_kelas' => 'Id Kelas',
            'id_mapel' => 'Id Mapel',
            'id_kd' => 'Id Kd',
            'tema' => 'Tema',
            'judul' => 'Judul',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[SdNilaiHarianKeterampilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSdNilaiHarianKeterampilans()
    {
        return $this->hasMany(SdNilaiHarianKeterampilan::className(), ['id_tema' => 'id_tema']);
    }

    /**
     * Gets query for [[Kd]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKd()
    {
        return $this->hasOne(KompetensiDasarKeterampilan::className(), ['id_kd_keterampilan' => 'id_kd']);
    }
      public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
     public function getMapel()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mapel' => 'id_mapel']);
    }
    
}
