<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tinggi_badan".
 *
 * @property int $id_tinggi_badan
 * @property int $id_sekolah
 * @property int $id_siswa
 * @property string $aspek_penilaian
 * @property string $created_at
 * @property string $updated_at
 */
class TinggiBadan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tinggi_badan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sekolah', 'id_siswa', 'aspek_penilaian'], 'required'],
            [['id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['created_at', 'updated_at','aspek_penilaian'], 'safe'],
            [['nilai'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tinggi_badan' => 'Id Tinggi Badan',
            'id_sekolah' => 'Id Sekolah',
            'id_siswa' => 'Id Siswa',
            'id_kelas' => 'Id Kelas',
            'id_semester' => 'Id Semester',
            'aspek_penilaian' => 'Aspek Penilaian',
            'nilai' => 'Nilai',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_siswa' => 'id_siswa']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['id_semester' => 'id_semester']);
    }
}
