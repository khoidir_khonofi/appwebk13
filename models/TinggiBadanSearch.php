<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TinggiBadan;

/**
 * TinggiBadanSearch represents the model behind the search form of `app\models\TinggiBadan`.
 */
class TinggiBadanSearch extends TinggiBadan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tinggi_badan', 'id_sekolah', 'id_siswa','id_kelas', 'id_semester'], 'integer'],
            [['aspek_penilaian', 'created_at', 'updated_at','nilai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TinggiBadan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tinggi_badan' => $this->id_tinggi_badan,
            'id_sekolah' => $this->id_sekolah,
            'id_siswa' => $this->id_siswa,
            'id_kelas' => $this->id_kelas,
            'id_semester' => $this->id_semester,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'nilai' => $this->nilai,
        ]);

        $query->andFilterWhere(['like', 'aspek_penilaian', $this->aspek_penilaian]);

        return $dataProvider;
    }
}
