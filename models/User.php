<?php 

namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\User; //mendifinisikan model class User yang telah kita generate tadi.
use Yii;

use yii\base\NotSupportedException;

use yii\behaviors\TimestampBehavior;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    // public $id;
    // public $username;
    // public $password;
    // public $authKey;
    // public $accessToken;
    // public $role;

    public static function tableName()
     { 
        return 'user'; 
    }



    public static function findIdentity($id)
    {
        //mencari user User berdasarkan IDnya dan hanya dicari 1.
        return static::findOne(['id_user' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        //mencari user User berdasarkan accessToken dan hanya dicari 1.
        $user = User::find()->where(['accessToken'=>$token])->one(); 
        if(count($user)){
            return new static($user);
        }
        return null;
    }

    public static function findByUsername($username)
    {
        //mencari user User berdasarkan username dan hanya dicari 1.
           return static::find()->where(['username' => $username])->ANDWHERE(['status' => 'Aktif'])->one();
    }

    public static function findByEmail($email)
    {
        //mencari user User berdasarkan username dan hanya dicari 1.
        $user = User::find()->where(['email'=>$email])->one(); 
        if(count($user)){
            return new static($user);
        }
        return null;
    }

    public function getId()
    {
        return $this->id_user;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
        //return Yii::$app->security->validatePassword($password, $this->password);
    }
}