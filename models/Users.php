<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id_user
 * @property string $username
 * @property string $password
 * @property string $role
 * @property string $authKey
 * @property string $accessToken
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'role'], 'required'],
            [['role'], 'string'],
            [['created_at', 'updated_at','status'], 'safe'],
            [['username', 'password', 'authKey', 'accessToken','email'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'role' => 'Mendaftar Sebagai',
            'status' => 'Status',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getGuru()
    {
        return $this->hasOne(Guru::className(), ['id_user' => 'id_user']);
    }

    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id_user' => 'id_user']);
    }
    public function getSekolah()
    {
        return $this->hasOne(Sekolah::className(), ['id_user' => 'id_user']);
    }
}
