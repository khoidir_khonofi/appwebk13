<?php

namespace app\modules\api\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\User;
use app\models\Guru;
use app\models\MataPelajaran;
use app\models\Mengajar;
use app\models\Kelas;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\SmaNilaiHarianPengetahuan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\SmaNilaiHarianKeterampilan;
use app\models\SmpNilaiHarianKeterampilan;
/**
 * Default controller for the `api` module
 */
class MengajarController extends ActiveController
{
	public $modelClass = 'app\models\Mengajar';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'update' => ['GET'],
               	'testing' => ['POST']
            ],
    	];
    	return $behavior;
    }

	public function actionIndex($token)
	{
        
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = Mengajar::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $model = Mengajar::find()->where(['id_guru' => $guru->id_guru])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = Mengajar::find()->where(['id_kelas' => $siswa->id_kelas])->all();
        }
		
        foreach ($model as $key => $value) {
            $tmp[] = [
                'id_mengajar' => $value->id_mengajar,
                'id_kelas' => $value->id_kelas,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'nama_guru' => $value->guru->nama,
                'nama_mata_pelajaran' => $value->matapelajaran->nama_mata_pelajaran,
                'foto' => $value->guru->foto,
                'status' => $value->status,
            ];
        }
       
		return $tmp;
	}

    public function actionKelas($id)
    {
        $model = Siswa::find()->where(['id_kelas' => $id, 'status' => 'Aktif'])->all();
        $temp = [];
        foreach ($model as $key => $value) {
           $temp[] = [
                'kelas' => [
                    'id' => $value->id_kelas,
                    'nama' => $value->kelas->tingkat_kelas ?? "",
                ],
                'siswa' => [
                    'id' => $value->id_siswa,
                    'nama' => $value->nama,
                    'nis' => $value->nis,
                    'foto' => 'http://192.168.43.13/tugasakhir/web/foto_siswa/'.$value->foto,
                ],
           ];
        }
        return $temp;
    }

    public function actionNilaiharianmengajarsd($id)
    {

        $model = SdNilaiHarianPengetahuan::find()->where(['id_kelas' => $id])->all();
        
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_pengetahuan' => $value->id_nilai_harian_pengetahuan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdPengetahuan->no_kd,
                // 'tema' => $value->tema->tema ? : 0,
                // 'subtema' => $value->subtema->subtema ? $value->subtema->subtema :'',
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nama_penilaian' => $value->nama_penilaian,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

     public function actionNilaiharianmengajarsmp($id)
     {
        $model = SmpNilaiHarianPengetahuan::find()->where(['id_kelas' => $id])->all();
        
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_pengetahuan' => $value->id_nilai_harian_pengetahuan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdPengetahuan->no_kd,
                'nama_penilaian' => $value->nama_penilaian,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
     }

     public function actionNilaiharianmengajarsma($id)
     {
        $model = SmaNilaiHarianPengetahuan::find()->where(['id_kelas' => $id])->all();
        
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_pengetahuan' => $value->id_nilai_harian_pengetahuan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdPengetahuan->no_kd,
                'nama_penilaian' => $value->nama_penilaian,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
     }

    public function actionNilaiharianketerampilanmengajarsd($id)
    {
        $model = SdNilaiHarianKeterampilan::find()->where(['id_kelas' => $id])->all();

        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_keterampilan' => $value->id_nilai_harian_keterampilan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdKeterampilan->no_kd,
                // 'tema' => $value->temaktr->tema ? $value->temaktr->tema : '',
                // 'jenis_penilaian' => $value->jenis_penilaian ? $value->jenis_penilaian : '',
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionNilaiharianketerampilanmengajarsmp($id)
    {
        $model = SmpNilaiHarianKeterampilan::find()->where(['id_kelas' => $id])->all();
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_keterampilan' => $value->id_nilai_harian_keterampilan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdKeterampilan->no_kd,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nama_penilaian' => $value->nama_penilaian,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionNilaiharianketerampilanmengajarsma($id)
    {
        $model = SmaNilaiHarianKeterampilan::find()->where(['id_kelas' => $id])->all();

        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_keterampilan' => $value->id_nilai_harian_keterampilan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdKeterampilan->no_kd,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nama_penilaian' => $value->nama_penilaian,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionDelete($id)
    {
        $model = Mengajar::find()->where(['id_mengajar' => $id])->one();
        if ($model->delete()) {
            return "Berhasil";
        }else{
            return "Gagal";
        }
    }

    public function actionSelectkelas($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
        $kelas  = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        return $kelas;
    }
    public function actionSelectmapel()
    {
        $model  = MataPelajaran::find()->all();
        return $model;
    }

    public function actionSelectguru()
    {
        $model  = Guru::find()->all();
        return $model;
    }

    public function actionAktif($id)
    {
        $model = Mengajar::find()->where(['id_mengajar' => $id])->one();
        $model->status = 'Aktif';
        if ($model->save()) {
            return "Sukses";
        }
            return "Gagal";
    }
    public function actionNonaktif($id)
    {
        $model = Mengajar::find()->where(['id_mengajar' => $id])->one();
        $model->status = 'Tidak Aktif';
        if ($model->save()) {
            return "Sukses";
        }
            return "Gagal";
    }
}