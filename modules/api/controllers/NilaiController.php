<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdKeterampilan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\BobotPenilaian;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TemaKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\KompetensiDasarPengetahuan;
use app\models\Sekolah;
use app\models\NilaiAkhir;
use app\models\Tema;
use app\models\Subtema;
use app\models\User;
use app\models\Mengajar;
use app\models\Kelas;
use app\models\Guru;
use app\models\MataPelajaran;

/**
 * Default controller for the `api` module
 */
class NilaiController extends ActiveController
{
	public $modelClass = 'app\models\SdNilaiHarianPengetahuan';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        unset($actions['sdinputnilaipengetahuan']);
        unset($actions['mapel']);
        unset($actions['tema']);
        unset($actions['subtema']);
        unset($actions['kade']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'sdinputnilaipengetahuan' => ['POST'],
            ],
    	];
    	return $behavior;
    }

    public function actionIndex()
    {
        $model = Siswa::find()->all();
        
        return $model;
    }

      public function actionIndexpengetahuan($token)
    {
       $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SdNilaiHarianPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = SdNilaiHarianPengetahuan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = SdNilaiHarianPengetahuan::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               return "Gagal";
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SdNilaiHarianPengetahuan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_pengetahuan' => $value->id_nilai_harian_pengetahuan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdPengetahuan->no_kd,
                // 'tema' => $value->tema->tema,
                // 'subtema' => $value->subtema->subtema,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nama_penilaian' => $value->nama_penilaian,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionIndexketerampilan($token)
    {
         $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SdNilaiHarianKeterampilan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = SdNilaiHarianKeterampilan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = SdNilaiHarianKeterampilan::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               return "Gagal";
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SdNilaiHarianKeterampilan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_keterampilan' => $value->id_nilai_harian_keterampilan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdKeterampilan->no_kd,
                // 'tema' => $value->temaktr->tema,
                // 'jenis_penilaian' => $value->jenis_penilaian,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionIndexnilaisdpengetahuan()
    {
        $model = SdNilaiHarianPengetahuan::find()->all();
        return $model;
    }

    public function actionMapel($id)
    {
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        $model = MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all();
        $arr = [];
        foreach ($model as $item) {
            
            $arr[] = $item->kelas->tingkat_kelas;
        }
        
        return [

                'datamapel' => $model, 
                'Tingkatkelas' => $arr
            ];
    }

    public function actionKade()
    {
        $model = KompetensiDasarPengetahuan::find()->all();
        
        return $model;
    }

    public function actionKadeketerampilan()
    {
        $model = KompetensiDasarKeterampilan::find()->all();

        return $model;
    }

    public function actionTema()
    {
        $model = Tema::find()->all();
        
        return $model;
    }

    public function actionTemaketerampilan()
    {
        $model = TemaKeterampilan::find()->all();
        
        return $model;
    }

    public function actionSubtemaketerampilan()
    {
        $model = TeknikPenilaianKeterampilan::find()->all();
        
        return $model;
    }

    public function actionSubtema()
    {
        $model = Subtema::find()->all();
        
        return $model;
    }


    public function actionSdinputnilaipengetahuan()
    {
        $siswa = Siswa::find()->where(['id_siswa' => Yii::$app->request->post('id_siswa')])->one();

    
        $model = new SdNilaiHarianPengetahuan();
        $nilai = new NilaiKdPengetahuan();
        
        $model->id_siswa = $siswa->id_siswa;
        $model->id_sekolah = $siswa->id_sekolah;
        $model->id_kelas = $siswa->id_kelas;
        $model->id_semester = $siswa->id_semester;

       // return Yii::$app->request->getBodyParams();
        $model->load(Yii::$app->request->getBodyParams(),'');
        if($model->save()){
             $cekkd = NilaiKdPengetahuan::find()
                        ->where([
                            'id_kd_pengetahuan' => Yii::$app->request->post('id_kd_pengetahuan'),
                            'id_semester' => $siswa->id_semester    
                        ])
                        ->andWhere(['id_siswa' => $siswa->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SdNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        if($cekkd->save()){
                            return "Data berhasil di Simpan";
                        }
                        else{
                         return "Data gagal";   
                        }


                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $siswa->id_sekolah;
                        $nilai->id_kelas = $siswa->id_kelas;
                        $nilai->id_siswa = $siswa->id_siswa;
                        $nilai->id_mapel = Yii::$app->request->post('id_mapel');
                        $nilai->id_kd_pengetahuan = Yii::$app->request->post('id_kd_pengetahuan');
                        $nilai->id_semester = $siswa->id_semester;
                        $nilai->nph = Yii::$app->request->post('nilai');
                        
                        if($nilai->save()){
                            return "Data berhasil di Simpan";
                        }
                        else{
                         return "Data gagal";   
                        }
                    }

        }
        return "Data gagal";
    }
        
        
    

    public function actionSdinputnilaiketerampilan()
    {
       
        $model = new SdNilaiHarianKeterampilan();
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
           
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();

            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->created_by = 20;
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            if ($model->save()) {

                $cekkd = NilaiKdKeterampilan::find()
                ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan, 'id_semester' => $model->id_semester])
                ->andWhere(['id_siswa' => $model->id_siswa])
                ->one();

                if ($cekkd) {

                    $cek = SdNilaiHarianKeterampilan::find()
                    ->select("MAX(nilai) as nilai_max")
                    ->where([
                        'id_siswa'=> $cekkd->id_siswa,
                        'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan,
                        'id_semester' => $model->id_semester
                    ])
                    ->groupBy(['jenis_penilaian'])
                    ->all();

                    $rata_rata=0;
                    foreach ($cek as $key => $value) {
                        $rata_rata += $value->nilai_max;
                    }
                    $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                    $cekkd->skor = round($rata_rata);
                    $cekkd->save();

                  
                }else{
                    $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                    $nilai->id_sekolah = $model->id_sekolah;
                    $nilai->id_kelas = $model->id_kelas;
                    $nilai->id_siswa = $model->id_siswa;
                    $nilai->id_mapel = $model->id_mapel;
                    $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                    $nilai->id_semester = $model->id_semester;
                    $nilai->skor = $model->nilai;
                    $nilai->save();
                }

                    $ceknilaikd = NilaiAkhir::find()
                        ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();
        
                    if ($ceknilaikd) {
                        $x = NilaiKdKeterampilan::find()
                                ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($x as $key => $value) {
                            $row = count($x);
                            $hitung += $value['skor'];
                            $result = $hitung/$row;
                            $hasil = round($result);
                        }
                        $ceknilaikd->nilai_keterampilan = $hasil;
            
                        $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                        $tinggi = NilaiKdKeterampilan::find()
                                        //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                        ->orderBy(['skor' => SORT_DESC])
                                        ->one();
                        $rendah = NilaiKdKeterampilan::find()
                                        //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                        ->orderBy(['skor' => SORT_ASC])
                                        ->one();
                        $nilai = NilaiKdKeterampilan::find()
                                        //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                        ->all();
                        $row = count($nilai);
            
                        $interval = (100-$mapel->kkm_keterampilan)/3;
                            $a = 100 - $interval;
                            $b = $a - $interval;
                            $c = $b - $interval;
                            if ($ceknilaikd->nilai_keterampilan >= $a) {
                                $pre = "A";
                            }elseif($ceknilaikd->nilai_keterampilan >= $b){
                                $pre = "B";
                            }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                                $pre = "C";
                            }else{
                                $pre = "D";
                            }
                        $ceknilaikd->predikat_keterampilan = $pre;
            
                                    if ($ceknilaikd->predikat_keterampilan == "A") {
                                        if ($row == 1) {
                                            $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                        if ($row == 1) {
                                            $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    
                                    }elseif($ceknilaikd->predikat_keterampilan == "C"){
                                        if ($row == 1) {
                                            $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    }else{
                                        if ($row == 1) {
                                            $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                    }
                        $ceknilaikd->deskripsi_keterampilan = $ket;
                        if ($ceknilaikd->save()) {
                        	return "berhasil cek";
                        }else{
                        	return $ceknilaikd->errors;
                        }
        
                    }else{
                        $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                        $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                        $nilaiakhir->id_siswa = $nilai->id_siswa;
                        $nilaiakhir->id_kelas = $nilai->id_kelas;
                        $nilaiakhir->id_mapel = $nilai->id_mapel;
                        $nilaiakhir->id_semester = $nilai->id_semester;
                        $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                        $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                        $tinggi = NilaiKdKeterampilan::find()
                                        //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                        ->andWhere(['id_siswa' => $model->id_siswa])
                                        ->orderBy(['skor' => SORT_DESC])
                                        ->one();
                        $rendah = NilaiKdKeterampilan::find()
                                        //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                        ->andWhere(['id_siswa' => $model->id_siswa])
                                        ->orderBy(['skor' => SORT_ASC])
                                        ->one();
                        $nilai = NilaiKdKeterampilan::find()
                                        //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                        ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                        ->andWhere(['id_siswa' => $model->id_siswa])
                                        ->all();
                        $row = count($nilai);
        
                        $interval = (100-$mapel->kkm_keterampilan)/3;
                            $a = 100 - $interval;
                            $b = $a - $interval;
                            $c = $b - $interval;
                            if ($nilaiakhir->nilai_keterampilan >= $a) {
                                $pre = "A";
                            }elseif($nilaiakhir->nilai_keterampilan >= $b){
                                $pre = "B";
                            }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                                $pre = "C";
                            }else{
                                $pre = "D";
                            }
                        $nilaiakhir->predikat_keterampilan = $pre;
        
        
        
                                    if ($nilaiakhir->predikat_keterampilan == "A") {
                                        if ($row == 1) {
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                        if ($row == 1) {
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    
                                    }elseif($nilaiakhir->predikat_keterampilan == "C"){
                                        if ($row == 1) {
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                        
                                    }else{
                                        if ($row == 1) {
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                        }else{
                                            $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                        }
                                    }
                                $nilaiakhir->deskripsi_keterampilan = $ket;
                                if ($nilaiakhir->save()) {
                                	return "berhasil nilai akhir";
                                }else{
                                	return $nilaiakhir->errors;
                                }
                    }
                }return "Gagal Nyimpan";
            }

        }

    public function actionSelect($id)
    {
        $model = NilaiKdPengetahuan::find()->where(['id_nilai_kd_pengetahuan' => $id])->one();
        return $model;
    }

    public function actionSdinputnilaikd($id)
    {
        $model = NilaiKdPengetahuan::find()->where(['id_nilai_kd_pengetahuan' => $id])->one();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $bobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->all();
            if($bobot){
                foreach ($bobot as $key => $value) {
                    $total = $value['bobot_nph'] + $value['bobot_npts'] + $value['bobot_npas'];
                    $nilaikd =  ($value['bobot_nph']*$model->nph)+
                                ($value['bobot_npts']* $model->npts)+
                                ($value['bobot_npas']* $model->npas);
                    $hasil = $nilaikd/$total;
                }
               
            }else{
                $hasil = ($model->nph + $model->npts + $model->npas)/3;
            }
            $model->nilai_kd = round($hasil);
            if ($model->save()) {

                $ceknilaiakhir = NilaiAkhir::find()
                                ->where([
                                    'id_siswa' => $model->id_siswa,
                                    'id_semester' => $model->id_semester,
                                    'id_mapel' => $model->id_mapel,
                                ])
                                ->one();
                

                if ($ceknilaiakhir) {
                    $nilai = NilaiKdPengetahuan::find()
                            ->where([
                                'id_mapel' => $ceknilaiakhir->id_mapel, 
                                'id_semester' => $ceknilaiakhir->id_semester, 
                                'id_siswa' => $ceknilaiakhir->id_siswa
                            ])
                            ->all();
                    $hitung = 0;
                    foreach ($nilai as $key => $value) {
                        $row = count($nilai);
                        $hitung += $value['nilai_kd'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaiakhir->nilai_pengetahuan = $hasil;

                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaiakhir->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuan::find()
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($ceknilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaiakhir->predikat_pengetahuan = $pre;

                            if ($ceknilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else{
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $ceknilaiakhir->deskripsi_pengetahuan = $x;

                    if ($ceknilaiakhir->save()) {
                        return "sukses";
                    }else{
                        return $ceknilaiakhir->errors;
                    }
                }else{
                    $nilaiakhir->id_nilai_kd = $model->id_nilai_kd_pengetahuan;
                    $nilaiakhir->id_sekolah = $model->id_sekolah;
                    $nilaiakhir->id_siswa = $model->id_siswa;
                    $nilaiakhir->id_kelas = $model->id_kelas;
                    $nilaiakhir->id_mapel = $model->id_mapel;
                    $nilaiakhir->id_semester = $model->id_semester;
                    $nilaiakhir->nilai_pengetahuan = round($model->nilai_kd);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuan::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($nilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_pengetahuan = $pre;

                            if ($nilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($nilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($nilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else {
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $nilaiakhir->deskripsi_pengetahuan = $x;
                    if ($nilaiakhir->save()) {
                        return "sukses nilaiakhir";
                    }
                        return $nilaiakhir->errors;
                    }
                
                }
            }else{
                return "galga model";
            }

    }

    public function actionDeletenilaikdpengetahuan($id)
    {
        $nilai = NilaiAkhir::find()->where(['id_nilai_kd' => $id])->one();
        $model = NilaiKdPengetahuan::find()->where(['id_nilai_kd_pengetahuan' => $id])->one();
        if ($nilai) {
            return "Data masih ada di nilai akhir";
        }else{
           if ($model->delete()) {
               return "Sukses";
           }else{
                return "Gagal";
           }
        }
    }

    public function actionDeletepengetahuan($id)
    {
        $model = SdNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        $nilaikd = NilaiKdPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        if ($nilaikd) {
            return "Gagal, data masih ada di nilai KD";
            # code...
        }else{
            if ($model->delete()) {
                return "sukses";
            }else{
                return "gagal";
            }
        }
    }

    public function actionDeleteketerampilan($id)
    {
        $model = SdNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        $nilaikd = NilaiKdKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        if ($nilaikd) {
            return "Gagal, data masih ada di nilai KD";
            # code...
        }else{
            if ($model->delete()) {
                return "sukses";
            }else{
                return "gagal";
            }
        }
    }

    public function actionUpdateharainpengetahuan($id)
    {
        $model = SdNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        $nilai = new NilaiKdPengetahuan();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'')) {
           
            $model->save();
            $cekkd = NilaiKdPengetahuan::find()
                        ->where([
                            'id_kd_pengetahuan' => $model->id_kd_pengetahuan,
                            'id_semester' => $model->id_semester    
                        ])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SdNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        if ($cekkd->save()) {
                            return "Sukses";
                        }else{
                            return "Gagal";
                        }

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                       if ( $nilai->save()) {
                           return "Sukses";
                       }else{
                        return "gagal";
                       }
                    }
        }
    }

   
     public function actionSelectpengetahuan($id)
    {
        $model = SdNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        return [
                'data' => [
                    'nama' => $model->mapel->nama_mata_pelajaran,
                    'nama_penilaian' => $model->nama_penilaian,
                    'nilai' => $model->nilai,
                    'kade' => $model->kdPengetahuan->no_kd,
                    'tema' => $model->tema->tema,
                    'subtema' => $model->subtema->subtema,
                    'nama_siswa' => $model->siswa->nama,
                    'id_mapel' => $model->id_mapel,
                    'id_kade' => $model->id_kd_pengetahuan,
                    'id_tema' => $model->id_tema,
                    'id_subtema' => $model->id_subtema,

                ],
        ];
    }
     public function actionSelectketerampilan($id)
    {
        $model = SdNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        return [
                'data' => [
                    'nama' => $model->mapel->nama_mata_pelajaran,
                    'nilai' => $model->nilai,
                    'nama_siswa' => $model->siswa->nama,


                ],
        ];
    }


    public function actionUpdateketerampilan($id)
    {
        $model = SdNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();

        if ($model->load(Yii::$app->request->getBodyParams(),'')  OR $nilai->load(Yii::$app->request->getBodyParams(),'')  OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
            if ($model->save()) {
                $cekkd = NilaiKdKeterampilan::find()
            ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan])
            ->andWhere(['id_siswa' => $model->id_siswa])
            ->one();

            if ($cekkd) {
                $cek = SdNilaiHarianKeterampilan::find()
                ->select("MAX(nilai) as nilai_max")
                ->where([
                    'id_siswa'=> $cekkd->id_siswa,
                    'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan
                ])
                ->groupBy(['jenis_penilaian'])
                ->all();

                $rata_rata=0;
                foreach ($cek as $key => $value) {
                    $rata_rata += $value->nilai_max;
                }
                $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                $cekkd->skor = round($rata_rata);
                $cekkd->save();

            }else{
                $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                $nilai->id_sekolah = $model->id_sekolah;
                $nilai->id_kelas = $model->id_kelas;
                $nilai->id_siswa = $model->id_siswa;
                $nilai->id_mapel = $model->id_mapel;
                $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                $nilai->id_semester = $model->id_semester;
                $nilai->skor = $model->nilai;
                $nilai->save();
            }
            $ceknilaikd = NilaiAkhir::find()
                    ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                    ->andWhere(['id_siswa' => $model->id_siswa])
                    ->one();
    
                if ($ceknilaikd) {
                $x = NilaiKdKeterampilan::find()
                        ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                        ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                        ->all();
                $hitung = 0;
                foreach ($x as $key => $value) {
                    $row = count($x);
                    $hitung += $value['skor'];
                    $result = $hitung/$row;
                    $hasil = round($result);
                }
                $ceknilaikd->nilai_keterampilan = $hasil;
    
                $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                $tinggi = NilaiKdKeterampilan::find()
                                //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->orderBy(['skor' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdKeterampilan::find()
                                //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                ->all();
                $row = count($nilai);
    
                $interval = (100-$mapel->kkm_keterampilan)/3;
                    $a = 100 - $interval;
                    $b = $a - $interval;
                    $c = $b - $interval;
                    if ($ceknilaikd->nilai_keterampilan >= $a) {
                        $pre = "A";
                    }elseif($ceknilaikd->nilai_keterampilan >= $b){
                        $pre = "B";
                    }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                        $pre = "C";
                    }else{
                        $pre = "D";
                    }
                $ceknilaikd->predikat_keterampilan = $pre;
    
                            if ($ceknilaikd->predikat_keterampilan == "A") {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            
                            }elseif ( $ceknilaikd->predikat_keterampilan == "C"){
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                                
                            }else{
                                if ($row == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                }
                            }
                $ceknilaikd->deskripsi_keterampilan = $ket;
                if ($ceknilaikd->save()) {
                    return "Berhasil";
                }else{
                    return "Gagal";
                }
    
                }else{
                    $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                    $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                    $nilaiakhir->id_siswa = $nilai->id_siswa;
                    $nilaiakhir->id_kelas = $nilai->id_kelas;
                    $nilaiakhir->id_mapel = $nilai->id_mapel;
                    $nilaiakhir->id_semester = $nilai->id_semester;
                    $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
    
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($nilaiakhir->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($nilaiakhir->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_keterampilan = $pre;
    
    
    
                                if ($nilaiakhir->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($nilaiakhir->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                            $nilaiakhir->deskripsi_keterampilan = $ket;
                           if ( $nilaiakhir->save()) {
                               return "Berhasil";
                           }else{
                                return "Gagal";
                           }
                }
            }else{
                return "Berhasil";
            }

            
        }
    }

   
}
