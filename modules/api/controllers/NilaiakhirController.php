<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\SmaNilaiHarianPengetahuan;
use app\models\SmaNilaiHarianKeterampilan;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use app\models\NilaiKdPengetahuan;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\KompetensiDasarPengetahuan;
use app\models\Sekolah;
use app\models\User;
use app\models\KkmSatuanPendidikan;
use app\models\Guru;
use app\models\Kelas;
use app\models\NilaiAkhir;
use app\models\NilaiAkhirKkmSatuanPendidikan;
use app\models\MataPelajaran;

/**
 * Default controller for the `api` module
 */
class NilaiakhirController extends ActiveController
{
	public $modelClass = 'app\models\NilaiAkhir';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
            ],
    	];
    	return $behavior;
    }
    // public function actionIndex($role="")
    // {
    //     switch($role){
    //         case "siswa-sd":
    //             $model = NilaiAkhir::find()->joinWith('siswa')->joinWith('mapel')->asArray()->all();
    //         break;
    //         case "siswa-smp":
    //             $model = NilaiAkhir::find()->joinWith('siswa')->joinWith('mapel')->asArray()->all();
    //         break;
    //     }
        
    //     return $model;
    // }



    public function actionIndex($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $model = NilaiAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiAkhir::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }

        
        foreach ($model as  $value) {
            $temp[] = [
                'id_nilai' => $value->id_nilai,
                'nama_siswa' => $value->siswa->nama,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai_pengetahuan' => $value->nilai_pengetahuan,
                'predikat_pengetahuan' => $value->predikat_pengetahuan,
                'deskripsi_pengetahuan' => $value->deskripsi_pengetahuan,
                'nilai_keterampilan' => $value->nilai_keterampilan,
                'predikat_keterampilan' => $value->predikat_keterampilan,
                'deskripsi_keterampilan' => $value->deskripsi_keterampilan,

            ];
         }
        
        return $temp;
    }

    public function actionIndexkkmsatuanpendidikan($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }

        
        foreach ($model as  $value) {
            $temp[] = [
                'id_nilai' => $value->id_nilai_akhir,
                'nama_siswa' => $value->siswa->nama,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai_pengetahuan' => $value->nilai_pengetahuan,
                'predikat_pengetahuan' => $value->predikat_pengetahuan,
                'deskripsi_pengetahuan' => $value->deskripsi_pengetahuan,
                'nilai_keterampilan' => $value->nilai_keterampilan,
                'predikat_keterampilan' => $value->predikat_keterampilan,
                'deskripsi_keterampilan' => $value->deskripsi_keterampilan,
                

            ];
         }
        
        return $temp;
    }

    public function actionNilaiakhirsiswa($id)
    {
        $model = NilaiAkhir::find()->where(['id_siswa' => $id])->joinWith('mapel')->asArray()->all();
        return $model;
    }

    public function actionSelect($id)
    {
        $model = NilaiAkhir::find()->where(['id_nilai' => $id])->one();
        return $model;
    }
     public function actionSelectkkmsp($id)
    {
        $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_nilai_akhir' => $id])->one();
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = NilaiAkhir::find()->where(['id_nilai' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->save()) {
            return "berhasil";
        }
            return "gagal";
    }
     public function actionUpdatekkmsp($id)
    {
        $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_nilai_akhir' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->save()) {
            return "berhasil";
        }
            return "gagal";
    }

    public function actionDelete($id)
    {
        $model = NilaiAkhir::find()->where(['id_nilai' => $id])->one();
        if ($model->delete()) {
            return "Berhasil";
        }
            return "gagal";
    }
    public function actionDeletekkmsp($id)
    {
        $model = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_nilai_akhir' => $id])->one();
        if ($model->delete()) {
            return "Berhasil";
        }
            return "gagal";
    }

    public function actionNilaiakhirkkmsatuanpendidikan($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
         if ($user->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $nilai = NilaiAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }else{
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilai = NilaiAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }
        
                
        foreach ($nilai as $key => $nilaiakhir) {
            $model = new NilaiAkhirKkmSatuanPendidikan();
                
            $kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $nilaiakhir->id_kelas])->one();
            $cek = NilaiAkhirKkmSatuanPendidikan::find()
                    ->where(['id_siswa' => $nilaiakhir->id_siswa, 'id_semester' => $nilaiakhir->id_semester])
                    ->andWhere(['id_mapel' => $nilaiakhir->id_mapel])
                    ->one();
            if($cek){
                $cek->id_sekolah = $nilaiakhir->id_sekolah;
                $cek->id_kelas = $nilaiakhir->id_kelas;
                $cek->id_siswa = $nilaiakhir->id_siswa;
                $cek->id_semester = $nilaiakhir->id_semester;
                $cek->id_mapel = $nilaiakhir->id_mapel;
                $cek->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $cek->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $cek->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                $interval = (100-$kkm->kkm)/3;
                $a = 100-$interval;
                $b = $a-$interval;
                $c = $b-$interval;
                $d = $c-$interval;
                    if ($cek->nilai_pengetahuan >= $a) {
                        $ket = "A";
                    }elseif ($cek->nilai_pengetahuan >= $b) {
                        $ket = "B";
                    }elseif ($cek->nilai_pengetahuan >= $kkm->kkm) {
                        $ket = "C";
                    }else{
                        $ket = "D";
                    }
                $cek->predikat_pengetahuan = $ket;
                    if ($cek->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($cek->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($cek->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $cek->predikat_keterampilan = $ktk;
                $tinggi = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nilai_kd' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nilai_kd' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuan::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->all();
                $row = count($nilai);
                if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($cek->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }

                        }else {
                            if($row == 1){
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                        $cek->deskripsi_pengetahuan = $x;
                        $cek->save();
                    }
                
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                     if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($cek->predikat_keterampilan == "A") {
                           
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                    $cek->deskripsi_keterampilan = $ket;
                    $cek->save();
                    }  
                

            }else{
                $model->id_sekolah = $nilaiakhir->id_sekolah;
                $model->id_kelas = $nilaiakhir->id_kelas;
                $model->id_siswa = $nilaiakhir->id_siswa;
                $model->id_semester = $nilaiakhir->id_semester;
                $model->id_mapel = $nilaiakhir->id_mapel;
                $model->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $model->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $model->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                //$kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $model->id_kelas])->one();
                    $interval = (100-$kkm->kkm)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    $d = $c-$interval;
                        if ($model->nilai_pengetahuan >= $a) {
                            $ket = "A";
                        }elseif ($model->nilai_pengetahuan >= $b) {
                            $ket = "B";
                        }elseif ($model->nilai_pengetahuan >= $kkm->kkm) {
                            $ket = "C";
                        }else{
                            $ket = "D";
                        }
                $model->predikat_pengetahuan = $ket;
                    if ($model->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($model->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($model->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $model->predikat_keterampilan = $ktk;
                    $tinggi = NilaiKdPengetahuan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nilai_kd' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nilai_kd' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuan::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
                    if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($model->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }else{
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                    $model->deskripsi_pengetahuan = $x;
                    $model->save();
                    }
                        
                    
                    
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                    if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($model->predikat_keterampilan == "A") {
                            
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                        $model->deskripsi_keterampilan = $ket;
                        $model->save();
                    }
                    
                
            }
        }
    }

    public function actionNilaiakhirkkmsatuanpendidikansekolahmenengah($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        if ($user->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $nilai = NilaiAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }else{
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $nilai = NilaiAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }
        
                
        foreach ($nilai as $key => $nilaiakhir) {
            $model = new NilaiAkhirKkmSatuanPendidikan();
                
            $kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $nilaiakhir->id_kelas])->one();
            $cek = NilaiAkhirKkmSatuanPendidikan::find()
                    ->where(['id_siswa' => $nilaiakhir->id_siswa, 'id_semester' => $nilaiakhir->id_semester])
                    ->andWhere(['id_mapel' => $nilaiakhir->id_mapel])
                    ->one();
            if($cek){
                $cek->id_sekolah = $nilaiakhir->id_sekolah;
                $cek->id_kelas = $nilaiakhir->id_kelas;
                $cek->id_siswa = $nilaiakhir->id_siswa;
                $cek->id_semester = $nilaiakhir->id_semester;
                $cek->id_mapel = $nilaiakhir->id_mapel;
                $cek->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $cek->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $cek->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                $interval = (100-$kkm->kkm)/3;
                $a = 100-$interval;
                $b = $a-$interval;
                $c = $b-$interval;
                $d = $c-$interval;
                    if ($cek->nilai_pengetahuan >= $a) {
                        $ket = "A";
                    }elseif ($cek->nilai_pengetahuan >= $b) {
                        $ket = "B";
                    }elseif ($cek->nilai_pengetahuan >= $kkm->kkm) {
                        $ket = "C";
                    }else{
                        $ket = "D";
                    }
                $cek->predikat_pengetahuan = $ket;
                    if ($cek->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($cek->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($cek->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $cek->predikat_keterampilan = $ktk;
                $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nph' => SORT_DESC])
                                ->one();
                $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->orderBy(['nph' => SORT_ASC])
                                ->one();
                $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                ->andWhere(['id_siswa' => $cek->id_siswa])
                                ->all();
                $row = count($nilai);
                if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($cek->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($cek->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }

                        }else {
                            if($row == 1){
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                        $cek->deskripsi_pengetahuan = $x;
                        $cek->save();
                    }
                
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    ->where([ 'id_mapel'=>$cek->id_mapel, 'id_semester' => $cek->id_semester])
                                    ->andWhere(['id_siswa' => $cek->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                     if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($cek->predikat_keterampilan == "A") {
                           
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($cek->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$cek->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$cek->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                    $cek->deskripsi_keterampilan = $ket;
                    $cek->save();
                    }  
                
                    

            }else{
                $model->id_sekolah = $nilaiakhir->id_sekolah;
                $model->id_kelas = $nilaiakhir->id_kelas;
                $model->id_siswa = $nilaiakhir->id_siswa;
                $model->id_semester = $nilaiakhir->id_semester;
                $model->id_mapel = $nilaiakhir->id_mapel;
                $model->id_nilai_kd = $nilaiakhir->id_nilai_kd;
                $model->nilai_pengetahuan = $nilaiakhir->nilai_pengetahuan;
                $model->nilai_keterampilan = $nilaiakhir->nilai_keterampilan;
                //$kkm = KkmSatuanPendidikan::find()->where(['id_kelas' => $model->id_kelas])->one();
                    $interval = (100-$kkm->kkm)/3;
                    $a = 100-$interval;
                    $b = $a-$interval;
                    $c = $b-$interval;
                    $d = $c-$interval;
                        if ($model->nilai_pengetahuan >= $a) {
                            $ket = "A";
                        }elseif ($model->nilai_pengetahuan >= $b) {
                            $ket = "B";
                        }elseif ($model->nilai_pengetahuan >= $kkm->kkm) {
                            $ket = "C";
                        }else{
                            $ket = "D";
                        }
                $model->predikat_pengetahuan = $ket;
                    if ($model->nilai_keterampilan >= $a) {
                        $ktk = "A";
                    }elseif ($model->nilai_keterampilan >= $b) {
                        $ktk = "B";
                    }elseif ($model->nilai_keterampilan >= $kkm->kkm) {
                        $ktk = "C";
                    }else{
                        $ktk = "D";
                    }
                $model->predikat_keterampilan = $ktk;
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
                    if($nilaiakhir->deskripsi_pengetahuan != NULL){
                        if ($model->predikat_pengetahuan == "A") {
                            
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }elseif ($model->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                        }else{
                            if($row == 1){
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                            }else{
                                $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                            }
                        }
                    $model->deskripsi_pengetahuan = $x;
                    $model->save();
                    }
                        
                    
                    
                    $tinggiktk = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendahktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilaiktk = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $rowktk = count($nilaiktk);
                    if($nilaiakhir->deskripsi_keterampilan != NULL){
                        if ($model->predikat_keterampilan == "A") {
                            
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "B") {
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }elseif ($model->predikat_keterampilan == "C"){
                                if ($rowktk == 1) {
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul;
                                }else{
                                    $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                                }
                        }else{
                            if ($rowktk == 1) {
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul;
                            }else{
                                $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggiktk->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendahktk->kdketerampilan->judul;
                            }
                        }
                        $model->deskripsi_keterampilan = $ket;
                        $model->save();
                    }
                        
                
            }
        }
    }
}
