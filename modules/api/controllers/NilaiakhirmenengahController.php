<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianKeterampilan;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TemaKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\KompetensiDasarPengetahuan;
use app\models\Sekolah;
use app\models\NilaiAkhir;
use app\models\Guru;
use app\models\MataPelajaran;
use app\models\BobotPenilaian;

/**
 * Default controller for the `api` module
 */
class NilaiakhirmenengahController extends ActiveController
{
	public $modelClass = 'app\models\NilaiKdPengetahuanAkhirSekolahMenengah';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        unset($actions['smpinputnilaiharianpengetahuan']);
        unset($actions['mapel']);
        unset($actions['kade']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'smpinputnilaiharianpengetahuan' => ['POST'],
            ],
    	];
    	return $behavior;
    }

    public function actionIndex()
    {
        $model = Siswa::find()->all();
        
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_nilai_kd_pengetahuan_akhir' => $id])->one();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $bobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->all();
            if($bobot){
                foreach ($bobot as $key => $value) {
                    $total = $value['bobot_nph'] + $value['bobot_npts'] + $value['bobot_npas'];
                    $nilaikd =  ($value->bobot_nph * $model->nilai_kd)+
                                ($value->bobot_npts * $model->npts)+
                                ($value->bobot_npas * $model->npas);
                    $hasil = $nilaikd/$total;
                }
               
            }else{
                $hasil = ($model->nilai_kd + $model->npts + $model->npas)/3;
            }
            $model->nilai_akhir_kd = round($hasil);
            
            if ($model->save()) {
                $ceknilaiakhir = NilaiAkhir::find()
                            ->where([
                                'id_siswa' => $model->id_siswa,
                                'id_semester' => $model->id_semester,
                                'id_mapel' => $model->id_mapel,
                            ])
                            ->one();
            

                if ($ceknilaiakhir) {
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                            ->where([
                                'id_mapel' => $ceknilaiakhir->id_mapel, 
                                'id_semester' => $ceknilaiakhir->id_semester, 
                                'id_siswa' => $ceknilaiakhir->id_siswa
                            ])
                            ->all();
                    $hitung = 0;
                    foreach ($nilai as $key => $value) {
                        $row = count($nilai);
                        $hitung += $value['nilai_akhir_kd'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaiakhir->nilai_pengetahuan = $hasil;

                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaiakhir->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($ceknilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaiakhir->predikat_pengetahuan = $pre;

                            if ($ceknilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else{
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $ceknilaiakhir->deskripsi_pengetahuan = $x;

                    if ($ceknilaiakhir->save()) {
                        return "sukses ceknilaiakhir";
                    }else{
                        return $ceknilaiakhir->errors;
                    }
                }else{
                    $nilaiakhir->id_nilai_kd = $model->id_nilai_kd_pengetahuan_akhir;
                    $nilaiakhir->id_sekolah = $model->id_sekolah;
                    $nilaiakhir->id_siswa = $model->id_siswa;
                    $nilaiakhir->id_kelas = $model->id_kelas;
                    $nilaiakhir->id_mapel = $model->id_mapel;
                    $nilaiakhir->id_semester = $model->id_semester;
                    $nilaiakhir->nilai_pengetahuan = round($model->nilai_akhir_kd);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($nilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_pengetahuan = $pre;

                            if ($nilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($nilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($nilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else {
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $nilaiakhir->deskripsi_pengetahuan = $x;
                    if ($nilaiakhir->save()) {
                        return "sukses nilai akhir";
                    }
                    return $nilaiakhir->errors;
                }
            }else{
                return $model->errors;
            }

            
        }
        
    }
}