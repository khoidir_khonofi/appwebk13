<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\SmaNilaiHarianPengetahuan;
use app\models\SmaNilaiHarianKeterampilan;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\BobotPenilaian;
use app\models\NilaiKdPengetahuan;
use app\models\KompetensiDasarKeterampilan;
use app\models\TemaKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\KompetensiDasarPengetahuan;
use app\models\Sekolah;
use app\models\NilaiAkhir;
use app\models\User;
use app\models\Mengajar;
use app\models\Kelas;
use app\models\Tema;
use app\models\Subtema;
use app\models\Guru;
use app\models\MataPelajaran;

/**
 * Default controller for the `api` module
 */
class NilaikdController extends ActiveController
{
	public $modelClass = 'app\models\NilaiKdPengetahuanSekolahMenengah';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'indexpengetahuan'  => ['GET'],
                'nilai' => ['PUT']
            ],
    	];
    	return $behavior;
    }

    public function actionIndex()
    {
        $model = Siswa::find()->all();
        
        return $model;
    }

    public function actionIndexpengetahuan($id)
    {
        $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_siswa' => $id])->all();
            foreach ($model as $key => $value) {
                $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'kdpengetahuan' => [
                        'no_kd' => $value->kdpengetahuan->no_kd,
                    ],
                    'kelas' => [
                        'tingkat_kelas' => $value->kelas->tingkat_kelas,
                        'nama' => $value->kelas->nama,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_pengetahuan_sekolah_menengah,
                        'nph' => $value->nph,
                    ],
                    'kkm' => [
                        'kkm' => $value->kdpengetahuan->kkm,
                    ],
                ];
            }
        return $temp;
    }
    public function actionIndexpengetahuanakhir($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_siswa' => $id])->all();
        foreach ($model as $key => $value) {
                $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_kd_pengetahuan_akhir,
                        'nilai_kd' => $value->nilai_kd,
                        'npts' => $value->npts,
                        'npas' => $value->npas,
                        'nilai_akhir_kd' => $value->nilai_akhir_kd,
                    ],
                ];
            }
        return $temp;
    }

    public function actionLihatsemuakd($token)
    {
         $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               $model = NilaiKdPengetahuanSekolahMenengah::find()->all();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
            foreach ($model as $key => $value) {
                $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'kdpengetahuan' => [
                        'no_kd' => $value->kdpengetahuan->no_kd,
                    ],
                    'kelas' => [
                        'tingkat_kelas' => $value->kelas->tingkat_kelas,
                        'nama' => $value->kelas->nama,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_pengetahuan_sekolah_menengah,
                        'nph' => $value->nph,
                    ],
                    'kkm' => [
                        'kkm' => $value->kdpengetahuan->kkm,
                    ],
                ];
            }
        return $temp;
    }

    public function actionLihattotalkd($token)
    {
         $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->all();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
                $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_kd_pengetahuan_akhir,
                        'nilai_kd' => $value->nilai_kd,
                        'npts' => $value->npts,
                        'npas' => $value->npas,
                        'nilai_akhir_kd' => $value->nilai_akhir_kd,
                        'updated_at' => $value->updated_at,
                    ],
                ];
            }
        return $temp;
    }

     public function actionNilaiperkdketerampilan($id)
    {
        $model = NilaiKdKeterampilan::find()->where(['id_siswa' => $id])->all();
        foreach ($model as $key => $value) {
             $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_kd_keterampilan,
                        'skor' => $value->skor,
                    ],
                    'kdketerampilan' => [
                        'no_kd' => $value->kdketerampilan->no_kd,
                    ],
                ];
        }
        return $temp;
    }
    public function actionNilaiakhirsiswa()
    {
        $model = NilaiAkhir::find()->joinWith('mapel')->asArray()->all();
        return $model;
    }
    public function actionLihatnilaisemuakdsd($token)
    {
         $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = NilaiKdPengetahuan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = NilaiKdPengetahuan::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               $model = NilaiKdPengetahuan::find()->all();
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = NilaiKdPengetahuan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
             $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_kd_pengetahuan,
                        'nph' => $value->nph,
                        'npts' => $value->npts,
                        'npas' => $value->npas,
                    ],
                    'kdpengetahuan' => [
                        'no_kd' => $value->kdpengetahuan->no_kd,
                    ],
                ];
        }
        return $temp;
    }



     public function actionNilaikdpengetahuan($id)
    {
        $model = NilaiKdPengetahuan::find()->where(['id_siswa' => $id])->all();
        foreach ($model as $key => $value) {
             $temp[] = [
                    'siswa' => [
                        'id_siswa' => $value->id_siswa,
                        'nama' => $value->siswa->nama,
                    ],
                    'mapel' => [
                        'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                    ],
                    'nilai' => [
                        'id' => $value->id_nilai_kd_pengetahuan,
                        'nph' => $value->nph,
                        'npts' => $value->npts,
                        'npas' => $value->npas,
                    ],
                    'kdpengetahuan' => [
                        'no_kd' => $value->kdpengetahuan->no_kd,
                    ],
                    'kkm' => [
                        'kkm' => $value->kdpengetahuan->kkm,
                    ],
                ];
        }
        return $temp;
    }
    public function actionNilai($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::findOne(['id_nilai_kd_pengetahuan_akhir' => $id]);
        if ($model->load(Yii::$app->request->getBodyParams(),'')) {
            $bobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->all();
            if($bobot){
                foreach ($bobot as $key => $value) {
                    $total = $value['bobot_nph'] + $value['bobot_npts'] + $value['bobot_npas'];
                    $nilaikd =  ($value->bobot_nph * $model->nilai_kd)+
                                ($value->bobot_npts * $model->npts)+
                                ($value->bobot_npas * $model->npas);
                    $hasil = $nilaikd/$total;
                }
               
            }else{
                $hasil = ($model->nilai_kd + $model->npts + $model->npas)/3;
            }
            $model->npts = Yii::$app->request->post('npts');
            $model->npas = Yii::$app->request->post('npas');
            $model->nilai_akhir_kd = round($hasil);
            if ($model->save()) {
                return "sukses";
            }else{
                return $model->errors;
            }
        }
    }

    public function actionSelect($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::findOne(['id_nilai_kd_pengetahuan_akhir' => $id]);
        return $model;
    }

    public function actionNilainpts($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::findOne(['id_nilai_kd_pengetahuan_akhir' => $id]);
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $bobot = BobotPenilaian::find()->where(['id_kelas' => $model->id_kelas])->all();
            if($bobot){
                foreach ($bobot as $key => $value) {
                    $total = $value['bobot_nph'] + $value['bobot_npts'] + $value['bobot_npas'];
                    $nilaikd =  ($value->bobot_nph * $model->nilai_kd)+
                                ($value->bobot_npts * $model->npts)+
                                ($value->bobot_npas * $model->npas);
                    $hasil = $nilaikd/$total;
                }
               
            }else{
                $hasil = ($model->nilai_kd + $model->npts + $model->npas)/3;
            }
            $model->nilai_akhir_kd = round($hasil);
            if ($model->save()) {
                    $ceknilaiakhir = NilaiAkhir::find()
                                ->where([
                                    'id_siswa' => $model->id_siswa,
                                    'id_semester' => $model->id_semester,
                                    'id_mapel' => $model->id_mapel,
                                ])
                                ->one();
                

                if ($ceknilaiakhir) {
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                            ->where([
                                'id_mapel' => $ceknilaiakhir->id_mapel, 
                                'id_semester' => $ceknilaiakhir->id_semester, 
                                'id_siswa' => $ceknilaiakhir->id_siswa
                            ])
                            ->all();
                    $hitung = 0;
                    foreach ($nilai as $key => $value) {
                        $row = count($nilai);
                        $hitung += $value['nilai_akhir_kd'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaiakhir->nilai_pengetahuan = $hasil;

                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaiakhir->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$ceknilaiakhir->id_mapel, 'id_semester' => $ceknilaiakhir->id_semester,'id_siswa' => $ceknilaiakhir->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($ceknilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($ceknilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaiakhir->predikat_pengetahuan = $pre;

                            if ($ceknilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($ceknilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else{
                                if($row == 1){
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $ceknilaiakhir->deskripsi_pengetahuan = $x;

                    if ($ceknilaiakhir->save()) {
                        return "berhasil cek";
                    }else{
                        return $ceknilaiakhir->errors;
                    }
                }else{
                    $nilaiakhir->id_nilai_kd = $model->id_nilai_kd_pengetahuan_akhir;
                    $nilaiakhir->id_sekolah = $model->id_sekolah;
                    $nilaiakhir->id_siswa = $model->id_siswa;
                    $nilaiakhir->id_kelas = $model->id_kelas;
                    $nilaiakhir->id_mapel = $model->id_mapel;
                    $nilaiakhir->id_semester = $model->id_semester;
                    $nilaiakhir->nilai_pengetahuan = round($model->nilai_akhir_kd);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdPengetahuanSekolahMenengah::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->orderBy(['nph' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester, 'id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);

                    $interval = (100-$mapel->kkm_pengetahuan)/3;
                        $a = 100-$interval;
                        $b = $a-$interval;
                        $c = $b-$interval;
                        if ($nilaiakhir->nilai_pengetahuan >= $a) {
                            $pre = "A";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $b) {
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_pengetahuan >= $mapel->kkm_pengetahuan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_pengetahuan = $pre;

                            if ($nilaiakhir->predikat_pengetahuan == "A") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                

                            }elseif ($nilaiakhir->predikat_pengetahuan == "B") {
                                if ($row == 1) {
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }elseif ($nilaiakhir->predikat_pengetahuan == "C"){
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                                
                            }else {
                                if($row == 1){
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul;
                                }else{
                                    $x  =  "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdpengetahuan->judul." Dan perlu bimbingan dalam ".$rendah->kdpengetahuan->judul;
                                }
                            }
                    $nilaiakhir->deskripsi_pengetahuan = $x;
                    if ($nilaiakhir->save()) {
                        return "berhasil";
                    }else{
                        return $nilaiakhir->errors;
                    }
                    }
            }else{
                return "gagal";
            }
            

            
        }
    }

    public function actionDeletekd($id)
    {
        $model = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_nilai_pengetahuan_sekolah_menengah' => $id])->one();
        $cek = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_nilai_pengetahuan_sekolah_menengah' => $id])->one();
        if ($cek) {
            return "Gagal, data masih ada di nilai kd akhir";
        }else{
            if ($model->delete()) {
                return "Berhasil hapus data";
            }else{
                return "Gagal";
            }
        }
    }

    public function actionDeletekdakhir($id)
    {
        $model = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_nilai_kd_pengetahuan_akhir' => $id])->one();
        $cek = NilaiAkhir::find()->where(['id_nilai_kd' => $id])->one();
        if ($cek) {
            return "Gagal, data masih ada di nilai akhir";
        }else{
            if ($model->delete()) {
                return "Berhasil hapus data";
            }else{
                return "Gagal";
            }
        }
    }

    
}