<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianKeterampilan;
use app\models\NilaiKdKeterampilan;
use app\models\NilaiKdPengetahuanSekolahMenengah;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TemaKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\KompetensiDasarPengetahuan;
use app\models\Sekolah;
use app\models\NilaiAkhir;
use app\models\Tema;
use app\models\User;
use app\models\Kelas;
use app\models\Mengajar;
use app\models\Subtema;
use app\models\Guru;
use app\models\MataPelajaran;

/**
 * Default controller for the `api` module
 */
class NilaismpController extends ActiveController
{
	public $modelClass = 'app\models\SmpNilaiHarianPengetahuan';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        unset($actions['smpinputnilaiharianpengetahuan']);
        unset($actions['mapel']);
        unset($actions['kade']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'smpinputnilaiharianpengetahuan' => ['POST'],
            ],
    	];
    	return $behavior;
    }

    public function actionIndex()
    {
        $model = Siswa::find()->all();
        
        return $model;
    }

     public function actionMapel($id)
    {
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        $model = MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all();
        $arr = [];
        foreach ($model as $item) {
            
            $arr[] = $item->kelas->tingkat_kelas;
        }
        
        return [

                'datamapel' => $model, 
                'Tingkatkelas' => $arr
            ];
    }

     public function actionIndexpengetahuan($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SmpNilaiHarianPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = SmpNilaiHarianPengetahuan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = SmpNilaiHarianPengetahuan::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               return "Gagal";
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SmpNilaiHarianPengetahuan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_pengetahuan' => $value->id_nilai_harian_pengetahuan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdPengetahuan->no_kd,
                'nama_penilaian' => $value->nama_penilaian,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionIndexketerampilan($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SmpNilaiHarianKeterampilan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
            
            if ($kelas) {
                $model = SmpNilaiHarianKeterampilan::find()->where(['id_kelas' => $kelas->id_kelas])->all();
            }elseif ($mengajar){
                $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                $model = SmpNilaiHarianKeterampilan::find()->where(['id_mapel' => $mapel->id_mapel])->all();
            }else{
               return "Gagal";
            }
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SmpNilaiHarianKeterampilan::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_nilai_harian_keterampilan' => $value->id_nilai_harian_keterampilan,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'nama_kelas' => $value->kelas->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'no_kd' => $value->kdKeterampilan->no_kd,
                'nama_mata_pelajaran' => $value->mapel->nama_mata_pelajaran,
                'nama_penilaian' => $value->nama_penilaian,
                'nilai' => $value->nilai,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionKade()
    {
        $model = KompetensiDasarPengetahuan::find()->all();
        
        return $model;
    }

    public function actionKadektr()
    {
        $model = KompetensiDasarKeterampilan::find()->all();
        
        return $model;
    }

     public function actionSmpinputnilaiharianpengetahuan()
    {
        $model = new SmpNilaiHarianPengetahuan();
        $nilai = new NilaiKdPengetahuanSekolahMenengah();
        $nilaikdakhir = new NilaiKdPengetahuanAkhirSekolahMenengah();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'') OR $nilaikdakhir->load(Yii::$app->request->getBodyParams(),'')) {

            
            $siswa = Siswa::find()->where(['id_siswa' => Yii::$app->request->post('id_siswa')])->one();

            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            // $model->save();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    $cekkd = NilaiKdPengetahuanSekolahMenengah::find()
                        ->where([
                            'id_kd_pengetahuan' => $model->id_kd_pengetahuan, 
                            'id_semester' => $model->id_semester
                        ])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SmpNilaiHarianPengetahuan::find()
                                ->where([
                                    'id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,
                                    'id_semester' => $cekkd->id_semester
                                ])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save();
                    }

                     $ceknilaikd = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                        ->where([
                            'id_siswa' => $model->id_siswa, 
                            'id_semester' => $model->id_semester
                        ])->andWhere([
                            'id_mapel' => $model->id_mapel
                        ])
                        ->one();
                    if ($ceknilaikd) {
                        $hitungnilaikd = NilaiKdPengetahuanSekolahMenengah::find()
                            ->where([
                                'id_siswa' => $ceknilaikd->id_siswa, 
                                'id_semester' => $ceknilaikd->id_semester
                            ])
                            ->andWhere([
                                'id_mapel' => $ceknilaikd->id_mapel
                            ])
                            ->all();
                        $hitung = 0;
                        foreach ($hitungnilaikd as $key => $value) {
                            $row = count($hitungnilaikd);
                            $hitung += $value['nph'];
                            $result = $hitung/$row;
                            $hasil = round($result);
                        }
                        $ceknilaikd->nilai_kd = $hasil;
                        if ($ceknilaikd->save()) {
                            return "ceknilaikd save";
                        }else{
                            return "gagal ceknilaikd";
                        }

                    }else{
                        $nilaikdakhir->id_nilai_pengetahuan_sekolah_menengah = $nilai->id_nilai_pengetahuan_sekolah_menengah;
                        $nilaikdakhir->id_sekolah = $nilai->id_sekolah;
                        $nilaikdakhir->id_siswa = $nilai->id_siswa;
                        $nilaikdakhir->id_kelas = $nilai->id_kelas;
                        $nilaikdakhir->id_mapel = $nilai->id_mapel;
                        $nilaikdakhir->id_semester = $nilai->id_semester;
                        $nilaikdakhir->nilai_kd = $nilai->nph;
                        if ($nilaikdakhir->save()) {
                            return "nilaikdakhir save";
                        }else{
                            return "galga nilaikdakhir";
                        }
                    }
                }
            } catch (Exception $e) {
                Yii::error($e->getMessage());
            }

            $transaction->rollBack();
        }

        
    }

    public function actionSmpinputnilaiharianketerampilan()
    {
        $model = new SmpNilaiHarianKeterampilan();
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $siswa = Siswa::find()->where(['id_siswa' => Yii::$app->request->post('id_siswa')])->one();

            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->id_kelas = $siswa->id_kelas;
            $model->id_semester = $siswa->id_semester;
            // $model->save();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    $cekkd = NilaiKdKeterampilan::find()
                        ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                       $cek = SmpNilaiHarianKeterampilan::find()
                        ->select("MAX(nilai) as nilai_max")
                        ->where([
                            'id_siswa'=> $cekkd->id_siswa,
                            'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan
                        ])
                        ->groupBy(['nama_penilaian'])
                        ->all();

                        $rata_rata=0;
                        foreach ($cek as $key => $value) {
                            $rata_rata += $value->nilai_max;
                        }
                        $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                        $cekkd->skor = $rata_rata;
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->skor = $model->nilai;
                        $nilai->save(); 
                    }
                $ceknilaikd = NilaiAkhir::find()
                    ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                    ->andWhere(['id_siswa' => $model->id_siswa])
                    ->one();
    
                if ($ceknilaikd) {
                    $x = NilaiKdKeterampilan::find()
                            ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                            ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                            ->all();
                    $hitung = 0;
                    foreach ($x as $key => $value) {
                        $row = count($x);
                        $hitung += $value['skor'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaikd->nilai_keterampilan = $hasil;
        
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->all();
                    $row = count($nilai);
        
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($ceknilaikd->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($ceknilaikd->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaikd->predikat_keterampilan = $pre;
        
                                if ($ceknilaikd->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($ceknilaikd->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                    $ceknilaikd->deskripsi_keterampilan = $ket;
                    if ($ceknilaikd->save()) {
                        return "sukses";
                    }else{
                        return $ceknilaikd->errors;
                    }
        
                }else{
                    $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                    $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                    $nilaiakhir->id_siswa = $nilai->id_siswa;
                    $nilaiakhir->id_kelas = $nilai->id_kelas;
                    $nilaiakhir->id_mapel = $nilai->id_mapel;
                    $nilaiakhir->id_semester = $nilai->id_semester;
                    $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
    
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($nilaiakhir->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($nilaiakhir->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_keterampilan = $pre;
    
    
    
                                if ($nilaiakhir->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($nilaiakhir->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$nilaiakhir->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                            $nilaiakhir->deskripsi_keterampilan = $ket;
                            if ($nilaiakhir->save()) {
                                return "berhasil";
                            }else{
                                return $nilaiakhir->errors;
                            }
                }
            }
    
                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                }
    
                $transaction->rollBack();
    
               
            }

    }

    public function actionUpdatepengetahuan($id)
    {
        $model = SmpNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        $nilai = new NilaiKdPengetahuanSekolahMenengah();
        $nilaikdakhir = new NilaiKdPengetahuanAkhirSekolahMenengah();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'') OR $nilaikdakhir->load(Yii::$app->request->getBodyParams(),'')) {
                if ($model->save()) {
                    $cekkd = NilaiKdPengetahuanSekolahMenengah::find()
                        ->where(['id_kd_pengetahuan' => $model->id_kd_pengetahuan,'id_semester' => $model->id_semester])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                        $test = SmpNilaiHarianPengetahuan::find()
                                ->where(['id_kd_pengetahuan' => $cekkd->id_kd_pengetahuan,'id_semester' => $cekkd->id_semester])
                                ->andWhere(['id_siswa'=> $cekkd->id_siswa])
                                ->all();
                        $hitung = 0;
                        foreach ($test as $key => $value) {
                            $row = count($test);
                            $hitung += $value['nilai'];
                            $hasil = $hitung/$row;
                        }
                        $cekkd->nph = round($hasil);
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_pengetahuan = $model->id_nilai_harian_pengetahuan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_pengetahuan = $model->id_kd_pengetahuan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->nph = $model->nilai;
                        $nilai->save(); 
                    }

                     $ceknilaikd = NilaiKdPengetahuanAkhirSekolahMenengah::find()
                        ->where([
                            'id_siswa' => $model->id_siswa, 
                            'id_semester' => $model->id_semester
                        ])->andWhere([
                            'id_mapel' => $model->id_mapel
                        ])
                        ->one();
                    if ($ceknilaikd) {
                        $hitungnilaikd = NilaiKdPengetahuanSekolahMenengah::find()
                            ->where([
                                'id_siswa' => $ceknilaikd->id_siswa, 
                                'id_semester' => $ceknilaikd->id_semester
                            ])
                            ->andWhere([
                                'id_mapel' => $ceknilaikd->id_mapel
                            ])
                            ->all();
                        $hitung = 0;
                        foreach ($hitungnilaikd as $key => $value) {
                            $row = count($hitungnilaikd);
                            $hitung += $value['nph'];
                            $result = $hitung/$row;
                            $hasil = round($result);
                        }
                        $ceknilaikd->nilai_kd = $hasil;
                       if ( $ceknilaikd->save()) {
                           return "berhasil cek";
                       }else{
                            return "Gagal";
                       }

                    }else{
                        $nilaikdakhir->id_nilai_pengetahuan_sekolah_menengah = $nilai->id_nilai_pengetahuan_sekolah_menengah;
                        $nilaikdakhir->id_sekolah = $nilai->id_sekolah;
                        $nilaikdakhir->id_siswa = $nilai->id_siswa;
                        $nilaikdakhir->id_kelas = $nilai->id_kelas;
                        $nilaikdakhir->id_mapel = $nilai->id_mapel;
                        $nilaikdakhir->id_semester = $nilai->id_semester;
                        $nilaikdakhir->nilai_kd = $nilai->nph;
                        if ($nilaikdakhir->save()) {
                            return "Berhasil";
                        }else{
                            return "Gagal";
                        }
                    }

                }
        }
    }

    public function actionSelectpengetahuan($id)
    {
        $model = SmpNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        return [
                'data' => [
                    'nama' => $model->mapel->nama_mata_pelajaran,
                    'nilai' => $model->nilai,
                    'nama_penilaian' => $model->nama_penilaian,
                    'kade' => $model->kdPengetahuan->no_kd,
                    'nama_siswa' => $model->siswa->nama,
                    'id_mapel' => $model->id_mapel,
                    'id_kd_pengetahuan' => $model->id_kd_pengetahuan,

                ],
        ];
    }

     public function actionSelectketerampilan($id)
    {
        $model = SmpNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        return [
                'data' => [
                    'nama' => $model->mapel->nama_mata_pelajaran,
                    'nilai' => $model->nilai,
                    'nama_penilaian' => $model->nama_penilaian,
                    'nama_siswa' => $model->siswa->nama,


                ],
        ];
    }


    public function actionDeletepengetahuan($id)
    {
        $model = SmpNilaiHarianPengetahuan::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        $cek = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_nilai_harian_pengetahuan' => $id])->one();
        if ($cek) {
            return "Data masih ada di nilai kd pengetahuan";
        }else{
            if ($model->delete()) {
                return " berhasil";
            }else{
                return "Gagal";
            }
        }
    }

    public function actionDeleteketerampilan($id)
    {
        $model = SmpNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        $cek = NilaiKdKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        if ($cek) {
            return "Data masih ada di nilai kd keterampilan";
        }else{
            if ($model->delete()) {
                return " berhasil";
            }else{
                return "Gagal";
            }
        }
    }

    public function actionUpdateketerampilan($id)
    {
        $model = SmpNilaiHarianKeterampilan::find()->where(['id_nilai_harian_keterampilan' => $id])->one();
        $nilai = new NilaiKdKeterampilan();
        $nilaiakhir = new NilaiAkhir();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $nilai->load(Yii::$app->request->getBodyParams(),'') OR $nilaiakhir->load(Yii::$app->request->getBodyParams(),'')) {
                if ($model->save()) {
                    $cekkd = NilaiKdKeterampilan::find()
                        ->where(['id_kd_keterampilan' => $model->id_kd_keterampilan])
                        ->andWhere(['id_siswa' => $model->id_siswa])
                        ->one();

                    if ($cekkd){
                       $cek = SmpNilaiHarianKeterampilan::find()
                        ->select("MAX(nilai) as nilai_max")
                        ->where([
                            'id_siswa'=> $cekkd->id_siswa,
                            'id_kd_keterampilan'=>$cekkd->id_kd_keterampilan
                        ])
                        ->groupBy(['nama_penilaian'])
                        ->all();

                        $rata_rata=0;
                        foreach ($cek as $key => $value) {
                            $rata_rata += $value->nilai_max;
                        }
                        $rata_rata = $rata_rata == 0 ? 0 : ($rata_rata/(count($cek)));
                        $cekkd->skor = $rata_rata;
                        $cekkd->save();

                    }else{
                        $nilai->id_nilai_harian_keterampilan = $model->id_nilai_harian_keterampilan;
                        $nilai->id_sekolah = $model->id_sekolah;
                        $nilai->id_kelas = $model->id_kelas;
                        $nilai->id_siswa = $model->id_siswa;
                        $nilai->id_mapel = $model->id_mapel;
                        $nilai->id_kd_keterampilan = $model->id_kd_keterampilan;
                        $nilai->id_semester = $model->id_semester;
                        $nilai->skor = $model->nilai;
                        $nilai->save(); 
                    }
                $ceknilaikd = NilaiAkhir::find()
                    ->where(['id_mapel' => $model->id_mapel, 'id_semester' => $model->id_semester])
                    ->andWhere(['id_siswa' => $model->id_siswa])
                    ->one();
    
                if ($ceknilaikd) {
                    $x = NilaiKdKeterampilan::find()
                            ->where(['id_mapel' => $ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                            ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                            ->all();
                    $hitung = 0;
                    foreach ($x as $key => $value) {
                        $row = count($x);
                        $hitung += $value['skor'];
                        $result = $hitung/$row;
                        $hasil = round($result);
                    }
                    $ceknilaikd->nilai_keterampilan = $hasil;
        
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $ceknilaikd->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$ceknilaikd->id_mapel, 'id_semester' => $ceknilaikd->id_semester])
                                    ->andWhere(['id_siswa' => $ceknilaikd->id_siswa])
                                    ->all();
                    $row = count($nilai);
        
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($ceknilaikd->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($ceknilaikd->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($ceknilaikd->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $ceknilaikd->predikat_keterampilan = $pre;
        
                                if ($ceknilaikd->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($ceknilaikd->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($ceknilaikd->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                    $ceknilaikd->deskripsi_keterampilan = $ket;
                    if ($ceknilaikd->save()) {
                        return "Berhasil";
                    }else{
                        return "Gagal";
                    }
    
                }else{
                    $nilaiakhir->id_nilai_kd = $nilai->id_nilai_kd_keterampilan;
                    $nilaiakhir->id_sekolah = $nilai->id_sekolah;
                    $nilaiakhir->id_siswa = $nilai->id_siswa;
                    $nilaiakhir->id_kelas = $nilai->id_kelas;
                    $nilaiakhir->id_mapel = $nilai->id_mapel;
                    $nilaiakhir->id_semester = $nilai->id_semester;
                    $nilaiakhir->nilai_keterampilan = round($nilai->skor);
                    $mapel = MataPelajaran::find()->where(['id_mapel' => $model->id_mapel])->one();
                    $tinggi = NilaiKdKeterampilan::find()
                                    //->select("MAX(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_DESC])
                                    ->one();
                    $rendah = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->orderBy(['skor' => SORT_ASC])
                                    ->one();
                    $nilai = NilaiKdKeterampilan::find()
                                    //->select("MIN(nilai) as nilai, id_kd_pengetahuan")
                                    ->where([ 'id_mapel'=>$model->id_mapel, 'id_semester' => $model->id_semester])
                                    ->andWhere(['id_siswa' => $model->id_siswa])
                                    ->all();
                    $row = count($nilai);
    
                    $interval = (100-$mapel->kkm_keterampilan)/3;
                        $a = 100 - $interval;
                        $b = $a - $interval;
                        $c = $b - $interval;
                        if ($nilaiakhir->nilai_keterampilan >= $a) {
                            $pre = "A";
                        }elseif($nilaiakhir->nilai_keterampilan >= $b){
                            $pre = "B";
                        }elseif ($nilaiakhir->nilai_keterampilan >= $mapel->kkm_keterampilan) {
                            $pre = "C";
                        }else{
                            $pre = "D";
                        }
                    $nilaiakhir->predikat_keterampilan = $pre;
    
                            
                                if ($nilaiakhir->predikat_keterampilan == "A") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Sangat baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }elseif ($nilaiakhir->predikat_keterampilan == "B") {
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Baik dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                
                                }elseif ($nilaiakhir->predikat_keterampilan == "C"){
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Cukup dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                    
                                }else{
                                    if ($row == 1) {
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul;
                                    }else{
                                        $ket = "<b>".$model->siswa->nama."</b>"." Kurang dalam ".$tinggi->kdketerampilan->judul." Dan perlu bimbingan dalam ".$rendah->kdketerampilan->judul;
                                    }
                                }
                        $nilaiakhir->deskripsi_keterampilan = $ket;
                        if ($nilaiakhir->save()) {
                            return "Berhasil";

                        }else{
                            return "Gagal";
                        }
                }
            }
    
               
            }

    }
}