<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\NilaiKdPengetahuan;
use app\models\SikapSosial;
use app\models\SikapSpiritual;
use app\models\SikapAkhir;
use app\models\Siswa;
use app\models\Kelas;
use app\models\User;
use app\models\Guru;
use app\models\Sekolah;
use app\models\NilaiKdKeterampilan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\Semester;
use app\models\JurnalSikap;
use yii\helpers\Url;
class SikapController extends ActiveController
{
	public $modelClass = 'app\models\SikapSosial';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        unset($actions['sosial']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'sosial' => ['POST'],
            ],
    	];
    	return $behavior;
    }

    public function actionSikapsosial($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapSosial::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $model = SikapSosial::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapSosial::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as  $value) {
            $temp[] = [
                'id_sikap_sosial' => $value->id_sikap_sosial,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'butir_sikap' => $value->butir_sikap,
                'nilai' => $value->nilai,

            ];
         }
        
        return $temp;
    }

    public function actionKelas()
    {
        $model = Kelas::find()->all();
        foreach ($model as $key => $value) {
            $temp[] = [
                'nama' => $value->nama,
                'tingkat_kelas' => $value->tingkat_kelas,
            ];
        }
        return $temp;
    }

    public function actionSiswa()
    {
        $model = Siswa::find()->all();
        foreach ($model as $key => $value) {
            $temp[] = [
                'nama' => $value->nama,
            ];
        }
        return $temp;
    }

    public function actionSemester()
    {
        $model = Semester::find()->all();
        foreach ($model as $key => $value) {
            $temp[] = [
                'semester' => $value->semester,
            ];
        }
        return $temp;
    }

    public function actionButirsikap($id)
    {
        $model = SikapSosial::find()->where(['id_sikap_sosial' => $id])->one();
        return [
            'data' => [
                'butir_sikap' => $model->butir_sikap,
                'nilai' => $model->nilai,
                'nama' => $model->siswa->nama,
            ],
        ];
    }
    public function actionButirsikapspiritual($id)
    {
        $model = SikapSpiritual::find()->where(['id_sikap_spiritual' => $id])->one();
        return [
            'data' => [
                'butir_sikap' => $model->butir_sikap,
                'nilai' => $model->nilai,
                'nama' => $model->siswa->nama,
            ],
        ];
    }

    public function actionSikapspiritual($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapSpiritual::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $model = SikapSpiritual::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapSpiritual::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as  $value) {
            $temp[] = [
                'id_sikap_spiritual' => $value->id_sikap_spiritual,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'butir_sikap' => $value->butir_sikap,
                'nilai' => $value->nilai,

            ];
         }
        
        return $temp;
    }

    public function actionSikapakhir($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();

        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapAkhir::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            $model = SikapAkhir::find()->where(['id_kelas' => $kelas->id_kelas])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = SikapAkhir::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as  $value) {
            $temp[] = [
                'id_sikap' => $value->id_sikap,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'keterangan_sosial' => $value->keterangan_sosial,
                'keterangan_spiritual' => $value->keterangan_spiritual,

            ];
         }
        
        return $temp;
    }

    public function actionSikapakhirdetail($id)
    {
        $model = SikapAkhir::find()->where(['id_siswa' => $id])->all();
        $temp = [];
        foreach ($model as $key => $value) {
            $temp[] = [
                'siswa' => [
                    'id' => $value->id_siswa,
                    'nama' => $value->siswa->nama
                ],
                'sikap' => [
                    'id_sikap' => $value->id_sikap,
                    'keterangan_sosial' => $value->keterangan_sosial,
                    'keterangan_spiritual' => $value->keterangan_spiritual,
                ],
            ];
        }
        return $temp;
    }

    public function actionSosial($id)
    {
    	$model = new SikapSosial();
        $sikapakhir = new SikapAkhir();
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $sikapakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $ceksikap = SikapSosial::find()->where(['id_siswa' => $id, 'butir_sikap' => Yii::$app->request->post('butir_sikap')])->one();
            if ($ceksikap) {
                return "<script>alert('butir sikap ".$ceksikap->butir_sikap." atas nama ". $ceksikap->siswa->nama." sudah ada'); window.location='index.php?r=sikap-sosial';</script>";
            }else{
                
                $model->id_sekolah = $siswa->id_sekolah;
                $model->id_siswa = $siswa->id_siswa;
                $model->id_kelas = $siswa->id_kelas;
                $model->id_semester = $siswa->id_semester;
                if ($model->save()) {
	                $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
	                if ($cek) {
	                    $tinggi = SikapSosial::find()
	                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
	                            ->orderBy(['nilai' => SORT_DESC])
	                            ->one();
	                    $rendah = SikapSosial::find()
	                            ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
	                            ->orderBy(['nilai' => SORT_ASC])
	                            ->one();
	                    if ($cek->keterangan_sosial == NULL) {
	                        if ($tinggi->nilai >= 90) {
	                            $ket = "Sangat baik dalam ".$model->butir_sikap;
	                        }else{
	                            $ket = "Baik dalam ".$model->butir_sikap;
	                        }
	                        
	                    }else{
	                        if ($tinggi->nilai >= 90) {
	                            $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
	                        }else{
	                            $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
	                        }
	                        
	                    }
	                    $cek->keterangan_sosial = $ket;
	                    
                        if ($cek->save()) {
                            return "berhasil ceksave";
                        }else{
                            return $cek->errors;
                        }
	                }else{
	                    $tes = SikapSosial::find()
	                            ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
	                            ->one();
	                    $sikapakhir->id_sekolah = $siswa->id_sekolah;
	                    $sikapakhir->id_siswa = $siswa->id_siswa;
	                    $sikapakhir->id_kelas = $siswa->id_kelas;
	                    $sikapakhir->id_semester = $siswa->id_semester;
	                    $sikapakhir->id_sikap_sosial = $model->id_sikap_sosial;
	                    if ($tes->nilai >= 90) {
	                        $sikapakhir->keterangan_sosial = "Sangat Baik dalam ".$tes->butir_sikap;
	                    }else{
	                        $sikapakhir->keterangan_sosial = "Baik dalam ".$tes->butir_sikap;
	                    }
	                    
	                    if ($sikapakhir->save()) {
	                    	return "save sikap akhir";
	                    }else{
	                    	return $sikapakhir->errors;
	                    }
	                }
                }else{
                	return $model->errors;
                }
    
                
            }    
        }
    }

    public function actionSpiritual($id)
    {
        $model = new SikapSpiritual();
        $sikapakhir = new SikapAkhir();
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $sikapakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->request->post('id_user')])->one();
            $ceksikap = SikapSpiritual::find()->where(['id_siswa' => $id, 'butir_sikap' => Yii::$app->request->post('butir_sikap')])->one();
            if ($ceksikap) {
                return "<script>alert('butir sikap ".$ceksikap->butir_sikap." atas nama ". $ceksikap->siswa->nama." sudah ada'); window.location='index.php?r=sikap-spiritual';</script>";
            }else{
                
                $model->id_sekolah = $siswa->id_sekolah;
                $model->id_siswa = $siswa->id_siswa;
                $model->id_kelas = $siswa->id_kelas;
                $model->id_semester = $siswa->id_semester;
                if ($model->save()) {
                    $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
                    if ($cek) {
                        $tinggi = SikapSpiritual::find()
                                ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                                ->orderBy(['nilai' => SORT_DESC])
                                ->one();
                        $rendah = SikapSpiritual::find()
                                ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                                ->orderBy(['nilai' => SORT_ASC])
                                ->one();
                        if ($cek->keterangan_spiritual == NULL) {
                            if ($tinggi->nilai >= 90) {
                                $ket = "Sangat baik dalam ".$model->butir_sikap;
                            }else{
                                $ket = "Baik dalam ".$model->butir_sikap;
                            }
                            
                        }else{
                            if ($tinggi->nilai >= 90) {
                                $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                            }else{
                                $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                            }
                            
                        }
                        $cek->keterangan_spiritual = $ket;
                        
                        if ($cek->save()) {
                            return "berhasil ceksave";
                        }else{
                            return $cek->errors;
                        }
                    }else{
                        $tes = SikapSpiritual::find()
                                ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
                                ->one();
                        $sikapakhir->id_sekolah = $siswa->id_sekolah;
                        $sikapakhir->id_siswa = $siswa->id_siswa;
                        $sikapakhir->id_kelas = $siswa->id_kelas;
                        $sikapakhir->id_semester = $siswa->id_semester;
                        $sikapakhir->id_sikap_spiritual = $model->id_sikap_spiritual;
                        if ($tes->nilai >= 90) {
                            $sikapakhir->keterangan_spiritual = "Sangat Baik dalam ".$tes->butir_sikap;
                        }else{
                            $sikapakhir->keterangan_spiritual = "Baik dalam ".$tes->butir_sikap;
                        }
                        
                        if ($sikapakhir->save()) {
                            return "save sikap akhir";
                        }else{
                            return $sikapakhir->errors;
                        }
                    }
                }else{
                    return $model->errors;
                }
    
                
            }    
        }
    }

    public function actionUpdate($id)
    {
        $model = SikapSosial::find()->where(['id_sikap_sosial' => $id])->one();
        $sikapakhir = new SikapAkhir();

        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $sikapakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->save();

            $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
            if ($cek) {
                $tinggi = SikapSosial::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_DESC])
                        ->one();
                $rendah = SikapSosial::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_ASC])
                        ->one();
                if ($cek->keterangan_sosial == NULL) {
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$model->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$model->butir_sikap;
                    }
                    
                }else{
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }
                    
                }
                
                $cek->keterangan_sosial = $ket;
                if ($cek->save()) {
                    return "Berhasil";
                }else{
                    return "gagal";
                }
            }else{
                $tes = SikapSosial::find()
                        ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
                        ->one();
                $sikapakhir->id_sekolah = $model->id_sekolah;
                $sikapakhir->id_siswa = $model->id_siswa;
                $sikapakhir->id_kelas = $model->id_kelas;
                $sikapakhir->id_semester = $model->id_semester;
                $sikapakhir->id_sikap_sosial = $model->id_sikap_sosial;
                if ($tes->nilai >= 90) {
                    $sikapakhir->keterangan_sosial = "Sangat Baik dalam ".$tes->butir_sikap;
                }else{
                    $sikapakhir->keterangan_sosial = "Baik dalam ".$tes->butir_sikap;
                }
                if ($sikapakhir->save()) {
                    return "berhasil";
                }else{
                    return "gagal";
                }
            }

        }
    }

    public function actionDelete($id)
    {
        $model = SikapSosial::find()->where(['id_sikap_sosial' => $id])->one();
        if ($model) {
            if ($model->delete()) {
                return "Berhasil";
            }
        }else{
            return "Hapus gagal";
        }
    }
     public function actionDeletespiritual($id)
    {
        $model = SikapSpiritual::find()->where(['id_sikap_spiritual' => $id])->one();
        if ($model) {
            if ($model->delete()) {
                return "Berhasil";
            }
        }else{
            return "Hapus gagal";
        }
    }

    public function actionUpdatespiritual($id)
    {
        $model = SikapSpiritual::find()->where(['id_sikap_spiritual' => $id])->one();
        $sikapakhir = new SikapAkhir();

        if ($model->load(Yii::$app->request->getBodyParams(),'') OR $sikapakhir->load(Yii::$app->request->getBodyParams(),'')) {
            $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
            
            $model->id_sekolah = $siswa->id_sekolah;
            $model->save();

            $cek = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
            if ($cek) {
                $tinggi = SikapSpiritual::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_DESC])
                        ->one();
                $rendah = SikapSpiritual::find()
                        ->where(['id_siswa' => $cek->id_siswa, 'id_semester' => $cek->id_semester])
                        ->orderBy(['nilai' => SORT_ASC])
                        ->one();
                if ($cek->keterangan_spiritual == NULL) {
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$model->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$model->butir_sikap;
                    }
                    
                }else{
                    if ($tinggi->nilai >= 90) {
                        $ket = "Sangat baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }else{
                        $ket = "Baik dalam ".$tinggi->butir_sikap." dan telah mampu meningkatkan ".$rendah->butir_sikap;
                    }
                    
                }
                
                $cek->keterangan_spiritual  = $ket;
                if ($cek->save()) {
                    return "Berhasil";
                }else{
                    return "gagal";
                }
            }else{
                $tes = SikapSpiritual::find()
                        ->where(['id_siswa' => $siswa->id_siswa, 'id_semester' => $siswa->id_semester])
                        ->one();
                $sikapakhir->id_sekolah = $model->id_sekolah;
                $sikapakhir->id_siswa = $model->id_siswa;
                $sikapakhir->id_kelas = $model->id_kelas;
                $sikapakhir->id_semester = $model->id_semester;
                $sikapakhir->id_sikap_spiritual = $model->id_sikap_spiritual;
                if ($tes->nilai >= 90) {
                    $sikapakhir->keterangan_spiritual = "Sangat Baik dalam ".$tes->butir_sikap;
                }else{
                    $sikapakhir->keterangan_spiritual = "Baik dalam ".$tes->butir_sikap;
                }
                if ($sikapakhir->save()) {
                    return "berhasil";
                }else{
                    return "gagal";
                }
            }

        }
    }

    public function actionDeletesikapakhir($id)
    {
        $model = SikapAkhir::find()->where(['id_sikap' => $id])->one();
        if ($model->delete()) {
            return "Berhasil hapus data";
        }
            return "gagal";
    }

    public function actionUpdatesikapakhir($id)
    {
        $model = SikapAkhir::find()->where(['id_sikap' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->save()) {
            return "berhasil";
        }
            return "gagal";
    }

    public function actionSelectsikapakhir($id)
    {
        $model = SikapAkhir::find()->where(['id_sikap' => $id])->one();
        return  [
            'data' => [
                'sosial' => $model->keterangan_sosial,
                'spiritual' => $model->keterangan_spiritual,
            ],
        ];
    }

    public function actionJurnalsikap($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = JurnalSikap::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $model = JurnalSikap::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = JurnalSikap::find()->where(['id_siswa' => $siswa->id_siswa])->all();
        }
        foreach ($model as $key => $value) {
            $temp[] = [
                'id_jurnal_sikap' => $value->id_jurnal_sikap,
                'id_kelas' => $value->id_kelas,
                'id_siswa' => $value->id_siswa,
                'nama_siswa' => $value->siswa->nama,
                'sikap' => $value->sikap,
                'catatan_perilaku' => $value->catatan_perilaku,
                'butir_sikap' => $value->butir_sikap,
                'penilaian' => $value->penilaian,
                'tindak_lanjut' => $value->tindak_lanjut,
                'tingkat_kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'created_at' => $value->created_at,
            ];
        }
        return $temp;
    }

    public function actionPostjurnalsikap($id)
    {
        $siswa = Siswa::find()->where(['id_siswa' => $id])->one();
        $model = new JurnalSikap();

        if ($model->load(Yii::$app->request->getBodyParams(),'')) {
            $cek = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->AndWhere(['sikap' => $model->sikap])->one();
            if ($cek) {
                return  'Sikap "'.$model->sikap.'" atas nama "'.$model->siswa->nama.'" telah dibuat';
            }else{
                 $model->id_sekolah = $siswa->id_sekolah;
                 $model->id_semester = $siswa->id_semester;
                 $model->id_kelas = $siswa->id_kelas;
                if ($model->save()) {
                    return "Berhasil";
                }else{
                    return "Gagal";
                }
            }
           
        }
    }

    public function actionDeletejurnal($id)
    {
        $model = JurnalSikap::find()->where(['id_jurnal_sikap' => $id])->one();
        if ($model->delete()) {
            return "Berhasil";
        }
            return "Gagal";
    }

    public function actionLoad()
    {
            $model = new SikapAkhir();
            if ($model->load(Yii::$app->request->getBodyParams(),'')) {
               
                $cekjurnal = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->one();
                if (!$cekjurnal) {
                    return "Siswa tidak ada di jurnal sikap";
                }else{
                    $ceksiswa = SikapAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->one();
                    if ($ceksiswa) {
                            $jurnal = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa])->one();
                            $sosial = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'sikap' => 'Sosial'])->one();
                            $spiritual = JurnalSikap::find()->where(['id_siswa' => $ceksiswa->id_siswa, 'sikap' => 'Spiritual'])->one();
                            $siswa = Siswa::find()->where(['id_siswa' => $ceksiswa->id_siswa])->one();
                            $ceksiswa->id_siswa = $jurnal->id_siswa;
                            $ceksiswa->id_kelas = $siswa->id_kelas;
                            $ceksiswa->id_semester = $siswa->id_semester;
                            $ceksiswa->id_sekolah = $siswa->id_sekolah;
                            $ceksiswa->keterangan_sosial = $sosial->catatan_perilaku;
                            $ceksiswa->keterangan_spiritual = $spiritual->catatan_perilaku;
                            if ($ceksiswa->save()) {
                                return "Berhasil";
                            }else{
                                return "Gagal";
                            }
                    }else{
                            $jurnal = JurnalSikap::find()->where(['id_siswa' => $model->id_siswa])->all();
                            $newmodel = new SikapAkhir();
                            foreach ($jurnal as $key => $value) {
                                $siswa = Siswa::find()->where(['id_siswa' => $value->id_siswa])->one();
                                $sosial = JurnalSikap::find()->where(['id_siswa' => $siswa->id_siswa, 'sikap' => 'Sosial'])->one();
                                $spiritual = JurnalSikap::find()->where(['id_siswa' => $siswa->id_siswa, 'sikap' => 'Spiritual'])->one();
                                $newmodel->id_siswa = $value->id_siswa;
                                $newmodel->id_kelas = $siswa->id_kelas;
                                $newmodel->id_semester = $siswa->id_semester;
                                $newmodel->id_sekolah = $siswa->id_sekolah;
                                $newmodel->keterangan_sosial = $sosial->catatan_perilaku;
                                $newmodel->keterangan_spiritual = $spiritual->catatan_perilaku;
                                if ($newmodel->save()) {
                                    return "berhasil";
                                }else{
                                    return "Gagal";
                                }
                            }
                    }
                }
            }
            return "del";
    }
}