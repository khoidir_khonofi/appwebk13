<?php

namespace app\modules\api\controllers;

use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\User;
use app\models\Sekolah;
use app\models\Guru;
/**
 * Default controller for the `api` module
 */
class SiswaController extends ActiveController
{
	public $modelClass = 'app\models\Siswa';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
                'update' => ['GET'],
               	'testing' => ['POST']
            ],
    	];
    	return $behavior;
    }

    public function actionIndex($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }else{
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $model = Siswa::find()->where(['id_guru' => $guru->id_guru])->all();
        }
    	
    	foreach ($model as $key => $value) {
            $temp[] = [
                'nama' => $value->nama,
                'kelas' => $value->kelas->tingkat_kelas,
                'nama_kelas' => $value->kelas->nama,
                'foto' => 'http://192.168.43.13/tugasakhir/web/foto_siswa/'.$value->foto,
                'nis' => $value->nis,
                'alamat' => $value->alamat,
                'tempat_lahir' => $value->tempat_lahir,
                'tanggal_lahir' => $value->tanggal_lahir,
            ];
        }
        return $temp;
    }

    public function actionUpdate($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $model;
        }
    	
    }
    public function actionCreate()
    {
        $model = new Siswa();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $model;
        }
    }

    public function actionTesting(){
    	return \Yii::$app->request->bodyParams;
    }
}
