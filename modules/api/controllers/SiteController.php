<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Sekolah;
use app\models\Users;
use app\models\Guru;
use app\models\User;
use app\models\Kelas;
use app\models\Pengumuman;
use yii\rest\ActiveController;
use app\models\Siswa;
use yii\web\Response;
use yii\helpers\Url;

/**
 * Default controller for the `api` module
 */
class SiteController extends ActiveController
{
	public $modelClass = 'app\models\LoginForm';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
            ],
    	];
    	return $behavior;
    }
    public function actionIndex()
    {
        $model = User::find()->all();
        return $model;
    }

    public function actionLogin()
    {   
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->login()) {
            return [
            	'code' => Yii::$app->response->statusCode = 200,
            	'data' => [
            		'token' => Yii::$app->user->identity->authKey,
                    'username' => Yii::$app->user->identity->username,
                    'email' => Yii::$app->user->identity->email,
                    'role' => Yii::$app->user->identity->role,

          		]
            ];
        }
        
        return [
            	'code' => Yii::$app->response->statusCode = 400,
            	'data' => []
            ];
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->HomeUrl.'?r=web');
    }

    public function actionRegister()
    {
        
        $model = new Users();
        $guru = new Guru();
        $siswa = new Siswa();
        $sekolah = new Sekolah();

        if ($model->load(Yii::$app->request->getBodyParams(),'') AND $guru->load(Yii::$app->request->getBodyParams(),'') AND $siswa->load(Yii::$app->request->getBodyParams(),'') AND $sekolah->load(Yii::$app->request->getBodyParams(),'')) {
            $cekusername = Users::find()->where(['username' => $model->username])->one();
            if($cekusername){
                return "Username Telah Digunakan, coba username yang lain";
            }else{

            
                if($model->role == "Administrator"){
                    $npsn = Sekolah::find()->where(['npsn' => $sekolah->npsn])->one();
                    if($npsn){
                        return "Npsn sudah terdaftar";
                    }else{
                        $model->authKey = Yii::$app->security->generateRandomString();
                        $model->accessToken = "accessToken";
                        $model->status = "Aktif";
                        if ( $model->save()) {
                            $sekolah->id_user = $model->id_user;
                            $sekolah->save();
                            return  "Pendaftaran Berhasil";
                        }else{
                            return "Gagal";
                        }
                        
                    }

                }elseif($model->role=="Guru"){
                    $model->authKey = Yii::$app->security->generateRandomString();
                    $model->accessToken = "accessToken";
                    $model->status = "Tidak Aktif";
                    if ($model->save()) {
                        $guru->id_user = $model->id_user;
                        $guru->save();
                        return "Pendaftaran Berhasil dan tunggu persetujuan dari admin";
                    }else{
                        return "Gagal guru";
                    }
                    
                }else{
                    $walikelas = Kelas::find()->where(['id_kelas' => Yii::$app->request->post('id_kelas')])->one();
                    $model->authKey = Yii::$app->security->generateRandomString();
                    $model->accessToken = "accessToken";
                    $model->status = "Tidak Aktif";
                    if ($model->save()) {
                        $siswa->id_user = $model->id_user;
                        $siswa->id_semester = $walikelas->id_semester;
                        $siswa->id_guru = $walikelas->id_guru;
                        $siswa->save();
                        return  "Pendaftaran Berhasil dan tunggu persetujuan dari admin:)";
                    }else{
                        return "Gagal siswa";
                    }
                    
                }
            }
        }
        return "Gagal";
    }

    public function actionProfil($token)
    {
         $user = User::find()->where(['authKey' => $token])->one();
        if ($user->role == 'Administrator') {
            $model = Sekolah::find()->where(['id_user' => $user->id_user])->all();
            foreach ($model as $key => $value) {
                $temp[] = [
                    'nama_sekolah' => $value->nama_sekolah,
                    'foto' => $value->foto,
                ];
            }return $temp;
        }elseif ($user->role == 'Guru') {
            $model = Guru::find()->where(['id_user' => $user->id_user])->all();
            foreach ($model as $key => $value) {
                $temp[] =[
                    'nama' => $value->nama,
                    'foto' => $value->foto,
                    'nip' => $value->nip,
                    'id_guru' => $value->id_guru,
                ];
            }
            return $temp;
        }else{
            $model = Siswa::find()->where(['id_user' => $user->id_user])->all();
            foreach ($model as $key => $value) {
                $temp[] =[
                    'id_siswa' => $value->id_siswa,
                    'nama' => $value->nama,
                    'foto' => $value->foto,
                    'nis' => $value->nis,
                    'chat_id_telegram' => $value->chat_id_telegram,
                ];
            }
            return $temp;
            
        }
        
    }

    public function actionSelectsiswa($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        $model = Siswa::find()->where(['id_user' => $user->id_user])->one();
        return $model;
    }

    public function actionSelectguru($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        $model = Guru::find()->where(['id_user' => $user->id_user])->one();
        return $model;
    }

    public function actionUpdatesiswa($id)
    {
        $model = Siswa::find()->where(['id_siswa' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->save()) {
            return "Berhasil";
        }
        return "Gagal";
    }
     public function actionUpdateguru($id)
    {
        $model = Guru::find()->where(['id_guru' => $id])->one();
        if ($model->load(Yii::$app->request->getBodyParams(),'') && $model->save()) {
            return "Berhasil";
        }
        return "Gagal";
    }

    public function actionPilihsekolah()
    {
        $model = Sekolah::find()->all();
        return $model;
    }
    public function actionPilihguru()
    {
        $model = Guru::find()->all();
        return $model;
    }
    public function actionPilihkelas()
    {
        $kelas = Kelas::find()->where(['status' => 'Aktif'])->all();
        foreach ($kelas as $value) {
            $temp[] = [
                'id_sekolah' => $value->id_sekolah,
                'tingkat_kelas' => $value->tingkat_kelas,
                'id_kelas' => $value->id_kelas,
                'nama' => $value->nama,
                'id_guru' => $value->id_guru,
                'gurunama' => $value->guru->nama,
            ];
        }
        return $temp;
    }
     public function actionPilihwalikelas()
    {
       $guru = Kelas::find()->all();
            foreach ($guru as $value) {
            $temp[] = [
                'id_sekolah' => $value->id_sekolah,
                'tingkat_kelas' => $value->tingkat_kelas,
                'id_kelas' => $value->id_kelas,
                'nama' => $value->nama,
                'id_guru' => $value->id_guru,
                'nama' => $value->guru->nama,
            ];
        }
        return $temp;
    }

    public function actionPengumuman($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        if ($user->role == 'Administrator') {
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = Pengumuman::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
        }elseif ($user->role == 'Guru') {
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $model = Pengumuman::find()->where(['id_sekolah' => $guru->id_sekolah])->all();
        }else{
            $siswa = Siswa::find()->where(['id_user' => $user->id_user])->one();
            $model = Pengumuman::find()->where(['id_sekolah' => $siswa->id_sekolah])->all();
        }
        return $model;
    }
}