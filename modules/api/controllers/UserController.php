<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Sekolah;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\Siswa;
use app\models\Guru;
use yii\helpers\Url;
use app\models\User;
use app\models\Users;
/**
 * Default controller for the `api` module
 */
class UserController extends ActiveController
{
	public $modelClass = 'app\models\User';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
    	$actions = parent::actions();

        unset($actions['index'], $actions['view'], $actions['delete'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
    	$behavior = parent::behaviors();

    	$behavior['ContentNegotiator'] = [
    		'class' => ContentNegotiator::class,
    		'formats' => [
    			'application/json' => Response::FORMAT_JSON
    		],
    	];

    	$behavior['verbs'] = [
			'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'  => ['GET'],
            ],
    	];
    	return $behavior;
    }

    public function actionIndex($token)
    {
        $user = User::find()->where(['authKey' => $token])->one();
        if($user->role =="Administrator"){
            $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
            $model = Users::find()->joinWith('guru')->where(['guru.id_sekolah' => $sekolah->id_sekolah])->all();    
             
        }elseif($user->role == "Guru"){
            $guru = Guru::find()->where(['id_user' => $user->id_user])->one();
            $model = Users::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru])->all();
        }else{
            $model = Users::find()->where(['id_user' => $user->id_user])->all();
        }
        return $model;
    }

    public function actionSelect($id)
    {
        $model = User::find()->where(['id_user' => $id])->one();
        return $model;
    }

    
    public function actionLihatsiswa($id)
    {
        $model = User::find()->where(['id_user' => $id])->all();
        $temp = [];
        foreach ($model as $key => $value) {
           $temp[] = [
               
                'siswa' => [
                    'id' => $value->id_siswa,
                    'nama' => $value->nama,
                    'nis' => $value->nis,
                    'foto' => 'http://192.168.43.13/tugasakhir/web/foto_siswa/'.$value->foto,
                ],
                'user' => [
                    'id' => $value->id_user,
                ],
           ];
        }
        return $temp;
    }

    public function actionUpdate($id)
    {
        $model = User::find()->where(['id_user' => $id])->one();

        if ($model->load(Yii::$app->request->getBodyParams(),'')) {
            $model->role = Yii::$app->request->post('role');
            $model->status = Yii::$app->request->post('status');
            $model->email = Yii::$app->request->post('email');
            $model->username = Yii::$app->request->post('username');
            $model->password = Yii::$app->request->post('password');
           if ( $model->save()) {
               return "Berhasil update data";
           }else{
                return $model->errors;
           }
            
        }
        return $model->errors;
    }

    public function actionAktif($id)
    {
        $model = User::find()->where(['id_user' => $id])->one();
        $siswa = Siswa::find()->where(['id_user' => $id])->one();
        $siswa->status = 'Aktif';
        $model->status = 'Aktif';
        if ($model->save() && $siswa->save()) {
            return "Sukses";
        }
        return "gagal";
        
    }
    public function actionTidakaktif($id)
    {
        $model = User::find()->where(['id_user' => $id])->one();
        $siswa = Siswa::find()->where(['id_user' => $id])->one();
            $model->status = 'Tidak Aktif';
            $siswa->status = 'Tidak Aktif';
            if ($model->save() && $siswa->save()) {
                return "Sukses";
            }
            return "gagal";
    }

    public function actionDelete($id)
    {
        $siswa = Siswa::find()->where(['id_user' => $id])->one();
        $guru = Guru::find()->where(['id_user' => $id])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $id])->one();
        $model = User::find()->where(['id_user' => $id])->one();
        if ($siswa || $guru || $sekolah) {
            return "Data masih dipakai";
        }else{
             if ($model->delete()) {
                return "Sukses";
            }
            return "gagal";
        }
    }

    public function actionSekolah($token)
    {
            $modelUser = User::find()->where(['authKey' => $token])->one();
            if ($modelUser) {
                if($modelUser->role == 'Administrator'){
                    $model = Sekolah::find()->where(['id_user' => $modelUser->id_user])->one();
                    return $model->jenjang_pendidikan;
                }elseif ($modelUser->role == 'Guru') {
                    $model = Guru::find()->where(['id_user' => $modelUser->id_user])->one();
                    return $model->tblsekolah->jenjang_pendidikan;
                }else{
                    $model = Siswa::find()->where(['id_user' => $modelUser->id_user])->one();
                    return $model->tblsekolah->jenjang_pendidikan;
                }
            }
            return "Token kosong";
           
    }

    public function actionCreate()
    {
        $model = new User();
        $siswa = new Siswa();
        $guru = new Guru();
        if ($model->load(Yii::$app->request->getBodyParams(),'')) {
            # code...
        }
    }
        
    
}