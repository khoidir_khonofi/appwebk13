-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2021 at 09:04 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_k13`
--

-- --------------------------------------------------------

--
-- Table structure for table `bobot_penilaian`
--

CREATE TABLE `bobot_penilaian` (
  `id` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `bobot_nph` int(11) NOT NULL,
  `bobot_npts` int(11) NOT NULL,
  `bobot_npas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bobot_penilaian`
--

INSERT INTO `bobot_penilaian` (`id`, `id_sekolah`, `id_kelas`, `bobot_nph`, `bobot_npts`, `bobot_npas`) VALUES
(4, 14, 10, 2, 1, 1),
(5, 14, 15, 2, 1, 2),
(8, 14, 17, 2, 1, 1),
(9, 14, 13, 2, 1, 1),
(10, 15, 11, 2, 1, 1),
(12, 17, 20, 2, 1, 1),
(17, 14, 16, 2, 1, 1),
(19, 15, 12, 2, 1, 1),
(20, 17, 19, 2, 1, 1),
(21, 14, 9, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `esktrakurikuler`
--

CREATE TABLE `esktrakurikuler` (
  `id_ekstrakurikuler` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `kegiatan_ekstrakurikuler` varchar(200) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `esktrakurikuler`
--

INSERT INTO `esktrakurikuler` (`id_ekstrakurikuler`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `kegiatan_ekstrakurikuler`, `keterangan`, `created_at`, `updated_at`) VALUES
(3, 14, 36, 9, 2, 'Pramuka', 'sdsd', '2020-09-02 07:45:59', '2020-09-02 07:45:59'),
(4, 14, 39, 10, 6, 'Pramuka', 'erfs', '2020-09-02 08:09:23', '2020-09-02 08:09:23'),
(5, 14, 61, 9, 2, 'Pramuka', 'rajin', '2020-10-25 08:48:55', '2020-10-25 08:48:55'),
(6, 17, 45, 20, 8, 'Pramuka', 'sds', '2020-11-29 09:50:32', '2020-11-29 09:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `nip` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `id_user`, `id_sekolah`, `nip`, `nama`, `alamat`, `jenis_kelamin`, `jabatan`, `foto`) VALUES
(91, 143, 14, '23532', 'eka sumardi', 'kubu', 'Laki-laki', 'ter', '20140729_232151.jpg'),
(92, 145, 14, '6723547', 'diki fadli', 'sei majo', 'Laki-laki', 'pegawai', ''),
(93, 147, 15, '34w62346', 'evin', 'bkjb', 'Laki-laki', 'wali kelas A', ''),
(94, 151, 14, '287352378', 'rano', 'gsdfg', 'Laki-laki', 'rew', 'FB_IMG_14429781571964964.jpg'),
(95, 146, 15, '5464288', 'awaludin', 'jtyn', 'Laki-laki', 'testt', ''),
(96, 160, 14, '6325675', 'suci padila', 'kubu', 'Perempuan', 'guru bahasa inggris', 'IMG_7990397131713.jpeg'),
(97, 161, 14, '38476235', 'Deli Safitri', 'UK', 'Perempuan', 'pengajar', 'IMG20180728075203.jpg'),
(98, 164, 17, '346346', 'deni ansori', 'datuk rambai', '', 'guru fisika', 'FB_IMG_14429784826561822.jpg'),
(99, 165, 17, '47474677', 'Elfira', 'sei majo', 'Perempuan', 'guru kimia', 'DSC02084-1.jpg'),
(100, 169, 15, '745747', 'mimin', 'duri', 'Perempuan', 'guru mengajar', ''),
(103, 175, 15, '57457', 'anto', 'ujung paret', 'Laki-laki', 'mengajar', ''),
(105, 177, 15, '765454657687980', 'fauzi', 'sungai kubu', 'Laki-laki', 'mengajar', ''),
(106, 180, 17, '06856745', 'Rohim Widodo', 'sidodadi', 'Laki-laki', 'wali kelas', ''),
(107, 190, 17, '4357346', 'Rozana', 'nnah', 'Perempuan', 'guru sejarah', ''),
(112, 215, 17, '468348', 'ahmad daud', 'sungai majo', 'Laki-laki', 'guru ppkn', ''),
(113, 216, 17, '4357836', 'evi lestari', 'bagan batu', 'Perempuan', 'guru bahasa inggris', ''),
(114, 217, 15, '687698', 'tika', 'rumbai', 'Perempuan', 'pegawai', 'tika.png'),
(116, 270, 14, '', '', '', 'Laki-laki', '', ''),
(117, 272, 15, '', '', '', 'Laki-laki', '', ''),
(118, 316, 14, '', '', '', 'Laki-laki', '', ''),
(119, 331, 15, '', '', '', 'Laki-laki', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_sikap`
--

CREATE TABLE `jurnal_sikap` (
  `id_jurnal_sikap` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `sikap` enum('Sosial','Spiritual') NOT NULL,
  `catatan_perilaku` text NOT NULL,
  `butir_sikap` varchar(200) NOT NULL,
  `penilaian` enum('Sangat Baik','Perlu Bimbingan','Baik') NOT NULL,
  `tindak_lanjut` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurnal_sikap`
--

INSERT INTO `jurnal_sikap` (`id_jurnal_sikap`, `id_sekolah`, `id_kelas`, `id_semester`, `id_siswa`, `sikap`, `catatan_perilaku`, `butir_sikap`, `penilaian`, `tindak_lanjut`, `created_at`, `updated_at`) VALUES
(1, 14, 9, 2, 35, 'Sosial', 'bisa tanggung jawab', 'tanggung jawab', 'Sangat Baik', 'baik sekali\r\n', '2020-09-26 04:34:26', '2021-01-04 12:40:40'),
(5, 15, 12, 4, 46, 'Spiritual', 'hilhigi', 'giugiu', 'Baik', 'sdsfsf', '2020-12-14 08:36:50', '2021-01-05 12:28:17'),
(6, 14, 16, 1, 38, 'Spiritual', 'baik dalam melakukannya\r\n', 'sdfsf', 'Baik', 'sfs', '2020-12-15 06:58:47', '2021-01-04 12:40:56'),
(7, 17, 20, 0, 45, 'Sosial', 'sadakj', 'Baik dalam Disiplin', 'Perlu Bimbingan', 'secercah', '2020-12-16 06:01:14', '2020-12-16 06:01:14'),
(8, 14, 13, 0, 40, 'Spiritual', 'sdszd', 'Baik dalam Disiplin', 'Baik', 'sd', '2020-12-22 00:46:44', '2020-12-22 00:46:44'),
(9, 14, 9, 2, 35, 'Spiritual', 'sadsad', 'asdasda', 'Baik', 'sadas', '2021-01-03 09:23:00', '2021-01-04 12:41:18'),
(10, 14, 9, 2, 71, 'Spiritual', 'test gundul', 'tanggung jawab', 'Sangat Baik', 'lnjut\r\n', '2021-01-04 12:13:44', '2021-01-04 12:41:27'),
(11, 17, 19, 7, 44, 'Sosial', 'rajin bolos', 'rajin', 'Sangat Baik', '', '2021-01-15 02:22:01', '2021-01-15 02:22:01'),
(12, 17, 20, 8, 45, 'Spiritual', 'fgdfg', 'Berprilaku Syukur', 'Baik', 'dfdg', '2021-02-17 08:31:31', '2021-02-17 08:31:31'),
(13, 17, 19, 7, 44, 'Spiritual', 'u6ujyj', 'Berdoa sebelum dan sesudah melakukan kegiatan', 'Sangat Baik', '7u', '2021-02-17 11:52:39', '2021-02-17 11:52:39'),
(14, 14, 9, 2, 61, 'Spiritual', 'Test androis', 'Berprilaku Syukur', 'Perlu Bimbingan', 'Lanjut', '2021-02-18 13:38:15', '2021-02-18 13:38:15'),
(16, 14, 9, 2, 61, 'Sosial', 'Bdhd', 'Tanggung Jawab', 'Perlu Bimbingan', 'Udhhd', '2021-02-19 23:09:37', '2021-02-19 23:09:37');

-- --------------------------------------------------------

--
-- Table structure for table `kd_keterampilan`
--

CREATE TABLE `kd_keterampilan` (
  `id_kd` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `no_kd` varchar(20) NOT NULL,
  `judul` text NOT NULL,
  `mata_pelajaran` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kd_keterampilan`
--

INSERT INTO `kd_keterampilan` (`id_kd`, `kelas`, `no_kd`, `judul`, `mata_pelajaran`) VALUES
(6, 1, '4.1', 'Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', 'Bahasa Indonesia'),
(7, 1, '4.2', '4.2 Mempraktikkan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara meletakkan buku, jarak antara mata dan buku, gerakan tangan atas-bawah, kiri-kanan, latihan pelenturan gerakan tangan dengan gerakan menulis di udara/pasir/ meja, melemaskan jari dengan mewarnai, menjiplak, menggambar, membuat garis tegak, miring, lurus, dan lengkung, menjiplak berbagai bentuk gambar, lingkaran, dan bentuk huruf di tempat bercahaya terang) dengan benar ', 'Bahasa Indonesia'),
(8, 1, '4.3', 'Melafalkan bunyi vokal dan konsonan dalam kata bahasa Indonesia atau bahasa daerah ', 'Bahasa Indonesia'),
(9, 1, '4.4', ' Menyampaikan penjelasan (berupa gambar dan tulisan) tentang anggota tubuh dan panca indera serta perawatannya  menggunakan kosakata bahasa Indonesia dengan bantuan bahasa daerah secara lisan dan/atau tulis ', 'Bahasa Indonesia'),
(10, 1, '4.5', 'Mengemukakan penjelasan tentang cara memelihara kesehatan dengan pelafalan kosakata Bahasa Indonesia yang tepat dan dibantu dengan bahasa daerah', 'Bahasa Indonesia'),
(11, 1, '4.1', 'Menyajikan bilangan cacah sampai dengan 99 yang bersesuaian dengan banyak anggota kumpulan objek yang disajikan ', 'Matematika'),
(12, 1, '4.2', 'Menuliskan lambang bilangan  sampai dua angka yang menyatakan banyak anggota suatu kumpulan objek dengan ide nilai tempat  ', 'Matematika'),
(13, 1, '4.3', 'Mengurutkan bilangan-bilangan sampai dua angka dari bilangan terkecil ke bilangan terbesar atau sebaliknya  dengan menggunakan kumpulan benda-benda konkret', 'Matematika'),
(14, 1, '4.4', 'Menyelesaikan masalah kehidupan sehari-hari yang berkaitan dengan  penjumlahan dan pengurangan bilangan  yang melibatkan bilangan cacah sampai dengan 99  \r\n ', 'Matematika'),
(15, 1, '4.5', 'Memprediksi dan membuat pola bilangan yang berkaitan dengan kumpulan benda/gambar/gerakan atau lainnya ', 'Matematika'),
(16, 2, '4.1', ' Membaca dan menyajikan bilangan  cacah dan lambangnya berdasarkan nilai tempat dengan menggunakan model konkret ', 'Matematika'),
(17, 2, '4.2', 'Mengurutkan bilangan-bilangan   dari bilangan terkecil ke bilangan terbesar atau sebaliknya  ', 'Matematika'),
(18, 2, '4.3', 'Menyelesaikan masalah penjumlahan dan pengurangan bilangan yang melibatkan bilangan cacah sampai dengan 999 dalam kehidupan sehari-hari serta mengaitkan penjumlahan dan pengurangan ', 'Matematika'),
(19, 2, '4.4', 'Menyelesaikan masalah perkalian dan pembagian yang melibatkan bilangan cacah dengan hasil kali sampai dengan 100 dalam kehidupan sehari-hari serta mengaitkan perkalian dan pembagian ', 'Matematika'),
(20, 2, '4.5', ' Mengurutkan nilai mata uang serta mendemonstrasikan berbagai kesetaraan pecahan mata uang ', 'Matematika'),
(21, 2, '4.1', 'Menirukan ungkapan, ajakan, perintah, penolakan dalam cerita atau lagu anak-anak dengan bahasa yang santun ', 'Bahasa Indonesia'),
(22, 2, '4.2', 'Melaporkan penggunaan kosakata Bahasa Indonesia yang tepat atau bahasa daerah hasil pengamatan tentang keragaman benda berdasarkan bentuk dan wujudnya dalam bentuk teks tulis,  lisan, dan visual \r\nKOMPETENSI ', 'Bahasa Indonesia'),
(23, 2, '4.3', 'Melaporkan penggunaan kosakata Bahasa Indonesia yang tepat atau bahasa daerah hasil pengamatan tentang lingkungan geografis, kehidupan ekonomi, sosial dan budaya di lingkungan sekitar dalam bentuk teks tulis, lisan, dan visual ', 'Bahasa Indonesia'),
(24, 2, '4.4', ' Menyajikan penggunaan kosakata bahasa Indonesia yang tepat atau bahasa daerah hasil pengamatan tentang lingkungan sehat dan lingkungan tidak sehat di lingkungan sekitar serta cara menjaga kesehatan lingkungan dalam bentuk teks tulis, lisan, dan visual ', 'Bahasa Indonesia'),
(25, 2, '4.5', 'Membacakan teks puisi anak tentang alam dan lingkungan dalam bahasa Indonesia dengan lafal, intonasi, dan ekspresi yang tepat sebagai bentuk ungkapan diri', 'Bahasa Indonesia'),
(26, 3, '4.1', 'Menyajikan hasil informasi tentang konsep perubahan wujud benda dalam kehidupan seharihari dalam bentuk lisan, tulis, dan visual menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(27, 3, '4.2', ' Menyajikan hasil penggalian informasi tentang konsep sumber dan bentuk energi dalam bentuk tulis dan visual menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(28, 3, '4.3', 'Menyajikan hasil penggalian informasi tentang konsep perubahan cuaca  dan pengaruhnya terhadap kehidupan manusia dalam bentuk tulis menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(29, 3, '4.4', ' Menyajikan laporan tentang konsep ciri-ciri, kebutuhan (makanan dan tempat hidup), pertumbuhan dan perkembangan makhluk hidup yang ada di lingkungan setempat secara tertulis menggunakan kosakata baku dan kalimat efektif \r\n ', 'Bahasa Indonesia'),
(30, 3, '4.5', 'Menyajikan hasil wawancara tentang cara-cara perawatan tumbuhan dan hewan dalam bentuk tulis dan visual menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(31, 4, '4.1', 'Menata informasi yang didapat dari teks berdasarkan keterhubungan antargagasan ke dalam kerangka tulisan ', 'Bahasa Indonesia'),
(32, 4, '4.2', ' Menyajikan hasil pengamatan tentang keterhubungan antargagasan ke dalam tulisan ', 'Bahasa Indonesia'),
(33, 4, '4.3', ' Melaporkan hasil wawancara menggunakan kosakata baku dan kalimat efektif dalam bentuk teks tulis ', 'Bahasa Indonesia'),
(34, 4, '4.4', ' Menyajikan petunjuk penggunaan alat dalam bentuk teks tulis dan visual menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(35, 4, '4.5', ' Mengomunikasikan pendapat pribadi tentang isi buku sastra yang dipilih dan dibaca sendiri secara lisan dan tulis yang didukung oleh alasan ', 'Bahasa Indonesia'),
(36, 5, '4.1', ' Menyajikan hasil identifikasi pokok pikiran dalam teks tulis dan lisan secara lisan, tulis, dan visual', 'Bahasa Indonesia'),
(37, 5, '4.2', ' Menyajikan hasil klasifikasi informasi yang didapat dari buku yang dikelompokkan dalam aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana menggunakan kosakata baku ', 'Bahasa Indonesia'),
(38, 5, '4.3', ' Menyajikan ringkasan  teks penjelasan (eksplanasi) dari media cetak atau elektronik dengan menggunakan kosakata baku dan kalimat efektif secara lisan, tulis, dan visual \r\n \r\n ', 'Bahasa Indonesia'),
(39, 5, '4.4', 'Memeragakan kembali informasi yang disampaikan paparan iklan dari media cetak atau elektronik dengan bantuan lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(40, 5, '4.5', ' Memaparkan informasi penting dari teks narasi sejarah menggunakan aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana serta kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(41, 3, '4.1', ' Menyelesaikan masalah yang melibatkan penggunaan  sifat-sifat operasi hitung pada bilangan cacah ', 'Matematika'),
(42, 3, '4.2', 'Menggunakan bilangan cacah dan pecahan sederhana (seperti 1/2, 1/3 , dan 1/4 ) yang disajikan pada garis bilangan ', 'Matematika'),
(43, 3, '4.3', '  Menilai apakah suatu bilangan dapat dinyatakan sebagai jumlah, selisih, hasil kali, atau hasil bagi dua bilangan cacah ', 'Matematika'),
(44, 3, '4.4', 'Menyajikan pecahan sebagai bagian dari keseluruhan menggunakan benda-benda konkret ', 'Matematika'),
(45, 3, '4.5', ' Menyelesaikan masalah penjumlahan dan pengurangan pecahan berpenyebut sama ', 'Matematika'),
(46, 4, '4.1', 'Mengidentifikasi  pecahan-pecahan senilai  dengan gambar  dan model konkret ', 'Matematika'),
(47, 4, '4.2', ' Mengidentifikasi berbagai bentuk pecahan (biasa, campuran, desimal, dan persen) dan hubungan di antaranya ', 'Matematika'),
(48, 4, '4.3', ' Menyelesaikan masalah  penaksiran dari jumlah, selisih, hasil kali, dan hasil bagi dua bilangan cacah maupun pecahan dan desimal \r\n', 'Matematika'),
(49, 4, '4.4', 'Mengidentifikasi faktor dan kelipatan suatu bilangan ', 'Matematika'),
(50, 4, '4.5', 'Mengidentifikasi bilangan prima ', 'Matematika'),
(51, 4, '4.1', 'Menyajikan laporan hasil pengamatan tentang bentuk dan fungsi bagian tubuh hewan dan tumbuhan ', 'IPA'),
(52, 4, '4.2', '  Membuat skema siklus hidup beberapa jenis mahluk hidup yang ada di lingkungan sekitarnya, dan slogan upaya pelestariannya ', 'IPA'),
(53, 4, '4.3', '  Mendemonstrasikan manfaat gaya dalam kehidupan sehari-hari, misalnya gaya otot, gaya listrik, gaya magnet, gaya gravitasi, dan gaya gesekan ', 'IPA'),
(54, 4, '4.4', ' Menyajikan hasil percobaan tentang hubungan antara gaya dan gerak \r\n', 'IPA'),
(55, 4, '4.5', '  Menyajikan laporan hasil pengamatan dan penelusuran informasi tentang berbagai perubahan bentuk energi ', 'IPA'),
(56, 5, '4.1', ' Membuat model sederhana alat gerak manusia atau hewan ', 'IPA'),
(57, 5, '4.2', ' Membuat model sederhana organ pernapasan manusia ', 'IPA'),
(58, 5, '4.3', 'Menyajikan karya tentang konsep organ dan fungsi pencernaan pada hewan atau manusia. ', 'IPA'),
(59, 5, '4.4', ' Menyajikan karya tentang organ peredaran darah pada manusia ', 'IPA'),
(60, 5, '4.5', 'Membuat karya tentang konsep jaring-jaring makanan dalam suatu ekosistem ', 'IPA'),
(61, 6, '4.1', 'Menyajikan karya tentang perkembangangbiakan tumbuhan ', 'IPA'),
(62, 6, '4.2', 'Menyajikan karya tentang cara menyikapi ciri-ciri pubertas yang dialami', 'IPA'),
(63, 6, '4.3', 'Menyajikan karya tentang cara makhluk hidup menyesuaikan diri dengan lingkungannya, sebagai hasil penelusuran berbagai sumber ', 'IPA'),
(64, 6, '4.4', 'Melakukan percobaan rangkaian listrik sederhana secara seri dan paralel ', 'IPA'),
(65, 6, '4.5', 'Membuat laporan hasil percobaan tentang sifat-sifat magnet dan penerapannya dalam kehidupan sehari-hari ', 'IPA'),
(66, 1, '4.1', 'Menceritakan simbol-simbol sila Pancasila pada Lambang Garuda sila Pancasila ', 'PPKN'),
(67, 1, '4.2', 'Menceritakan kegiatan sesuai dengan aturan yang berlaku dalam kehidupan sehari-hari di rumah ', 'PPKN'),
(68, 1, '4.3', 'Menceritakan pengalaman kebersamaan dalam keberagaman  kehidupan individu di rumah', 'PPKN'),
(69, 1, '4.4', 'Menceritakan pengalaman kerjasama dalam keberagaman di rumah \r\n ', 'PPKN'),
(70, 2, '4.1', 'Menjelaskan hubungan gambar pada lambang Negara dengan silasila Pancasila ', 'PPKN'),
(71, 2, '4.2', ' Menceritakan kegiatan sesuai aturan dan tata tertib yang berlaku di sekolah ', 'PPKN'),
(72, 2, '4.3', ' Mengelompokkan jenis-jenis keberagaman karakteristik individu di sekolah ', 'PPKN'),
(73, 2, '4.4', ' Menceritakan pengalaman melakukan kegiatan yang mencerminkan persatuan dalam keberagaman di sekolah ', 'PPKN'),
(74, 3, '4.1', 'Menceritakan arti gambar pada lambang negara “Garuda Pancasila” ', 'PPKN'),
(75, 3, '4.2', ' Menyajikan hasil identifikasi kewajiban dan hak sebagai anggota keluarga dan warga sekolah ', 'PPKN'),
(76, 3, '4.3', ' Menyajikan makna keberagaman karakteristik individu di lingkungan sekitar ', 'PPKN'),
(77, 3, '4.4', ' Menyajikan bentuk-bentuk kebersatuan dalam keberagaman di lingkungan sekitar ', 'PPKN'),
(78, 4, '4.1', ' Menjelaskan makna hubungan simbol dengan sila-sila Pancasila sebagai satu kesatuan dalam kehidupan sehari-hari ', 'PPKN'),
(79, 4, '4.2', ' Menyajikan hasil identifikasi pelaksanaan kewajiban dan hak sebagai warga masyarakat dalam kehidupan sehari-hari ', 'PPKN'),
(80, 4, '4.3', 'Mengemukakan manfaat keberagaman karakteristik individu dalam kehidupan seharihari \r\n \r\n ', 'PPKN'),
(81, 4, '4.4', 'Menyajikan berbagai bentuk keberagaman suku bangsa, sosial, dan budaya di Indonesia yang terikat persatuan dan kesatuan ', 'PPKN'),
(82, 5, '4.1', 'Menyajikan hasil identifikasi nilai-nilai Pancasila dalam kehidupan sehari-hari ', 'PPKN'),
(83, 5, '4.2', 'Menjelaskan hak, kewajiban, dan tanggung jawab sebagai warga masyarakat dalam kehidupan sehari-hari ', 'PPKN'),
(84, 5, '4.3', ' Menyelenggarakan kegiatan yang mendukung keberagaman sosial budaya masyarakat ', 'PPKN'),
(85, 5, '4.4', ' Menyajikan hasil penggalian tentang manfaat persatuan dan kesatuan untuk membangun kerukunan', 'PPKN'),
(86, 6, '4.1', 'Menyajikan hasil analisis pelaksanaan nilai-nilai Pancasila dalam kehidupan sehari-hari ', 'PPKN'),
(87, 6, '4.2', ' Menyajikan hasil analisis pelaksanaan kewajiban, ha, dan tanggung jawab sebagai warga masyarakat beserta dampaknya dalam kehidupan sehari-hari ', 'PPKN'),
(88, 6, '4.3', ' Mengampanyekan manfaat keanekaragaman sosial, budaya, dan ekonomi ', 'PPKN'),
(89, 6, '4.4', '  Menyajikan hasil telaah persatuan dan kesatuan terhadap kehidupan berbangsa dan bernegara beserta dampaknya \r\n \r\n ', 'PPKN'),
(90, 4, '4.1', 'Menyajikan hasil identifikasi karakteristik ruang dan pemanfaatan sumber daya alam untuk kesejahteraan masyarakat  dari tingkat  kota/kabupaten sampai tingkat provinsi. \r\n3', 'IPS'),
(91, 4, '4.2', ' Menyajikan hasil identifikasi mengenai keragaman sosial, ekonomi, budaya, etnis, dan agama di provinsi setempat sebagai identitas bangsa Indonesia; serta hubungannya dengan karakteristik ruang. ', 'IPS'),
(92, 4, '4.3', ' Menyajikan hasil identifikasi kegiatan ekonomi dan hubungannya dengan berbagai bidang pekerjaan, serta kehidupan sosial dan budaya di lingkungan sekitar sampai provinsi. ', 'IPS'),
(93, 4, '4.4', 'Menyajikan hasil identifikasi kerajaan Hindu dan/atau Buddha dan/atau Islam di lingkungan daerah setempat, serta pengaruhnya pada kehidupan masyarakat masa kini. ', 'IPS'),
(94, 5, '4.1', 'Menyajikan hasil identifikasi karakteristik geografis Indonesia sebagai negara  kepulauan/maritim dan agraris serta pengaruhnya terhadap  kehidupan ekonomi, sosial, budaya, komunikasi, serta transportasi. ', 'IPS'),
(95, 5, '4.2', ' Menyajikan hasil analisis tentang interaksi manusia dengan lingkungan  dan pengaruhnya terhadap pembangunan sosial, budaya, dan ekonomi masyarakat Indonesia.  \r\n', 'IPS'),
(96, 5, '4.3', ' Menyajikan hasil analisis tentang peran ekonomi dalam upaya menyejahterakan kehidupan masyarakat di bidang sosial dan budaya untuk memperkuat kesatuan dan persatuan bangsa. ', 'IPS'),
(97, 5, '4.4', ' Menyajikan hasil identifikasi mengenai faktor-faktor penting penyebab penjajahan bangsa Indonesia dan upaya bangsa Indonesia dalam mempertahankan kedaulatannya. ', 'IPS'),
(98, 6, '4.1', 'Menyajikan hasil identifikasi karakteristik geografis dan kehidupan sosial budaya, ekonomi, dan politik di wilayah ASEAN. ', 'IPS'),
(99, 6, '4.2', 'Menyajikan hasil analisis mengenai perubahan sosial budaya dalam rangka modernisasi bangsa Indonesia', 'IPS'),
(100, 6, '4.3', ' Menyajikan hasil analisis tentang posisi dan peran Indonesia dalam kerja sama di bidang ekonomi, politik, sosial, budaya, teknologi, dan pendidikan dalam lingkup ASEAN. ', 'IPS'),
(101, 6, '4.4', ' Menyajikan laporan tentang makna proklamasi kemerdekaan, upaya mempertahankan kemerdekaan, dan upaya mengembangkan kehidupan kebangsaan yang sejahtera. ', 'IPS'),
(102, 1, '4.1', 'Mempraktikkan gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(103, 1, '4.2', 'Mempraktikkan gerak dasar nonlokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(104, 1, '4.3', 'Mempraktikkan pola gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional  ', 'PJOK'),
(105, 1, '4.4', 'Mempraktikkan sikap tubuh (duduk, membaca, berdiri, jalan), dan bergerak secara lentur serta seimbang dalam rangka pembentukan tubuh melalui permainan sederhana dan atau tradisional ', 'PJOK'),
(106, 1, '4.5', 'Mempraktikkan berbagai pola gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai ', 'PJOK'),
(107, 1, '4.6', 'Mempraktikkan gerak dasar lokomotor dan non-lokomotor sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama \r\n', 'PJOK'),
(108, 1, '4.7', 'Mempraktikkan berbagai pengenalan  aktivitas air dan menjaga keselamatan diri/orang lain dalam aktivitas air*** ', 'PJOK'),
(109, 1, '4.8', 'Menceritakan bagian-bagian tubuh, bagian tubuh yang boleh dan tidak boleh disentuh orang lain, cara menjaga kebersihannya, dan kebersihan pakaian ', 'PJOK'),
(110, 2, '4.1', 'Mempraktikkan variasi gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(111, 2, '4.2', 'Mempraktikkan variasi gerak dasar non-lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(112, 2, '4.3', 'Mempraktikkan variasi gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(113, 2, '4.4', ' Mempraktikkan prosedur bergerak secara seimbang,  lentur, dan kuat   dalam rangka pengembangan kebugaran jasmani melalui permainan sederhana dan atau tradisional', 'PJOK'),
(114, 2, '4.5', ' Mempraktikkan variasi berbagai pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai ', 'PJOK'),
(115, 2, '4.6', 'Mempraktikkan penggunaan variasi gerak dasar lokomotor dan nonlokomotor sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(116, 2, '4.7', 'Mempraktikkan penggunaan gerak dasar lokomotor, non-lokomotor, dan manipulatif dalam bentuk permainan, dan menjaga keselamatan diri/orang lain dalam aktivitas air*** ', 'PJOK'),
(117, 2, '4.8', 'Menceritakan manfaat pemanasan dan pendinginan, serta berbagai hal yang harus dilakukan dan dihindari sebelum, selama, dan setelah melakukan aktivitas fisik ', 'PJOK'),
(118, 2, '4.9', 'Menceritakan cara menjaga kebersihan lingkungan (tempat tidur, rumah, kelas, lingkungan sekolah). ', 'PJOK'),
(119, 3, '4.1', 'Mempraktikkan gerak kombinasi  gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(120, 3, '4.2', ' Mempraktikkan gerak kombinasi gerak dasar non-lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(121, 3, '4.3', ' Mempraktikkan kombinasi gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(122, 3, '4.4', 'Mempraktikkan bergerak secara seimbang,  lentur, lincah, dan berdaya tahan  dalam rangka pengembangan kebugaran jasmani melalui permainan sederhana dan atau tradisional', 'PJOK'),
(123, 3, '4.5', 'Mempraktikkan kombinasi berbagai pola gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai \r\n', 'PJOK'),
(124, 3, '4.6', 'Mempraktikkan penggunaan kombinasi gerak dasar lokomotor, non-lokomotor dan manipulatif sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(125, 3, '4.7', 'Mempraktikkan gerak dasar mengambang (water trappen)  dan meluncur di air serta menjaga keselamatan diri/orang lain dalam aktivitas air*** ', 'PJOK'),
(126, 3, '4.8', 'Menceritakan bentuk dan manfaat istirahat dan pengisian waktu luang untuk menjaga kesehatan ', 'PJOK'),
(127, 3, '4.9', ' Menceritakan perlunya memilih  makanan bergizi dan jajanan sehat untuk menjaga kesehatan tubuh ', 'PJOK'),
(128, 4, '4.1', 'Mempraktikkan variasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(129, 4, '4.2', 'Mempraktikkan variasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam permainan bola kecil sederhana dan atau tradisional* ', 'PJOK'),
(130, 4, '4.3', 'Mempraktikkan variasi pola dasar  jalan, lari,  lompat, dan lempar melalui permainan/olahraga yang dimodifikasi dan atau olahraga tradisional \r\n \r\n ', 'PJOK'),
(131, 4, '4.4', 'Mempraktikkan gerak dasar lokomotor dan non lokomotor untuk membentuk gerak dasar seni beladiri** ', 'PJOK'),
(132, 4, '4.5', 'Mempraktikkan berbagai aktivitas kebugaran jasmani melalui berbagai bentuk latihan; daya tahan, kekuatan, kecepatan,  dan kelincahan  untuk mencapai berat badan ideal  ', 'PJOK'),
(133, 4, '4.6', 'Mempraktikkan variasi dan kombinasi berbagai pola gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai ', 'PJOK'),
(134, 4, '4.7', 'Mempraktikkan variasi gerak dasar  langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama ', 'PJOK'),
(135, 4, '4.8', 'Mempraktikkan gerak dasar satu gaya renang *** ', 'PJOK'),
(136, 4, '4.9', 'Mendemonstrasikan cara penanggulangan jenis cidera secara sederhana saat melakukan aktivitas fisik dan dalam kehidupan seharihari. ', 'PJOK'),
(137, 4, '4.10', ' Mendemonstrasikan perilaku terpuji dalam pergaulan sehari-hari (antar teman sebaya, orang yang lebih tua, dan orang yang lebih muda) ', 'PJOK'),
(138, 5, '4.1', ' Mempraktikkan kombinasi gerak lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(139, 5, '4.2', ' Mempraktikkan kombinasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai permainan bola kecil sederhana dan atau tradisional* ', 'PJOK'),
(140, 5, '4.3', ' Mempraktikkan kombinasi gerak dasar  jalan, lari,  lompat, dan lempar melalui permainan/olahraga yang dimodifikasi dan atau olahraga tradisional \r\n', 'PJOK'),
(141, 5, '4.4', ' Mempraktikkan variasi gerak dasar lokomotor dan non lokomotor untuk membentuk gerak dasar seni beladiri** ', 'PJOK'),
(142, 5, '4.5', 'Mempraktikkan aktivitas latihan daya tahan jantung (cardio respiratory) untuk pengembangan kebugaran jasmani', 'PJOK'),
(143, 5, '4.6', ' Mempraktikkan kombinasi pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) untuk membentuk keterampilan dasar senam menggunakan alat ', 'PJOK'),
(144, 5, '4.7', 'Mempraktikkan  pengunaan kombinasi gerak dasar  langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(145, 5, '4.8', ' Mempraktikkan salah satu gaya renang dengan koordinasi yang baik pada jarak tertentu *** ', 'PJOK'),
(146, 5, '4.9', ' Menerapkan konsep pemeliharaan diri dan orang lain dari penyakit menular dan tidak menular ', 'PJOK'),
(147, 5, '4.10', ' Memaparkan bahaya merokok, meminum minuman keras, dan mengonsumsi narkotika,   zat-zat aditif (NAPZA) dan obat berbahaya  lainnya terhadap kesehatan tubuh ', 'PJOK'),
(148, 6, '4.1', ' Mempraktikkan variasi dan kombinasi gerak dasar lokomotor, non-lokomotor, dan manipulatif dengan kontrol yang baik dalam permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(149, 6, '4.2', ' Mempraktikkan variasi dan kombinasi gerak dasar lokomotor, non-lokomotor, dan manipulatif dengan kontrol yang baik dalam permainan bola kecil sederhana dan atau tradisional* ', 'PJOK'),
(150, 6, '4.3', ' Mempraktikkan variasi dan kombinasi gerak dasar jalan, lari, lompat, dan lempar dengan kontrol yang baik melalui permainan dan atau olahraga tradisional ', 'PJOK'),
(151, 6, '4.4', 'Mempraktikkan variasi dan kombinasi gerak dasar lokomotor, non lokomotor, dan manipulatif untuk membentuk gerak dasar seni beladiri** ', 'PJOK'),
(152, 6, '4.5', ' Mempratikkan latihan kebugaran jasmani dan pengukuran tingkat kebugaran jasmani pribadi secara sederhana (contoh: menghitung denyut nadi, menghitung kemampuan melakukan push up, menghitung kelenturan tungkai) ', 'PJOK'),
(153, 6, '4.6', 'Mempraktikkan rangkaian tiga pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dengan konsisten, tepat dan terkontrol dalam aktivitas senam \r\n', 'PJOK'),
(154, 6, '4.7', 'Mempraktikkan penggunaan variasi dan kombinasi gerak dasar rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama ', 'PJOK'),
(155, 6, '4.8', 'Mempraktikkan keterampilan salah satu gaya renang dan dasar-dasar penyelamatan diri*** ', 'PJOK'),
(156, 6, '4.9', ' Memaparkan perlunya pemeliharaan kebersihan alat reproduksi ', 'PJOK'),
(157, 1, '4.1', 'membuat karya ekspresi dua dan tiga dimensi ', 'Seni Budaya dan Prakarya'),
(158, 1, '4.2', ' menirukan elemen musik melalui lagu', 'Seni Budaya dan Prakarya'),
(159, 1, '4.3', 'meragakan gerak anggota tubuh melalui tari', 'Seni Budaya dan Prakarya'),
(160, 1, '4.4', 'membuat karya dari bahan alam ', 'Seni Budaya dan Prakarya'),
(161, 2, '4.1', 'membuat karya imajinatif dua dan tiga dimensi ', 'Seni Budaya dan Prakarya'),
(162, 2, '4.2', 'menampilkan pola irama sederhana melalui lagu anak-anak', 'Seni Budaya dan Prakarya'),
(163, 2, '4.3', ' meragakan gerak keseharian dan alam dalam tari ', 'Seni Budaya dan Prakarya'),
(164, 2, '4.4', ' membuat hiasan dari bahan alam dan buatan  ', 'Seni Budaya dan Prakarya'),
(165, 3, '4.1', ' membuat karya  dekoratif', 'Seni Budaya dan Prakarya'),
(166, 3, '4.2', 'menampilkan bentuk dan variasi irama melalui lagu  \r\n', 'Seni Budaya dan Prakarya'),
(167, 3, '4.3', 'meragakan dinamika gerak  tari ', 'Seni Budaya dan Prakarya'),
(168, 3, '4.4', 'membuat karya dengan teknik potong, lipat, dan sambung ', 'Seni Budaya dan Prakarya'),
(169, 4, '4.1', 'menggambar dan membentuk tiga dimensi', 'Seni Budaya dan Prakarya'),
(170, 4, '4.2', 'menyanyikan lagu dengan memperhatikan tempo dan tinggi rendah nada', 'Seni Budaya dan Prakarya'),
(171, 4, '4.3', 'meragakan gerak tari kreasi daerah', 'Seni Budaya dan Prakarya'),
(172, 4, '4.4', 'membuat karya kolase, montase, aplikasi, dan mozaik ', 'Seni Budaya dan Prakarya'),
(173, 5, '4.1', 'membuat gambar cerita ', 'Seni Budaya dan Prakarya'),
(174, 5, '4.2', 'menyanyikan lagu-lagu dalam berbagai tangga nada dengan iringan musik ', 'Seni Budaya dan Prakarya'),
(175, 5, '4.3', ' mempraktikkan pola lantai pada gerak tari kreasi dearah ', 'Seni Budaya dan Prakarya'),
(176, 5, '4.4', 'membuat karya seni rupa daerah ', 'Seni Budaya dan Prakarya'),
(177, 6, '4.1', 'membuat reklame \r\n', 'Seni Budaya dan Prakarya'),
(178, 6, '4.2', ' memainkan interval nada melalui lagu dan alat musik ', 'Seni Budaya dan Prakarya'),
(179, 6, '4.3', 'menampilkan tari kreasi daerah ', 'Seni Budaya dan Prakarya'),
(180, 6, '4.4', 'membuat patung  \r\n', 'Seni Budaya dan Prakarya'),
(181, 1, '4.6', ' Mengelompokkan bangun ruang dan bangun datar berdasarkan sifat tertentu dengan menggunakan berbagai benda konkret   ', 'Matematika'),
(182, 1, '4.7', ' Menyusun bangun-bangun datar untuk membentuk pola pengubinan ', 'Matematika'),
(183, 1, '4.8', 'Melakukan pengukuran panjang dan berat dalam  satuan tidak baku dengan menggunakan benda/situasi konkret ', 'Matematika'),
(184, 1, '4.9', 'Mengurutkan benda/kejadian/ keadaan berdasarkan panjang, berat, lamanya waktu, dan suhu ', 'Matematika'),
(185, 2, '4.6', 'Melakukan pengukuran panjang (termasuk jarak), berat, dan waktu dalam satuan baku, yang berkaitan dengan kehidupan sehari-hari  ', 'Matematika'),
(186, 2, '4.7', ' Menyajikan pecahan 1/2, 1/3 , dan 1/4 yang bersesuaian dengan bagian dari keseluruhan suatu benda konkret dalam kehidupan sehari-hari ', 'Matematika'),
(187, 2, '4.8', '  Mengidentifikasi ruas garis dengan menggunakan model konkret bangun datar dan bangun ruang  ', 'Matematika'),
(188, 2, '4.9', ' Mengklasifikasi bangun datar dan bangun ruang berdasarkan ciricirinya', 'Matematika'),
(189, 2, '4.10', ' Memprediksi pola barisan bangun datar dan bangun ruang menggunakan gambar atau benda konkret ', 'Matematika'),
(190, 3, '4.6', 'Menyelesaikan masalah yang berkaitan lama waktu suatu kejadian berlangsung ', 'Matematika'),
(191, 3, '4.7', ' Menyelesaikan masalah yang berkaitan dengan hubungan antarsatuan baku untuk panjang, berat, dan waktu yang umumnya digunakan dalam kehidupan sehari-hari ', 'Matematika'),
(192, 3, '4.8', ' Menyelesaikan masalah  luas dan volume dalam satuan tidak baku  dengan menggunakan benda konkret ', 'Matematika'),
(193, 3, '4.9', ' Mengidentifikasi simetri lipat dan simetri putar pada bangun datar menggunakan benda konkret \r\n', 'Matematika'),
(194, 3, '4.10', ' Menyajikan dan menyelesaikan masalah yang berkaitan dengan  keliling bangun datar ', 'Matematika'),
(195, 3, '4.11', ' Mengidentifikasi jenis sudut, (sudut siku-siku, sudut lancip, dan sudut tumpul), dan satuan pengukuran tidak baku', 'Matematika'),
(196, 3, '4.12', ' Mengelompokkan berbagai bangun datar berdasarkan sifat-sifat yang dimiliki ', 'Matematika'),
(197, 3, '4.13', ' Menyajikan data berkaitan dengan diri peserta didik yang disajikan dalam diagram gambar ', 'Matematika'),
(198, 4, '4.6', 'Menyelesaikan masalah yang berkaitan dengan faktor persekutuan, faktor persekutuan terbesar (FPB), kelipatan persekutuan, dan kelipatan persekutuan terkecil (KPK) dari dua bilangan berkaitan dengan kehidupan sehari-hari \r\n ', 'Matematika'),
(199, 4, '4.7', ' Menyelesaikan masalah  pembulatan hasil pengukuran panjang dan berat ke satuan terdekat \r\n', 'Matematika'),
(200, 4, '4.8', ' Mengidentifikasi segibanyak beraturan dan segibanyak tidak beraturan ', 'Matematika'),
(201, 4, '4.9', 'Menyelesaikan masalah berkaitan dengan  keliling dan luas persegi, persegipanjang, dan segitiga termasuk melibatkan pangkat dua dengan akar pangkat dua ', 'Matematika'),
(202, 4, '4.10', ' Mengidentifikasi hubungan antar garis (sejajar, berpotongan, berhimpit) menggunakan model konkret ', 'Matematika'),
(203, 4, '4.11', 'Mengumpulkan data diri peserta didik dan lingkungannya dan menyajikan dalam bentuk diagram batang ', 'Matematika'),
(204, 4, '4.12', ' Mengukur sudut pada bangun datar dalam satuan baku dengan menggunakan busur derajat ', 'Matematika'),
(205, 5, '4.1', 'Menyelesaikan masalah yang berkaitan dengan penjumlahan dan pengurangan dua pecahan dengan penyebut berbeda ', 'Matematika'),
(206, 5, '4.2', 'Menyelesaikan masalah yang berkaitan dengan perkalian dan pembagian pecahan dan desimal ', 'Matematika'),
(207, 5, '4.3', ' menyelesaikan masalah yang berkaitan dengan perbandingan dua besaran yang berbeda (kecepatan, debit) ', 'Matematika'),
(208, 5, '4.4', ' Menyelesaikan masalah yang berkaitan dengan skala pada denah ', 'Matematika'),
(209, 5, '4.5', ' Menyelesaikan masalah yang berkaitan dengan volume bangun ruang dengan menggunakan satuan volume (seperti kubus satuan) melibatkan pangkat tiga dan akar pangkat tiga ', 'Matematika'),
(210, 5, '4.6', ' Membuat jaring-jaring  bangun ruang sederhana (kubus dan balok) ', 'Matematika'),
(211, 5, '4.7', ' Menganalisis data yang berkaitan dengan diri peserta didik atau lingkungan sekitar serta cara pengumpulannya ', 'Matematika'),
(212, 5, '4.8', ' Mengorganisasikan dan menyajikan data  yang berkaitan dengan diri peserta didik dan membandingkan dengan data dari lingkungan sekitar dalam bentuk daftar, tabel, diagram gambar (piktogram), diagram batang, atau diagram garis \r\n \r\n ', 'Matematika'),
(213, 6, '4.1', 'Menggunakan konsep bilangan bulat negatif (termasuk mengggunakan garis bilangan) untuk menyatakan situasi seharihari \r\n', 'Matematika'),
(214, 6, '4.2', ' Menyelesaikan masalah yang berkaitan  dengan operasi penjumlahan, pengurangan, perkalian, dan pembagian yang melibatkan bilangan bulat negatif dalam kehidupan sehari-hari ', 'Matematika'),
(215, 6, '4.3', 'Menyelesaikan masalah yang berkaitan operasi hitung campuran yang melibatkan bilangan cacah, pecahan dan/atau desimal dalam berbagai bentuk sesuai urutan operasi', 'Matematika'),
(216, 6, '4.4', 'Mengidentifikasi titik pusat, jarijari, diameter, busur, tali busur, tembereng, dan juring ', 'Matematika'),
(217, 6, '4.5', 'Menaksir keliling dan luas lingkaran serta menggunakannya untuk menyelesaikan masalah ', 'Matematika'),
(218, 6, '4.6', 'Mengidentifikasi prisma, tabung, limas, kerucut, dan bola ', 'Matematika'),
(219, 6, '4.7', ' Mengidentifikasi bangun ruang yang merupakan gabungan dari beberapa bangun ruang, serta luas permukaan dan volumenya ', 'Matematika'),
(220, 6, '4.8', ' Menyelesaikan masalah yang berkaitan dengan modus, median, dan mean dari data tunggal dalam penyelesaian masalah ', 'Matematika'),
(221, 1, '4.6', 'Menggunakan kosakata bahasa Indonesia dengan ejaan yang tepat dan dibantu dengan bahasa daerah mengenai  berbagai jenis benda di lingkungan sekitar dalam teks tulis sederhana ', 'Bahasa Indonesia'),
(222, 1, '4.7', ' Menyampaikan penjelasan dengan kosakata Bahasa Indonesia dan dibantu dengan bahasa daerah mengenai peristiwa siang dan malam  dalam teks tulis dan gambar', 'Bahasa Indonesia'),
(223, 1, '4.8', 'Mempraktikan ungkapan  terima kasih, permintaan maaf, tolong, dan pemberian pujian, dengan menggunakan bahasa yang santun kepada orang lain  secara lisan dan tulis \r\n \r\n ', 'Bahasa Indonesia'),
(224, 1, '4.9', 'Menggunakan kosakata dan ungkapan yang tepat untuk perkenalan diri, keluarga, dan orang-orang di tempat tinggalnya secara sederhana dalam bentuk  lisan dan tulis ', 'Bahasa Indonesia'),
(225, 1, '4.10', ' Menggunakan kosakata yang tepat dalam percakapan tentang hubungan kekeluargaan dengan menggunakan bantuan gambar/bagan  silsilah keluarga ', 'Bahasa Indonesia'),
(226, 1, '4.11', ' Melisankan puisi anak atau syair lagu (berisi ungkapan kekaguman, kebanggaan, hormat kepada orang tua, kasih sayang, atau persahabatan) sebagai bentuk ungkapan diri ', 'Bahasa Indonesia'),
(227, 2, '4.6', '  Menyampaikan  ungkapanungkapan santun (menggunakan kata “maaf”, “tolong”) untuk hidup rukun dalam kemajemukan ', 'Bahasa Indonesia'),
(228, 2, '4.7', 'Menulis dengan tulisan tegak bersambung menggunakan huruf kapital (awal kalimat, nama bulan, hari, dan nama diri) serta tanda titik pada kalimat berita dan tanda tanya pada kalimat tanya dengan benar \r\n ', 'Bahasa Indonesia'),
(229, 2, '4.8', ' Menceritakan kembali teks dongeng binatang (fabel) yang menggambarkan sikap hidup rukun yang telah dibaca secara nyaring sebagai bentuk ungkapan diri', 'Bahasa Indonesia'),
(230, 2, '4.9', ' Menirukan kata sapaan dalam dongeng secara lisan dan tulis ', 'Bahasa Indonesia'),
(231, 2, '4.10', 'Menulis teks dengan menggunakan huruf kapital (nama Tuhan,  nama agama, nama orang), serta tanda  titik dan tanda tanya pada akhir kalimat dengan benar ', 'Bahasa Indonesia'),
(232, 3, '4.6', 'Meringkas informasi tentang perkembangan teknologi produksi, komunikasi, dan transportasi di lingkungan setempat secara tertulis menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(233, 3, '4.7', 'Menjelaskan konsep delapan arah mata angin dan pemanfaatannya dalam denah dalam bentuk tulis dan visual menggunakan kosakata baku  dan kalimat efektif ', 'Bahasa Indonesia'),
(234, 3, '4.8', ' Memeragakan pesan dalam dongeng sebagai bentuk ungkapan diri menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(235, 3, '4.9', 'Menyajikan hasil identifikasi tentang lambang/simbol (rambu lalu lintas, pramuka, dan lambang negara) beserta artinya dalam bentuk visual dan tulis menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(236, 3, '4.10', 'Memeragakan ungkapan atau kalimat saran, masukan,  dan penyelesaian masalah (sederhana) sebagai bentuk ungkapan diri menggunakan kosakata baku dan kalimat efektif yang dibuat sendiri ', 'Bahasa Indonesia'),
(237, 4, '4.6', ' Melisankan puisi hasil karya pribadi dengan lafal, intonasi, dan ekspresi yang tepat sebagai bentuk ungkapan diri ', 'Bahasa Indonesia'),
(238, 4, '4.7', ' Menyampaikan pengetahuan baru dari teks nonfiksi ke dalam tulisan dengan bahasa sendiri ', 'Bahasa Indonesia'),
(239, 4, '4.8', 'Menyampaikan hasil membandingkan pengetahuan lama dengan pengetahuan baru secara tertulis dengan bahasa sendiri ', 'Bahasa Indonesia'),
(240, 4, '4.9', 'Menyampaikan hasil identifikasi tokoh-tokoh yang terdapat pada teks fiksi secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(241, 4, '4.10', ' Menyajikan hasil membanding-kan watak setiap tokoh pada teks fiksi secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(242, 5, '4.6', ' Melisankan pantun hasil karya pribadi dengan lafal, intonasi, dan ekspresi yang tepat sebagai bentuk ungkapan diri ', 'Bahasa Indonesia'),
(243, 5, '4.7', ' Menyajikan konsep-konsep yang saling berkaitan pada teks nonfiksi ke dalam tulisan dengan bahasa sendiri ', 'Bahasa Indonesia'),
(244, 5, '4.8', ' Menyajikan kembali peristiwa atau tindakan dengan memperhatikan latar cerita yang terdapat pada teks fiksi ', 'Bahasa Indonesia'),
(245, 5, '4.9', 'Membuat surat undangan (ulang tahun, kegiatan sekolah, kenaikan kelas, dll.) dengan kalimat efektif dan memperhati-kan penggunaan ejaan \r\n \r\n ', 'Bahasa Indonesia'),
(246, 6, '4.1', ' Menyajikan simpulan secara lisan dan tulis dari teks laporan hasil pengamatan atau  wawancara yang diperkuat oleh bukti', 'Bahasa Indonesia'),
(247, 6, '4.2', ' Menyajikan hasil penggalian informasi  dari teks penjelasan (eksplanasi) ilmiah secara lisan, tulis, dan visual dengan menggunakan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(248, 6, '4.3', ' Menyampaikan pidato hasil karya pribadi dengan menggunakan kosakata baku dan kalimat efektif sebagai bentuk ungkapan diri ', 'Bahasa Indonesia'),
(249, 6, '4.4', 'Memaparkan informasi penting dari buku sejarah secara lisan, tulis, dan visual dengan menggunakan aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana serta memperhatikan penggunaan kosakata baku dan kalimat efektif ', 'Bahasa Indonesia'),
(250, 6, '4.5', ' Mengubah teks puisi ke dalam teks prosa dengan tetap memperhatikan makna isi teks puisi ', 'Bahasa Indonesia'),
(251, 6, '4.6', 'Mengisi teks formulir (pendaftaran, kartu anggota, pengiriman uang melalui bank/kantor pos,  daftar riwayat hidup, dll.) sesuai petunjuk pengisiannya ', 'Bahasa Indonesia'),
(252, 6, '4.7', ' Menyampaikan kemungkinan informasi yang diperoleh berdasarkan membaca judul teks nonfiksi secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(253, 6, '4.8', ' Menyampaikan hasil  membandingkan informasi yang diharapkan dengan informasi yang diperoleh setelah membaca teks nonfiksi secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(254, 6, '4.9', ' Menyampaikan penjelasan tentang tuturan dan tindakan tokoh serta penceritaan penulis dalam teks fiksi secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(255, 6, '4.10', 'Menyajikan hasil pengaitan peristiwa yang  dialami tokoh dalam cerita fiksi dengan pengalaman pribadi secara lisan, tulis, dan visual \r\n \r\n ', 'Bahasa Indonesia'),
(256, 7, '4.1', ' Menjelaskan isi teks deskripsi   objek (tempat wisata, tempat bersejarah, pentas seni daerah, kain tradisional, dll) yang didengar dan dibaca secara lisan, tulis, dan visual ', 'Bahasa Indonesia'),
(257, 7, '4.2', ' Menyajikan data, gagasan, kesan dalam bentuk teks deskripsi tentang objek (sekolah, tempat wisata,  tempat bersejarah, dan⁄atau suasana pentas seni daerah)  secara tulis dan lisan dengan memperhatikan struktur, kebahasaan baik secara lisan maupun tulis ', 'Bahasa Indonesia'),
(258, 7, '4.3', ' Menceritakan kembali isi teks narasi (cerita imajinasi) yang didengar dan dibaca secara lisan, tulis, dan visual', 'Bahasa Indonesia'),
(259, 7, '4.4', ' Menyajikan gagasan kreatif dalam bentuk cerita imajinasi secara lisan dan tulis dengan memperhatikan struktur, penggunaan bahasa, atau aspek lisan  ', 'Bahasa Indonesia'),
(260, 7, '4.5', ' Menyimpulkan isi teks prosedur tentang cara memainkan alat musik daerah, tarian daerah, cara membuat cinderamata,  dan/atau kuliner khas daerah) yang dibaca dan didengar   \r\n ', 'Bahasa Indonesia'),
(261, 7, '4.6', ' Menyajikan  data rangkaian kegiatan  ke dalam bentuk teks prosedur (tentang cara memainkan alat musik daerah, tarian daerah, cara membuat cinderamata, dll) dengan memperhatikan struktur, unsur kebahasaan, dan isi secara lisan dan tulis ', 'Bahasa Indonesia'),
(262, 7, '4.7', ' Menyimpulkan isi teks laporan hasil observasi berupa buku pengetahuan yang dibaca dan didengar ', 'Bahasa Indonesia'),
(263, 7, '4.8', ' Menyajikan  rangkuman teks laporan hasil observasi yang berupa buku pengetahuan  secara lisan dan tulis dengan memperhatikan kaidah kebahasaan atau aspek lisan', 'Bahasa Indonesia'),
(264, 7, '4.9', ' Membuat peta pikiran/sinopsis tentang isi buku nonfiksi/buku fiksi yang dibaca ', 'Bahasa Indonesia'),
(265, 7, '4.10', ' Menyajikan tanggapan secaralisan, tulis, dan visualterhadap isi buku fiksi/nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(266, 7, '4.11', 'Menyimpulkan isi (kabar, keperluan, permintaan, dan/atau permohonan) surat pribadi dan surat dinas yang dibaca atau diperdengarkan ', 'Bahasa Indonesia'),
(267, 7, '4.12', ' Menulis surat (pribadi dan dinas) untuk kepentingan resmi dengan memperhatikan struktur teks,  kebahasaan, dan isi ', 'Bahasa Indonesia'),
(268, 7, '4.13', ' Menyimpulkan isi puisi  rakyat (pantun, syair, dan bentuk puisi rakyat setempat) yang disajikan dalam bentuk tulis dan lisan ', 'Bahasa Indonesia'),
(269, 7, '4.14', 'Menelaah struktur dan kebahasaan puisi  rakyat (pantun, syair, dan bentuk puisi rakyat setempat) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(270, 7, '4.15', ' Menceritakan kembali isi cerita fabel/legenda daerah setempat yang dibaca/didengar ', 'Bahasa Indonesia'),
(271, 7, '4.16', ' Memerankan isi fabel/legenda   daerah setempat yang dibaca dan didengar ', 'Bahasa Indonesia'),
(272, 8, '4.1', ' Menyimpulkan isi berita (membanggakan dan memotivasi) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(273, 8, '4.2', ' Menyajikan data dan  informasi dalam bentuk berita  secara lisan dan tulis dengan memperhatikan struktur, kebahasaan,  atau  aspek lisan (lafal, intonasi, mimik, dan kinesik)', 'Bahasa Indonesia'),
(274, 8, '4.3', ' Menyimpulkan isi iklan, slogan, atau poster (membanggakan dan memotivasi) dari berbagai sumber ', 'Bahasa Indonesia'),
(275, 8, '4.4', ' Menyajikan  gagasan, pesan, dan ajakan dalam bentuk iklan, slogan, atau poster  secara lisan dan  tulis ', 'Bahasa Indonesia'),
(276, 8, '4.5', ' Menyimpulkan isi teks eksposisi  (artikel ilmiah populer dari koran dan majalah) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(277, 8, '4.6', ' Menyajikan gagasan dan pendapat ke dalam bentuk teks eksposisi  artikel ilmiah populer (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) secara lisan dan tertulis dengan memperhatikan struktur, unsur kebahasaan, dan aspek lisan ', 'Bahasa Indonesia'),
(278, 8, '4.7', ' Menyimpulkan unsur-unsur pembangun dan makna teks puisi  yang diperdengarkan atau dibaca ', 'Bahasa Indonesia'),
(279, 8, '4.8', ' Menyajikan gagasan, perasaan, dan pendapat dalam bentuk teks puisi  secara tulis/lisan dengan memperhatikan unsur-unsur pembangun puisi  ', 'Bahasa Indonesia'),
(280, 8, '4.9', ' Meringkas  isi  teks eksplanasi yang berupa proses terjadinya suatu fenomena dari  beragam  sumber yang didengar  dan dibaca', 'Bahasa Indonesia'),
(281, 8, '4.10', ' Menyajikan informasi dan data dalam bentuk teks eksplanasi proses terjadinya suatu fenomena secara lisan dan tulis dengan memperhatikan struktur, unsur kebahasaan, atau aspek lisan ', 'Bahasa Indonesia'),
(282, 8, '4.11', 'Menceritakan kembali isi teks ulasan tentang kualitas karya (film, cerpen, puisi, novel, karya seni daerah) yang dibaca atau didengar ', 'Bahasa Indonesia'),
(283, 8, '4.12', 'Menyajikan tanggapan tentang kualitas karya (film, cerpen, puisi, novel, karya seni daerah, dll.) dalam bentuk teks ulasan  secara lisan dan tulis dengan memperhatikan struktur, unsur kebahasaan, atau aspek lisan ', 'Bahasa Indonesia'),
(284, 8, '4.13', ' Menyimpulkan  isi saran, ajakan, arahan, pertimbangan tentang berbagai hal positif permasalahan aktual dari teks persuasi (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(285, 8, '4.14', ' Menyajikan teks persuasi (saran, ajakan, arahan, dan pertimbangan) secara tulis dan lisan dengan memperhatikan struktur, kebahasaan, atau aspek lisan ', 'Bahasa Indonesia'),
(286, 8, '4.15', 'Menginterpretasi drama (tradisional dan modern) yang dibaca dan ditonton/didengar ', 'Bahasa Indonesia'),
(287, 8, '4.16', 'Menyajikan drama dalam bentuk pentas atau naskah ', 'Bahasa Indonesia'),
(288, 8, '4.17', 'Membuat peta konsep/garis alur dari buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(289, 8, '4.18', ' Menyajikan tanggapan terhadap buku fiksi dan nonfiksi yang dibaca  secara lisan/tertulis ', 'Bahasa Indonesia'),
(290, 9, '4.1', 'Menyimpulkan tujuan, bahan/ alat, langkah, dan hasil dalam laporan percobaan yang didengar dan/atau dibaca  \r\n', 'Bahasa Indonesia'),
(291, 9, '4.2', 'Menyajikan tujuan, bahan/ alat, langkah, dan hasil dalam laporan percobaan  secara tulis dan lisan dengan memperhatikan kelengkapan data, struktur, aspek  kebahasaan, dan aspek lisan ', 'Bahasa Indonesia'),
(292, 9, '4.3', ' Menyimpulkan gagasan,  pandangan, arahan, atau pesan dalam pidato (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan/atau dibaca ', 'Bahasa Indonesia'),
(293, 9, '4.4', 'Menuangkan gagasan, pikiran, arahan atau pesan dalam pidato (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) secara lisan dan/atau tulis dengan memperhatikan struktur dan kebahasaan. ', 'Bahasa Indonesia'),
(294, 9, '4.5', ' Menyimpulkan unsur-unsur pembangun karya sastra dengan bukti yang mendukung dari cerita pendek yang dibaca atau didengar ', 'Bahasa Indonesia'),
(295, 9, '4.6', ' Mengungkapkan pengalaman dan gagasan dalam bentuk cerita pendek dengan memperhatikan struktur dan kebahasaan ', 'Bahasa Indonesia'),
(296, 9, '4.7', ' Menyimpulkan isi teks  tanggapan berupa  kritik, sanggahan, atau pujian  (mengenai lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(297, 9, '4.8', ' Mengungkapkan  kritik, sanggahan, atau pujian dalam bentuk teks tanggapan  secara lisan dan/atau tulis dengan memperhatikan struktur dan kebahasaan ', 'Bahasa Indonesia'),
(298, 9, '4.9', ' Menyimpulkan isi gagasan, pendapat, argumen yang mendukung dan yang kontra serta solusi atas permasalahan aktual  dalam teks diskusi yang didengar dan dibaca ', 'Bahasa Indonesia'),
(299, 9, '4.10', '   Menyajikan gagasan/pendapat, argumen yang mendukung dan yang kontra serta solusi atas permasalahan aktual  dalam teks diskusi dengan memperhatikan struktur dan aspek  kebahasaan, dan aspek lisan (intonasi, gesture, pelafalan)', 'Bahasa Indonesia'),
(300, 9, '4.11', 'Menyimpulkan isi ungkapan simpati, kepedulian, empati atau perasaan pribadi dalam bentuk  cerita inspiratif yang dibaca dan didengar', 'Bahasa Indonesia'),
(301, 9, '4.12', ' Mengungkapkan rasa simpati, empati, kepedulian, dan perasaan dalam bentuk cerita inspiratif dengan memperhatikan struktur cerita dan aspek kebahasaan ', 'Bahasa Indonesia'),
(302, 9, '4.13', 'Membuat peta konsep/garis alur dari buku fiksi dan nonfiksi yang dibaca  ', 'Bahasa Indonesia'),
(303, 9, '4.14', ' Menyajikan tanggapan terhadap buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(304, 9, '4.15', '  Membuat peta pikiran/ rangkuman alur tentang isi buku nonfiksi/ buku fiksi yang dibaca ', 'Bahasa Indonesia'),
(305, 9, '4.16', ' Menyajikan tanggapan terhadap isi buku fiksi nonfiksi yang dibaca', 'Bahasa Indonesia'),
(306, 7, '4.1', 'Menyajikan data hasil pengukuran dengan alat ukur yang sesuai pada diri sendiri, makhluk hidup lain, dan benda-benda di sekitar dengan menggunakan satuan tak baku dan satuan baku ', 'IPA'),
(307, 7, '4.2', ' Menyajikan hasil pengklasifikasian makhluk hidup dan benda di lingkungan sekitar berdasarkan karakteristik yang diamati \r\n \r\n ', 'IPA'),
(308, 7, '4.3', 'Menyajikan hasil penyelidikan atau karya tentang sifat larutan, perubahan fisika dan perubahan kimia, atau pemisahan campuran ', 'IPA'),
(309, 7, '4.4', 'Melakukan percobaan untuk menyelidiki pengaruh kalor terhadap suhu dan wujud benda serta perpindahan kalor ', 'IPA'),
(310, 7, '4.5', ' Menyajikan hasil percobaan tentang perubahan bentuk energi, termasuk fotosintesis ', 'IPA'),
(311, 7, '4.6', ' Membuat model struktur sel tumbuhan/hewan ', 'IPA'),
(312, 7, '4.7', ' Menyajikan hasil pengamatan terhadap interaksi makhluk hidup dengan lingkungan sekitarnya ', 'IPA'),
(313, 7, '4.8', ' Membuat tulisan tentang gagasan penyelesaian  masalah pencemaran di lingkungannya berdasarkan hasil pengamatan', 'IPA'),
(314, 7, '4.9', ' Membuat tulisan tentang gagasan adaptasi/penanggulangan masalah perubahan iklim ', 'IPA'),
(315, 7, '4.10', ' Mengomunikasikan upaya pengurangan resiko dan dampak bencana alam serta tindakan penyelamatan diri pada saat terjadi bencana sesuai dengan jenis ancaman bencana di daerahnya ', 'IPA'),
(316, 7, '4.11', ' Menyajikan karya tentang dampak rotasi dan revolusi bumi dan bulan bagi kehidupan di bumi, berdasarkan hasil pengamatan atau penelusuran berbagai sumber informasi ', 'IPA'),
(317, 8, '4.1', ' Menyajikan karya tentang berbagai gangguan pada sistem gerak, serta upaya menjaga kesehatan sistem gerak manusia', 'IPA'),
(318, 8, '4.2', ' Menyajikan hasil penyelidikan pengaruh gaya terhadap gerak benda ', 'IPA'),
(319, 8, '4.3', ' Menyajikan hasil penyelidikan atau pemecahan masalah tentang manfaat penggunaan pesawat sederhana dalam kehidupan sehari-hari \r\n ', 'IPA'),
(320, 8, '4.4', ' Menyajikan karya dari hasil penelusuran berbagai sumber informasi tentang teknologi yang terinspirasi dari hasil pengamatan struktur tumbuhan ', 'IPA'),
(321, 8, '4.5', 'Menyajikan hasil penyelidikan tentang pencernaan mekanis dan kimiawi', 'IPA'),
(322, 8, '4.6', 'Membuat karya tulis tentang dampak penyalahgunaan zat aditif dan zat adiktif bagi kesehatan ', 'IPA'),
(323, 8, '4.7', ' Menyajikan hasil percobaan pengaruh aktivitas (jenis, intensitas, atau durasi) pada frekuensi denyut jantung \r\n ', 'IPA');
INSERT INTO `kd_keterampilan` (`id_kd`, `kelas`, `no_kd`, `judul`, `mata_pelajaran`) VALUES
(324, 8, '4.8', ' Menyajikan data hasil percobaan untuk menyelidiki tekanan zat cair pada kedalaman tertentu, gaya apung, dan kapilaritas, misalnya dalam batang tumbuhan ', 'IPA'),
(325, 8, '4.9', 'Menyajikan karya tentang upaya menjaga kesehatan sistem pernapasan ', 'IPA'),
(326, 8, '4.10', '  Membuat karya tentang sistem ekskresi pada manusia dan penerapannya dalam menjaga kesehatan diri ', 'IPA'),
(327, 8, '4.11', ' Menyajikan hasil percobaan  tentang getaran, gelombang, dan bunyi ', 'IPA'),
(328, 8, '4.12', ' Menyajikan hasil percobaan tentang pembentukan bayangan pada cermin dan lensa ', 'IPA'),
(329, 9, '4.1', ' Menyajikan hasil penelusuran informasi dari berbagai sumber terkait kesehatan dan upaya pencegahan gangguan pada organ reproduksi ', 'IPA'),
(330, 9, '4.2', 'Menyajikan karya hasil perkembangbiakan pada tumbuhan ', 'IPA'),
(331, 9, '4.3', ' Menyajikan hasil penelusuran informasi dari berbagai sumber terkait tentang tanaman dan hewan hasil pemuliaan ', 'IPA'),
(332, 9, '4.4', ' Menyajikan hasil pengamatan tentang gejala listrik statis dalam kehidupan sehari-hari ', 'IPA'),
(333, 9, '4.5', ' Menyajikan hasil rancangan dan pengukuran berbagai rangkaian listrik ', 'IPA'),
(334, 9, '4.6', ' Membuat karya sederhana yang memanfaatkan prinsip elektromagnet dan/atau induksi elektromagnetik ', 'IPA'),
(335, 9, '4.7', 'Membuat salah satu produk bioteknologi konvensional yang ada di lingkungan sekitar ', 'IPA'),
(336, 9, '4.8', ' Menyajikan hasil penyelidikan tentang sifat dan pemanfaatan bahan dalam kehidupan sehari-hari ', 'IPA'),
(337, 9, '4.9', 'Menyajikan hasil penyelidikan tentang sifat-sifat tanah dan pentingnya tanah bagi kehidupan ', 'IPA'),
(338, 9, '4.10', 'Menyajikan karya tentang proses dan produk teknologi sederhana yang  ramah lingkungan ', 'IPA'),
(339, 7, '4.1', 'Menyaji hasil analisis proses\r\nperumusan dan penetapan Pancasila\r\nsebagai Dasar Negara', 'PPKN'),
(340, 7, '4.2', 'Mengampanyekan perilaku sesuai\r\nnorma-norma yang berlaku dalam\r\nkehidupan bermasyarakat untuk\r\nmewujudkan keadilan', 'PPKN'),
(341, 7, '4.3', 'Menjelaskan proses kesejarahan\r\nperumusan dan pengesahan\r\nUndang-undang Dasar Negara\r\nRepublik Indonesia Tahun 1945', 'PPKN'),
(342, 7, '4.4', 'Mendemonstrasikan hasil\r\nidentifikasi suku, agama, ras dan\r\nantargolongan dalam bingkai\r\nBhinneka Tunggal Ika', 'PPKN'),
(343, 7, '4.5', 'Menunjukkan bentuk-bentuk kerja\r\nsama di pelbagai bidang kehidupan\r\nmasyarakat', 'PPKN'),
(344, 7, '4.6', 'Melaksanakan penelitian sederhana\r\nuntuk mengilustrasikan\r\nkarakteristik daerah tempat\r\ntinggalnya sebagai bagian utuh dari\r\nNegara Kesatuan Republik Indonesia\r\nberdasarkan rancangan yang telah\r\ndibuat', 'PPKN'),
(345, 8, '4.1', 'Menyaji hasil telaah nilai-nilai\r\nPancasila sebagai dasar negara dan\r\npandanganhidup bangsa dalam\r\nkehidupan sehari-hari', 'PPKN'),
(346, 8, '4.2', 'Menyajikan hasil telaah makna,\r\nkedudukan dan fungsi UndangUndang Dasar Negara Republik\r\nIndonesia Tahun 1945 dalam\r\npenerapan kehidupan sehari-hari', 'PPKN'),
(347, 8, '4.3', 'Mendemonstrasikan pola\r\npengembangan tata urutan\r\nperaturan perundang-undangan\r\ndalam sistem hukum nasional\r\nnasional di Indonesia', 'PPKN'),
(348, 8, '4.4', 'Menyaji hasil penalaran tentang\r\ntokoh kebangkitan nasional dalam\r\nperjuangan kemerdekaan Republik\r\nIndonesia', 'PPKN'),
(349, 8, '4.5', 'Mengaitkan hasil proyeksi nilai-nilai\r\ndan semangat Sumpah Pemuda\r\nTahun 1928 dalam bingkai Bhineka\r\nTunggal Ika dengan kehidupan\r\nsehari-hari', 'PPKN'),
(350, 8, '4.6', 'Mengorganisasikan kegiatan\r\nlingkungan yang mencerminkan\r\nsemangat dan komitmen\r\nkebangsaan untuk memperkuat\r\nNegara Kesatuan Republik Indonesia', 'PPKN'),
(351, 9, '4.1', 'Merancang dan melakukan\r\npenelitian sederhana tentang\r\nperistiwa dan dinamika yang terjadi\r\ndi masyarakat terkait penerapan\r\nPancasila sebagai dasar negara dan\r\npandangan hidup bangsa', 'PPKN'),
(352, 9, '4.2', 'Menyajikan hasil sintesis isi alinea\r\ndan pokok pikiran yang terkandung\r\ndalam Pembukaan Undang-Undang\r\nDasar Negara Republik Indonesia\r\ntahun 1945', 'PPKN'),
(353, 9, '4.3', 'Memaparkan penerapan tentang\r\nbentuk dan kedaualatan negara\r\nsesuai Undang-Undang Dasar\r\nNegara Republik Indonesia tahun\r\n1945', 'PPKN'),
(354, 9, '4.4', 'Mendemonstrasikan hasil analisis\r\nprinsip persatuan dalam\r\nkeberagaman suku, agama, ras, dan\r\nantargolongan (SARA) dalam bingkai\r\nBhinneka Tunggal Ika', 'PPKN'),
(355, 9, '4.5', 'Menyampaikan hasil analisis prinsip\r\nharmoni dalam keberagaman suku,\r\nagama, ras, dan antargolongan\r\n(SARA) sosial, budaya, ekonomi, dan\r\ngender dalam bingkai Bhinneka\r\nTunggal Ika', 'PPKN'),
(356, 9, '4.6', 'Mengorganisasikan kegiatan\r\nlingkungan yang mencerminkan\r\nkonsep cinta tanah air dalam\r\nkonteks kehidupan sehari-hari', 'PPKN'),
(357, 7, '4.1', 'menyusun teks interaksi\r\ninterpersonal lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan menyapa,\r\nberpamitan, mengucapkan\r\nterimakasih, dan meminta maaf,\r\ndan menanggapinya dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(358, 7, '4.2', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait jati diri,\r\npendek dan sederhana, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(359, 7, '4.3', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait nama\r\nhari, bulan, nama waktu dalam\r\nhari, waktu dalam bentuk angka,\r\ntanggal, dan tahun, dengan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(360, 7, '4.4', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait nama\r\ndan jumlah binatang, benda, dan\r\nbangunan publik yang dekat dengan\r\nkehidupan siswa sehari-hari,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(361, 7, '4.5', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait sifat\r\norang, binatang, dan benda, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(362, 7, '4.6', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait tingkah\r\nlaku/tindakan/fungsi orang,\r\nbinatang, dan benda, dengan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(363, 7, '4.7', 'teks deskriptif\r\n4.7.1 menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks deskriptif lisan\r\ndan tulis, sangat pendek dan\r\nsederhana, terkait orang, binatang,\r\ndan benda\r\n4.7.2 menyusun teks deskriptif lisan dan\r\ntulis, sangat pendek dan sederhana,\r\nterkait orang, binatang, dan benda,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan, secara benar dan\r\nsesuai konteks', 'Bahasa Inggris'),
(364, 7, '4.8', 'menangkap makna secara\r\nkontekstual terkait dengan fungsi\r\nsosial dan unsur kebahasaan lirik\r\nlagu terkait kehidupan remaja\r\nSMP/MTs', 'Bahasa Inggris'),
(365, 8, '4.1', 'menyusun teks interaksi\r\ninterpersonal lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan meminta\r\nperhatian, mengecek pemahaman,\r\nmenghargai kinerja, serta meminta\r\ndan mengungkapkan pendapat, dan\r\nmenanggapinya dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(366, 8, '4.2', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkemampuan dan kemauan,\r\nmelakukan suatu tindakan, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(367, 8, '4.3', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkeharusan, larangan, dan\r\nhimbauan, dengan memperhatikan\r\nfungsi sosial, struktur teks, dan\r\nunsur kebahasaan yang benar dan\r\nsesuai konteks', 'Bahasa Inggris'),
(368, 8, '4.4', 'menyusun teks interaksi\r\ninterpersonal lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan menyuruh,\r\nmengajak, meminta ijin, dan\r\nmenanggapinya dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(369, 8, '4.5', 'menyusun teks khusus dalam\r\nbentuk greeting card, sangat pendek\r\ndan sederhana, terkait hari-hari\r\nspesial dengan memperhatikan\r\nfungsi sosial, struktur teks, dan\r\nunsur kebahasaan, secara benar\r\ndan sesuai konteks', 'Bahasa Inggris'),
(370, 8, '4.6', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkeberadaan orang, benda, binatang,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(371, 8, '4.7', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkeadaan/tindakan/\r\nkegiatan/kejadian yang\r\ndilakukan/terjadi secara rutin atau\r\nmerupakan kebenaran umum,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(372, 8, '4.8', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkeadaan/tindakan/kegiatan/kejadi\r\nan yang sedang dilakukan/\r\nberlangsung saat diucapkan,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(373, 8, '4.9', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nperbandingan jumlah dan sifat\r\norang, binatang, benda, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(374, 8, '4.10', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait keadaan\r\n/tindakan/ kegiatan/kejadian yang\r\ndilakukan/terjadi, rutin maupun\r\ntidak rutin, atau menjadi kebenaran\r\numum di waktu lampau, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(375, 8, '4.11', 'teks recount\r\n4.11.1 menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks recount lisan dan\r\ntulis, sangat pendek dan\r\nsederhana, terkait pengalaman\r\npribadi di waktu lampau (personal\r\nrecount)\r\n4.11.2 menyusun teks recount lisan dan\r\ntulis, sangat pendek dan\r\nsederhana, terkait pengalaman\r\npribadi di waktu lampau (personal\r\nrecount), dengan memperhatikan\r\nfungsi sosial, struktur teks, dan\r\nunsur kebahasaan, secara benar\r\ndan sesuai konteks', 'Bahasa Inggris'),
(376, 8, '4.12', 'teks pesan singkat dan\r\npengumuman/pemberitahuan\r\n(notice)\r\n4.12.1 menangkap makna secara\r\nkontekstual terkait dengan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan pesan singkat dan\r\npengumuman/pemberitahuan\r\n(notice) lisan dan tulis, sangat\r\npendek dan sederhana, terkait\r\nkegiatan sekolah\r\n4.12.2 menyusun teks khusus dalam\r\nbentuk pesan singkat dan\r\npengumuman/pemberitahuan\r\n(notice), sangat pendek dan\r\nsederhana, terkait kegiatan\r\nsekolah, dengan memperhatikan\r\nfungsi sosial, struktur teks, dan\r\nunsur kebahasaan, secara benar\r\ndan sesuai konteks', 'Bahasa Inggris'),
(377, 8, '4.13', 'menangkap makna secara\r\nkontekstual terkait fungsi sosial\r\ndan unsur kebahasaan lirik lagu\r\nterkait kehidupan remaja\r\nSMP/MTs', 'Bahasa Inggris'),
(378, 9, '4.1', 'menyusun teks interaksi\r\ninterpersonal lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan menyatakan\r\nharapan, doa, dan ucapan selamat\r\natas suatu kebahagiaan dan\r\nprestasi, dan menanggapinya,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(379, 9, '4.2', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait maksud,\r\ntujuan, persetujuan melakukan\r\nsuatu tindakan/kegiatan, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(380, 9, '4.3', 'menangkap makna secara\r\nkontekstual terkait dengan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan teks khusus dalam\r\nbentuk label pendek dan sederhana,\r\nterkait obat/makanan/minuman', 'Bahasa Inggris'),
(381, 9, '4.4', 'menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks prosedur lisan dan\r\ntulis, sangat pendek dan sederhana,\r\ndalam bentuk resep dan manual', 'Bahasa Inggris'),
(382, 9, '4.5', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait\r\nkeadaan/tindakan/\r\nkegiatan/kejadian yang sedang\r\ndilakukan/terjadi pada saat ini,\r\nwaktu lampau, dan waktu yang\r\nakan datang, dengan\r\nmemperhatikan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(383, 9, '4.6', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait dengan\r\nkeadaan/ tindakan/kegiatan/\r\nkejadian yang sudah/telah\r\ndilakukan/terjadi di waktu lampau\r\ndikaitkan dengan keadaan\r\nsekarang, tanpa menyebutkan\r\nwaktu terjadinya secara spesifik,\r\ndengan memperhatikan fungsi\r\nsosial, struktur teks, dan unsur\r\nkebahasaan yang benar dan sesuai\r\nkonteks', 'Bahasa Inggris'),
(384, 9, '4.7', 'menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks naratif, lisan dan\r\ntulis, sangat pendek dan sederhana,\r\nterkait fairy tales', 'Bahasa Inggris'),
(385, 9, '4.8', 'menyusun teks interaksi\r\ntransaksional lisan dan tulis sangat\r\npendek dan sederhana yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait keadaan\r\n/tindakan/ kegiatan/ kejadian\r\ntanpa perlu menyebutkan\r\npelakunya dengan memperhatikan\r\nfungsi sosial, struktur teks dan\r\nunsur kebahasaan yang benar dan\r\nsesuai konteks. (perhatikan unsur\r\nkebahasaan passive voice)', 'Bahasa Inggris'),
(386, 9, '4.9', 'teks information report\r\n4.9.1 menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks information report\r\nlisan dan tulis, sangat pendek dan\r\nsederhana, terkait topik yang\r\ntercakup dalam mata pelajaran lain\r\ndi Kelas IX\r\n4.9.2 menyusun teks information report\r\nlisan dan tulis, sangat pendek dan\r\nsederhana, terkait topik yang\r\ntercakup dalam mata pelajaran lain\r\ndi Kelas IX, dengan memperhatikan\r\nfungsi sosial, struktur teks, dan\r\nunsur kebahasaan, secara benar\r\ndan sesuai konteks', 'Bahasa Inggris'),
(387, 9, '4.10', 'menangkap makna secara\r\nkontekstual terkait fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks khusus dalam\r\nbentuk iklan, pendek dan\r\nsederhana, terkait produk dan jasa', 'Bahasa Inggris'),
(388, 9, '4.11', 'menangkap makna secara\r\nkontekstual terkait fungsi sosial dan\r\nunsur kebahasaan lirik lagu terkait\r\nkehidupan remaja SMP/MTs', 'Bahasa Inggris'),
(389, 7, '4.1', 'menggambar flora, fauna, dan alam\r\nbenda', 'Seni Rupa'),
(390, 7, '4.2', 'menggambar gubahan flora, fauna,\r\ndan bentuk geometrik menjadi\r\nragam hias', 'Seni Rupa'),
(391, 7, '4.3', 'membuat karya dengan berbagai\r\nmotif ragam hias pada bahan\r\nbuatan', 'Seni Rupa'),
(392, 7, '4.4', 'membuat karya dengan berbagai\r\nmotif ragam hias pada bahan alam', 'Seni Rupa'),
(393, 7, '4.1', 'menyanyikan lagu dengan satu\r\nsuara secara berkelompok dalam\r\nbentuk unisono', 'Seni Musik'),
(394, 7, '4.2', 'menyanyikan lagu dengan dua\r\nsuara atau lebih dalam bentuk\r\nkelompok vokal', 'Seni Musik'),
(395, 7, '4.3', 'memainkan alat musik sederhana\r\nsecara perorangan', 'Seni Musik'),
(396, 7, '4.4', 'memainkan ansamble musik\r\nsejenis dan campuran', 'Seni Musik'),
(397, 7, '4.1', 'memeragakan gerak tari\r\nberdasarkan unsur ruang waktu\r\ndan tenaga', 'Seni Tari'),
(398, 7, '4.2', 'memeragakan gerak tari\r\nberdasarkan ruang waktu dan\r\ntenaga sesuai iringan', 'Seni Tari'),
(399, 7, '4.3', 'memeragakan gerak tari sesuai\r\ndengan level dan pola lantai', 'Seni Tari'),
(400, 7, '4.4', 'memeragakan gerak tari\r\nberdasarkan level dan pola lantai\r\nsesuai iringan', 'Seni Tari'),
(401, 7, '4.1', 'emeragakan adegan fragmen\r\nsesuai konsep, teknik dan prosedur\r\nseni peran', 'Seni Teater'),
(402, 7, '4.2', 'menyusun naskah sesuai kaidah\r\npementasan fragmen', 'Seni Teater'),
(403, 7, '4.3', 'merancang pementasan fragmen\r\nsesuai konsep, teknik dan\r\nprosedur', 'Seni Teater'),
(404, 7, '4.4', 'mementaskan fragmen sesuai\r\nkonsep, teknik, dan prosedur', 'Seni Teater'),
(405, 8, '4.1', 'menggambar menggunakan model\r\ndengan berbagai bahan dan teknik\r\nberdasarkan pengamatan', 'Seni Rupa'),
(406, 8, '4.2', 'menggambar illustrasi dengan\r\nteknik manual atau digital', 'Seni Rupa'),
(407, 8, '4.3', 'membuat poster dengan berbagai\r\nbahan dan teknik', 'Seni Rupa'),
(408, 8, '4.4', 'menggambar komik dengan\r\nberbagai teknik', 'Seni Rupa'),
(409, 8, '4.1', 'menyanyikan lagu-lagu daerah\r\nyang sesuai dengan teknik dan\r\ngayanya sesuai dialektika atau\r\nintonasi kedaerahan', 'Seni Musik'),
(410, 8, '4.2', 'menyanyikan lagu-lagu daerah\r\ndengan dua suara atau lebih secara\r\nberkelompok', 'Seni Musik'),
(411, 8, '4.3', 'memainkan salah satu alat musik\r\ntradisional secara perorangan', 'Seni Musik'),
(412, 8, '4.4', 'memainkan alat-alat musik\r\ntradisional secara berkelompok', 'Seni Musik'),
(413, 8, '4.1', 'memeragakan keunikan gerak tari\r\ntradisional dengan menggunakan\r\nunsur pendukung tari', 'Seni Tari'),
(414, 8, '4.2', 'memeragakan tari tradisional\r\ndengan menggunakan unsur\r\npendukung tari sesuai iringan', 'Seni Tari'),
(415, 8, '4.3', 'memeragakan cara menerapkan\r\ngerak tari tradisional berdasarkan\r\npola lantai dengan menggunakan\r\nunsur pendukung tari', 'Seni Tari'),
(416, 8, '4.4', 'memeragakan tari tradisional\r\nberdasarkan pola lantai dengan\r\nmenggunakan unsur pendukung\r\ntari sesuai iringan', 'Seni Tari'),
(417, 8, '4.1', 'meragakan gerak pantomim sesuai\r\nkonsep, teknik, dan prosedur seni\r\nperan', 'Seni Teater'),
(418, 8, '4.2', 'menyusun naskah sesuai kaidah\r\npementasan pantomim', 'Seni Teater'),
(419, 8, '4.3', 'merancang pementasan pantomim\r\nsesuai konsep, teknik dan prosedur', 'Seni Teater'),
(420, 8, '4.4', 'mementaskan pantomim sesuai\r\nkonsep, teknik, dan prosedur', 'Seni Teater'),
(421, 9, '4.1', 'membuat karya seni lukis dengan\r\nberbagai bahan dan teknik', 'Seni Rupa'),
(422, 9, '4.2', 'membuat karya seni patung dengan\r\nberbagai bahan dan teknik', 'Seni Rupa'),
(423, 9, '4.3', 'membuat karya seni grafis dengan\r\nberbagai bahan dan teknik', 'Seni Rupa'),
(424, 9, '4.4', 'menyelenggarakan pameran seni\r\nrupa', 'Seni Rupa'),
(425, 9, '4.1', 'mengembangkan ornamentasi\r\nritmis maupun melodis lagu dalam\r\nbentuk vokal solo/tunggal', 'Seni Musik'),
(426, 9, '4.2', 'mengembangkan ornamentasi\r\nritmis maupun melodis lagu dalam\r\nbentuk kelompok vokal', 'Seni Musik'),
(427, 9, '4.3', 'memainkan karya-karya musik\r\npopuler dengan vokal dan atau alat\r\nmusik secara individual', 'Seni Musik'),
(428, 9, '4.4', 'menampilkan hasil pengembangan\r\nornamentasi ritmis maupun\r\nmelodis musik populer dalam\r\nbentuk ansambel', 'Seni Musik'),
(429, 9, '4.1', 'memeragakan keunikan gerak tari\r\nkreasi berdasarkan unsur\r\npendukung tari', 'Seni Tari'),
(430, 9, '4.2', 'memeragakan tari kreasi dengan\r\nmenggunakan unsur pendukung\r\ntari sesuai iringan', 'Seni Tari'),
(431, 9, '4.3', 'memeragakan cara menerapkan\r\ngerak tari kreasi berdasarkan pola\r\nlantai dengan menggunakan unsur\r\npendukung tari', 'Seni Tari'),
(432, 9, '4.4', 'memeragakan tari kreasi\r\nberdasarkan pola lantai dengan\r\nmenggunakan unsur pendukung\r\ntari sesuai iringan', 'Seni Tari'),
(433, 9, '4.1', 'memeragakan adegan drama\r\nmusikal dan/atau operet sesuai\r\nkonsep, teknik dan prosedur seni\r\nperan', 'Seni Teater'),
(434, 9, '4.2', 'memeragakan adegan drama\r\nmusikal dan/atau operet sesuai\r\nkonsep, teknik dan prosedur seni\r\nperan', 'Seni Teater'),
(435, 9, '4.3', 'merancang pementasan drama\r\nmusikal dan atau operet sesuai\r\nkonsep, teknik, dan prosedur', 'Seni Teater'),
(436, 9, '4.4', 'mementaskan drama musikal\r\ndan/atau operet sesuai konsep,\r\nteknik, dan prosedur', 'Seni Teater'),
(437, 1, '4.1', 'melafalkan huruf-huruf hijaiyyah dan harakatnya secara lengkap', 'Pendidikan Agama Islam dan BP'),
(438, 1, '4.2.1', 'melafalkan Q.S. al-Fatihah dan Q.S. al-Ikhlas dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(439, 1, '4.3', 'melafalkan Q.S. al-Fatihah dan Q.S. al-Ikhlas dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(440, 1, '4.2.2', 'menunjukkan hafalan Q.S. alFatihah dan Q.S. al-Ikhlas dengan\r\nbenar dan jelas', 'Pendidikan Agama Islam dan BP'),
(441, 1, '4.4', 'menunjukkan bukti-bukti keesaan Allah Swt. berdasarkan pengamatan terhadap dirinya dan makhluk ciptaan-Nya yang dijumpai di sekitar rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(442, 1, '4.5', 'melafalkan al-Asmau al-Husna: arRahman, ar-Rahim, danal-Malik', 'Pendidikan Agama Islam dan BP'),
(443, 1, '4.6', 'melafalkan dua kalimat syahadat dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(444, 1, '4.7', 'melafalkan doa sebelum dan sesudah belajar dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(445, 1, '4.8', 'mencontohkan perilaku hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(446, 1, '4.9', 'mencontohkan cara berkata yang baik, sopan, dan santun', 'Pendidikan Agama Islam dan BP'),
(447, 1, '4.10', 'mencontohkan perilaku bersyukur, pemaaf, jujur, dan percaya diri', 'Pendidikan Agama Islam dan BP'),
(448, 1, '4.11', 'mempraktikkan tata cara bersuci', 'Pendidikan Agama Islam dan BP'),
(449, 1, '4.12.1', 'melaksanakan salat dan kegiatan\r\nagama di sekitar rumahnya melalui\r\npengamatan', 'Pendidikan Agama Islam dan BP'),
(450, 1, '4.12.2', 'mencontohkan kegiatan agama di sekitar rumahnya', 'Pendidikan Agama Islam dan BP'),
(451, 1, '4.13', 'menceritakan kisah keteladanan Nabi Adam a.s.', 'Pendidikan Agama Islam dan BP'),
(452, 1, '4.14', 'menceritakan kisah keteladanan Nabi Idris a.s.', 'Pendidikan Agama Islam dan BP'),
(453, 1, '4.15', 'menceritakan kisah keteladanan Nabi Nuh a.s.', 'Pendidikan Agama Islam dan BP'),
(454, 1, '4.16', 'menceritakan kisah keteladanan Nabi Hud a.s.', 'Pendidikan Agama Islam dan BP'),
(455, 1, '4.17', 'menceritakan kisah keteladanan\r\nNabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(456, 2, '4.1', 'melafalkan huruf hijaiyyah bersambung sesuai dengan makharijul huruf', 'Pendidikan Agama Islam dan BP'),
(457, 2, '4.2.1', 'melafalkan Q.S. an-Nas dan Q.S. al- ‘Asr dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(458, 2, '4.2.2', 'menunjukkan hafalan Q.S. an-Nas dan Q.S. al-‘Asr dengan benar dan jelas', 'Pendidikan Agama Islam dan BP'),
(459, 2, '4.3', 'menunjukkan perilaku rajin belajar sebagai implementasi pemahaman makna Hadis yang terkait dengan anjuran menuntut ilmu', 'Pendidikan Agama Islam dan BP'),
(460, 2, '4.4', 'menunjukkan perilaku hidupbersih dan sehat sebagai implementasi pemahaman makna Hadis tentang kebersihan dan kesehatan', 'Pendidikan Agama Islam dan BP'),
(461, 2, '4.5', 'melafalkan al-Asmau al-Husna: alQuddus, as-Salam, dan al-Khaliq', 'Pendidikan Agama Islam dan BP'),
(462, 2, '4.6', 'melafalkan doa sebelum dan sesudah makan', 'Pendidikan Agama Islam dan BP'),
(463, 2, '4.7', 'mencontohkan perilaku kasih sayang kepada sesama', 'Pendidikan Agama Islam dan BP'),
(464, 2, '4.8', 'mencontohkan sikap kerja sama dan saling tolong menolong', 'Pendidikan Agama Islam dan BP'),
(465, 2, '4.9', 'mempraktikkan wudu dan doanya dengan tertib dan benar', 'Pendidikan Agama Islam dan BP'),
(466, 2, '4.10', 'mempraktikkan salat dengan tata cara dan bacaan yang benar', 'Pendidikan Agama Islam dan BP'),
(467, 2, '4.11', 'menceritakan kisah keteladanan Nabi Saleh a.s.', 'Pendidikan Agama Islam dan BP'),
(468, 2, '4.12', 'menceritakan kisah keteladanan Nabi Lut a.s.', 'Pendidikan Agama Islam dan BP'),
(469, 2, '4.13', 'menceritakan kisah keteladanan Nabi Ishaq a.s.', 'Pendidikan Agama Islam dan BP'),
(470, 2, '4.14', 'menceritakan kisah keteladanan Nabi Ya’qub a.s.', 'Pendidikan Agama Islam dan BP'),
(471, 2, '4.15', 'menceritakan kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(472, 3, '4.1.1', 'membaca kalimat-kalimat dalam Q.S. an-Nasr dan al-Kausar dengan benar', 'Pendidikan Agama Islam dan BP'),
(473, 3, '4.1.2', 'menulis kalimat-kalimat dalam Q.S. an-Nasr dan al-Kausar dengan benar', 'Pendidikan Agama Islam dan BP'),
(474, 3, '4.13', 'menunjukkan hafalan Q.S. an-Nasr dan al-Kausar dengan lancar', 'Pendidikan Agama Islam dan BP'),
(475, 3, '4.2', 'mencontohkan perilaku mandiri, percaya diri, dan bertanggung jawab sebagai implementasi makna Hadis yang terkandung', 'Pendidikan Agama Islam dan BP'),
(476, 3, '4.3', 'melakukan pengamatan terhadap diri dan makhluk ciptaan Allah yang dijumpai di sekitar rumah dan sekolah sebagai implementasi iman terhadap keesaan Allah Yang Maha Pencipta', 'Pendidikan Agama Islam dan BP'),
(477, 3, '4.4', 'membaca al-Asmau al-Husna: alWahhab, al-‘Alim, dan as-Sami‘ dengan jelas dan benar', 'Pendidikan Agama Islam dan BP'),
(478, 3, '4.5', 'mencontohkan perilaku tawaduk, ikhlas, dan mohon pertolongan', 'Pendidikan Agama Islam dan BP'),
(479, 3, '4.6', 'mencontohkan perilaku peduli terhadap sesama sebagai implementasi pemahaman Q.S. alKausar', 'Pendidikan Agama Islam dan BP'),
(480, 3, '4.7', 'mencontohkan sikap bersyukur', 'Pendidikan Agama Islam dan BP'),
(481, 3, '4.8', 'menunjukkan contoh makna salat sebagai wujud dari pemahaman Q.S. al-Kausar', 'Pendidikan Agama Islam dan BP'),
(482, 3, '4.9', 'mempraktikkan tata cara zikir dan doa setelah salat secara benar', 'Pendidikan Agama Islam dan BP'),
(483, 3, '4.10', 'menceritakan pengalaman hikmah pelaksanaan ibadah salat di rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(484, 3, '4.11', 'menceritakan kisah keteladanan Nabi Yusuf a.s.', 'Pendidikan Agama Islam dan BP'),
(485, 3, '4.12', 'menceritakan kisah keteladanan Nabi Syu’aib a.s.', 'Pendidikan Agama Islam dan BP'),
(486, 3, '4.14', 'menceritakan kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(487, 4, '4.1.1', 'membaca Q.S. al-Falaq dan Q.S alFīl dengan tartil', 'Pendidikan Agama Islam dan BP'),
(488, 4, '4.1.2', 'menulis kalimat-kalimat dalam Q.S. al-Falaq dan Q.S al-Fīl dengan benar', 'Pendidikan Agama Islam dan BP'),
(489, 4, '4.1.3', 'menunjukkan hafalan Q.S. al-Falaq dan Q.S al-Fīl dengan lancar', 'Pendidikan Agama Islam dan BP'),
(490, 4, '4.2', 'melakukan pengamatan terhadap makhluk ciptaan Allah di sekitar rumah dan sekolah sebagai upaya mengenal Allah itu ada', 'Pendidikan Agama Islam dan BP'),
(491, 4, '4.3', 'membaca al-Asmau al-Husna: AlBasir, Al-‘Adil, dan Al-‘Azim dengan jelas dan benar', 'Pendidikan Agama Islam dan BP'),
(492, 4, '4.4', 'melakukan pengamatan diri dan alam sekitar sebagai implementasi makna iman kepada malaikatmalaikat Allah', 'Pendidikan Agama Islam dan BP'),
(493, 4, '4.5', 'mencontohkan makna iman kepada Rasul Allah', 'Pendidikan Agama Islam dan BP'),
(494, 4, '4.6', 'mencontohkan sikap santun dan menghargai teman, baik di rumah, sekolah, maupun di masyarakat sekitar', 'Pendidikan Agama Islam dan BP'),
(495, 4, '4.7', 'mencontohkan sikap rendah hati', 'Pendidikan Agama Islam dan BP'),
(496, 4, '4.8', 'mencontohkan perilaku hemat', 'Pendidikan Agama Islam dan BP'),
(497, 4, '4.9', 'mencontohkan perilaku jujur dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(498, 4, '4.10', 'mencontohkan perilaku amanah dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(499, 4, '4.11', 'mencontohkan perilaku hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(500, 4, '4.12', 'menunjukkan perilaku gemar\r\nmembaca', 'Pendidikan Agama Islam dan BP'),
(501, 4, '4.13', 'menunjukkan sikap pantang menyerah', 'Pendidikan Agama Islam dan BP'),
(502, 4, '4.14', 'mempraktikkan tata cara bersuci dari hadas kecil sesuai ketentuan syari’at Islam', 'Pendidikan Agama Islam dan BP'),
(503, 5, '4.1.1', 'membaca Q.S. at-Tīn dan Q.S. alMā’ūn dengan tartīl', 'Pendidikan Agama Islam dan BP'),
(504, 5, '4.1.2', 'menulis kalimat-kalimat dalam Q.S. at-Tīn dan Q.S. al-Mā’ūn dengan benar', 'Pendidikan Agama Islam dan BP'),
(505, 5, '4.1.3', 'menunjukkan hafalan Q.S. at-Tīn dan Q.S. al-Mā’ūn dengan lancar', 'Pendidikan Agama Islam dan BP'),
(506, 5, '4.2', 'membaca al-Asmau al-Husna: AlMumit, Al-Hayy, Al-Qayyum, dan AlAhad dengan jelas dan benar', 'Pendidikan Agama Islam dan BP'),
(507, 5, '4.3', 'menunjukkan hafalan nama-nama Rasul Allah dan Rasul Ulul ‘Azmi', 'Pendidikan Agama Islam dan BP'),
(508, 5, '4.4', 'menunjukkan makna diturunkannya kitab-kitab suci melalui rasul-rasul-Nya sebagai implementasi rukun iman', 'Pendidikan Agama Islam dan BP'),
(509, 5, '4.5', 'menunjukkan perilaku jujur dalam kehidupan sehai-hari', 'Pendidikan Agama Islam dan BP'),
(510, 5, '4.6', 'mencontohkan perilaku hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(511, 5, '4.7', ' mencontohkan sikap saling menghargai sesama manusia', 'Pendidikan Agama Islam dan BP'),
(512, 5, '4.8', 'mencontohkan sikap sederhana dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(513, 5, '4.9', 'mencontohkan sikap ikhlas beramal dalam kehidupan seharihari', 'Pendidikan Agama Islam dan BP'),
(514, 5, '4.10', 'menunjukkan hikmah puasa Ramadan yang dapat membentuk akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(515, 5, '4.11', 'mempraktikkan tatacara salat tarawih dan tadarus al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(516, 5, '4.12', 'menceritakan kisah keteladanan Nabi Dawud a.s.', 'Pendidikan Agama Islam dan BP'),
(517, 5, '4.13', 'menceritakan kisah keteladanan Nabi Sulaiman a.s.', 'Pendidikan Agama Islam dan BP'),
(518, 5, '4.14', 'menceritakan kisah keteladanan Nabi Ilyas a.s.', 'Pendidikan Agama Islam dan BP'),
(519, 5, '4.15', 'menceritakan kisah keteladanan Nabi Ilyasa’ a.s.', 'Pendidikan Agama Islam dan BP'),
(520, 5, '4.16', 'menceritakan kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(521, 5, '4.17', 'menceritakan kisah keteladanan Luqman sebagaimana terdapat dalam al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(522, 6, '4.1.1', 'membaca Q.S. Al-Kafirun, Q.S. AlMaidah/5:2-3 dan Q.S. alHujurat/49:12-13 dengan jelas dan\r\nbenar', 'Pendidikan Agama Islam dan BP'),
(523, 6, '4.1.2', 'menulis Q.S. Al-Kafirun, Q.S. AlMaidah/5:2-3 dan Q.S. alHujurat/49:12-13 dengan benar', 'Pendidikan Agama Islam dan BP'),
(524, 6, '4.1.3', 'menunjukkan hafalan Q.S. AlKafirun, Q.S. Al-Maidah/5:2-3 dan Q.S. al-Hujurat/49:12-13 dengan benar', 'Pendidikan Agama Islam dan BP'),
(525, 6, '4.2', 'membaca al-Asmau al-Husna: AsSamad, Al-Muqtadir, Al-Muqaddim, dan Al-Baqi dengan jelas dan benar', 'Pendidikan Agama Islam dan BP'),
(526, 6, '4.3', 'menunjukkan contoh hikmah beriman kepada hari akhir yang dapat membentuk perilaku akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(527, 6, '4.4', 'menunjukkan hikmah beriman kepada qadha dan qadar yang dapat membentuk perilaku akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(528, 6, '4.5', 'mencontohkan perilaku hormat dan  patuh kepada orangtua, guru, dan sesama anggota keluarga', 'Pendidikan Agama Islam dan BP'),
(529, 6, '4.6', 'menunjukkan sikap toleran dan simpatik terhadap sesama sebagai wujud dari pemahaman Q.S. alKafirun', 'Pendidikan Agama Islam dan BP'),
(530, 6, '4.7', 'menunjukkan hikmah zakat, infaq, dan sedekah sebagai implementasi rukun Islam', 'Pendidikan Agama Islam dan BP'),
(531, 6, '4.8', 'menceritakan kisah keteladanan Nabi Yunus a.s.', 'Pendidikan Agama Islam dan BP'),
(532, 6, '4.9', 'menceritakan kisah keteladanan Nabi Yunus a.s.', 'Pendidikan Agama Islam dan BP'),
(533, 6, '4.10', 'menceritakan kisah keteladanan Nabi Yahya a.s.', 'Pendidikan Agama Islam dan BP'),
(534, 6, '4.11', 'menceritakan kisah keteladanan Nabi Isa a.s.', 'Pendidikan Agama Islam dan BP'),
(535, 6, '4.12', 'menceritakan kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(536, 6, '4.13', 'menceritakan kisah keteladanan sahabat-sahabat Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(537, 6, '4.14', 'menceritakan kisah keteladanan Ashabul Kahfi sebagaimana terdapat dalam al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(538, 7, '4.1', 'Menyelesaikan masalah yang berkaitan dengan urutan beberapa bilangan bulat dan pecahan (biasa, campuran, desimal, persen)', 'Matematika'),
(539, 7, '4.2', 'Menyelesaikan masalah yang berkaitan dengan operasi hitung bilangan bulat dan pecahan', 'Matematika'),
(540, 7, '4.3', 'Menyelesaikan masalah yang berkaitan dengan bilangan dalam bentuk bilangan berpangkat bulat positif dan negatif', 'Matematika'),
(541, 7, '4.4', 'Menyelesaikan masalah kontekstual yang berkaitan dengan himpunan, himpunan bagian, himpunan semesta, himpunan kosong, komplemen himpunan dan operasi biner pada himpunan', 'Matematika'),
(542, 7, '4.5', 'Menyelesaikan masalah yang berkaitan dengan bentuk aljabar dan operasi pada bentuk aljabar', 'Matematika'),
(543, 7, '4.6', 'Menyelesaikan masalah yang berkaitan dengan persamaan dan pertidaksamaan linear satu variabel', 'Matematika'),
(544, 7, '4.7', 'Menyelesaikan masalah yang berkaitan dengan rasio dua besaran (satuannya sama dan berbeda)', 'Matematika'),
(545, 7, '4.8', 'Menyelesaikan masalah yang berkaitan dengan perbandingan senilai dan berbalik nilai', 'Matematika'),
(546, 7, '4.9', 'Menyelesaikan masalah berkaitan dengan aritmetika sosial (penjualan, pembelian, potongan, keuntungan, kerugian, bunga tunggal, persentase, bruto, neto, tara)', 'Matematika'),
(547, 7, '4.10', 'Menyelesaikan masalah yang berkaitan dengan hubungan antar sudut sebagai akibat dari dua garis sejajar yang dipotong oleh garis transversal', 'Matematika'),
(548, 7, '4.11', 'Menyelesaikan masalah kontekstual yang berkaitan dengan luas dan keliling segiempat (persegi, persegipanjang, belahketupat, jajargenjang, trapesium, dan layanglayang) dan segitiga', 'Matematika'),
(549, 7, '4.12', 'Menyajikan dan menafsirkan data dalam bentuk tabel, diagram garis, diagram batang, dan diagram lingkaran', 'Matematika'),
(550, 8, '4.1', 'Menyelesaikan masalah yang berkaitan dengan pola pada barisan bilangan dan barisan konfigurasi objek', 'Matematika'),
(551, 8, '4.2', 'Menyelesaikan masalah yang berkaitan dengan kedudukan titik dalam bidang koordinat Kartesius', 'Matematika'),
(552, 8, '4.3', 'Menyelesaikan masalah yang berkaitan dengan relasi dan fungsi dengan menggunakan berbagai representasi', 'Matematika'),
(553, 8, '4.4', 'Menyelesaikan masalah kontekstual yang berkaitan dengan fungsi linear sebagai persamaan garis lurus', 'Matematika'),
(554, 8, '4.5', 'Menyelesaikan masalah yang berkaitan dengan sistem persamaan linear dua variabel', 'Matematika'),
(555, 8, '4.6', 'Menyelesaikan masalah yang berkaitan dengan teorema Pythagoras dan tripel Pythagoras', 'Matematika'),
(556, 8, '4.7', 'Menyelesaikan masalah yang berkaitan dengan sudut pusat, sudut keliling, panjang busur, dan luas juring lingkaran, serta hubungannya', 'Matematika'),
(557, 8, '4.8', 'Menyelesaikan masalah yang berkaitan dengan garis singgung persekutuan luar dan persekutuan dalam dua lingkaran', 'Matematika'),
(558, 8, '4.9', 'Menyelesaikan masalah yang berkaitan dengan luas permukaan dan volume bangun ruang sisi datar (kubus, balok, prima dan limas), serta gabungannya', 'Matematika'),
(559, 8, '4.10', 'Menyajikan dan menyelesaikan masalah yang berkaitan dengan distribusi data, nilai rata-rata,median, modus, dan sebaran data untuk mengambil kesimpulan,membuat  keputusan, dan membuat prediksi', 'Matematika'),
(560, 8, '4.11', 'Menyelesaikan masalah yang berkaitan dengan peluang empirik dan teoretik suatu kejadian dari suatu percobaan', 'Matematika'),
(561, 9, '4.1', 'Menyelesaikan masalah yang berkaitan dengan sifat-sifat operasi bilangan berpangkat bulat dan bentuk akar', 'Matematika'),
(562, 9, '4.2', 'Menyelesaikan masalah yang berkaitan dengan persamaan kuadrat', 'Matematika'),
(563, 9, '4.3', 'Menyajikan fungsi kuadrat menggunakan tabel, persamaan, dan grafik', 'Matematika'),
(564, 9, '4.4', 'Menyajikan dan menyelesaikan masalah kontekstual dengan menggunakan sifat-sifat fungsi kuadrat', 'Matematika'),
(565, 9, '4.5', 'Menyelesaikan masalah kontekstual yang berkaitan dengan transformasi geometri (refleksi, translasi, rotasi, dan dilatasi)', 'Matematika'),
(566, 9, '4.6', 'Menyelesaikan masalah yang berkaitan dengan kesebangunan dan kekongruenan antar bangun datar', 'Matematika'),
(567, 9, '4.7', 'Menyelesaikan masalah kontekstual yang berkaitan dengan luas permukaan dan volume bangun ruang sisi lengkung (tabung, kerucut, dan bola), serta gabungan beberapa bangun ruang sisi lengkung', 'Matematika'),
(568, 7, '4.1', ' Menjelaskan konsep ruang (lokasi, distribusi, potensi, iklim, bentuk muka bumi, geologis, flora dan fauna) dan interaksi antarruang di Indonesia serta pengaruhnya terhadap\r\nkehidupan manusia Indonesia dalam aspek ekonomi, sosial, budaya, dan pendidikan.', 'IPS'),
(569, 7, '4.2', 'Menyajikan hasil identifikasi tentang interaksi sosial dalam ruang dan pengaruhnya terhadap kehidupan sosial, ekonomi, dan budaya dalam nilai dan norma serta kelembagaan\r\nsosial budaya.', 'IPS'),
(570, 7, '4.3', 'Menjelaskan hasil analisis tentang konsep interaksi antara manusia dengan ruang sehingga menghasilkan berbagai kegiatan ekonomi (produksi, distribusi, konsumsi, permintaan, dan penawaran) dan interaksi antarruang untuk keberlangsungan kehidupan ekonomi, sosial, dan budaya Indonesia.', 'IPS'),
(571, 7, '4.4', 'Menguraikan kronologi perubahan, dan kesinambungan dalam kehidupan bangsa Indonesia pada aspek politik, sosial, budaya, geografis, dan pendidikan sejak masa\r\npraaksara sampai masa HinduBuddha dan Islam.', 'IPS'),
(572, 8, '4.1', 'Menyajikan hasil telaah tentang perubahan keruangan dan interaksi antarruang di Indonesia dan negaranegara ASEAN yang diakibatkan oleh faktor alam dan manusia (teknologi,\r\nekonomi, pemanfaatan lahan, politik) dan pengaruhnya terhadap keberlangsungan kehidupan ekonomi, sosial, budaya, dan politik.', 'IPS'),
(573, 8, '4.2', 'Menyajikan hasil analisis tentang pengaruh interaksi sosial dalam ruang yang berbeda terhadap kehidupan sosial dan budaya serta pengembangan kehidupan kebangsaan.', 'IPS'),
(574, 8, '4.3', 'Menyajikan hasil analisis tentang keunggulan dan keterbatasan ruang dalam permintaan dan penawaran serta teknologi, dan pengaruhnya terhadap interaksi antarruang bagi kegiatan ekonomi, sosial, budaya, di Indonesia dan negara-negara ASEAN.', 'IPS'),
(575, 8, '4.4', 'Menyajikan hasil analisis kronologi, perubahan dan kesinambungan ruang (geografis, politik, ekonomi, pendidikan, sosial, budaya) dari masa penjajahan sampai tumbuhnya\r\nsemangat kebangsaan.', 'IPS'),
(576, 9, '4.1', 'Menyajikan hasil telaah tentang perubahan keruangan dan interaksi antarruang negara-negara Asia dan benua lainnya yang diakibatkan faktor alam, manusia dan pengaruhnya terhadap keberlangsungan kehidupan manusia dalam ekonomi, sosial, pendidikan dan politik', 'IPS'),
(577, 9, '4.2', 'Menyajikan hasil analisis tentang perubahan kehidupan sosial budaya Bangsa Indonesia dalam menghadapi arus globalisasi untuk memperkokoh kehidupan kebangsaan', 'IPS'),
(578, 9, '4.3', 'Menyajikan hasil analisis tentang ketergantungan antarruang dilihat dari konsep ekonomi (produksi, distribusi, konsumsi, harga, pasar) dan pengaruhnya terhadap migrasi penduduk, transportasi, lembaga sosial dan ekonomi, pekerjaan, pendidikan, dan kesejahteraan masyarakat', 'IPS'),
(579, 9, '4.4', 'Menyajikan hasil analisis kronologi, perubahan dan kesinambungan ruang (geografis, politik, ekonomi, pendidikan, sosial, budaya) dari awal kemerdekaan sampai awal reformasi', 'IPS'),
(580, 7, '4.1', 'Mempraktikkan gerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional', 'PJOK'),
(581, 7, '4.2', 'Mempraktikkan gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional.', 'PJOK'),
(582, 7, '4.3', 'Mempraktikkan gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional. ', 'PJOK'),
(583, 7, '4.4', 'Mempraktikkan gerak spesifik seni beladiri.', 'PJOK'),
(584, 7, '4.5', 'Mempraktikkan latihan peningkatan derajat kebugaran jasmani yang terkait dengan kesehatan (daya tahan, kekuatan, komposisi tubuh, dan kelenturan) dan pengukuran hasilnya', 'PJOK'),
(585, 7, '4.6', 'Mempraktikkan berbagai keterampilan dasar spesifik senam lantai', 'PJOK'),
(586, 7, '4.7', 'Mempraktikkan variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunanB lengan mengikuti irama (ketukan) tanpa/dengan musik sebagai pembentuk gerak pemanasan dalam aktivitas gerak berirama', 'PJOK'),
(587, 7, '4.8', 'Mempraktikkan konsep gerak spesifik salah satu gaya renang dengan koordinasi yang baik. ', 'PJOK'),
(588, 7, '4.9', 'Memaparkan perkembangan tubuh remaja yang meliputi perubahan fisik sekunder dan mental.', 'PJOK'),
(589, 7, '4.10', 'Memaparkan pola makan sehat, bergizi dan seimbang sertapengaruhnya terhadap kesehatan.', 'PJOK'),
(590, 8, '4.1', ' Mempraktikkan variasi gerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional', 'PJOK'),
(591, 8, '4.2', 'Mempraktikkan variasi gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional', 'PJOK'),
(592, 8, '4.3', 'Mempraktikkan variasi gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional', 'PJOK'),
(593, 8, '4.4', 'Mempraktikkan variasi gerak spesifik seni beladiri', 'PJOK'),
(594, 8, '4.5', 'Mempraktikkan latihan peningkatan derajat kebugaran jasmani yang terkait dengan keterampilan (kecepatan, kelincahan, keseimbanga, dan koordinasi) serta pengukuran\r\nhasilnya', 'PJOK'),
(595, 8, '4.6', 'Mempraktikkan kombinasi keterampilan berbentuk rangkaian gerak sederhana dalam aktivitas spesifik senam lantai', 'PJOK'),
(596, 8, '4.7', 'Mempraktikkan prosedur variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik sebagai pembentuk gerak pemanasan dan inti latihan dalam aktivitas gerak berirama', 'PJOK'),
(597, 8, '4.8', 'Mempraktikkan gerak spesifik salah satu gaya renang dalam permainan air dengan atau tanpa alat', 'PJOK'),
(598, 8, '4.9', 'Memaparkan perlunya pencegahan terhadap “bahaya pergaulan bebas', 'PJOK'),
(599, 8, '4.10', 'Memaparkan cara menjaga keselamatan diri dan orang lain di jalan raya', 'PJOK'),
(600, 9, '4.1', 'Mempraktikkan variasi dan kombinasigerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional', 'PJOK'),
(601, 9, '4.2', 'Mempraktikkan variasi dan kombinasi gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional.', 'PJOK'),
(602, 9, '4.3', 'Mempraktikkan kombinasi gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional.', 'PJOK'),
(603, 9, '4.4', 'Mempraktikkan variasidan  kombinasi gerak spesifik seni beladiri.', 'PJOK'),
(604, 9, '4.5', 'Mempraktikkan penyusunan program pengembangan komponen kebugaran jasmani terkait dengan kesehatan dan keterampilan secara sederhana.', 'PJOK'),
(605, 9, '4.6', 'Mempraktikkan kombinasi keterampilan berbentuk rangkaian gerak sederhana secara konsisten, tepat, dan terkontrol dalam aktivitas spesifik senam lantai', 'PJOK'),
(606, 9, '4.7', 'Mempraktikkan variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik sebagai pembentuk gerak pemanasan, inti latihan, dan pendinginan dalam aktivitas gerak berirama', 'PJOK'),
(607, 9, '4.8', 'Mempraktikkan gerak spesifik salah satu gaya renang dalam bentuk perlombaan', 'PJOK'),
(608, 9, '4.9', 'Memaparkan tindakan P3K pada kejadian darurat, baik pada diri sendiri maupun orang lain', 'PJOK'),
(609, 9, '4.10', 'Memaparkan peran aktivitas fisik terhadap pencegahan penyakit', 'PJOK'),
(610, 7, '4.1.1', 'membaca Q.S. al-Mujadilah /58: 11 dan Q.S. ar-Rahman /55: 33 dengan tartil', 'Pendidikan Agama Islam dan BP'),
(611, 7, '4.1.2', 'menunjukkan hafalan Q.S. alMujadilah /58: 11, Q.S. ar-Rahman /55: 33 dan Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(612, 7, '4.1.3', 'menyajikan keterkaitan semangat menuntut ilmu dengan pesan Q.S. al-Mujadilah /58: 1 dan Q.S. arRahman /55: 33', 'Pendidikan Agama Islam dan BP'),
(613, 7, '4.2.1', ' membaca Q.S. an-Nisa/4: 146, Q.S. al-Baqarah/2: 153, dan Q.S. Ali Imran/3: 134 dengan tartil', 'Pendidikan Agama Islam dan BP'),
(614, 7, '4.2.2', 'menunjukkan hafalan Q.S. anNisa/4: 146, Q.S. al-Baqarah/2: 153, dan Q.S. Ali Imrān/3: 134 serta Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(615, 7, '4.2.3', 'menyajikan keterkaitan ikhlas, sabar, dan pemaaf dengan pesan Q.S. an-Nisa/4: 146, Q.S. alBaqarah/2: 153, dan Q.S. Ali Imran/3: 134', 'Pendidikan Agama Islam dan BP'),
(616, 7, '4.3', 'menyajikan contoh perilaku yang mencerminkan orang yang meneladani al-Asma’u al-Husna: al- ’Alim, al-Khabir, as- Sami’, dan alBashir', 'Pendidikan Agama Islam dan BP'),
(617, 7, '4.4', 'menyajikan contoh perilaku yang mencerminkan iman kepada malaikat Allah Swt.', 'Pendidikan Agama Islam dan BP'),
(618, 7, '4.5', 'menyajikan makna perilaku jujur, amanah, dan istiqamah', 'Pendidikan Agama Islam dan BP'),
(619, 7, '4.6', 'menyajikan makna hormat dan patuh kepada orang tua dan guru, dan empati terhadap sesama', 'Pendidikan Agama Islam dan BP'),
(620, 7, '4.7', 'menyajikan cara bersuci dari hadas besar', 'Pendidikan Agama Islam dan BP'),
(621, 7, '4.8', 'mempraktikkan salat berjamaah', 'Pendidikan Agama Islam dan BP'),
(622, 7, '4.9', 'mempraktikkan salat Jumat\r\n', 'Pendidikan Agama Islam dan BP'),
(623, 7, '4.10', 'mempraktikkan salat jamak dan qasar', 'Pendidikan Agama Islam dan BP'),
(624, 7, '4.11', 'menyajikan strategi perjuangan yang dilakukan Nabi Muhammad saw. periode Makkah', 'Pendidikan Agama Islam dan BP'),
(625, 7, '4.12', 'menyajikan strategi perjuangan yang dilakukan Nabi Muhammad saw. periode Madinah', 'Pendidikan Agama Islam dan BP'),
(626, 7, '4.13', 'menyajikan strategi perjuangan dan kepribadian al-Khulafa al-Rasyidun', 'Pendidikan Agama Islam dan BP'),
(627, 8, '4.1.1', 'membaca Q.S. al-Furqan/25: 63, Q.S. al-Isra’/17: 26-27 dengan tartil', 'Pendidikan Agama Islam dan BP'),
(628, 8, '4.1.2', 'menunjukkan hafalan Q.S. alFurqan/25: 63, Q.S. Al-Isra’/17: 26-27 serta Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(629, 8, '4.1.3', 'menyajikan keterkaitan rendah hati, hemat, dan hidup sederhana dengan pesan Q.S. al-Furqan/25: 63, Q.S. al-Isra’/17: 26-27', 'Pendidikan Agama Islam dan BP'),
(630, 8, '4.2.1', 'membaca Q.S. an-Nahl/16: 114 terkait dengan tartil', 'Pendidikan Agama Islam dan BP'),
(631, 8, '4.2.2', 'menunjukkan hafalan Q.S. anNahl/16: 114 serta Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(632, 8, '4.2.3', ' menyajikan keterkaitan mengonsumsi makanan dan minuman yang halal dan bergizi dalam kehidupan sehari-hari dengan pesan Q.S. an-Nahl/16: 114', 'Pendidikan Agama Islam dan BP'),
(633, 8, '4.3', 'menyajikan dalil naqli tentang beriman kepada Kitab-kitab Allah Swt.', 'Pendidikan Agama Islam dan BP'),
(634, 8, '4.4', 'menyajikan dalil naqli tentang iman kepada Rasul Allah Swt.', 'Pendidikan Agama Islam dan BP'),
(635, 8, '4.5', 'menyajikan dampak bahaya mengomsumsi minuman keras, judi, dan pertengkaran', 'Pendidikan Agama Islam dan BP'),
(636, 8, '4.6', 'menyajikan cara menerapkan perilaku jujur dan adil', 'Pendidikan Agama Islam dan BP'),
(637, 8, '4.7', 'menyajikan cara berbuat baik, hormat, dan patuh kepada orang tua dan guru', 'Pendidikan Agama Islam dan BP'),
(638, 8, '4.8', 'menyajikan contoh perilaku gemar beramal saleh dan berbaik sangka kepada sesama', 'Pendidikan Agama Islam dan BP'),
(639, 8, '4.9', 'mempraktikkan salat sunah berjamaah dan munfarid', 'Pendidikan Agama Islam dan BP'),
(640, 8, '4.10', 'mempraktikkan sujud syukur, sujud sahwi, dan sujud tilawah', 'Pendidikan Agama Islam dan BP'),
(641, 8, '4.11', 'menyajikan hikmah pelaksanaan puasa wajib dan puasa sunah', 'Pendidikan Agama Islam dan BP'),
(642, 8, '4.12', 'menyajikan hikmah mengonsumsi makanan yang halal dan bergizisesuai ketentuan dengan al-Qur’an dan Hadis', 'Pendidikan Agama Islam dan BP'),
(643, 8, '4.13', 'menyajikan rangkaian sejarah pertumbuhan ilmu pengetahuan pada masa Bani Umayah', 'Pendidikan Agama Islam dan BP'),
(644, 8, '4.14', 'menyajikan rangkaian sejarah pertumbuhan ilmu pengetahuan pada masa Abbasiyah', 'Pendidikan Agama Islam dan BP');
INSERT INTO `kd_keterampilan` (`id_kd`, `kelas`, `no_kd`, `judul`, `mata_pelajaran`) VALUES
(645, 9, '4.1.1', 'membaca Q.S. az-Zumar/39: 53, Q.S. an-Najm/53: 39-42, dan Q.S. Ali Imran/3: 159 dengan tartil', 'Pendidikan Agama Islam dan BP'),
(646, 9, '4.1.2', 'menunjukkan hafalan Q.S. azZumar/39: 53, Q.S. an-Najm/53: 39- 42, Q.S. Ali Imran/3: 159 serta Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(647, 9, '4.1.3', 'menyajikan keterkaitan optimis, ikhtiar, dan tawakal dengan pesan Q.S. az-Zumar/39: 53, Q.S. anNajm/53: 39-42, dan Q.S. Ali Imran/3: 159', 'Pendidikan Agama Islam dan BP'),
(648, 9, '4.2.1', 'membaca Q.S. al-Hujurat/49: 13 dengan tartil', 'Pendidikan Agama Islam dan BP'),
(649, 9, '4.2.2', 'menunjukkan hafalan Q.S. alHujurat/ 49: 13 serta Hadis terkait dengan lancar', 'Pendidikan Agama Islam dan BP'),
(650, 9, '4.2.3', ' menyajikan keterkaitan toleransi dan menghargai perbedaan dengan pesan Q.S. al-Hujurat/ 49: 13', 'Pendidikan Agama Islam dan BP'),
(651, 9, '4.3', 'menyajikan dalil naqli yang menjelaskan gambaran kejadian hari akhir', 'Pendidikan Agama Islam dan BP'),
(652, 9, '4.4', 'menyajikan dalil naqli tentang adanya Qadha dan Qadar', 'Pendidikan Agama Islam dan BP'),
(653, 9, '4.5', 'menyajikan penerapan perilaku jujur dan menepati janji dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(654, 9, '4.6', 'menyajikan cara berbakti dan taat kepada orang tua dan guru', 'Pendidikan Agama Islam dan BP'),
(655, 9, '4.7', 'menyajikan contoh perilaku tata krama, sopan-santun, dan rasa malu', 'Pendidikan Agama Islam dan BP'),
(656, 9, '4.8', 'mempraktikkan ketentuan zakat', 'Pendidikan Agama Islam dan BP'),
(657, 9, '4.9', 'mempraktikkan manasik haji', 'Pendidikan Agama Islam dan BP'),
(658, 9, '4.10', 'memperagakan tata cara penyembelihan hewan', 'Pendidikan Agama Islam dan BP'),
(659, 9, '4.11', 'menjalankan pelaksanaan ibadah qurban dan aqiqah di lingkungan sekitar rumah', 'Pendidikan Agama Islam dan BP'),
(660, 9, '4.12', 'menyajikan rangkaian sejarah perkembangan Islam di Nusantara', 'Pendidikan Agama Islam dan BP'),
(661, 9, '4.13', 'menyajikan sejarah dan perkembangan tradisi Islam Nusantara', 'Pendidikan Agama Islam dan BP'),
(662, 1, '4.1', 'membuat karya sederhana yang menunjukkan bertanggung jawab terhadap dirinya sebagai ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(663, 1, '4.2', 'menyajikan karya berkaitan dangan anggota tubuhnya sebagai ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(664, 1, '4.3', 'membuat proyek sederhana yang berkaitan dengan sikap mengasihi keluarga dan teman', 'Pendidikan Agama Kristen dan BP'),
(665, 1, '4.4', 'melakukan tindakan sederhana dalam memelihara alam ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(666, 2, '4.1', 'mempraktikkan sikap hormat kepada orang tua dan orang yang lebih tua', 'Pendidikan Agama Kristen dan BP'),
(667, 2, '4.2', 'mempraktikkan tanggung jawab dalam keluarga melalui tindakan sederhana sesuai usianya', 'Pendidikan Agama Kristen dan BP'),
(668, 2, '4.3', 'menyajikan cara menjaga dan menerapkan hidup rukun di sekolah dan di lingkungannya', 'Pendidikan Agama Kristen dan BP'),
(669, 2, '4.4', 'menerapkan perilaku disiplin di sekolah, rumah, dan lingkungan sekitar', 'Pendidikan Agama Kristen dan BP'),
(670, 3, '4.1', 'menerapkan sikap peduli terhadap iklim dan gejala-gejala alam', 'Pendidikan Agama Kristen dan BP'),
(671, 3, '4.2', 'membuat karya yang berkaitan dengan tanggung jawab dalam memelihara flora dan fauna di sekitarnya', 'Pendidikan Agama Kristen dan BP'),
(672, 3, '4.3', 'membuat proyek yang berkaitan dengan pergaulan sesama manusia walaupun berbeda suku, budaya, bangsa, dan agama', 'Pendidikan Agama Kristen dan BP'),
(673, 3, '4.4', 'melakukan tindakan sederhana sebagai wujud ikut serta menjaga keutuhan ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(674, 4, '4.1', 'menyajikan contoh sederhana yang berkaitan dengan perilaku bersyukur dalam berbagai peristiwa kehidupan', 'Pendidikan Agama Kristen dan BP'),
(675, 4, '4.2', 'membuat proyek sederhana terkait dengan sikap bersyukur dalam berbagai peristiwa rantai kehidupan manusia di sekitarnya', 'Pendidikan Agama Kristen dan BP'),
(676, 4, '4.3', 'membuat karya yang mengekpresikan keterbatasannya sebagai manusia', 'Pendidikan Agama Kristen dan BP'),
(677, 4, '4.4', 'menyajikan contoh pemeliharaan Allah dalam kehidupan manusia', 'Pendidikan Agama Kristen dan BP'),
(678, 5, '4.1', 'menyajikan contoh cara hidup manusia yang sudah bertobat', 'Pendidikan Agama Kristen dan BP'),
(679, 5, '4.2', 'mempraktikkan cara hidup sebagai orang yang sudah diselamatkan Allah di dalam Yesus Kristus', 'Pendidikan Agama Kristen dan BP'),
(680, 5, '4.3', 'membuat karya-karya kreatif sebagai ungkapan syukur atas pertolongan Roh Kudus dalam hidup orang yang sudah diselamatkan', 'Pendidikan Agama Kristen dan BP'),
(681, 5, '4.4', 'membuat karya terkait dengan menjadi manusia baru', 'Pendidikan Agama Kristen dan BP'),
(682, 6, '4.1', 'menyajikan contoh ibadah yang berkenan kepada Allah', 'Pendidikan Agama Kristen dan BP'),
(683, 6, '4.2', 'mempraktikkan kesetiaan beribadah, berdoa, dan membaca Alkitab', 'Pendidikan Agama Kristen dan BP'),
(684, 6, '4.3', 'mendemonstrasikan berbagai bentuk pelayanan terhadap sesama sebagai ibadah yang sejati kepada Allah', 'Pendidikan Agama Kristen dan BP'),
(685, 6, '4.4', 'membuat proyek yang melibatkan seluruh hidupnya sebagai ibadah yang sejati kepada Allah', 'Pendidikan Agama Kristen dan BP');

-- --------------------------------------------------------

--
-- Table structure for table `kd_pengetahuan`
--

CREATE TABLE `kd_pengetahuan` (
  `id_kd` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `no_kd` varchar(20) NOT NULL,
  `judul` text NOT NULL,
  `mata_pelajaran` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kd_pengetahuan`
--

INSERT INTO `kd_pengetahuan` (`id_kd`, `kelas`, `no_kd`, `judul`, `mata_pelajaran`) VALUES
(1, 1, '3.1', 'Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', 'Bahasa Indonesia'),
(2, 1, '3.2', 'Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan ', 'Bahasa Indonesia'),
(3, 1, '3.3', ' Menguraikan lambang bunyi  vokal dan konsonan  dalam kata bahasa Indonesia atau bahasa daerahatau bahasa daerah ', 'Bahasa Indonesia'),
(4, 1, '3.4', 'Menentukan kosakata tentang anggota tubuh dan pancaindra serta perawatannya melalui teks pendek (berupa gambar, tulisan, slogan sederhana, dan/atau syair lagu) dan eksplorasi lingkungan ', 'Bahasa Indonesia'),
(5, 1, '3.5', 'Mengenal kosakata tentang cara memelihara kesehatan melalui teks pendek (berupa gambar, tulisan, dan slogan sederhana) dan/atau eksplorasi lingkungan.', 'Bahasa Indonesia'),
(6, 1, '3.1', 'Menjelaskan makna bilangan cacah sampai dengan 99 sebagai banyak anggota suatu kumpulan objek', 'Matematika'),
(7, 1, '3.2', 'Menjelaskan bilangan sampai dua angka dan nilai tempat penyusun lambang bilangan menggunakan kumpulan benda konkret serta cara membacanya', 'Matematika'),
(8, 1, '3.3', ' Membandingkan dua bilangan sampai dua angka dengan menggunakan kumpulan bendabenda konkret ', 'Matematika'),
(9, 1, '3.4', 'Menjelaskan dan melakukan penjumlahan dan pengurangan bilangan yang melibatkan bilangan cacah sampai dengan 99 dalam kehidupan sehari-hari serta mengaitkan penjumlahan dan pengurangan ', 'Matematika'),
(10, 1, '3.5', 'Mengenal pola bilangan yang berkaitan dengan kumpulan benda/gambar/gerakan atau lainnya ', 'Matematika'),
(11, 2, '3.1', 'Menjelaskan makna bilangan cacah dan menentukan lambangnya berdasarkan nilai tempat dengan menggunakan model konkret serta cara membacanya ', 'Matematika'),
(12, 2, '3.2', 'Membandingkan dua bilangan cacah  \r\n ', 'Matematika'),
(13, 2, '3.3', 'Menjelaskan dan melakukan penjumlahan dan pengurangan bilangan yang melibatkan  bilangan cacah sampai dengan 999 dalam kehidupan sehari-hari serta mengaitkan penjumlahan dan pengurangan ', 'Matematika'),
(14, 2, '3.4', 'Menjelaskan perkalian dan pembagian yang melibatkan bilangan cacah dengan hasil kali sampai dengan 100 dalam kehidupan sehari-hari serta mengaitkan perkalian dan pembagian ', 'Matematika'),
(15, 2, '3.5', 'Menjelaskan nilai dan kesetaraan pecahan mata uang ', 'Matematika'),
(16, 2, '3.1', ' Merinci ungkapan, ajakan, perintah, penolakan yang terdapat dalam teks cerita atau lagu yang menggambarkan sikap hidup rukun \r\n', 'Bahasa Indonesia'),
(17, 2, '3.2', 'Menguraikan kosakata dan konsep tentang keragaman benda berdasarkan bentuk dan wujudnya dalam bahasa Indonesia atau bahasa daerah melalui teks tulis, lisan, visual, dan/atau eksplorasi lingkungan. ', 'Bahasa Indonesia'),
(18, 2, '3.3', 'Menentukan kosakata dan konsep tentang lingkungan geografis, kehidupan ekonomi, sosial dan budaya di lingkungan sekitar dalam bahasa Indonesia atau bahasa daerah melalui teks tulis, lisan, visual, dan/atau eksplorasi lingkungan. ', 'Bahasa Indonesia'),
(19, 2, '3.4', 'Menenetukan kosakata dan konsep tentang lingkungan sehat dan lingkungan tidak sehat di lingkungan sekitar serta cara menjaga kesehatan lingkungan dalam Bahasa Indonesia atau bahasa daerah melalui teks tulis, lisan, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(20, 2, '3.5', 'Mencermati puisi anak dalam bahasa Indonesia atau bahasa daerah melalui teks tulis dan lisan ', 'Bahasa Indonesia'),
(21, 3, '3.1', 'Menggali informasi tentang konsep perubahan wujud benda dalam kehidupan sehari-hari yang disajikan dalam bentuk lisan, tulis, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(22, 3, '3.2', 'Menggali informasi tentang sumber dan bentuk energi yang disajikan dalam bentuk lisan, tulis, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(23, 3, '3.3', 'Menggali informasi tentang perubahan cuaca  dan pengaruhnya terhadap kehidupan manusia yang disajikan dalam bentuk lisan, tulis, visual, dan/atau eksplorasi lingkungan \r\n ', 'Bahasa Indonesia'),
(24, 3, '3.4', 'Mencermati kosakata dalam teks tentang konsep ciri-ciri, kebutuhan (makanan dan tempat hidup), pertumbuhan, dan perkembangan makhluk hidup yang ada di lingkungan setempat yang disajikan dalam bentuk lisan, tulis, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(25, 3, '3.5', 'Menggali informasi tentang caracara perawatan tumbuhan dan hewan melalui wawancara dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(26, 4, '3.1', ' Mencermati gagasan pokok dan gagasan pendukung yang diperoleh dari teks lisan, tulis, atau visual ', 'Bahasa Indonesia'),
(27, 4, '3.2', 'Mencermati keterhubungan antargagasan yang didapat dari teks lisan, tulis, atau visual', 'Bahasa Indonesia'),
(28, 4, '3.3', 'Menggali informasi dari seorang tokoh melalui wawancara menggunakan daftar pertanyaan ', 'Bahasa Indonesia'),
(29, 4, '3.4', 'Membandingkan teks petunjuk penggunaan dua alat yang sama dan berbeda ', 'Bahasa Indonesia'),
(30, 4, '3.5', 'Menguraikan pendapat pribadi tentang isi buku sastra (cerita, dongeng, dan sebagainya) ', 'Bahasa Indonesia'),
(31, 5, '3.1', 'Menentukan pokok pikiran dalam teks lisan dan tulis ', 'Bahasa Indonesia'),
(32, 5, '3.2', 'Mengklasifikasi informasi yang didapat dari buku ke dalam  aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana ', 'Bahasa Indonesia'),
(33, 5, '3.3', 'Meringkas teks penjelasan (eksplanasi) dari media cetak atau elektronik ', 'Bahasa Indonesia'),
(34, 5, '3.4', ' Menganalisis informasi yang disampaikan paparan iklan dari media cetak atau elektronik ', 'Bahasa Indonesia'),
(35, 5, '3.5', 'Menggali informasi penting dari teks narasi sejarah yang disajikan secara lisan dan tulis menggunakan aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana ', 'Bahasa Indonesia'),
(36, 3, '3.1', 'Menjelaskan sifat-sifat operasi hitung pada bilangan cacah ', 'Matematika'),
(37, 3, '3.2', 'Menjelaskan bilangan cacah dan pecahan sederhana (seperti 1/2, 1/3 , dan 1/4) yang disajikan pada garis bilangan  \r\n4', 'Matematika'),
(38, 3, '3.3', 'Menyatakan suatu bilangan sebagai  jumlah, selisih, hasil kali, atau hasil bagi dua bilangan cacah ', 'Matematika'),
(39, 3, '3.4', 'Menggeneralisasi ide pecahan sebagai bagian dari keseluruhan menggunakan benda-benda konkret ', 'Matematika'),
(40, 3, '3.5', ' Menjelaskan dan melakukan penjumlahan dan pengurangan pecahan berpenyebut sama ', 'Matematika'),
(41, 4, '3.1', 'Menjelaskan pecahan-pecahan senilai  dengan gambar  dan model konkret ', 'Matematika'),
(42, 4, '3.2', 'Menjelaskan berbagai bentuk pecahan (biasa, campuran, desimal, dan persen) dan hubungan di antaranya ', 'Matematika'),
(43, 4, '3.3', 'Menjelaskan dan melakukan penaksiran dari jumlah, selisih, hasil kali, dan hasil bagi dua bilangan cacah maupun pecahan dan desimal ', 'Matematika'),
(44, 4, '3.4', 'Menjelaskan faktor dan kelipatan suatu bilangan ', 'Matematika'),
(45, 4, '3.5', 'Menjelaskan bilangan prima', 'Matematika'),
(46, 4, '3.1', ' Menganalisis hubungan antara bentuk dan fungsi bagian tubuh pada hewan dan tumbuhan \r\n', 'IPA'),
(47, 4, '3.2', ' Membandingkan siklus hidup beberapa jenis makhluk hidup serta mengaitkan dengan upaya pelestariannya ', 'IPA'),
(48, 4, '3.3', ' Mengidentifikasi macam-macam gaya, antara lain: gaya otot, gaya listrik, gaya magnet, gaya gravitasi, dan gaya gesekan ', 'IPA'),
(49, 4, '3.4', 'Menghubungkan gaya dengan gerak pada peristiwa di lingkungan sekitar  \r\n', 'IPA'),
(50, 4, '3.5', ' Mengidentifikasi berbagai sumber energi, perubahan bentuk energi, dan sumber energi alternatif (angin, air, matahari, panas bumi, bahan bakar organik, dan nuklir) dalam kehidupan sehari-hari ', 'IPA'),
(51, 5, '3.1', 'Menjelaskan alat gerak dan fungsinya pada hewan dan manusia serta cara memelihara kesehatan alat gerak manusia ', 'IPA'),
(52, 5, '3.2', ' Menjelaskan organ pernafasan dan fungsinya pada hewan dan manusia, serta cara memelihara kesehatan organ pernapasan manusia ', 'IPA'),
(53, 5, '3.3', 'Menjelaskan organ pencernaan dan fungsinya pada hewan dan manusia serta cara memelihara kesehatan organ pencernaan manusia \r\n', 'IPA'),
(54, 5, '3.4', 'Menjelaskan organ peredaran darah dan fungsinya pada hewan dan manusia serta cara memelihara kesehatan organ peredaran darah manusia ', 'IPA'),
(55, 5, '3.5', 'Menganalisis hubungan antar komponen ekosistem dan jaring-jaring makanan di lingkungan sekitar \r\n ', 'IPA'),
(56, 6, '3.1', 'Membandingkan cara perkembangbiakan tumbuhan dan hewan ', 'IPA'),
(57, 6, '3.2', 'Menghubungkan ciri pubertas pada laki-laki dan perempuan dengan kesehatan reproduksi ', 'IPA'),
(58, 6, '3.3', 'Menganalisis cara makhluk hidup menyesuaikan diri dengan lingkungan \r\n ', 'IPA'),
(59, 6, '3.4', 'Mengidentifikasi komponen-komponen listrik dan fungsinya dalam rangkaian listrik sederhana ', 'IPA'),
(60, 6, '3.5', 'Mengidentifikasi sifat-sifat magnet dalam kehidupan sehari-hari ', 'IPA'),
(61, 1, '3.1', 'Mengenal simbol sila-sila  Pancasila dalam lambang negara “Garuda Pancasila” ', 'PPKN'),
(62, 1, '3.2', ' Mengidentifikasi aturan yang berlaku dalam kehidupan seharihari di rumah ', 'PPKN'),
(63, 1, '3.3', 'Mengidentifikasi keberagaman karateristik individu di rumah ', 'PPKN'),
(64, 1, '3.4', 'Mengidentifikasi bentuk kerjasama dalam keberagaman di rumah ', 'PPKN'),
(65, 2, '3.1', ' Mengidentifikasi hubungan antara simbol dan sila-sila Pancasila dalam lambang negara “Garuda Pancasila” ', 'PPKN'),
(66, 2, '3.2', ' Mengidentifikasi aturan dan tata tertib  yang berlaku di sekolah ', 'PPKN'),
(67, 2, '3.3', 'Mengidentifikasi jenis-jenis keberagaman karakteristik individu di sekolah ', 'PPKN'),
(68, 2, '3.4', 'Memahami makna bersatu dalam keberagaman di sekolah ', 'PPKN'),
(69, 3, '3.1', ' Memahami  arti gambar pada lambang negara “Garuda Pancasila”', 'PPKN'),
(70, 3, '3.2', 'Mengidentifikasi kewajiban dan hak sebagai anggota keluarga dan warga sekolah', 'PPKN'),
(71, 3, '3.3', 'Menjelaskan makna keberagaman karakteristik individu di lingkungan sekitar ', 'PPKN'),
(72, 3, '3.4', 'Memahami makna bersatu dalam keberagaman di lingkungan sekitar ', 'PPKN'),
(73, 4, '3.1', ' Memahami makna hubungan simbol dengan sila-sila Pancasila ', 'PPKN'),
(74, 4, '3.2', ' Mengidentifikasi pelaksanaan kewajiban dan hak sebagai warga masyarakat dalam kehidupan sehari-hari', 'PPKN'),
(75, 4, '3.3', 'Menjelaskan manfaat keberagaman karakteristik individu dalam kehidupan seharihari', 'PPKN'),
(76, 4, '3.4', 'Mengidentifikasi berbagai bentuk keberagaman suku bangsa, sosial, dan budaya di Indonesia yang terikat persatuan dan kesatuan ', 'PPKN'),
(77, 5, '3.1', 'Mengidentifikasi nilai-nilai Pancasila dalam kehidupan sehari-hari ', 'PPKN'),
(78, 5, '3.2', 'Memahami hak, kewajiban dan tanggung jawab sebagai warga dalam kehidupan sehari-hari', 'PPKN'),
(79, 5, '3.3', ' Menelaah keberagaman sosial budaya masyarakat ', 'PPKN'),
(80, 5, '3.4', ' Menggali manfaat persatuan dan kesatuan untuk membangun kerukunan hidup ', 'PPKN'),
(81, 6, '3.1', 'Menganalisis penerapan nilainilai Pancasila dalam kehdupan sehari-hari ', 'PPKN'),
(82, 6, '3.2', 'Menganalisis pelaksanaan kewajiban, hak, dan tanggung jawab sebagai warga negara beserta dampaknya dalam kehidupan sehari-hari ', 'PPKN'),
(83, 6, '3.3', 'Menelaah keberagaman sosial, budaya, dan ekonomi masyarakat', 'PPKN'),
(84, 6, '3.4', ' Menelaah persatuan dan kesatuan terhadap kehidupan berbangsa dan bernegara beserta dampaknya ', 'PPKN'),
(85, 4, '3.1', 'Mengidentifikasi karakteristik ruang dan pemanfaatan sumber daya alam untuk kesejahteraan masyarakat  dari tingkat  kota/kabupaten sampai tingkat provinsi', 'IPS'),
(86, 4, '3.2', ' Mengidentifikasi keragaman sosial, ekonomi, budaya, etnis, dan agama di provinsi setempat sebagai identitas bangsa Indonesia; serta hubungannya dengan karakteristik ruang. ', 'IPS'),
(87, 4, '3.3', ' Mengidentifikasi kegiatan ekonomi dan hubungannya dengan berbagai bidang pekerjaan, serta kehidupan sosial dan budaya di lingkungan sekitar sampai provinsi. ', 'IPS'),
(88, 4, '3.4', ' Mengidentifikasi kerajaan Hindu dan/atau Buddha dan/atau Islam di lingkungan daerah setempat,serta pengaruhnya pada kehidupan masyarakat masa kini. ', 'IPS'),
(89, 5, '3.1', 'Mengidentifikasi karakteristik geografis Indonesia sebagai negara  kepulauan/maritim dan agraris serta pengaruhnya terhadap  kehidupan ekonomi, sosial, budaya, komunikasi, serta transportasi. ', 'IPS'),
(90, 5, '3.2', ' Menganalisis bentuk bentuk interaksi manusia dengan lingkungan dan pengaruhnya terhadap pembangunan sosial, budaya, dan ekonomi masyarakat Indonesia. ', 'IPS'),
(91, 5, '3.3', ' Menganalisis peran ekonomi dalam upaya menyejahterakan kehidupan masyarakat di bidang sosial dan budaya untuk memperkuat kesatuan dan persatuan bangsa. ', 'IPS'),
(92, 5, '3.4', 'Mengidentifikasi faktor-faktor penting penyebab penjajahan  bangsa Indonesia dan upaya bangsa Indonesia dalam mempertahankan kedaulatannya. ', 'IPS'),
(93, 6, '3.1', ' Mengidentifikasi karakteristik geografis dan kehidupan sosial budaya, ekonomi, politik di wilayah ASEAN', 'IPS'),
(94, 6, '3.2', ' Menganalisis perubahan sosial budaya dalam rangka modernisasi bangsa Indonesia. \r\n', 'IPS'),
(95, 6, '3.3', ' Menganalisis posisi dan peran Indonesia dalam kerja sama di bidang ekonomi, politik, sosial, budaya, teknologi, dan pendidikan  dalam lingkup ASEAN. ', 'IPS'),
(96, 6, '3.4', 'Memahami makna proklamasi kemerdekaan, upaya mempertahankan kemerdekaan, dan upaya mengembangkan kehidupan kebangsaan yang sejahtera. ', 'IPS'),
(97, 1, '3.1', 'Memahami gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(98, 1, '3.2', 'Memahami gerak dasar nonlokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(99, 1, '3.3', 'Memahami pola gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(100, 1, '3.4', 'Memahami  menjaga sikap tubuh (duduk, membaca, berdiri, jalan), dan bergerak secara lentur serta seimbang dalam rangka pembentukan tubuh melalui permainan sederhana dan atau tradisional \r\n', 'PJOK'),
(101, 1, '3.5', 'Memahami berbagai gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai \r\n', 'PJOK'),
(102, 1, '3.6', 'Memahami gerak dasar lokomotor dan non-lokomotor sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama ', 'PJOK'),
(103, 1, '3.7', 'Memahami berbagai pengenalan  aktivitas air dan menjaga keselamatan diri/orang lain dalam           aktivitas air*** ', 'PJOK'),
(104, 1, '3.8', 'Memahami bagian-bagian tubuh, bagian tubuh yang boleh dan tidak boleh disentuh orang lain, cara menjaga kebersihannya, dan kebersihan pakaian ', 'PJOK'),
(105, 2, '3.1', 'Memahami  variasi gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(106, 2, '3.2', ' Memahami variasi gerak dasar non-lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(107, 2, '3.3', 'Memahami variasi gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional \r\n \r\n ', 'PJOK'),
(108, 2, '3.4', 'Memahami bergerak secara seimbang, lentur, dan kuat dalam rangka pengembangan kebugaran jasmani melalui permainan sederhana dan atau tradisional', 'PJOK'),
(109, 2, '3.5', 'Memahami variasi berbagai pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai \r\n', 'PJOK'),
(110, 2, '3.6', 'Memahami penggunaan variasi gerak dasar lokomotor dan nonlokomotor sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(111, 2, '3.7', 'Memahami prosedur penggunaan  gerak dasar lokomotor, nonlokomotor,dan manipulatif dalam bentuk permainan, dan menjaga keselamatan diri/orang lain dalam aktivitas air *** ', 'PJOK'),
(112, 2, '3.8', 'Memahami manfaat pemanasan dan pendinginan, serta berbagai hal yang harus dilakukan dan dihindari sebelum, selama, dan setelah melakukan aktivitas fisik ', 'PJOK'),
(113, 2, '3.9', 'Memahami cara menjaga kebersihan lingkungan (tempat tidur, rumah, kelas, lingkungan sekolah, dan lain-lain) ', 'PJOK'),
(114, 3, '3.1', 'Memahami kombinasi gerak dasar lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(115, 3, '3.2', 'Memahami kombinasi gerak dasar non-lokomotor sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(116, 3, '3.3', 'Memahami kombinasi gerak dasar manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai bentuk permainan sederhana dan atau tradisional ', 'PJOK'),
(117, 3, '3.4', 'Memahami bergerak secara seimbang,  lentur, lincah, dan berdaya tahan  dalam rangka pengembangan kebugaran jasmani melalui permainan sederhana dan atau tradisional \r\n', 'PJOK'),
(118, 3, '3.5', 'Memahami kombinasi berbagai pola gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan, dan mendarat) dalam aktivitas senam lantai ', 'PJOK'),
(119, 3, '3.6', 'Memahami penggunaan kombinasi gerak dasar lokomotor, nonlokomotor dan manipulatif sesuai dengan irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(120, 3, '3.7', 'Memahami prosedur gerak dasar mengambang (water trappen)  dan meluncur di air serta menjaga keselamatan diri/orang lain dalam aktivitas air*** ', 'PJOK'),
(121, 3, '3.8', 'Memahami bentuk dan manfaat istirahat dan pengisian waktu luang untuk menjaga kesehatan ', 'PJOK'),
(122, 3, '3.9', 'Memahami perlunya memilih  makanan bergizi dan jajanan sehat untuk menjaga kesehatan tubuh ', 'PJOK'),
(123, 4, '3.1', 'Memahami variasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(124, 4, '3.2', 'Memahami variasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam permainan bola kecil sederhana dan atau tradisional*', 'PJOK'),
(125, 4, '3.3', 'Memahami variasi gerak dasar  jalan, lari,  lompat, dan lempar melalui permainan/olahraga yang dimodifikasi dan atau olahraga tradisional', 'PJOK'),
(126, 4, '3.4', 'Menerapkan gerak dasar lokomotor dan non-lokomotor untuk membentuk gerak dasar seni beladiri*', 'PJOK'),
(127, 4, '3.5', 'Memahami berbagai bentuk aktivitas kebugaran jasmani melalui berbagai latihan; daya tahan, kekuatan, kecepatan,  dan kelincahan  untuk mencapai berat badan ideal ', 'PJOK'),
(128, 4, '3.6', ' Menerapkan variasi dan kombinasi berbagai pola gerak dominan  (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dalam aktivitas senam lantai', 'PJOK'),
(129, 4, '3.7', 'Menerapkan variasi gerak dasar  langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama', 'PJOK'),
(130, 4, '3.8', 'Memahami gerak dasar satu gaya renang*** ', 'PJOK'),
(131, 4, '3.9', 'Memahami jenis cidera dan cara penanggulangannya secara sederhana saat melakukan aktivitas fisik dan dalam kehidupan sehari-hari ', 'PJOK'),
(132, 4, '3.10', 'Menganalisis perilaku terpuji dalam pergaulan sehari-hari (antar teman sebaya, orang yang lebih tua, dan orang yang lebih muda) ', 'PJOK'),
(133, 5, '3.1', 'Memahami kombinasi gerak lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(134, 5, '3.2', 'Memahami kombinasi gerak dasar lokomotor, non-lokomotor, dan manipulatif sesuai dengan konsep tubuh, ruang, usaha, dan keterhubungan dalam berbagai permainan bola kecil sederhana dan atau tradisional* \r\n', 'PJOK'),
(135, 5, '3.3', 'Memahami kombinasi gerak dasar  jalan, lari,  lompat, dan lempar melalui permainan/olahraga yang dimodifikasi dan atau olahraga tradisional ', 'PJOK'),
(136, 5, '3.4', 'Menerapkan variasi gerak dasar lokomotor dan non lokomotor untuk membentuk gerak dasar seni beladiri**', 'PJOK'),
(137, 5, '3.5', ' Memahami aktivitas latihan daya tahan jantung (cardio respiratory) untuk pengembangan kebugaran jasmani ', 'PJOK'),
(138, 5, '3.6', 'Memahami kombinasi pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) untuk membentuk keterampilan dasar senam menggunakan alat ', 'PJOK'),
(139, 5, '3.7', 'Memahami  penggunaan kombinasi gerak dasar  langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama ', 'PJOK'),
(140, 5, '3.8', 'Memahami salah satu gaya renang dengan koordinasi yang baik pada jarak tertentu*** ', 'PJOK'),
(141, 5, '3.9', ' Memahami konsep pemeliharaan diri dan orang lain dari penyakit menular dan tidak menular ', 'PJOK'),
(142, 5, '3.10', 'Memahami bahaya merokok, minuman keras, dan narkotika,   zat-zat aditif (NAPZA) dan obat berbahaya  lainnya terhadap kesehatan tubuh ', 'PJOK'),
(143, 6, '3.1', 'Memahami variasi dan kombinasi gerak dasar lokomotor, nonlokomotor, dan manipulatif dengan kontrol yang baik dalam permainan bola besar sederhana dan atau tradisional* ', 'PJOK'),
(144, 6, '3.2', 'Memahami variasi dan kombinasi gerak dasar lokomotor, nonlokomotor, dan manipulatif dengan kontrol yang baik dalam permainan bola kecil sederhana dan atau tradisional* ', 'PJOK'),
(145, 6, '3.3', 'Memahami variasi dan kombinasi gerak dasar jalan, lari, lompat, dan lempar dengan kontrol yang baik melalui permainan dan atau olahraga tradisional ', 'PJOK'),
(146, 6, '3.4', 'Memahami variasi dan kombinasi gerak dasar lokomotor, non lokomotor, dan manipulatif untuk membentuk gerak dasar seni beladiri** ', 'PJOK'),
(147, 6, '3.5', 'Memahami latihan kebugaran jasmani dan pengukuran tingkat kebugaran jasmani pribadi secara sederhana (contoh: menghitung denyut nadi, menghitung kemampuan melakukan push up, menghitung kelenturan tungkai) ', 'PJOK'),
(148, 6, '3.6', 'Memahami rangkaian tiga pola gerak dominan (bertumpu, bergantung, keseimbangan, berpindah/lokomotor, tolakan, putaran, ayunan, melayang, dan mendarat) dengan konsisten, tepat dan terkontrol dalam aktivitas senam ', 'PJOK'),
(149, 6, '3.7', 'Memahami penggunaan variasi dan kombinasi gerak dasar rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik dalam aktivitas gerak berirama ', 'PJOK'),
(150, 6, '3.8', 'Memahami keterampilan salah satu gaya renang dan dasar-dasar penyelamatan diri*** ', 'PJOK'),
(151, 6, '3.9', 'Memahami perlunya pemeliharaan kebersihan alat reproduksi \r\n', 'PJOK'),
(152, 1, '3.1', 'mengenal karya ekspresi dua dan tiga dimensi ', 'Seni Budaya dan Prakarya'),
(153, 1, '3.2', 'mengenal elemen musik melalui lagu ', 'Seni Budaya dan Prakarya'),
(154, 1, '3.3', 'mengenal gerak anggota tubuh melalui tari ', 'Seni Budaya dan Prakarya'),
(155, 1, '3.4', ' mengenal bahan alam dalam berkarya ', 'Seni Budaya dan Prakarya'),
(156, 2, '3.1', ' mengenal karya imajinatif dua dan tiga dimensi ', 'Seni Budaya dan Prakarya'),
(157, 2, '3.2', ' mengenal pola irama sederhana melalui lagu anak-anak ', 'Seni Budaya dan Prakarya'),
(158, 2, '3.3', 'mengenal gerak keseharian dan alam dalam tari ', 'Seni Budaya dan Prakarya'),
(159, 2, '3.4', ' mengenal pengolahan bahan alam dan buatan dalam berkarya ', 'Seni Budaya dan Prakarya'),
(160, 3, '3.1', 'mengetahui unsur-unsur rupa dalam karya dekoratif ', 'Seni Budaya dan Prakarya'),
(161, 3, '3.2', 'mengetahui bentuk dan variasi pola irama dalam  lagu ', 'Seni Budaya dan Prakarya'),
(162, 3, '3.3', 'mengetahui dinamika gerak tari ', 'Seni Budaya dan Prakarya'),
(163, 3, '3.4', 'mengetahui teknik potong, lipat, dan sambung', 'Seni Budaya dan Prakarya'),
(164, 4, '3.1', 'mengetahui gambar dan bentuk tiga dimensi', 'Seni Budaya dan Prakarya'),
(165, 4, '3.2', 'mengetahui tanda tempo dan tinggi rendah nada ', 'Seni Budaya dan Prakarya'),
(166, 4, '3.3', 'mengetahui gerak tari kreasi daerah ', 'Seni Budaya dan Prakarya'),
(167, 4, '3.4', 'mengetahui karya seni rupa teknik tempel ', 'Seni Budaya dan Prakarya'),
(168, 5, '3.1', 'memahami gambar cerita ', 'Seni Budaya dan Prakarya'),
(169, 5, '3.2', 'memahami tangga nada ', 'Seni Budaya dan Prakarya'),
(170, 5, '3.3', ' memahami pola lantai dalam tari kreasi daerah ', 'Seni Budaya dan Prakarya'),
(171, 5, '3.4', 'memahami karya seni rupa daerah ', 'Seni Budaya dan Prakarya'),
(172, 6, '3.1', 'memahami reklame ', 'Seni Budaya dan Prakarya'),
(173, 6, '3.2', 'memahami interval nada ', 'Seni Budaya dan Prakarya'),
(174, 6, '3.3', 'memahami penampilan tari kreasi daerah ', 'Seni Budaya dan Prakarya'),
(175, 6, '3.4', 'memahami patung ', 'Seni Budaya dan Prakarya'),
(176, 1, '3.6', 'Mengenal bangun ruang dan bangun datar dengan menggunakan berbagai benda konkret ', 'Matematika'),
(177, 1, '3.7', 'Mengidentifikasi bangun datar yang dapat disusun membentuk pola pengubinan', 'Matematika'),
(178, 1, '3.8', 'Mengenal dan menentukan panjang dan berat dengan satuan tidak baku menggunakan benda/situasi konkret', 'Matematika'),
(179, 1, '3.9', 'Membandingkan panjang, berat, lamanya waktu, dan suhu  menggunakan benda/ situasi konkret ', 'Matematika'),
(180, 2, '3.6', 'Menjelaskan dan menentukan panjang (termasuk jarak), berat, dan waktu dalam satuan baku, yang berkaitan dengan kehidupan sehari-hari ', 'Matematika'),
(181, 2, '3.7', 'Menjelaskan pecahan 1/2, 1/3 , dan 1/4  menggunakan benda-benda konkret dalam kehidupan seharihari', 'Matematika'),
(182, 2, '3.8', 'Menjelaskan ruas garis dengan menggunakan model konkret bangun datar dan bangun ruang ', 'Matematika'),
(183, 2, '3.9', 'Menjelaskan bangun datar dan bangun ruang berdasarkan ciricirinya ', 'Matematika'),
(184, 2, '3.10', 'Menjelaskan pola barisan bangun datar dan bangun ruang menggunakan gambar atau benda konkret ', 'Matematika'),
(185, 3, '3.6', ' Menjelaskan dan menentukan  lama waktu  suatu kejadian berlangsung ', 'Matematika'),
(186, 3, '3.7', 'Mendeskripsikan dan menentukan hubungan antar satuan baku untuk  panjang, berat, dan waktu yang umumnya digunakan dalam kehidupan sehari-hari ', 'Matematika'),
(187, 3, '3.8', 'Menjelaskan dan menentukan luas dan volume dalam satuan tidak baku dengan menggunakan benda konkret ', 'Matematika'),
(188, 3, '3.9', ' Menjelaskan simetri lipat dan simetri putar pada bangun datar menggunakan benda konkret ', 'Matematika'),
(189, 3, '3.10', ' Menjelaskan dan menentukan keliling bangun datar ', 'Matematika'),
(190, 3, '3.11', ' Menjelaskan sudut, jenis sudut (sudut siku-siku, sudut lancip, dan sudut tumpul), dan satuan pengukuran tidak baku \r\n', 'Matematika'),
(191, 3, '3.12', ' Menganalisis berbagai bangun datar berdasarkan sifat-sifat yang dimiliki ', 'Matematika'),
(192, 3, '3.13', ' Menjelaskan data berkaitan dengan diri peserta didik yang disajikan dalam diagram gambar ', 'Matematika'),
(193, 4, '3.6', 'Menjelaskan dan menentukan faktor persekutuan, faktor persekutuan terbesar (FPB), kelipatan persekutuan, dan kelipatan persekutuan terkecil (KPK) dari dua bilangan berkaitan dengan kehidupan sehari-hari ', 'Matematika'),
(194, 4, '3.7', 'Menjelaskan dan melakukan pembulatan hasil pengukuran panjang dan berat ke satuan terdekat ', 'Matematika'),
(195, 4, '3.8', 'Menganalisis sifat-sifat segibanyak beraturan dan segibanyak tidak beraturan', 'Matematika'),
(196, 4, '3.9', 'Menjelaskan dan menentukan keliling dan luas persegi, persegipanjang, dan segitiga serta hubungan pangkat dua dengan akar pangkat dua ', 'Matematika'),
(197, 4, '3.10', ' Menjelaskan hubungan antar garis (sejajar, berpotongan, berhimpit) menggunakan model konkret ', 'Matematika'),
(198, 4, '3.11', ' Menjelaskan data diri peserta didik dan lingkungannya yang disajikan dalam bentuk diagram batang', 'Matematika'),
(199, 4, '3.12', 'Menjelaskan dan menentukan ukuran sudut pada bangun datar dalam satuan baku dengan menggunakan busur derajat ', 'Matematika'),
(200, 5, '3.1', 'Menjelaskan dan melakukan penjumlahan dan pengurangan dua pecahan dengan penyebut berbeda ', 'Matematika'),
(201, 5, '3.2', 'Menjelaskan dan melakukan perkalian dan pembagian pecahan dan desimal', 'Matematika'),
(202, 5, '3.3', 'Menjelaskan perbandingan dua besaran yang berbeda (kecepatan sebagai perbandingan jarak dengan waktu, debit sebagai perbandingan volume dan waktu) ', 'Matematika'),
(203, 5, '3.4', 'Menjelaskan skala melalui denah ', 'Matematika'),
(204, 5, '3.5', 'Menjelaskan, dan menentukan volume bangun ruang dengan menggunakan satuan volume (seperti kubus satuan) serta hubungan pangkat tiga dengan akar pangkat tiga', 'Matematika'),
(205, 5, '3.6', 'Menjelaskan dan menemukan jaring-jaring bangun ruang sederhana (kubus dan balok) ', 'Matematika'),
(206, 5, '3.7', 'Menjelaskan data yang berkaitan dengan diri peserta didik atau lingkungan sekitar serta cara pengumpulannya ', 'Matematika'),
(207, 5, '3.8', 'Menjelaskan penyajian data  yang berkaitan dengan diri peserta didik dan membandingkan dengan data dari lingkungan sekitar dalam bentuk daftar, tabel, diagram gambar (piktogram), diagram batang, atau diagram garis ', 'Matematika'),
(208, 6, '3.1', 'Menjelaskan bilangan bulat negatif (termasuk menggunakan garis bilangan) ', 'Matematika'),
(209, 6, '3.2', 'Menjelaskan dan melakukan operasi penjumlahan, pengurangan, perkalian, dan pembagian yang melibatkan bilangan bulat negatif ', 'Matematika'),
(210, 6, '3.3', 'Menjelaskan dan melakukan operasi hitung campuran yang melibatkan bilangan cacah, pecahan dan/atau desimal dalam berbagai bentuk sesuai urutan operasi ', 'Matematika'),
(211, 6, '3.4', 'Menjelaskan titik pusat, jari-jari, diameter, busur, tali busur, tembereng, dan juring', 'Matematika'),
(212, 6, '3.5', 'Menjelaskan taksiran keliling dan luas lingkaran ', 'Matematika'),
(213, 6, '3.6', 'Membandingkan prisma, tabung, limas, kerucut, dan bola. ', 'Matematika'),
(214, 6, '3.7', 'Menjelaskan bangun ruang yang merupakan gabungan dari beberapa bangun ruang, serta luas permukaan dan volumenya', 'Matematika'),
(215, 6, '3.8', 'Menjelaskan dan membandingkan modus, median, dan mean dari data tunggal untuk menentukan nilai mana yang paling tepat mewakili data', 'Matematika'),
(216, 1, '3.6', 'Menguraikan kosakata tentang berbagai jenis benda di lingkungan sekitar melalui teks pendek (berupa gambar, slogan sederhana, tulisan, dan/atau syair lagu) dan/atau eksplorasi lingkungan.', 'Bahasa Indonesia'),
(217, 1, '3.7', ' Menentukan kosakata yang berkaitan dengan peristiwa siang dan malam melalui teks pendek (gambar, tulisan, dan/atau syair lagu) dan/atau eksplorasi lingkungan. ', 'Bahasa Indonesia'),
(218, 1, '3.8', 'Merinci ungkapan penyampaian terima kasih, permintaan maaf, tolong, dan pemberian pujian, ajakan, pemberitahuan, perintah, dan petunjuk kepada orang lain dengan menggunakan bahasa yang santun secara lisan dan tulisan yang dapat dibantu dengan kosakata bahasa daerah ', 'Bahasa Indonesia'),
(219, 1, '3.9', 'Merinci kosakata dan ungkapan perkenalan diri, keluarga, dan orang-orang di tempat tinggalnya secara lisan dan tulis yang dapat dibantu dengan kosakata bahasa daerah', 'Bahasa Indonesia'),
(220, 1, '3.10', ' Menguraikan kosakata hubungan  kekeluargaan melalui gambar/bagan silsilah keluarga dalam bahasa Indonesia atau bahasa daerah ', 'Bahasa Indonesia'),
(221, 1, '3.11', 'Mencermati puisi anak/syair lagu (berisi ungkapan kekaguman, kebanggaan, hormat kepada orang tua, kasih sayang, atau persahabatan) yang diperdengarkan dengan tujuan untuk kesenangan ', 'Bahasa Indonesia'),
(222, 2, '3.6', 'Mencermati ungkapan permintaan maaf dan tolong melalui teks tentang budaya santun sebagai gambaran  sikap hidup rukun dalam kemajemukan masyarakat Indonesia', 'Bahasa Indonesia'),
(223, 2, '3.7', 'Mencermati tulisan tegak bersambung dalam cerita  dengan memperhatikan penggunaan  huruf kapital (awal kalimat, nama bulan dan hari, nama orang) serta mengenal tanda titik pada kalimat berita dan tanda tanya pada kalimat tanya ', 'Bahasa Indonesia'),
(224, 2, '3.8', 'Menggali informasi dari dongeng binatang (fabel) tentang sikap hidup rukun dari teks lisan dan tulis dengan tujuan untuk kesenangan ', 'Bahasa Indonesia'),
(225, 2, '3.9', ' Menentukan kata sapaan dalam dongeng secara lisan dan tulis ', 'Bahasa Indonesia'),
(226, 2, '3.10', 'Mencermati penggunaan huruf kapital (nama Tuhan nama orang, nama agama) serta tanda titik dan tanda tanya dalam kalimat yang benar', 'Bahasa Indonesia'),
(227, 3, '3.6', 'Mencermati isi teks informasi tentang perkembangan teknologi produksi, komunikasi, dan transportasi di lingkungan setempat ', 'Bahasa Indonesia'),
(228, 3, '3.7', 'Mencermati informasi tentang konsep delapan arah mata angin dan pemanfaatannya dalam denah dalam teks lisan, tulis, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(229, 3, '3.8', ' Menguraikan pesan dalam dongeng yang disajikan secara lisan, tulis, dan visual dengan tujuan untuk kesenangan ', 'Bahasa Indonesia'),
(230, 3, '3.9', 'Mengidentifi-kasi lambang/ simbol (rambu lalu lintas, pramuka, dan lambang negara) beserta artinya dalam teks lisan, tulis, visual, dan/atau eksplorasi lingkungan ', 'Bahasa Indonesia'),
(231, 3, '3.10', 'Mencermati ungkapan atau kalimat saran, masukan,  dan penyelesaian masalah (sederhana) dalam teks tulis', 'Bahasa Indonesia'),
(232, 4, '3.6', 'Menggali isi dan amanat puisi yang disajikan secara lisan dan tulis dengan tujuan untuk kesenangan', 'Bahasa Indonesia'),
(233, 4, '3.7', 'Menggali pengetahuan baru yang terdapat pada teks nonfiksi ', 'Bahasa Indonesia'),
(234, 4, '3.8', 'Membandingkan hal yang sudah diketahui dengan yang baru diketahui dari teks nonfiksi ', 'Bahasa Indonesia'),
(235, 4, '3.9', 'Mencermati tokoh-tokoh yang terdapat pada teks fiksi ', 'Bahasa Indonesia'),
(236, 4, '3.10', 'Membanding-kan watak setiap tokoh pada teks fiksi ', 'Bahasa Indonesia'),
(237, 5, '3.6', 'Menggali isi dan amanat pantun yang disajikan secara lisan dan tulis dengan tujuan untuk kesenangan ', 'Bahasa Indonesia'),
(238, 5, '3.7', ' Menguraikan konsep-konsep yang saling berkaitan pada teks nonfiksi ', 'Bahasa Indonesia'),
(239, 5, '3.8', ' Menguraikan urutan peristiwa atau tindakan yang terdapat pada teks nonfiksi ', 'Bahasa Indonesia'),
(240, 5, '3.9', 'Mencermati  penggunaan kalimat efektif dan ejaan dalam surat undangan (ulang tahun, kegiatan sekolah, kenaikan kelas, dll.) ', 'Bahasa Indonesia'),
(241, 6, '3.1', 'Menyimpulkan informasi berdasarkan teks laporan hasil pengamatan yang didengar dan dibaca', 'Bahasa Indonesia'),
(242, 6, '3.2', 'Menggali isi teks penjelasan (eksplanasi) ilmiah yang didengar dan dibaca ', 'Bahasa Indonesia'),
(243, 6, '3.3', 'Menggali isi teks pidato yang didengar dan dibaca', 'Bahasa Indonesia'),
(244, 6, '3.4', ' Menggali informasi penting dari buku sejarah menggunakan aspek: apa, di mana, kapan, siapa, mengapa, dan bagaimana ', 'Bahasa Indonesia'),
(245, 6, '3.5', 'Membandingkan karakteristik teks puisi dan teks prosa ', 'Bahasa Indonesia'),
(246, 6, '3.6', 'Mencermati petunjuk dan isi teks  formulir (pendaftaran, kartu anggota, pengiriman uang melalui bank/kantor pos,  daftar riwayat hidup, dsb.)', 'Bahasa Indonesia'),
(247, 6, '3.7', ' Memperkirakan informasi yang dapat diperoleh dari teks nonfiksi sebelum membaca (hanya berdasarkan membaca judulnya saja) ', 'Bahasa Indonesia'),
(248, 6, '3.8', 'Menggali informasi yang terdapat pada teks nonfiksi ', 'Bahasa Indonesia'),
(249, 6, '3.9', 'Menelusuri tuturan dan tindakan tokoh serta penceritaan penulis dalam teks fiksi ', 'Bahasa Indonesia'),
(250, 6, '3.10', 'Mengaitkan peristiwa yang dialami tokoh dalam cerita fiksi dengan pengalaman pribadi ', 'Bahasa Indonesia'),
(251, 7, '3.1', 'Mengidentifikasi informasi dalam teks deskripsi tentang objek (sekolah, tempat wisata, tempat bersejarah, dan atau suasana pentas seni daerah) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(252, 7, '3.2', 'Menelaah struktur dan kebahasaan dari  teks deskripsi tentang objek (sekolah, tempat wisata, tempat bersejarah, dan⁄atau suasana pentas seni daerah) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(253, 7, '3.3', 'Mengidentifikasi  unsur-unsur teks narasi (cerita imajinasi) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(254, 7, '3.4', 'Menelaah struktur dan kebahasaan teks narasi (cerita imajinasi) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(255, 7, '3.5', 'Mengidentifikasi teks prosedur tentang cara melakukan sesuatu  dan cara membuat (cara  memainkan alat musik/tarian daerah, cara membuat kuliner khas daerah, dll.) dari berbagai sumber yang dibaca dan didengar ', 'Bahasa Indonesia'),
(256, 7, '3.6', ' Menelaah struktur dan aspek kebahasaan  teks prosedur tentang cara melakukan sesuatu dan cara membuat (cara memainkan alat musik/tarian daerah, cara membuat kuliner khas daerah, dll.)  dari berbagai sumber yang dibaca dan didengar', 'Bahasa Indonesia'),
(257, 7, '3.7', 'Mengidentifikasi informasi dari   teks laporan hasil observasi berupa buku  pengetahuan yang dibaca atau diperdengarkan ', 'Bahasa Indonesia'),
(258, 7, '3.8  ', 'Menelaah  struktur, kebahasaan, dan isi teks laporan hasil observasi yang berupa buku  pengetahuan  yang dibaca atau diperdengarkan ', 'Bahasa Indonesia'),
(259, 7, '3.9', 'Menemukan unsur-unsur dari buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(260, 7, '3.10', 'Menelaah hubungan unsur-unsur  dalam buku fiksi dan nonfiksi', 'Bahasa Indonesia'),
(261, 7, '3.11', 'Mengidentifikasi  informasi (kabar, keperluan, permintaan, dan/atau permohonan) dari surat pribadi dan surat dinas yang dibaca dan didengar ', 'Bahasa Indonesia'),
(262, 7, '3.12', 'Menelaah unsur-unsur dan kebahasaan dari surat pribadi dan surat dinas yang dibaca dan didengar ', 'Bahasa Indonesia'),
(263, 7, '3.13', 'Mengidentifikasi informasi (pesan, rima, dan pilihan kata) dari puisi  rakyat (pantun, syair, dan bentuk puisi rakyat setempat) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(264, 7, '3.14', ' Menelaah struktur dan kebahasaan puisi  rakyat (pantun, syair, dan bentuk puisi rakyat setempat) yang dibaca dan didengar ', 'Bahasa Indonesia'),
(265, 7, '3.15', 'Mengidentifikasi informasi tentang fabel/legenda daerah setempat yang dibaca dan didengar', 'Bahasa Indonesia'),
(266, 7, '3.16', 'Menelaah  struktur dan kebahasaan fabel/legenda daerah setempat yang dibaca dan didengar ', 'Bahasa Indonesia'),
(267, 8, '3.1', 'Mengidentifikasi unsur-unsur  teks berita  (membanggakan dan memotivasi) yang didengar dan dibaca', 'Bahasa Indonesia'),
(268, 8, '3.2', 'Menelaah struktur dan kebahasaan teks berita (membanggakan dan memotivasi)  yang didengar dan dibaca ', 'Bahasa Indonesia'),
(269, 8, '3.3', ' Mengidentifikasi informasi teks iklan, slogan, atau poster (yang membuat bangga dan memotivasi) dari berbagai sumber yang dibaca dan didengar ', 'Bahasa Indonesia'),
(270, 8, '3.4', ' Menelaah pola penyajian dan kebahasaan teks iklan, slogan, atau poster (yang membuat bangga dan memotivasi) dari berbagai sumber yang dibaca dan didengar ', 'Bahasa Indonesia'),
(271, 8, '3.5', ' Mengidentifikasi informasi teks eksposisi berupa artikel ilmiah populer dari koran/majalah) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(272, 8, '3.6', ' Mengidentifikasi struktur, unsur kebahasaan, dan aspek lisandalamteks eksposisi  artikel ilmiah populer (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) yang diperdengarkan atau dibaca', 'Bahasa Indonesia'),
(273, 8, '3.7', 'Mengidentifikasi unsur-unsur pembangun teks puisi yang diperdengarkan atau dibaca ', 'Bahasa Indonesia'),
(274, 8, '3.8', 'Menelaah unsur-unsur pembangun teks puisi (perjuangan, lingkungan hidup, kondisi sosial, dan lain-lain) yang diperdengarkan atau dibaca \r\n ', 'Bahasa Indonesia'),
(275, 8, '3.9', 'Mengidentifikasi informasi dari teks ekplanasi berupa paparan kejadian suatu fenomena alam yang diperdengarkan atau dibaca ', 'Bahasa Indonesia'),
(276, 8, '3.10', 'Menelaah teks ekplanasi berupa paparan kejadian suatu fenomena alam yang diperdengarkan atau dibaca \r\n ', 'Bahasa Indonesia'),
(277, 8, '3.11', ' Mengidentifikasi  informasi pada teks  ulasan tentang kualitas karya (film, cerpen, puisi, novel, dan karya seni daerah) yang dibaca atau diperdengarkan ', 'Bahasa Indonesia'),
(278, 8, '3.12', 'Menelaah struktur  dan kebahasaan teks ulasan (film, cerpen, puisi, novel, dan karya seni daerah) yang diperdengarkan dan dibaca \r\n ', 'Bahasa Indonesia'),
(279, 8, '3.13', 'Mengidentifikasi jenis  saran, ajakan, arahan, dan pertimbangan tentang berbagai hal positif atas permasalahan aktual dari teks persuasi (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan dibaca ', 'Bahasa Indonesia'),
(280, 8, '3.14', ' Menelaah struktur dan kebahasaan teks persuasi yang berupa saran, ajakan, dan pertimbangan tentang berbagai permasalahan aktual (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) dari berbagai sumber yang didengar dan dibaca ', 'Bahasa Indonesia'),
(281, 8, '3.15', 'Mengidentifikasi unsur-unsur  drama (tradisional dan moderen) yang disajikan dalam bentuk pentas atau naskah ', 'Bahasa Indonesia'),
(282, 8, '3.16', 'Menelaah karakteristik unsur dan kaidah kebahasaan dalam teks drama yang berbentuk naskah atau pentas ', 'Bahasa Indonesia'),
(283, 8, '3.17', 'Menggali dan menemukan informasi dari buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(284, 8, '3.18', 'Menelaah unsur  buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(285, 9, '3.1', 'Mengidentifikasi informasi dari laporan  percobaan yang dibaca dan didengar  (percobaan sederhana untuk mendeteksi zat berbahaya pada makanan, adanya vitamin pada makanan, dll) ', 'Bahasa Indonesia'),
(286, 9, '3.2', 'Menelaah struktur dan kebahasaan dari teks laporan percobaan yang didengar atau dibaca (percobaan sederhana untuk mendeteksi zat berbahaya pada makanan, adanya vitamin pada makanan, dll)', 'Bahasa Indonesia'),
(287, 9, '3.3', 'Mengidentifikasi gagasan, pikiran, pandangan, arahan atau pesan dalam pidato persuasif tentang permasalahan aktual yang didengar dan dibaca ', 'Bahasa Indonesia'),
(288, 9, '3.4', 'Menelaah struktur dan ciri kebahasaan pidato persuasif tentang permasalahan aktual yang didengar dan dibaca ', 'Bahasa Indonesia'),
(289, 9, '3.5', 'Mengidentifikasi  unsur pembangun karya sastra dalam teks cerita pendek yang dibaca atau didengar ', 'Bahasa Indonesia'),
(290, 9, '3.6', 'Menelaah  struktur  dan aspek kebahasaan cerita pendek yang dibaca atau didengar ', 'Bahasa Indonesia'),
(291, 9, '3.7', 'Mengidentifikasi informasi berupa kritik, sanggahan, atau pujian dari teks tanggapan (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) yang didengar dan/atau dibaca ', 'Bahasa Indonesia'),
(292, 9, '3.8', 'Menelaah  struktur dan kebahasaan  dari teks tanggapan (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) berupa kritik, sanggahan, atau pujian yang didengar dan/atau  dibaca ', 'Bahasa Indonesia'),
(293, 9, '3.9', 'Mengidentifikasi  informasi teks diskusi berupa pendapat pro dan kontra dari permasalahan aktual yang dibaca dan didengar ', 'Bahasa Indonesia'),
(294, 9, '3.10', ' Menelaah  pendapat dan argumen yang mendukung dan yang kontra dalam teks diskusi berkaitan dengan permasalahan aktual yang dibaca dan didengar ', 'Bahasa Indonesia'),
(295, 9, '3.11', 'Mengidentifikasi isi ungkapan simpati, kepedulian, empati, atau perasaan pribadi dari teks cerita inspiratif yang dibaca dan didengar ', 'Bahasa Indonesia'),
(296, 9, '3.12', 'Menelaah struktur, kebahasaan, dan isi  teks cerita inspiratif ', 'Bahasa Indonesia'),
(297, 9, '3.13', 'Menggali informasi  unsur-unsur buku fiksi dan nonfiksi ', 'Bahasa Indonesia'),
(298, 9, '3.14', 'Menelaah hubungan antara unsurunsur buku fiksi/nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(299, 9, '3.15', 'Menemukan unsur-unsur dari buku fiksi dan nonfiksi yang dibaca ', 'Bahasa Indonesia'),
(300, 9, '3.16', ' Menelaah hubungan unsur-unsur dalam buku fiksi dan nonfiksi ', 'Bahasa Indonesia'),
(301, 7, '3.1', ' Menerapkan konsep pengukuran berbagai besaran dengan menggunakan satuan standar (baku) ', 'IPA'),
(302, 7, '3.2', 'Mengklasifikasikan makhluk hidup dan benda berdasarkan karakteristik yang diamati ', 'IPA'),
(303, 7, '3.3', 'Menjelaskan konsep campuran dan zat tunggal (unsur dan senyawa), sifat fisika dan kimia, perubahan fisika dan kimia dalam kehidupan sehari-hari ', 'IPA'),
(304, 7, '3.4', 'Menganalisis konsep suhu, pemuaian, kalor, perpindahan kalor, dan penerapannya dalam kehidupan sehari-hari termasuk mekanisme menjaga kestabilan suhu tubuh pada manusia dan hewan', 'IPA'),
(305, 7, '3.5', 'Menganalisis konsep energi, berbagai sumber energi, dan perubahan bentuk energi dalam kehidupan sehari-hari termasuk fotosintesis ', 'IPA'),
(306, 7, '3.6', 'Mengidentifikasi sistem organisasi kehidupan mulai dari tingkat sel sampai organisme dan komposisi utama penyusun sel', 'IPA'),
(307, 7, '3.7', 'Menganalisis interaksi antara makhluk hidup dan lingkungannya serta dinamika populasi akibat interaksi tersebut ', 'IPA'),
(308, 7, '3.8', ' Menganalisis terjadinya pencemaran lingkungan dan dampaknya bagi ekosistem ', 'IPA'),
(309, 7, '3.9', 'Menganalisis perubahan iklim dan dampaknya bagi ekosistem ', 'IPA'),
(310, 7, '3.10', 'Menjelaskan lapisan bumi, gunung api, gempa bumi, dan tindakan pengurangan resiko sebelum, pada saat, dan pasca bencana sesuai ancaman bencana di daerahnya ', 'IPA'),
(311, 7, '3.11', 'Menganalisis sistem tata surya, rotasi dan revolusi bumi, rotasi dan revolusi bulan, serta dampaknya bagi kehidupan di bumi ', 'IPA'),
(312, 8, '3.1', 'Menganalisis gerak pada makhluk hidup, sistem gerak pada manusia, dan upaya menjaga kesehatan sistem gerak', 'IPA'),
(313, 8, '3.2', 'Menganalisis gerak lurus, pengaruh gaya terhadap gerak berdasarkan Hukum Newton, dan penerapannya pada gerak benda dan gerak makhluk hidup \r\n', 'IPA'),
(314, 8, '3.3', 'Menjelaskan konsep usaha, pesawat sederhana, dan penerapannya dalam kehidupan sehari-hari termasuk kerja otot pada struktur rangka manusia ', 'IPA'),
(315, 8, '3.4', 'Menganalisis keterkaitan struktur jaringan tumbuhan dan fungsinya, serta teknologi yang terinspirasi oleh struktur tumbuhan ', 'IPA'),
(316, 8, '3.5', 'Menganalisis sistem pencernaan pada manusia dan memahami gangguan yang berhubungan dengan sistem pencernaan, serta upaya menjaga kesehatan sistem pencernaan ', 'IPA'),
(317, 8, '3.6', 'Menjelaskan berbagai zat aditif dalam makanan dan minuman, zat adiktif, serta dampaknya terhadap kesehatan ', 'IPA'),
(318, 8, '3.7', 'Menganalisis sistem peredaran darah pada manusia dan memahami gangguan pada sistem peredaran darah, serta upaya menjaga kesehatan sistem peredaran darah', 'IPA'),
(319, 8, '3.8', 'Menjelaskan tekanan zat dan penerapannya dalam kehidupan sehari-hari, termasuk tekanan darah, osmosis, dan kapilaritas jaringan angkut pada tumbuhan', 'IPA'),
(320, 8, '3.9', 'Menganalisis sistem pernapasan pada manusia dan memahami gangguan pada sistem pernapasan, serta upaya menjaga kesehatan sistem pernapasan ', 'IPA'),
(321, 8, '3.10', 'Menganalisis sistem ekskresi pada manusia dan memahami gangguan pada sistem ekskresi serta upaya menjaga kesehatan sistem ekskresi ', 'IPA'),
(322, 8, '3.11', 'Menganalisis konsep getaran, gelombang, dan bunyi dalam kehidupan sehari-hari termasuk sistem pendengaran manusia dan sistem sonar pada hewan', 'IPA'),
(323, 8, '3.12', 'Menganalisis sifat-sifat cahaya, pembentukan bayangan pada bidang datar dan lengkung serta penerapannya untuk menjelaskan proses penglihatan manusia, mata serangga, dan prinsip kerja alat optik ', 'IPA'),
(324, 9, '3.1', 'Menghubungkan sistem reproduksi pada manusia dan gangguan pada sistem reproduksi dengan penerapan pola hidup yang menunjang kesehatan reproduksi ', 'IPA'),
(325, 9, '3.2', 'Menganalisis sistem perkembangbiakan pada tumbuhan dan hewan serta penerapan teknologi pada sistem reproduksi tumbuhan dan hewan ', 'IPA'),
(326, 9, '3.3', 'Menerapkan konsep pewarisan sifat dalam pemuliaan dan kelangsungan makhluk hidup ', 'IPA'),
(327, 9, '3.4', 'Menjelaskan konsep listrik statis dan gejalanya dalam kehidupan sehari-hari, termasuk kelistrikan pada sistem saraf dan hewan yang mengandung listrik ', 'IPA'),
(328, 9, '3.5', 'Menerapkan konsep rangkaian listrik, energi dan daya listrik, sumber energi listrik dalam kehidupan sehari-hari termasuk sumber energi listrik alternatif, serta berbagai upaya menghemat energi listrik ', 'IPA'),
(329, 9, '3.6', ' Menerapkan konsep kemagnetan, induksi elektromagnetik, dan pemanfaatan medan magnet dalam kehidupan sehari-hari termasuk  pergerakan/navigasi hewan untuk mencari makanan dan migrasi ', 'IPA'),
(330, 9, '3.7', 'Menerapkan konsep bioteknologi dan perannya dalam kehidupan manusia ', 'IPA'),
(331, 9, '3.8', 'Menghubungkan konsep partikel materi (atom, ion,molekul), struktur zat sederhana dengan sifat bahan yang digunakan dalam kehidupan sehari- hari, serta dampak penggunaannya terhadap kesehatan manusia ', 'IPA'),
(332, 9, '3.9', 'Menghubungkan sifat fisika dan kimia tanah, organisme yang hidup dalam tanah, dengan pentingnya tanah untuk keberlanjutan kehidupan ', 'IPA'),
(333, 9, '3.10', ' Menganalisis proses dan produk teknologi ramah lingkungan untuk keberlanjutan kehidupan   ', 'IPA'),
(334, 7, '3.1', 'Menganalisis proses perumusan dan\r\npenetapan Pancasila sebagai Dasar\r\nNegara', 'PPKN'),
(335, 7, '3.2', 'Memahami norma-norma yang\r\nberlaku dalam kehidupan\r\nbermasyarakat untuk mewujudkan\r\nkeadilan', 'PPKN'),
(336, 7, '3.3', 'Menganalisis kesejarahan\r\nperumusan dan pengesahan\r\nUndang-undang Dasar Negara\r\nRepublik Indonesia Tahun 1945', 'PPKN');
INSERT INTO `kd_pengetahuan` (`id_kd`, `kelas`, `no_kd`, `judul`, `mata_pelajaran`) VALUES
(337, 7, '3.4', 'Mengidentifikasi keberagaman suku,\r\nagama, ras dan antargolognan dalam\r\nbingkai Bhinneka Tunggal Ika', 'PPKN'),
(338, 7, '3.5', 'Menganalisis bentuk-bentuk kerja\r\nsama dalam berbagai bidang\r\nkehidupan di masyarakat', 'PPKN'),
(339, 7, '3.6', 'Mengasosiasikan karakteristik\r\ndaerah dalam kerangka Negara\r\nKesatuan Republik Indonesia', 'PPKN'),
(340, 8, '3.1', 'Menelaah Pancasila sebagai dasar\r\nnegara dan pandangan hidup\r\nbangsa', 'PPKN'),
(341, 8, '3.2', 'Menelaah makna, kedudukan dan\r\nfungsi Undang-Undang Dasar\r\nNegara Republik Indonesia Tahun\r\n1945, serta peratuan perundanganundangan lainnya dalam sistem\r\nhukum nasional', 'PPKN'),
(342, 8, '3.3', 'Memahami tata urutan peraturan perundang-undangan dalam sistem hukum nasional nasional di Indonesia', 'PPKN'),
(343, 8, '3.4', 'Menganalisa makna dan arti\r\nKebangkitan nasional 1908 dalam\r\nperjuangan kemerdekaan Republik\r\nIndonsia', 'PPKN'),
(344, 8, '3.5', 'Memproyeksikan nilai dan semangat\r\nSumpah Pemuda tahun 1928 dalam\r\nbingkai Bhinneka Tunggal Ika', 'PPKN'),
(345, 8, '3.6', 'Menginterpretasikan semangat dan\r\nkomitmen kebangsaan kolektif\r\nuntuk memperkuat Negara Kesatuan\r\nRepublik Indonesia dalam kontek\r\nkehidupan siswa', 'PPKN'),
(346, 9, '3.1', 'Membandingkan antara peristiwa\r\ndan dinamika yang terjadi di\r\nmasyarakat dengan praktik ideal\r\nPancasila sebagai dasar negara dan\r\npandangan hidup bangsa', 'PPKN'),
(347, 9, '3.2', 'Mensintesiskan isi alinea dan pokok\r\npikiran yang terkandung dalam\r\nPembukaan Undang-Undang Dasar\r\nNegara Republik Indonesia tahun\r\n1945', 'PPKN'),
(348, 9, '3.3', 'Memahami ketentuan tentang\r\nbentuk dan kedaualatan negara\r\nsesuai Undang-Undang Dasar\r\nNegara Republik Indonesia tahun\r\n1945', 'PPKN'),
(349, 9, '3.4', 'Menganalisis prinsip persatuan\r\ndalam keberagaman suku, agama,\r\nras, dan antargolongan (SARA),\r\nsosial, budaya, ekonomi, dan gender\r\ndalam bingkai Bhinneka Tunggal Ika', 'PPKN'),
(350, 9, '3.5', 'Menganalisis prinsip harmoni dalam\r\nkeberagaman suku, agama, ras, dan\r\nantargolongan (SARA) sosial,\r\nbudaya, ekonomi, dan gender dalam\r\nbingkai Bhinneka Tunggal Ika', 'PPKN'),
(351, 9, '3.6', 'Mengkreasikan konsep cinta tanah\r\nair/bela negara dalam konteks\r\nNegara Kesatuan Republik Indonesia', 'PPKN'),
(352, 7, '3.1', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ninterpersonal lisan dan tulis yang\r\nmelibatkan tindakan menyapa,\r\nberpamitan, mengucapkan\r\nterimakasih, dan meminta maaf,\r\nserta menanggapinya, sesuai\r\ndengan konteks penggunaannya', 'Bahasa Inggris'),
(353, 7, '3.2', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ntransaksional lisan dan tulis yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait jati diri,\r\npendek dan sederhana, sesuai\r\ndengan konteks penggunaannya.\r\n{Perhatikan unsur kebahasaan dan\r\nkosa kata terkait hubungan\r\nkeluarga; pronoun (subjective,\r\nobjective, possessive)', 'Bahasa Inggris'),
(354, 7, '3.3', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ntransaksional lisan dan tulis yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait nama\r\nhari, bulan, nama waktu dalam\r\nhari, waktu dalam bentuk angka,\r\ntanggal, dan tahun, sesuai dengan\r\nkonteks penggunaannya.\r\n(Perhatikan kosa kata terkait angka\r\nkardinal dan ordinal)', 'Bahasa Inggris'),
(355, 7, '3.4', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ntransaksional lisan dan tulis yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait nama\r\ndan jumlah binatang, benda, dan\r\nbangunan publik yang dekat\r\ndengan kehidupan siswa seharihari, sesuai dengan konteks\r\npenggunaannya. (Perhatikan unsur\r\nkebahasaan dan kosa kata terkait\r\narticle a dan the, plural dan\r\nsingular)', 'Bahasa Inggris'),
(356, 7, '3.5', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ntransaksional lisan dan tulis yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait dengan\r\nsifat orang, binatang, benda sesuai\r\ndengan konteks penggunaannya.\r\n(Perhatikan unsur kebahasaan be,\r\nadjective)', 'Bahasa Inggris'),
(357, 7, '3.6', 'mengidentifikasi fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan teks interaksi\r\ntransaksional lisan dan tulis yang\r\nmelibatkan tindakan memberi dan\r\nmeminta informasi terkait dengan\r\ntingkah laku/tindakan/fungsi\r\norang, binatang, benda, sesuai\r\ndengan konteks penggunaannya.\r\n(Perhatikan unsur kebahasaan\r\nkalimat declarative, interogative,\r\nsimple present tense)', 'Bahasa Inggris'),
(358, 7, '3.7', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks\r\ndeskriptif lisan dan tulis dengan\r\nmemberi dan meminta informasi\r\nterkait dengan deskripsi orang,\r\nbinatang, dan benda, sangat pendek\r\ndan sederhana, sesuai dengan\r\nkonteks penggunaannya', 'Bahasa Inggris'),
(359, 7, '3.8', 'menafsirkan fungsi sosial dan\r\nunsur kebahasaan dalam lirik lagu\r\nterkait kehidupan remaja SMP/MTs', 'Bahasa Inggris'),
(360, 8, '3.1', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi interpersonal lisan dan\r\ntulis yang melibatkan tindakan\r\nmeminta perhatian, mengecek\r\npemahaman, menghargai kinerja,\r\nmeminta dan mengungkapkan\r\npendapat, serta menanggapinya,\r\nsesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(361, 8, '3.2', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait kemampuan dan kemauan,\r\nmelakukan suatu tindakan, sesuai\r\ndengan konteks penggunaannya.\r\n(Perhatikan unsur kebahasaan can,\r\nwill)', 'Bahasa Inggris'),
(362, 8, '3.3', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait keharusan, larangan, dan\r\nhimbauan, sesuai dengan konteks\r\npenggunaannya. (Perhatikan unsur\r\nkebahasaan must, should)', 'Bahasa Inggris'),
(363, 8, '3.4', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi interpersonal lisan dan\r\ntulis yang melibatkan tindakan\r\nmenyuruh, mengajak, meminta ijin,\r\nserta menanggapinya, sesuai\r\ndengan konteks penggunaannya', 'Bahasa Inggris'),
(364, 8, '3.5', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks khusus\r\ndalam bentuk greeting card, dengan\r\nmemberi dan meminta informasi\r\nterkait dengan hari-hari spesial,\r\nsesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(365, 8, '3.6', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait keberadaan orang, benda,\r\nbinatang, sesuai dengan konteks\r\npenggunaannya. (Perhatikan unsur\r\nkebahasaan there is/are)', 'Bahasa Inggris'),
(366, 8, '3.7', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait\r\nkeadaan/tindakan/kegiatan/\r\nkejadian yang dilakukan/terjadi\r\nsecara rutin atau merupakan\r\nkebenaran umum, sesuai dengan\r\nkonteks penggunaannya.\r\n(Perhatikan unsur kebahasaan\r\nsimple present tense)', 'Bahasa Inggris'),
(367, 8, '3.8', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait keadaan/tindakan/\r\nkegiatan/ kejadian yang sedang\r\ndilakukan/berlangsung saat\r\ndiucapkan, sesuai dengan konteks\r\npenggunaannya. (Perhatikan unsur\r\nkebahasaan present continuous\r\ntense)', 'Bahasa Inggris'),
(368, 8, '3.9', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait perbandingan jumlah dan\r\nsifat orang, binatang, benda, sesuai\r\ndengan konteks penggunaannya.\r\n(Perhatikan unsur kebahasaan\r\ndegree of comparison)', 'Bahasa Inggris'),
(369, 8, '3.10', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait\r\nkeadaan/tindakan/kegiatan/\r\nkejadian yang dilakukan/terjadi,\r\nrutin maupun tidak rutin, atau\r\nmenjadi kebenaran umum di waktu\r\nlampau, sesuai dengan konteks\r\npenggunaannya. (Perhatikan unsur\r\nkebahasaan simple past tense)', 'Bahasa Inggris'),
(370, 8, '3.11', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks personal\r\nrecount lisan dan tulis dengan\r\nmemberi dan meminta informasi\r\nterkait pengalaman pribadi di waktu\r\nlampau, pendek dan sederhana,\r\nsesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(371, 8, '3.12', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks khusus\r\ndalam bentuk pesan singkat dan\r\npengumuman/ pemberitahuan\r\n(notice), dengan memberi dan\r\nmeminta informasi terkait kegiatan\r\nsekolah, sesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(372, 8, '3.13', 'menafsirkan fungsi sosial dan\r\nunsur kebahasaan lirik lagu terkait\r\nkehidupan remaja SMP/MTs', 'Bahasa Inggris'),
(373, 9, '3.1', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi interpersonal lisan dan\r\ntulis yang melibatkan tindakan\r\nmenyatakan harapan, doa, dan\r\nucapan selamat atas suatu\r\nkebahagiaan dan prestasi, serta\r\nmenanggapinya, sesuai dengan\r\nkonteks penggunaannya', 'Bahasa Inggris'),
(374, 9, '3.2', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait maksud, tujuan,\r\npersetujuan melakukan suatu\r\ntindakan/kegiatan, sesuai dengan\r\nkonteks penggunaannya.\r\n(Perhatikan unsur kebahasaan to,\r\nin order to, so that (dis)agreement)', 'Bahasa Inggris'),
(375, 9, '3.3', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks khusus\r\ndalam bentuk label, dengan\r\nmeminta dan memberi informasi\r\nterkait obat/makanan/minuman,\r\nsesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(376, 9, '3.4', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks prosedur\r\nlisan dan tulis dengan memberi dan\r\nmeminta informasi terkait resep\r\nmakanan/minuman dan manual,\r\npendek dan sederhana, sesuai\r\ndengan konteks penggunaannya', 'Bahasa Inggris'),
(377, 9, '3.5', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait keadaan/tindakan/\r\nkegiatan/ kejadian yang sedang\r\ndilakukan/terjadi pada saat ini,\r\nwaktu lampau, dan waktu yang\r\nakan datang, sesuai dengan\r\nkonteks penggunaannya\r\n(perhatikan unsur kebahasaan\r\npresent continuous, past continuous,\r\nwill+continuous)', 'Bahasa Inggris'),
(378, 9, '3.6', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait keadaan/tindakan/\r\nkegiatan/ kejadian yang\r\nsudah/telah dilakukan/terjadi di\r\nwaktu lampau dikaitkan dengan\r\nkeadaan sekarang, tanpa\r\nmenyebutkan waktu terjadinya\r\nsecara spesifik, sesuai dengan\r\nkonteks penggunaannya\r\n(perhatikan unsur kebahasaan\r\npresent perfect tense)', 'Bahasa Inggris'),
(379, 9, '3.7', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks naratif\r\nlisan dan tulis dengan memberi dan\r\nmeminta informasi terkait fairy\r\ntales, pendek dan sederhana, sesuai\r\ndengan konteks penggunaannya', 'Bahasa Inggris'),
(380, 9, '3.8', 'menerapkan fungsi sosial, struktur\r\nteks, dan unsur kebahasaan teks\r\ninteraksi transaksional lisan dan\r\ntulis yang melibatkan tindakan\r\nmemberi dan meminta informasi\r\nterkait\r\nkeadaan/tindakan/kegiatan/\r\nkejadian tanpa perlu menyebutkan\r\npelakunya sesuai dengan konteks\r\npenggunaannya. (perhatikan unsur\r\nkebahasaan passive voice)', 'Bahasa Inggris'),
(381, 9, '3.9', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks\r\ninformation report lisan dan tulis\r\ndengan memberi dan meminta\r\ninformasi terkait mata pelajaran\r\nlain di Kelas IX, pendek dan\r\nsederhana, sesuai dengan konteks\r\npenggunaannya', 'Bahasa Inggris'),
(382, 9, '3.10', 'membandingkan fungsi sosial,\r\nstruktur teks, dan unsur\r\nkebahasaan beberapa teks khusus\r\ndalam bentuk iklan dengan\r\nmemberi dan meminta informasi\r\nterkait produk dan jasa, sesuai\r\ndengan konteks penggunaannya', 'Bahasa Inggris'),
(383, 9, '3.11', 'menafsirkan fungsi sosial dan\r\nunsur kebahasaan lirik lagu terkait\r\nkehidupan remaja SMP/MTs', 'Bahasa Inggris'),
(384, 7, '3.1', 'memahami unsur, prinsip, teknik\r\ndan prosedur menggambar flora,\r\nfauna dan alam benda dengan\r\nberbagai bahan', 'Seni Rupa'),
(385, 7, '3.2', 'memahami prinsip dan prosedur\r\nmenggambar gubahan flora, fauna,\r\ndan bentuk geometrik menjadi\r\nragam hias', 'Seni Rupa'),
(386, 7, '3.3', 'memahami prosedur penerapan\r\nragam hias pada bahan buatan', 'Seni Rupa'),
(387, 7, '3.4', 'memahami prosedur penerapan\r\nragam hias pada bahan alam', 'Seni Rupa'),
(388, 7, '3.1', 'memahami konsep dasar bernyanyi\r\nsatu suara secara berkelompok\r\ndalam bentuk unisono', 'Seni Musik'),
(389, 7, '3.2', 'memahami dasar bernyanyi dengan\r\ndua suara atau lebih secara\r\nberkelompok', 'Seni Musik'),
(390, 7, '3.3', 'memahami konsep dasar\r\npermainan alat musik sederhana\r\nsecara perorangan', 'Seni Musik'),
(391, 7, '3.4', 'memahami konsep dasar ansamble\r\nmusik.', 'Seni Musik'),
(392, 7, '3.1', 'memahami gerak tari berdasarkan\r\nunsur ruang waktu dan tenaga', 'Seni Tari'),
(393, 7, '3.2', 'memahami gerak tari berdasarkan\r\nruang waktu dan tenaga sesuai\r\niringan', 'Seni Tari'),
(394, 7, '3.3', 'memahami gerak tari sesuai\r\ndengan level dan pola lantai', 'Seni Tari'),
(395, 7, '3.4', 'memahami gerak tari sesuai level,\r\ndan pola lantai sesuai iringan', 'Seni Tari'),
(396, 7, '3.1', 'memahami konsep, teknik dan\r\nprosedur dasar seni peran untuk\r\npementasan fragmen', 'Seni Teater'),
(397, 7, '3.2', 'memahami teknik menyusun\r\nnaskah fragmen', 'Seni Teater'),
(398, 7, '3.3', 'memahami perancangan\r\npementasan fragmen sesuai\r\nkonsep, teknik dan prosedur', 'Seni Teater'),
(399, 7, '3.4', 'memahami pementasan fragmen\r\nsesuai konsep, teknik, dan\r\nprosedur', 'Seni Teater'),
(400, 8, '3.1', 'memahami unsur, prinsip, teknik,\r\ndan prosedur menggambar\r\nmenggunakan model dengan\r\nberbagai bahan', 'Seni Rupa'),
(401, 8, '3.2', 'memahami prosedur menggambar\r\nillustrasi dengan teknik manual\r\natau digital', 'Seni Rupa'),
(402, 8, '3.3', 'memahami prosedur menggambar\r\nposter dengan berbagai teknik', 'Seni Rupa'),
(403, 8, '3.4', 'memahami prosedur menggambar\r\nkomik dengan berbagai teknik', 'Seni Rupa'),
(404, 8, '3.1', 'memahami teknik dan gaya\r\nmenyanyi lagu-lagu daerah', 'Seni Musik'),
(405, 8, '3.2', 'memahami teknik dan gaya lagu\r\ndaerah dengan dua suara atau\r\nlebih secara berkelompok', 'Seni Musik'),
(406, 8, '3.3', 'memahami teknik permainan salah\r\nsatu alat musik tradisional secara\r\nperorangan', 'Seni Musik'),
(407, 8, '3.4', 'memahami teknik permainan alatalat musik tradisional secara\r\nberkelompok', 'Seni Musik'),
(408, 8, '3.1', 'memahami keunikan gerak tari\r\ntradisional dengan menggunakan\r\nunsur pendukung tari', 'Seni Tari'),
(409, 8, '3.2', 'memahami tari tradisional dengan\r\nmenggunakan unsur pendukung\r\ntari sesuai iringan', 'Seni Tari'),
(410, 8, '3.3', 'memahami penerapan pola lantai\r\ndan unsur pendukung gerak tari\r\ntradisional', 'Seni Tari'),
(411, 8, '3.4', 'memahami penerapan pola lantai\r\ntari tradisional berdasarkan unsur\r\npendukung tari sesuai iringan', 'Seni Tari'),
(412, 8, '3.1', 'memahami konsep, teknik dan\r\nprosedur dasar seni peran sesuai\r\nkaidah pementasan pantomim', 'Seni Teater'),
(413, 8, '3.2', 'memahami teknik menyusun\r\nnaskah sesuai kaidah pementasan\r\npantomim', 'Seni Teater'),
(414, 8, '3.3', 'memahami perancangan\r\npementasan pantomim sesuai\r\nkonsep, teknik dan prosedur', 'Seni Teater'),
(415, 8, '3.4', 'memahami pementasan pantomim\r\nsesuai konsep, teknik, dan\r\nprosedur', 'Seni Teater'),
(416, 9, '3.1', 'memahami unsur, prinsip, teknik,\r\ndan prosedur berkarya seni lukis\r\ndengan berbagai bahan', 'Seni Rupa'),
(417, 9, '3.2', 'memahami prosedur berkarya seni\r\npatung dengan berbagai bahan dan\r\nteknik', 'Seni Rupa'),
(418, 9, '3.3', 'memahami prosedur berkarya seni\r\ngrafis dengan berbagai bahan dan\r\nteknik', 'Seni Rupa'),
(419, 9, '3.4', 'memahami prosedur\r\npenyelenggaraan pameran karya\r\nseni rupa', 'Seni Rupa'),
(420, 9, '3.1', 'memahami teknik pengembangan\r\nornamentasi melodis dan ritmis\r\nlagu dalam bentuk vokal\r\nsolo/tunggal', 'Seni Musik'),
(421, 9, '3.2', 'memahami teknik pengembangan\r\nornamentasi ritmis maupun\r\nmelodis lagu dalam bentuk\r\nkelompok vokal', 'Seni Musik'),
(422, 9, '3.3', 'memahami konsep, bentuk, dan\r\nciri-ciri musik populer', 'Seni Musik'),
(423, 9, '3.4', 'memahami pertunjukan musik\r\npopuler', 'Seni Musik'),
(424, 9, '3.1', 'memahami keunikan gerak tari\r\nkreasi berdasarkan unsur\r\npendukung tari', 'Seni Tari'),
(425, 9, '3.2', 'memahami tari kreasi dengan\r\nmenggunakan unsur pendukung\r\ntari sesuai iringan', 'Seni Tari'),
(426, 9, '3.3', 'memahami penerapan pola lantai\r\ndan unsur pendukung gerak tari\r\nkreasi', 'Seni Tari'),
(427, 9, '3.4', 'memahami penerapan pola lantai\r\ntari kreasi berdasarkan unsur\r\npendukung tari sesuai iringan', 'Seni Tari'),
(428, 9, '3.1', 'memahami konsep, teknik dan\r\nprosedur dasar seni peran sesuai\r\nkaidah pementasan drama\r\nmusikal dan atau operet', 'Seni Teater'),
(429, 9, '3.2', 'memahami teknik menyusun\r\nnaskah sesuai kaidah pementasan\r\ndrama musikal dan atau operet', 'Seni Teater'),
(430, 9, '3.3', 'memahami perancangan\r\npementasan drama musikal dan\r\natau operet sesuai konsep, teknik,\r\ndan prosedur', 'Seni Teater'),
(431, 9, '3.4', 'memahami pementasan drama\r\nmusikal dan atau operet sesuai\r\nkonsep, teknik dan prosedur', 'Seni Teater'),
(432, 1, '3.1', 'mengetahui huruf-huruf hijaiyyah dan harakatnya secara lengkap', 'Pendidikan Agama Islam dan BP'),
(433, 1, '3.2', 'memahami pesan-pesan pokok Q.S. al-Fatihah dan Q.S. al-Ikhlas', 'Pendidikan Agama Islam dan BP'),
(434, 1, '3.3', 'memahami adanya Allah Swt. yang Maha Pengasih dan Maha Penyayang', 'Pendidikan Agama Islam dan BP'),
(435, 1, '3.4', 'memahami keesaan Allah Swt. berdasarkan pengamatan terhadap dirinya dan makhluk ciptaan-Nya yang dijumpai di sekitar rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(436, 1, '3.5', 'memahami makna al-Asmau alHusna: ar-Rahman, ar-Rahim, dan al-Malik', 'Pendidikan Agama Islam dan BP'),
(437, 1, '3.6', 'memahami makna dua kalimat syahadat', 'Pendidikan Agama Islam dan BP'),
(438, 1, '3.7', 'memahami makna doa sebelum dan sesudah belajar', 'Pendidikan Agama Islam dan BP'),
(439, 1, '3.8', 'memahami perilaku hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(440, 1, '3.9', 'memahami berkata yang baik, sopan, dan santun', 'Pendidikan Agama Islam dan BP'),
(441, 1, '3.10', 'memahami makna bersyukur, pemaaf, jujur, dan percaya diri', 'Pendidikan Agama Islam dan BP'),
(442, 1, '3.11', 'memahami tata cara bersuci', 'Pendidikan Agama Islam dan BP'),
(443, 1, '3.12', 'memahami salat dan kegiatan agama yang dianutnya di sekitar rumahnya melalui pengamatan', 'Pendidikan Agama Islam dan BP'),
(444, 1, '3.13', 'memahami kisah keteladanan Nabi Adam a.s.', 'Pendidikan Agama Islam dan BP'),
(445, 1, '3.14', 'memahami kisah keteladanan Nabi Idris a.s.', 'Pendidikan Agama Islam dan BP'),
(446, 1, '3.15', 'memahami kisah keteladanan Nabi Nuh a.s.', 'Pendidikan Agama Islam dan BP'),
(447, 1, '3.16', 'memahami kisah keteladanan Nabi Hud a.s.', 'Pendidikan Agama Islam dan BP'),
(448, 1, '3.17', 'memahami kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(449, 2, '3.1', 'mengetahui huruf hijaiyyah bersambung sesuai dengan makharijul huruf', 'Pendidikan Agama Islam dan BP'),
(450, 2, '3.2', 'memahami pesan-pesan pokok Q.S. an-Nas dan Q.S. al-‘Asr', 'Pendidikan Agama Islam dan BP'),
(451, 2, '3.3', 'memahami Hadis yang terkait dengan anjuran menuntut ilmu', 'Pendidikan Agama Islam dan BP'),
(452, 2, '3.4', 'memahami Hadis yang terkait dengan perilaku hidup bersih dan sehat', 'Pendidikan Agama Islam dan BP'),
(453, 2, '3.5', 'memahami makna al-Asmau alHusna: al-Quddus, as-Salam, dan al-Khaliq', 'Pendidikan Agama Islam dan BP'),
(454, 2, '3.6', 'memahami makna doa sebelum dan sesudah makan', 'Pendidikan Agama Islam dan BP'),
(455, 2, '3.7', 'memahami perilaku kasih sayang kepada sesama', 'Pendidikan Agama Islam dan BP'),
(456, 2, '3.8', 'memahami sikap kerja sama dan saling tolong menolong', 'Pendidikan Agama Islam dan BP'),
(457, 2, '3.9', 'memahami doa sebelum dan sesudah wudu', 'Pendidikan Agama Islam dan BP'),
(458, 2, '3.10', 'memahami tata cara salat dan bacaannya', 'Pendidikan Agama Islam dan BP'),
(459, 2, '3.11', 'memahami kisah keteladanan Nabi Saleh a.s.', 'Pendidikan Agama Islam dan BP'),
(460, 2, '3.12', 'memahami kisah keteladanan Nabi Lut a.s.', 'Pendidikan Agama Islam dan BP'),
(461, 2, '3.13', 'memahami kisah keteladanan Nabi Ishaq a.s.', 'Pendidikan Agama Islam dan BP'),
(462, 2, '3.14', 'memahami kisah keteladanan Nabi Ya‘qub a.s.', 'Pendidikan Agama Islam dan BP'),
(463, 2, '3.15', 'memahami kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(464, 3, '3.1', 'memahami makna Q.S. an-Nasr dan al-Kausar', 'Pendidikan Agama Islam dan BP'),
(465, 3, '3.2', 'memahami Hadis yang terkait dengan perilaku mandiri, percaya diri, dan bertanggung jawab', 'Pendidikan Agama Islam dan BP'),
(466, 3, '3.3', 'memahami keesaan Allah Yang Maha Pencipta berdasarkan pengamatan terhadap dirinya dan makhluk ciptaan-Nya yang dijumpai di sekitar rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(467, 3, '3.4', 'memahami makna al-Asmau alHusna: al-Wahhab, al-‘Alim, dan asSami‘', 'Pendidikan Agama Islam dan BP'),
(468, 3, '3.5', 'memahami perilaku tawaduk, ikhlas, dan mohon pertolongan', 'Pendidikan Agama Islam dan BP'),
(469, 3, '3.6', 'memahami sikap peduli terhadap sesama sebagai implementasi pemahaman Q.S. al-Kausar', 'Pendidikan Agama Islam dan BP'),
(470, 3, '3.7', 'memahami sikap bersyukur', 'Pendidikan Agama Islam dan BP'),
(471, 3, '3.8', 'memahami makna salat sebagai wujud dari pemahaman Q.S. alKausar', 'Pendidikan Agama Islam dan BP'),
(472, 3, '3.9', 'memahami makna zikir dan doa setelah salat', 'Pendidikan Agama Islam dan BP'),
(473, 3, '3.10', 'memahami hikmah ibadah salat melalui pengamatan dan pengalaman di rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(474, 3, '3.11', 'memahami kisah keteladanan Nabi Yusuf a.s.', 'Pendidikan Agama Islam dan BP'),
(475, 3, '3.12', 'memahami kisah keteladanan Nabi Syu‘aib a.s.', 'Pendidikan Agama Islam dan BP'),
(476, 3, '3.13', 'memahami kisah keteladanan Nabi Ibrahim a.s. dan Nabi Ismail a.s.', 'Pendidikan Agama Islam dan BP'),
(477, 3, '3.14', 'memahami kisah keteladanan nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(478, 4, '3.1', 'memahami makna Q.S. al-Falaq dan Q.S. al-Fil dengan baik dan benar', 'Pendidikan Agama Islam dan BP'),
(479, 4, '3.2', 'memahami Allah itu ada melalui pengamatan terhadap makhluk ciptaan-Nya di sekitar rumah dan sekolah', 'Pendidikan Agama Islam dan BP'),
(480, 4, '3.3', 'memahami makna al-Asmau alHusna: Al-Basir, Al-‘Adil, dan Al- ‘Azim', 'Pendidikan Agama Islam dan BP'),
(481, 4, '3.4', 'memahami makna iman kepada malaikat-malaikat Allah berdasarkan pengamatan terhadap dirinya dan alam sekitar', 'Pendidikan Agama Islam dan BP'),
(482, 4, '3.5', 'memahami makna iman kepada Rasul Allah', 'Pendidikan Agama Islam dan BP'),
(483, 4, '3.6', 'memahami sikap santun dan menghargai teman, baik di rumah, sekolah, maupun di masyarakat sekitar', 'Pendidikan Agama Islam dan BP'),
(484, 4, '3.7', 'mmemahami sikap rendah hati ', 'Pendidikan Agama Islam dan BP'),
(485, 4, '3.8', 'memahami perilaku hemat', 'Pendidikan Agama Islam dan BP'),
(486, 4, '3.9', 'memahami makna perilaku jujur dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(487, 4, '3.10', 'memahami makna perilaku amanah dalam kehidupan seharihari', 'Pendidikan Agama Islam dan BP'),
(488, 4, '3.11', 'memahami makna perilaku hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(489, 4, '3.12', 'memahami manfaat gemar membaca', 'Pendidikan Agama Islam dan BP'),
(490, 4, '3.13', 'memahami makna sikap pantang menyerah', 'Pendidikan Agama Islam dan BP'),
(491, 4, '3.14', 'memahami tata cara bersuci dari hadas kecil sesuai ketentuan syari’at Islam', 'Pendidikan Agama Islam dan BP'),
(492, 5, '3.1', 'memahami makna Q.S. at-Tīn dan Q.S. al-Mā’ūn dengan baik dan tartīl', 'Pendidikan Agama Islam dan BP'),
(493, 5, '3.2', 'memahami makna al-Asmau alHusna: Al-Mumit, Al-Hayy, AlQayyum, dan Al-Ahad', 'Pendidikan Agama Islam dan BP'),
(494, 5, '3.3', 'memahami nama-nama Rasul Allah dan Rasul Ulul ‘Azmi', 'Pendidikan Agama Islam dan BP'),
(495, 5, '3.4', 'memahami makna diturunkannya kitab-kitab suci melalui rasulrasul-Nya sebagai implementasi rukun iman', 'Pendidikan Agama Islam dan BP'),
(496, 5, '3.5', 'memahami makna perilaku jujur dalam kehidupan sehai-hari', 'Pendidikan Agama Islam dan BP'),
(497, 5, '3.6', 'memahami makna hormat dan patuh kepada orangtua dan guru', 'Pendidikan Agama Islam dan BP'),
(498, 5, '3.7', 'memahami makna saling menghargai sesama manusia', 'Pendidikan Agama Islam dan BP'),
(499, 5, '3.8', 'memahami makna sederhana dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(500, 5, '3.9', 'memahami makna ikhlas beramal dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(501, 5, '3.10', 'memahami hikmah puasa Ramadan yang dapat membentuk akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(502, 5, '3.11', 'memahami pelaksanaan salat tarawih dan tadarus al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(503, 5, '3.12', 'memahami kisah keteladanan Nabi Dawud a.s.', 'Pendidikan Agama Islam dan BP'),
(504, 5, '3.13', 'memahami kisah keteladanan Nabi Sulaiman a.s.', 'Pendidikan Agama Islam dan BP'),
(505, 5, '3.14', 'memahami kisah keteladanan Nabi Ilyas a.s.', 'Pendidikan Agama Islam dan BP'),
(506, 5, '3.15', 'memahami kisah keteladanan Nabi Ilyasa’ a.s.', 'Pendidikan Agama Islam dan BP'),
(507, 5, '3.16', 'memahami kisah keteladanan Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(508, 5, '3.17', 'memahami kisah keteladanan Luqman sebagaimana terdapat dalam al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(509, 6, '3.1', 'memahami makna Q.S. Al-Kafirun, Q.S. Al-Maidah/5:2-3 dan Q.S. alHujurat/49:12-13 dengan benar', 'Pendidikan Agama Islam dan BP'),
(510, 6, '3.2', 'memahami makna al-Asmau alHusna: As-Samad, Al-Muqtadir, AlMuqaddim, dan Al-Baqi', 'Pendidikan Agama Islam dan BP'),
(511, 6, '3.3', 'memahami hikmah beriman kepada hari akhir yang dapat membentuk perilaku akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(512, 6, '3.4', 'memahami hikmah beriman kepada qadha dan qadar yang dapat membentuk perilaku akhlak mulia', 'Pendidikan Agama Islam dan BP'),
(513, 6, '3.5', 'memahami perilaku hormat dan patuh kepada orangtua, guru, dan sesama anggota keluarga', 'Pendidikan Agama Islam dan BP'),
(514, 6, '3.6', 'memahami sikap toleran dan simpatik terhadap sesama sebagai wujud dari pemahaman Q.S. alKafirun', 'Pendidikan Agama Islam dan BP'),
(515, 6, '3.7', 'memahami hikmah zakat, infaq, dan sedekah sebagai implementasi rukun Islam', 'Pendidikan Agama Islam dan BP'),
(516, 6, '3.8', 'memahami kisah keteladanan Nabi Yunus a.s.', 'Pendidikan Agama Islam dan BP'),
(517, 6, '3.9', 'memahami kisah keteladanan Nabi Zakariya a.s.', 'Pendidikan Agama Islam dan BP'),
(518, 6, '3.10', 'memahami kisah keteladanan Nabi Yahya a.s.', 'Pendidikan Agama Islam dan BP'),
(519, 6, '3.11', 'memahami kisah keteladanan Nabi\r\nIsa a.s.', 'Pendidikan Agama Islam dan BP'),
(520, 6, '3.12', 'memahami kisah Nabi Muhammad\r\nsaw.', 'Pendidikan Agama Islam dan BP'),
(521, 6, '3.13', 'memahami kisah keteladanan sahabat-sahabat Nabi Muhammad saw.', 'Pendidikan Agama Islam dan BP'),
(522, 6, '3.14', 'memahami kisah keteladanan Ashabul Kahfi sebagaimana terdapat dalam al-Qur’an', 'Pendidikan Agama Islam dan BP'),
(523, 7, '3.1', 'Menjelaskan dan menentukan urutan pada bilangan bulat (positif dan negatif) dan pecahan (biasa, campuran, desimal, persen)', 'Matematika'),
(524, 7, '3.2', 'Menjelaskan dan melakukan operasi hitung bilangan bulat dan pecahan dengan memanfaatkan berbagai sifat operasi', 'Matematika'),
(525, 7, '3.3', 'Menjelaskan dan menentukan representasi bilangan dalam bentuk bilangan berpangkat bulat positif dan negatif', 'Matematika'),
(526, 7, '3.4', 'Menjelaskan himpunan, himpunan bagian, himpunan semesta, himpunan kosong, komplemen himpunan, dan melakukan operasi biner pada himpunan menggunakanmasalah kontekstual', 'Matematika'),
(527, 7, '3.5', 'Menjelaskan bentuk aljabar dan melakukan operasi pada bentuk aljabar (penjumlahan, pengurangan, perkalian, dan pembagian)', 'Matematika'),
(528, 7, '3.6', 'Menjelaskan persamaan dan pertidaksamaan linear satu variabel dan penyelesaiannya', 'Matematika'),
(529, 7, '3.7', 'Menjelaskan rasio dua besaran (satuannya sama dan berbeda)', 'Matematika'),
(530, 7, '3.8', 'Membedakan perbandingan senilai dan berbalik nilai dengan menggunakan tabel data, grafik, dan persamaan', 'Matematika'),
(531, 7, '3.9', 'Mengenal dan menganalisis berbagai situasi terkait aritmetika sosial (penjualan, pembelian, potongan, keuntungan, kerugian, bunga tunggal, persentase, bruto, neto,tara)', 'Matematika'),
(532, 7, '3.10', 'Menganalisis hubungan antar sudut sebagai akibat dari dua garis sejajar yang dipotong oleh garis transversal', 'Matematika'),
(533, 7, '3.11', 'Mengaitkan rumus keliling dan luas untuk berbagai jenis segiempat (persegi, persegipanjang, belahketupat, jajargenjang,trapesium, dan layang-layang) dan segitiga', 'Matematika'),
(534, 7, '3.12', 'Menganalisis hubungan antara data dengan cara penyajiannya (tabel, diagram garis, diagram batang, dan diagram lingkaran)', 'Matematika'),
(535, 8, '3.1', 'Membuat generalisasi dari pola pada barisan bilangan dan barisan konfigurasi objek', 'Matematika'),
(536, 8, '3.2', 'Menjelaskan kedudukan titik dalam bidang koordinat Kartesius yang dihubungkan dengan masalah kontekstual', 'Matematika'),
(537, 8, '3.3', 'Mendeskripsikan dan manyatakan relasi dan fungsi dengan menggunakan berbagai representasi (kata-kata, tabel, grafik, diagram, dan persamaan)', 'Matematika'),
(538, 8, '3.4', 'Menganalisis fungsi linear (sebagai persamaan garis lurus) dan menginterpretasikan grafiknya yang dihubungkan dengan masalah kontekstual', 'Matematika'),
(539, 8, '3.5', 'Menjelaskan sistem persamaan linear dua variabel dan penyelesaiannya yang dihubungkan dengan masalah kontekstual', 'Matematika'),
(540, 8, '3.6', 'Menjelaskan dan membuktikan teorema Pythagoras dan tripel Pythagoras', 'Matematika'),
(541, 8, '3.7', 'Menjelaskan sudut pusat, sudut keliling, panjang busur, dan luas juring lingkaran, serta hubungannya', 'Matematika'),
(542, 8, '3.8', 'Menjelaskan garis singgung persekutuan luar dan persekutuan dalam dua lingkaran dan cara melukisnya', 'Matematika'),
(543, 8, '3.9', 'Membedakan dan menentukan luas permukaan dan volume bangun ruang sisi datar (kubus, balok, prisma, dan limas)', 'Matematika'),
(544, 8, '3.10', 'Menganalisis data berdasarkan distribusi data, nilai rata-rata, median, modus, dan sebaran data untuk mengambil kesimpulan, membuat keputusan, dan membuat prediksi', 'Matematika'),
(545, 8, '3.11', 'Menjelaskan peluang empirik dan teoretik suatu kejadian dari suatu percobaan', 'Matematika'),
(546, 9, '3.1', 'Menjelaskan dan melakukan operasi bilangan berpangkat bilangan rasional dan bentuk akar, serta sifat-sifatnya', 'Matematika'),
(547, 9, '3.2', 'Menjelaskan persamaan kuadrat dan karakteristiknya berdasarkan akar-akarnya serta cara penyelesaiannya', 'Matematika'),
(548, 9, '3.3', 'Menjelaskan fungsi kuadrat dengan menggunakan tabel, persamaan, dan grafik', 'Matematika'),
(549, 9, '3.4', 'Menjelaskan hubungan antara koefisien dan diskriminan fungsi kuadrat dengan grafiknya', 'Matematika'),
(550, 9, '3.5', 'Menjelaskan transformasi geometri(refleksi, translasi, rotasi, dan dilatasi) yang dihubungkan dengan masalah kontekstual', 'Matematika'),
(551, 9, '3.6', 'Menjelaskan dan menentukan kesebangunan dan kekongruenan antar bangun datar', 'Matematika'),
(552, 9, '3.7', 'Membuat generalisasi luas permukaan dan volume berbagai bangun ruang sisi lengkung (tabung, kerucut, dan bola)', 'Matematika'),
(553, 7, '3.1', 'Memahami konsep ruang (lokasi, distribusi, potensi, iklim, bentuk muka bumi, geologis, flora, dan fauna) dan interaksi antarruang di Indonesia serta pengaruhnya terhadap\r\nkehidupan manusia dalam aspek ekonomi, sosial, budaya, dan pendidikan.', 'IPS'),
(554, 7, '3.2', ' Mengidentifikasi interaksi sosial dalam ruang dan pengaruhnya terhadap kehidupan sosial, ekonomi, dan budaya dalam nilai dan norma serta kelembagaan sosial budaya.', 'IPS'),
(555, 7, '3.3', 'Memahami konsep interaksi antara manusia dengan ruang sehingga menghasilkan berbagai kegiatan ekonomi (produksi, distribusi, konsumsi, permintaan, dan penawaran) dan interaksi antarruang untuk keberlangsungan kehidupan ekonomi, sosial, dan budaya Indonesia.', 'IPS'),
(556, 7, '3.4', 'Memahami kronologi perubahan, dan kesinambungan dalam kehidupan bangsa Indonesia pada aspek politik, sosial, budaya, geografis, dan pendidikan sejak masa praaksara sampai masa Hindu-Buddha dan Islam.', 'IPS'),
(557, 8, '3.1', ' Menelaah perubahan keruangan dan interaksi antarruang di Indonesia dan negara-negara ASEAN yang diakibatkan oleh faktor alam dan manusia (teknologi, ekonomi, pemanfaatan lahan, politik) dan pengaruhnya terhadap keberlangsungan kehidupan ekonomi,sosial, budaya, dan politik.', 'IPS'),
(558, 8, '3.2', 'Menganalisis pengaruh interaksi sosial dalam ruang yang berbeda terhadap kehidupan sosial dan budaya serta pengembangan kehidupan kebangsaan', 'IPS'),
(559, 8, '3.3', 'Menganalisis keunggulan dan keterbatasan ruang dalam permintaan dan penawaran serta teknologi, dan pengaruhnya terhadap interaksi antarruang bagi kegiatan ekonomi, sosial, dan budaya di Indonesia dan negara-negara ASEAN.', 'IPS'),
(560, 8, '3.4', 'Menganalisis kronologi, perubahan dan kesinambungan ruang (geografis, politik, ekonomi, pendidikan, sosial, budaya) dari masa penjajahan sampai tumbuhnya semangat\r\nkebangsaan.', 'IPS'),
(561, 9, '3.1', 'Menelaah perubahan keruangan dan interaksi antarruang negara-negara Asia dan benua lainnya yang diakibatkan faktor alam, manusia dan pengaruhnya terhadap keberlangsungan kehidupan manusia dalam ekonomi, sosial, pendidikan dan politik', 'IPS'),
(562, 9, '3.2', 'Menganalisis perubahan kehidupan sosial budaya Bangsa Indonesia dalam menghadapi arus globalisasi untuk memperkokoh kehidupan kebangsaan', 'IPS'),
(563, 9, '3.3', 'Menganalisis ketergantungan antarruang dilihat dari konsep ekonomi (produksi, distribusi, konsumsi, harga, pasar) dan pengaruhnya terhadap migrasi penduduk, transportasi, lembaga sosial dan ekonomi, pekerjaan, pendidikan, dan kesejahteraan masyarakat', 'IPS'),
(564, 9, '3.4', 'Menganalisis kronologi, perubahan dan kesinambungan ruang (geografis, politik, ekonomi, pendidikan, sosial, budaya) dari awal kemerdekaan sampai awal reformasI', 'IPS'),
(565, 7, '3.1', 'Memahami gerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional*)', 'PJOK'),
(566, 7, '3.2', 'Memahami gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional. ', 'PJOK'),
(567, 7, '3.3', 'Memahami gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional.', 'PJOK'),
(568, 7, '3.4', 'Memahami gerak spesifik seni beladiri.', 'PJOK'),
(569, 7, '3.5', 'Memahami konsep latihan peningkatan derajat kebugaran jasmani yang terkait dengan kesehatan (daya tahan, kekuatan, komposisi tubuh, dan kelenturan) dan pengukuran hasilnya', 'PJOK'),
(570, 7, '3.6', 'Memahami berbagai keterampilan dasar spesifik senam lantai', 'PJOK'),
(571, 7, '3.7', 'Memahami variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musiksebagai pembentuk gerak pemanasan dalam aktivitas gerak berirama', 'PJOK'),
(572, 7, '3.8', 'Memahami gerak spesifik salah satu gaya renang dengan koordinasi yang baik', 'PJOK'),
(573, 7, '3.9', 'Memahami perkembangan tubuh remaja yang meliputi perubahan fisik sekunder dan mental.', 'PJOK'),
(574, 7, '3.10', 'Memahami pola makan sehat, bergizi dan seimbang serta pengaruhnya terhadap kesehatan.', 'PJOK'),
(575, 8, '3.1', 'Memahami variasi gerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional', 'PJOK'),
(576, 8, '3.2', 'Memahami variasi gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional', 'PJOK'),
(577, 8, '3.3', 'Memahami variasi gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional', 'PJOK'),
(578, 8, '3.4', 'Memahami variasi gerak spesifik seni beladiri', 'PJOK'),
(579, 8, '3.5', 'Memahami konsep latihan peningkatan derajat kebugaran jasmani yang terkait dengan keterampilan (kecepatan, kelincahan, keseimbanga, dan koordinasi) serta pengukuran\r\nhasilnya', 'PJOK'),
(580, 8, '3.6', 'Memahami kombinasi keterampilanberbentuk rangkaian gerak sederhana dalam aktivitas spesifik senam lantai', 'PJOK'),
(581, 8, '3.7', 'Memahami variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik sebagai pembentuk gerak pemanasan dan inti latihan dalam aktivitas gerak berirama', 'PJOK'),
(582, 8, '3.8', 'Memahami gerak spesifik salah satu gaya renang dalam permainan air dengan atau tanpa alat', 'PJOK'),
(583, 8, '3.9', 'Memahami perlunya pencegahan terhadap “bahaya pergaulan bebas', 'PJOK'),
(584, 8, '3.10', 'Memahami cara menjaga keselamatan diri dan orang lain di jalan raya', 'PJOK'),
(585, 9, '3.1', 'Memahami variasi dan kombinasi gerak spesifik dalam berbagai permainan bola besar sederhana dan atau tradisional', 'PJOK'),
(586, 9, '3.2', 'Memahami kombinasi gerak spesifik dalam berbagai permainan bola kecil sederhana dan atau tradisional', 'PJOK'),
(587, 9, '3.3', 'Memahami kombinasi gerak spesifik jalan, lari, lompat, dan lempar dalam berbagai permainan sederhana dan atau tradisional. ', 'PJOK'),
(588, 9, '3.4', 'Memahami variasi dan kombinasi gerak spesifik seni beladiri. ', 'PJOK'),
(589, 9, '3.5', 'Memahami penyusunan program pengembangan komponen kebugaran jasmani terkait dengan kesehatan dan keterampilan secara sederhana', 'PJOK'),
(590, 9, '3.6', 'Memahami kombinasi keterampilan berbentuk rangkaian gerak sederhana secara konsisten, tepat, dan terkontrol dalam aktivitas spesifik senam lantai', 'PJOK'),
(591, 9, '3.7', 'Memahami variasi dan kombinasi gerak berbentuk rangkaian langkah dan ayunan lengan mengikuti irama (ketukan) tanpa/dengan musik sebagai pembentuk gerak pemanasan, inti latihan, dan pendinginan dalam aktivitas gerak berirama', 'PJOK'),
(592, 9, '3.8', 'Memahami gerak spesifik salah satu gaya renang dalam bentuk perlombaan', 'PJOK'),
(593, 9, '3.9', 'Memahami tindakan P3K pada kejadian darurat, baik pada diri sendiri maupun orang lain', 'PJOK'),
(594, 9, '3.10', 'Memahami peran aktivitas fisik terhadap pencegahan penyakit', 'PJOK'),
(595, 7, '3.1', 'memahami makna Q.S. alMujadilah /58: 11, Q.S. ar-Rahman /55: 33 dan Hadis terkait tentang menuntut ilmu', 'Pendidikan Agama Islam dan BP'),
(596, 7, '3.2', 'memahami makna Q.S. an-Nisa/4: 146, Q.S. al-Baqarah/2: 153, dan Q.S. Ali Imran/3: 134 serta Hadis terkait tentang ikhlas, sabar, dan pemaaf', 'Pendidikan Agama Islam dan BP'),
(597, 7, '3.3', 'memahami makna al-Asma‘u alHusna: al-’Alim, al-Khabir, as-Sami’, dan al-Bashir', 'Pendidikan Agama Islam dan BP'),
(598, 7, '3.4', 'memahami makna iman kepada malaikat berdasarkan dalil naqli', 'Pendidikan Agama Islam dan BP'),
(599, 7, '3.5', 'memahami makna perilaku jujur, amanah, dan istiqamah', 'Pendidikan Agama Islam dan BP'),
(600, 7, '3.6', 'memahami makna hormat dan patuh kepada kedua orang tua dan guru, dan empati terhadap sesama', 'Pendidikan Agama Islam dan BP'),
(601, 7, '3.7', 'memahami ketentuan bersuci dari hadas besar berdasarkan ketentuan syari’at Islam', 'Pendidikan Agama Islam dan BP'),
(602, 7, '3.8', 'memahami ketentuan salat berjemaah', 'Pendidikan Agama Islam dan BP'),
(603, 7, '3.9', 'memahami ketentuan salat Jumat', 'Pendidikan Agama Islam dan BP'),
(604, 7, '3.10', 'memahami ketentuan salat jamak qasar', 'Pendidikan Agama Islam dan BP'),
(605, 7, '3.11', 'memahami sejarah perjuangan Nabi Muhammad saw. periode Makkah', 'Pendidikan Agama Islam dan BP'),
(606, 7, '3.12', 'memahami sejarah perjuangan Nabi Muhammad saw. periode Madinah', 'Pendidikan Agama Islam dan BP'),
(607, 7, '3.13', 'memahami sejarah perjuangan dan kepribadian al-Khulafa al-Rasyidun', 'Pendidikan Agama Islam dan BP'),
(608, 8, '3.1', 'memahami Q.S. al-Furqan/25: 63, Q.S. al-Isra’/17: 26-27 dan Hadis terkait tentang rendah hati, hemat, dan hidup sederhana', 'Pendidikan Agama Islam dan BP'),
(609, 8, '3.2', 'memahami Q.S. an-Nahl/16: 114 dan Hadis terkait tentang mengonsumsi makanan dan minuman yang halal dan bergizi dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(610, 8, '3.3', 'memahami makna beriman kepada Kitab-kitab Allah Swt.', 'Pendidikan Agama Islam dan BP'),
(611, 8, '3.4', 'memahami makna beriman kepada Rasul Allah Swt.', 'Pendidikan Agama Islam dan BP'),
(612, 8, '3.5', 'memahami bahaya mengonsumsi minuman keras, judi, dan pertengkaran', 'Pendidikan Agama Islam dan BP'),
(613, 8, '3.6', 'memahami cara menerapkan perilaku jujur dan adil', 'Pendidikan Agama Islam dan BP'),
(614, 8, '3.7', 'memahami cara berbuat baik, hormat, dan patuh kepada orang tua dan guru', 'Pendidikan Agama Islam dan BP'),
(615, 8, '3.8', 'memahami makna perilaku gemar beramal saleh dan berbaik sangka kepada sesama', 'Pendidikan Agama Islam dan BP'),
(616, 8, '3.9', 'memahami tata cara salat sunah berjemaah dan munfarid', 'Pendidikan Agama Islam dan BP'),
(617, 8, '3.10', 'memahami tata cara sujud syukur, sujud sahwi, dan sujud tilawah', 'Pendidikan Agama Islam dan BP'),
(618, 8, '3.11', 'memahami tata cara puasa wajib dan sunah', 'Pendidikan Agama Islam dan BP'),
(619, 8, '3.12', 'memahami ketentuan makanan dan minuman yang halal dan haram berdasarkan al-Qur’an dan Hadis', 'Pendidikan Agama Islam dan BP'),
(620, 8, '3.13', 'memahami sejarah pertumbuhan ilmu pengetahuan masa Bani Umayah', 'Pendidikan Agama Islam dan BP'),
(621, 8, '3.14', 'memahami sejarah pertumbuhan ilmu pengetahuan masa Abbasiyah', 'Pendidikan Agama Islam dan BP'),
(622, 9, '3.1', 'memahami Q.S. az-Zumar/39: 53, Q.S. an-Najm/53: 39-42, Q.S. Ali Imrān/3: 159 tentang optimis, ikhtiar, dan tawakal serta Hadis terkait', 'Pendidikan Agama Islam dan BP'),
(623, 9, '3.2', 'memahami Q.S. al-Hujurat/49: 13 tentang toleransi dan menghargai perbedaan dan Hadis terkait', 'Pendidikan Agama Islam dan BP'),
(624, 9, '3.3', 'memahami makna iman kepada Hari Akhir berdasarkan pengamatan terhadap dirinya, alam sekitar, dan makhluk ciptaanNya', 'Pendidikan Agama Islam dan BP'),
(625, 9, '3.4', 'memahami makna iman kepada Qadha dan Qadar berdasarkan pengamatan terhadap dirinya, alam sekitar dan makhluk ciptaan-Nya', 'Pendidikan Agama Islam dan BP'),
(626, 9, '3.5', 'memahami penerapan jujur dan menepati janji dalam kehidupan sehari-hari', 'Pendidikan Agama Islam dan BP'),
(627, 9, '3.6', 'memahami cara berbakti dan taat kepada orang tua dan guru', 'Pendidikan Agama Islam dan BP'),
(628, 9, '3.7', 'memahami makna tata krama, sopan santun, dan rasa malu', 'Pendidikan Agama Islam dan BP'),
(629, 9, '3.8', 'memahami ketentuan zakat', 'Pendidikan Agama Islam dan BP'),
(630, 9, '3.9', 'memahami ketentuan ibadah haji dan umrah', 'Pendidikan Agama Islam dan BP'),
(631, 9, '3.10', 'memahami ketentuan penyembelihan hewan dalam Islam', 'Pendidikan Agama Islam dan BP'),
(632, 9, '3.11', ' memahami ketentuan qurban dan aqiqah', 'Pendidikan Agama Islam dan BP'),
(633, 9, '3.12', 'memahami sejarah perkembangan Islam di Nusantara', 'Pendidikan Agama Islam dan BP'),
(634, 9, '3.13', 'memahami sejarah tradisi Islam Nusantara', 'Pendidikan Agama Islam dan BP'),
(635, 1, '3.1', 'mengenal dirinya sebagai ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(636, 1, '3.2', 'memahami beragam kegunaan anggota tubuhnya sebagai ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(637, 1, '3.3', ' mengenal cara mengasihi keluarga dan teman', 'Pendidikan Agama Kristen dan BP'),
(638, 1, '3.4', 'memahami contoh sikap bersyukur dan bertanggung jawab dalam memelihara alam ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(639, 2, '3.1', 'memahami alasan menghormati orang tua dan yang lebih tua berdasarkan Alkitab', 'Pendidikan Agama Kristen dan BP'),
(640, 2, '3.2', 'memahami pentingnya tanggung jawab dalam keluarga', 'Pendidikan Agama Kristen dan BP'),
(641, 2, '3.3', 'memahami cara menjaga kerukunan di sekolah dan di lingkungannya', 'Pendidikan Agama Kristen dan BP'),
(642, 2, '3.4', 'mengenal bentuk disiplin di sekolah, rumah, dan di lingkungan sekitar', 'Pendidikan Agama Kristen dan BP'),
(643, 3, '3.1', 'memahami kehadiran Allah dalam iklim dan gejala-gejala alam', 'Pendidikan Agama Kristen dan BP'),
(644, 3, '3.2', 'memahami contoh tanggung jawab dalam memelihara flora dan fauna di sekitarnya', 'Pendidikan Agama Kristen dan BP'),
(645, 3, '3.3', 'memahami bahwa keberagaman budaya, suku, dan bangsa adalah kekayaan yang dikaruniakan Allah pada manusia', 'Pendidikan Agama Kristen dan BP'),
(646, 3, '3.4', 'memahami pentingnya berperan serta menjaga keutuhan ciptaan Allah', 'Pendidikan Agama Kristen dan BP'),
(647, 4, '3.1', 'memahami kehadiran Allah dalam berbagai peristiwa kehidupan', 'Pendidikan Agama Kristen dan BP'),
(648, 4, '3.2', 'memahami kemahakuasaan Allah dalam berbagai peristiwa rantai kehidupan manusia di sekitarnya', 'Pendidikan Agama Kristen dan BP'),
(649, 4, '3.3', 'memahami keterbatasannya sebagai manusia', 'Pendidikan Agama Kristen dan BP'),
(650, 4, '3.4', 'mengaplikasikan keyakinannya bahwa Allah memelihara manusia', 'Pendidikan Agama Kristen dan BP'),
(651, 5, '3.1', 'memahami bahwa semua manusia berdosa sehingga perlu bertobat', 'Pendidikan Agama Kristen dan BP'),
(652, 5, '3.2', 'memahami karya penyelamatan Allah dalam Yesus Kristus', 'Pendidikan Agama Kristen dan BP'),
(653, 5, '3.3', 'mengenal peran Roh Kudus dalam kehidupan orang yang sudah diselamatkan', 'Pendidikan Agama Kristen dan BP'),
(654, 5, '3.4', 'menerapkan contoh cara hidup manusia baru yang sudah bertoba', 'Pendidikan Agama Kristen dan BP'),
(655, 6, '3.1', 'memahami makna ibadah yang berkenan kepada Allah', 'Pendidikan Agama Kristen dan BP'),
(656, 6, '3.2', 'memahami pentingnya menjalin hubungan akrab dengan Allah sebagai wujud ibadah', 'Pendidikan Agama Kristen dan BP'),
(657, 6, '3.3', 'menganalisis makna melayani sesama sebagai ibadah yang berkenan kepada Allah', 'Pendidikan Agama Kristen dan BP'),
(658, 6, '3.4', 'memahami seluruh hidupnya sebagai ibadah sejati kepada Allah', 'Pendidikan Agama Kristen dan BP');

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE `kehadiran` (
  `id_kehadiran` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `alasan` varchar(200) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kehadiran`
--

INSERT INTO `kehadiran` (`id_kehadiran`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `alasan`, `keterangan`, `created_at`, `updated_at`) VALUES
(7, 15, 46, 12, 4, 'Sakit', 'bohong kalau sakit\r\n', '2020-09-15 21:48:06', '2020-09-15 21:48:06'),
(8, 14, 42, 13, 2, 'Sakit', '', '2020-11-26 05:41:49', '2020-11-26 05:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `tingkat_kelas` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `tahun_ajaran` varchar(15) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_guru`, `id_semester`, `id_sekolah`, `tingkat_kelas`, `nama`, `tahun_ajaran`, `status`, `created_at`, `updated_at`) VALUES
(9, 91, 2, 14, '1', 'A', '2020', 'Aktif', '2020-07-07 06:35:04', '2020-07-07 06:35:04'),
(10, 92, 2, 14, '2', 'B', '2020', 'Aktif', '2020-07-07 06:35:30', '2020-11-03 06:09:40'),
(11, 93, 3, 15, '7', 'A', '2020', 'Aktif', '2020-07-08 06:25:37', '2020-12-02 12:18:49'),
(12, 95, 4, 15, '8', 'B', '2019', 'Aktif', '2020-07-08 09:53:50', '2020-07-08 09:53:50'),
(13, 94, 2, 14, '4', 'B', '2020', 'Aktif', '2020-07-14 12:33:57', '2020-08-02 11:16:16'),
(15, 94, 1, 14, '3', 'B', '2020', 'Aktif', '2020-07-16 08:07:59', '2020-07-27 12:20:00'),
(16, 91, 1, 14, '4', 'A', '2020', 'Aktif', '2020-07-16 08:21:29', '2020-07-16 08:21:29'),
(17, 96, 6, 14, '4', 'C', '2021', 'Tidak Aktif', '2020-07-19 13:47:34', '2020-11-03 06:10:52'),
(18, 96, 2, 14, '1', 'B', '2020', 'Aktif', '2020-08-08 02:03:52', '2020-08-08 02:03:52'),
(19, 98, 7, 17, '10', 'A', '2020', 'Aktif', '2020-08-10 11:02:44', '2020-08-10 11:02:44'),
(20, 99, 8, 17, '12', 'A', '2020', 'Aktif', '2020-08-10 11:03:08', '2020-10-27 11:18:34'),
(21, 100, 4, 15, '7', 'B', '2019', 'Aktif', '2020-08-17 20:36:22', '2020-08-17 20:36:22'),
(22, 106, 7, 17, '10', 'B', '2020', 'Aktif', '2020-08-17 21:24:33', '2020-08-17 21:24:33'),
(23, 94, 1, 14, '1', 'A semster 2', '2021', 'Aktif', '2020-10-15 03:59:17', '2020-10-15 04:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `kkm_satuan_pendidikan`
--

CREATE TABLE `kkm_satuan_pendidikan` (
  `id_kkm` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `kkm` int(11) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kkm_satuan_pendidikan`
--

INSERT INTO `kkm_satuan_pendidikan` (`id_kkm`, `id_sekolah`, `id_kelas`, `kkm`, `status`, `created_at`, `updated_at`) VALUES
(2, 14, 10, 70, 'Aktif', '2020-08-04 12:16:57', '2020-10-25 06:51:18'),
(3, 14, 15, 65, 'Tidak Aktif', '2020-08-04 12:17:05', '2020-11-03 05:59:47'),
(8, 14, 13, 65, 'Aktif', '2020-08-09 11:27:11', '2020-08-09 11:27:11'),
(9, 15, 11, 65, 'Aktif', '2020-08-10 09:45:09', '2020-08-10 09:45:09'),
(14, 15, 12, 65, 'Aktif', '2020-08-23 00:53:37', '2020-08-23 00:53:37'),
(15, 17, 19, 65, 'Aktif', '2020-08-23 10:19:17', '2020-08-23 10:19:17'),
(16, 17, 20, 65, 'Aktif', '2020-08-24 20:24:34', '2020-08-24 20:24:34'),
(20, 17, 22, 70, 'Aktif', '2020-11-29 22:43:04', '2020-11-29 22:43:04'),
(21, 14, 9, 65, 'Aktif', '2020-12-27 21:27:52', '2020-12-27 21:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi_dasar_keterampilan`
--

CREATE TABLE `kompetensi_dasar_keterampilan` (
  `id_kd_keterampilan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `no_kd` varchar(20) NOT NULL,
  `judul` text NOT NULL,
  `kkm` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kompetensi_dasar_keterampilan`
--

INSERT INTO `kompetensi_dasar_keterampilan` (`id_kd_keterampilan`, `id_sekolah`, `id_kelas`, `id_mapel`, `no_kd`, `judul`, `kkm`, `created_at`, `updated_at`) VALUES
(78, 14, 9, 24, '4.1', 'Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', 60, '2020-08-08 01:17:02', '2020-08-08 01:27:28'),
(79, 14, 9, 24, '4.2', 'Mempraktikkan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara meletakkan buku, jarak antara mata dan buku, gerakan tangan atas-bawah, kiri-kanan, latihan pelenturan gerakan tangan dengan gerakan menulis di udara/pasir/ meja, melemaskan jari dengan mewarnai, menjiplak, menggambar, membuat garis tegak, miring, lurus, dan lengkung, menjiplak berbagai bentuk gambar, lingkaran, dan bentuk huruf di tempat bercahaya terang) dengan benar ', 65, '2020-08-08 01:17:03', '2020-08-08 13:28:05'),
(80, 14, 9, 24, '4.3', 'Melafalkan bunyi vokal dan konsonan dalam kata bahasa Indonesia atau bahasa daerah ', 60, '2020-08-08 01:17:03', '2020-08-08 01:27:41'),
(81, 14, 9, 24, '4.4', ' Menyampaikan penjelasan (berupa gambar dan tulisan) tentang anggota tubuh dan panca indera serta perawatannya  menggunakan kosakata bahasa Indonesia dengan bantuan bahasa daerah secara lisan dan/atau tulis ', 60, '2020-08-08 01:17:03', '2020-08-08 01:27:48'),
(82, 14, 9, 24, '4.5', 'Mengemukakan penjelasan tentang cara memelihara kesehatan dengan pelafalan kosakata Bahasa Indonesia yang tepat dan dibantu dengan bahasa daerah', 65, '2020-08-08 01:17:03', '2020-08-08 01:27:54'),
(83, 14, 9, 25, '4.1', 'Menyajikan bilangan cacah sampai dengan 99 yang bersesuaian dengan banyak anggota kumpulan objek yang disajikan ', 70, '2020-08-08 01:17:11', '2020-08-08 01:28:15'),
(84, 14, 9, 25, '4.2', 'Menuliskan lambang bilangan  sampai dua angka yang menyatakan banyak anggota suatu kumpulan objek dengan ide nilai tempat  ', 60, '2020-08-08 01:17:11', '2020-08-08 01:28:22'),
(85, 14, 9, 25, '4.3', 'Mengurutkan bilangan-bilangan sampai dua angka dari bilangan terkecil ke bilangan terbesar atau sebaliknya  dengan menggunakan kumpulan benda-benda konkret', 60, '2020-08-08 01:17:11', '2020-08-08 01:28:29'),
(86, 14, 9, 25, '4.4', 'Menyelesaikan masalah kehidupan sehari-hari yang berkaitan dengan  penjumlahan dan pengurangan bilangan  yang melibatkan bilangan cacah sampai dengan 99  \r\n ', 60, '2020-08-08 01:17:11', '2020-08-08 01:28:36'),
(87, 14, 9, 25, '4.5', 'Memprediksi dan membuat pola bilangan yang berkaitan dengan kumpulan benda/gambar/gerakan atau lainnya ', 60, '2020-08-08 01:17:11', '2020-08-08 01:28:43'),
(88, 14, 9, 24, '7.1', 'test', 80, '2020-08-08 13:43:51', '2020-08-08 13:43:51'),
(125, 15, 11, 31, '4.1', 'shiih', 80, '2020-08-10 08:42:22', '2020-08-10 08:42:22'),
(126, 15, 11, 31, '4.2', 'sdfh', 60, '2020-08-10 08:42:34', '2020-08-10 08:42:34'),
(127, 17, 19, 33, '4.1', 'ambuntest', 70, '2020-08-10 11:57:09', '2020-08-10 11:57:09'),
(128, 17, 19, 33, '4.2', 'kedua', 60, '2020-08-10 11:57:27', '2020-08-10 11:57:27'),
(129, 15, 11, 35, '4.1', 'Menyajikan data hasil pengukuran dengan alat ukur yang sesuai pada diri sendiri, makhluk hidup lain, dan benda-benda di sekitar dengan menggunakan satuan tak baku dan satuan baku ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(130, 15, 11, 35, '4.2', ' Menyajikan hasil pengklasifikasian makhluk hidup dan benda di lingkungan sekitar berdasarkan karakteristik yang diamati \r\n \r\n ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(131, 15, 11, 35, '4.3', 'Menyajikan hasil penyelidikan atau karya tentang sifat larutan, perubahan fisika dan perubahan kimia, atau pemisahan campuran ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(132, 15, 11, 35, '4.4', 'Melakukan percobaan untuk menyelidiki pengaruh kalor terhadap suhu dan wujud benda serta perpindahan kalor ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(133, 15, 11, 35, '4.5', ' Menyajikan hasil percobaan tentang perubahan bentuk energi, termasuk fotosintesis ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(134, 15, 11, 35, '4.6', ' Membuat model struktur sel tumbuhan/hewan ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(135, 15, 11, 35, '4.7', ' Menyajikan hasil pengamatan terhadap interaksi makhluk hidup dengan lingkungan sekitarnya ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(136, 15, 11, 35, '4.8', ' Membuat tulisan tentang gagasan penyelesaian  masalah pencemaran di lingkungannya berdasarkan hasil pengamatan', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(137, 15, 11, 35, '4.9', ' Membuat tulisan tentang gagasan adaptasi/penanggulangan masalah perubahan iklim ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(138, 15, 11, 35, '4.10', ' Mengomunikasikan upaya pengurangan resiko dan dampak bencana alam serta tindakan penyelamatan diri pada saat terjadi bencana sesuai dengan jenis ancaman bencana di daerahnya ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(139, 15, 11, 35, '4.11', ' Menyajikan karya tentang dampak rotasi dan revolusi bumi dan bulan bagi kehidupan di bumi, berdasarkan hasil pengamatan atau penelusuran berbagai sumber informasi ', 0, '2020-08-12 00:41:24', '2020-08-12 00:41:24'),
(140, 15, 12, 36, '4.1', ' Menyajikan karya tentang berbagai gangguan pada sistem gerak, serta upaya menjaga kesehatan sistem gerak manusia', 65, '2020-08-12 11:11:36', '2020-08-12 11:11:46'),
(141, 15, 12, 36, '4.2', ' Menyajikan hasil penyelidikan pengaruh gaya terhadap gerak benda ', 70, '2020-08-12 11:11:36', '2020-08-12 11:11:52'),
(142, 15, 12, 36, '4.3', ' Menyajikan hasil penyelidikan atau pemecahan masalah tentang manfaat penggunaan pesawat sederhana dalam kehidupan sehari-hari \r\n ', 60, '2020-08-12 11:11:36', '2020-08-12 11:11:57'),
(143, 15, 12, 36, '4.4', ' Menyajikan karya dari hasil penelusuran berbagai sumber informasi tentang teknologi yang terinspirasi dari hasil pengamatan struktur tumbuhan ', 60, '2020-08-12 11:11:36', '2020-08-23 11:15:14'),
(144, 15, 12, 36, '4.5', 'Menyajikan hasil penyelidikan tentang pencernaan mekanis dan kimiawi', 60, '2020-08-12 11:11:36', '2020-08-23 11:15:22'),
(145, 15, 12, 36, '4.6', 'Membuat karya tulis tentang dampak penyalahgunaan zat aditif dan zat adiktif bagi kesehatan ', 60, '2020-08-12 11:11:37', '2020-08-23 11:15:29'),
(146, 15, 12, 36, '4.7', ' Menyajikan hasil percobaan pengaruh aktivitas (jenis, intensitas, atau durasi) pada frekuensi denyut jantung \r\n ', 60, '2020-08-12 11:11:37', '2020-08-23 11:15:35'),
(147, 15, 12, 36, '4.8', ' Menyajikan data hasil percobaan untuk menyelidiki tekanan zat cair pada kedalaman tertentu, gaya apung, dan kapilaritas, misalnya dalam batang tumbuhan ', 60, '2020-08-12 11:11:37', '2020-08-23 11:15:43'),
(148, 15, 12, 36, '4.9', 'Menyajikan karya tentang upaya menjaga kesehatan sistem pernapasan ', 60, '2020-08-12 11:11:37', '2020-08-23 11:15:52'),
(149, 15, 12, 36, '4.10', '  Membuat karya tentang sistem ekskresi pada manusia dan penerapannya dalam menjaga kesehatan diri ', 60, '2020-08-12 11:11:37', '2020-08-23 11:15:58'),
(150, 15, 12, 36, '4.11', ' Menyajikan hasil percobaan  tentang getaran, gelombang, dan bunyi ', 60, '2020-08-12 11:11:37', '2020-08-23 11:16:06'),
(151, 15, 12, 36, '4.12', ' Menyajikan hasil percobaan tentang pembentukan bayangan pada cermin dan lensa ', 60, '2020-08-12 11:11:37', '2020-08-23 11:16:12'),
(152, 17, 20, 34, '3.1', 'testt', 70, '2020-08-23 23:07:20', '2020-08-23 23:07:20'),
(154, 15, 12, 70, '4.1', ' Menyimpulkan isi berita (membanggakan dan memotivasi) yang dibaca dan didengar ', 0, '2020-08-29 00:59:41', '2020-08-29 00:59:41'),
(155, 15, 12, 70, '4.2', ' Menyajikan data dan  informasi dalam bentuk berita  secara lisan dan tulis dengan memperhatikan struktur, kebahasaan,  atau  aspek lisan (lafal, intonasi, mimik, dan kinesik)', 0, '2020-08-29 00:59:41', '2020-08-29 00:59:41'),
(156, 15, 12, 70, '4.3', ' Menyimpulkan isi iklan, slogan, atau poster (membanggakan dan memotivasi) dari berbagai sumber ', 0, '2020-08-29 00:59:41', '2020-08-29 00:59:41'),
(157, 15, 12, 70, '4.4', ' Menyajikan  gagasan, pesan, dan ajakan dalam bentuk iklan, slogan, atau poster  secara lisan dan  tulis ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(158, 15, 12, 70, '4.5', ' Menyimpulkan isi teks eksposisi  (artikel ilmiah populer dari koran dan majalah) yang didengar dan dibaca ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(159, 15, 12, 70, '4.6', ' Menyajikan gagasan dan pendapat ke dalam bentuk teks eksposisi  artikel ilmiah populer (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) secara lisan dan tertulis dengan memperhatikan struktur, unsur kebahasaan, dan aspek lisan ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(160, 15, 12, 70, '4.7', ' Menyimpulkan unsur-unsur pembangun dan makna teks puisi  yang diperdengarkan atau dibaca ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(161, 15, 12, 70, '4.8', ' Menyajikan gagasan, perasaan, dan pendapat dalam bentuk teks puisi  secara tulis/lisan dengan memperhatikan unsur-unsur pembangun puisi  ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(162, 15, 12, 70, '4.9', ' Meringkas  isi  teks eksplanasi yang berupa proses terjadinya suatu fenomena dari  beragam  sumber yang didengar  dan dibaca', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(163, 15, 12, 70, '4.10', ' Menyajikan informasi dan data dalam bentuk teks eksplanasi proses terjadinya suatu fenomena secara lisan dan tulis dengan memperhatikan struktur, unsur kebahasaan, atau aspek lisan ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(164, 15, 12, 70, '4.11', 'Menceritakan kembali isi teks ulasan tentang kualitas karya (film, cerpen, puisi, novel, karya seni daerah) yang dibaca atau didengar ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(165, 15, 12, 70, '4.12', 'Menyajikan tanggapan tentang kualitas karya (film, cerpen, puisi, novel, karya seni daerah, dll.) dalam bentuk teks ulasan  secara lisan dan tulis dengan memperhatikan struktur, unsur kebahasaan, atau aspek lisan ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(166, 15, 12, 70, '4.13', ' Menyimpulkan  isi saran, ajakan, arahan, pertimbangan tentang berbagai hal positif permasalahan aktual dari teks persuasi (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan dibaca ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(167, 15, 12, 70, '4.14', ' Menyajikan teks persuasi (saran, ajakan, arahan, dan pertimbangan) secara tulis dan lisan dengan memperhatikan struktur, kebahasaan, atau aspek lisan ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(168, 15, 12, 70, '4.15', 'Menginterpretasi drama (tradisional dan modern) yang dibaca dan ditonton/didengar ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(169, 15, 12, 70, '4.16', 'Menyajikan drama dalam bentuk pentas atau naskah ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(170, 15, 12, 70, '4.17', 'Membuat peta konsep/garis alur dari buku fiksi dan nonfiksi yang dibaca ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(171, 15, 12, 70, '4.18', ' Menyajikan tanggapan terhadap buku fiksi dan nonfiksi yang dibaca  secara lisan/tertulis ', 0, '2020-08-29 00:59:42', '2020-08-29 00:59:42'),
(172, 14, 13, 28, '4.1', 'menggambar dan membentuk tiga dimensi', 0, '2020-11-14 09:41:17', '2020-11-14 09:41:17'),
(176, 17, 19, 121, '3.1', 'test', 60, '2021-01-01 07:39:55', '2021-01-01 07:39:55'),
(177, 17, 19, 123, '4.1', 'sfsf', 60, '2021-01-01 07:41:48', '2021-01-01 07:41:48'),
(178, 17, 19, 125, '4.1', 'sw', 60, '2021-01-01 07:47:46', '2021-01-01 07:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi_dasar_pengetahuan`
--

CREATE TABLE `kompetensi_dasar_pengetahuan` (
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `no_kd` varchar(20) NOT NULL,
  `judul` text NOT NULL,
  `kkm` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kompetensi_dasar_pengetahuan`
--

INSERT INTO `kompetensi_dasar_pengetahuan` (`id_kd_pengetahuan`, `id_sekolah`, `id_kelas`, `id_mapel`, `no_kd`, `judul`, `kkm`, `created_at`, `updated_at`) VALUES
(33, 14, 9, 24, '3.1', 'Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', 60, '2020-08-08 01:15:24', '2020-08-08 01:22:09'),
(34, 14, 9, 24, '3.2', 'Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan ', 60, '2020-08-08 01:15:24', '2020-08-08 01:22:21'),
(35, 14, 9, 24, '3.3', ' Menguraikan lambang bunyi  vokal dan konsonan  dalam kata bahasa Indonesia atau bahasa daerahatau bahasa daerah ', 70, '2020-08-08 01:15:24', '2020-08-08 01:22:28'),
(36, 14, 9, 24, '3.4', 'Menentukan kosakata tentang anggota tubuh dan pancaindra serta perawatannya melalui teks pendek (berupa gambar, tulisan, slogan sederhana, dan/atau syair lagu) dan eksplorasi lingkungan ', 75, '2020-08-08 01:15:24', '2020-08-08 01:22:39'),
(37, 14, 9, 24, '3.5', 'Mengenal kosakata tentang cara memelihara kesehatan melalui teks pendek (berupa gambar, tulisan, dan slogan sederhana) dan/atau eksplorasi lingkungan.', 60, '2020-08-08 01:15:24', '2020-08-08 01:22:46'),
(48, 14, 18, 26, '3.1', 'Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', 0, '2020-08-08 06:04:53', '2020-08-08 06:04:53'),
(49, 14, 18, 26, '3.2', 'Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan ', 0, '2020-08-08 06:04:53', '2020-08-08 06:04:53'),
(50, 14, 18, 26, '3.3', ' Menguraikan lambang bunyi  vokal dan konsonan  dalam kata bahasa Indonesia atau bahasa daerahatau bahasa daerah ', 0, '2020-08-08 06:04:53', '2020-08-08 06:04:53'),
(51, 14, 18, 26, '3.4', 'Menentukan kosakata tentang anggota tubuh dan pancaindra serta perawatannya melalui teks pendek (berupa gambar, tulisan, slogan sederhana, dan/atau syair lagu) dan eksplorasi lingkungan ', 0, '2020-08-08 06:04:53', '2020-08-08 06:04:53'),
(54, 14, 9, 25, '3.2', 'Menjelaskan bilangan sampai dua angka dan nilai tempat penyusun lambang bilangan menggunakan kumpulan benda konkret serta cara membacanya', 65, '2020-08-08 12:41:23', '2020-08-11 16:13:38'),
(55, 14, 9, 25, '3.3', ' Membandingkan dua bilangan sampai dua angka dengan menggunakan kumpulan bendabenda konkret ', 70, '2020-08-08 12:41:23', '2020-08-11 16:13:29'),
(56, 14, 9, 25, '3.4', 'Menjelaskan dan melakukan penjumlahan dan pengurangan bilangan yang melibatkan bilangan cacah sampai dengan 99 dalam kehidupan sehari-hari serta mengaitkan penjumlahan dan pengurangan ', 80, '2020-08-08 12:41:23', '2020-08-25 07:13:32'),
(57, 14, 9, 25, '3.5', 'Mengenal pola bilangan yang berkaitan dengan kumpulan benda/gambar/gerakan atau lainnya ', 60, '2020-08-08 12:41:23', '2020-08-25 07:13:39'),
(70, 14, 9, 25, '3.1', 'Menjelaskan makna bilangan cacah sampai dengan 99 sebagai banyak anggota suatu kumpulan objek', 60, '2020-08-08 23:24:01', '2020-08-25 07:13:46'),
(71, 14, 9, 25, '3.2', 'Menjelaskan bilangan sampai dua angka dan nilai tempat penyusun lambang bilangan menggunakan kumpulan benda konkret serta cara membacanya', 60, '2020-08-08 23:24:01', '2020-08-25 07:15:22'),
(72, 14, 9, 25, '3.3', ' Membandingkan dua bilangan sampai dua angka dengan menggunakan kumpulan bendabenda konkret ', 60, '2020-08-08 23:24:01', '2020-08-25 07:15:14'),
(73, 14, 9, 25, '3.4', 'Menjelaskan dan melakukan penjumlahan dan pengurangan bilangan yang melibatkan bilangan cacah sampai dengan 99 dalam kehidupan sehari-hari serta mengaitkan penjumlahan dan pengurangan ', 60, '2020-08-08 23:24:01', '2020-08-25 07:14:26'),
(74, 14, 9, 25, '3.5', 'Mengenal pola bilangan yang berkaitan dengan kumpulan benda/gambar/gerakan atau lainnya ', 60, '2020-08-08 23:24:01', '2020-08-25 07:15:05'),
(75, 14, 9, 25, '3.6', 'Mengenal bangun ruang dan bangun datar dengan menggunakan berbagai benda konkret ', 60, '2020-08-08 23:24:02', '2020-08-25 07:14:55'),
(76, 14, 9, 25, '3.7', 'Mengidentifikasi bangun datar yang dapat disusun membentuk pola pengubinan', 60, '2020-08-08 23:24:02', '2020-08-25 07:14:17'),
(77, 14, 9, 25, '3.8', 'Mengenal dan menentukan panjang dan berat dengan satuan tidak baku menggunakan benda/situasi konkret', 60, '2020-08-08 23:24:02', '2020-08-25 07:14:08'),
(78, 14, 9, 25, '3.9', 'Membandingkan panjang, berat, lamanya waktu, dan suhu  menggunakan benda/ situasi konkret ', 60, '2020-08-08 23:24:02', '2020-08-25 07:14:00'),
(79, 15, 11, 31, '3.1', 'dgd', 60, '2020-08-10 06:40:49', '2020-08-10 06:40:49'),
(80, 15, 11, 31, '3.2', 'dgfiuu', 60, '2020-08-10 08:23:25', '2020-08-10 08:23:25'),
(81, 17, 19, 33, '3.1', 'fgdhdfn', 60, '2020-08-10 11:05:42', '2020-08-10 11:05:42'),
(98, 15, 12, 36, '3.1', 'Menganalisis gerak pada makhluk hidup, sistem gerak pada manusia, dan upaya menjaga kesehatan sistem gerak', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(99, 15, 12, 36, '3.2', 'Menganalisis gerak lurus, pengaruh gaya terhadap gerak berdasarkan Hukum Newton, dan penerapannya pada gerak benda dan gerak makhluk hidup \r\n', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(100, 15, 12, 36, '3.3', 'Menjelaskan konsep usaha, pesawat sederhana, dan penerapannya dalam kehidupan sehari-hari termasuk kerja otot pada struktur rangka manusia ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(101, 15, 12, 36, '3.4', 'Menganalisis keterkaitan struktur jaringan tumbuhan dan fungsinya, serta teknologi yang terinspirasi oleh struktur tumbuhan ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(102, 15, 12, 36, '3.5', 'Menganalisis sistem pencernaan pada manusia dan memahami gangguan yang berhubungan dengan sistem pencernaan, serta upaya menjaga kesehatan sistem pencernaan ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(103, 15, 12, 36, '3.6', 'Menjelaskan berbagai zat aditif dalam makanan dan minuman, zat adiktif, serta dampaknya terhadap kesehatan ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(104, 15, 12, 36, '3.7', 'Menganalisis sistem peredaran darah pada manusia dan memahami gangguan pada sistem peredaran darah, serta upaya menjaga kesehatan sistem peredaran darah', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(105, 15, 12, 36, '3.8', 'Menjelaskan tekanan zat dan penerapannya dalam kehidupan sehari-hari, termasuk tekanan darah, osmosis, dan kapilaritas jaringan angkut pada tumbuhan', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(106, 15, 12, 36, '3.9', 'Menganalisis sistem pernapasan pada manusia dan memahami gangguan pada sistem pernapasan, serta upaya menjaga kesehatan sistem pernapasan ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(107, 15, 12, 36, '3.10', 'Menganalisis sistem ekskresi pada manusia dan memahami gangguan pada sistem ekskresi serta upaya menjaga kesehatan sistem ekskresi ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(108, 15, 12, 36, '3.11', 'Menganalisis konsep getaran, gelombang, dan bunyi dalam kehidupan sehari-hari termasuk sistem pendengaran manusia dan sistem sonar pada hewan', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(109, 15, 12, 36, '3.12', 'Menganalisis sifat-sifat cahaya, pembentukan bayangan pada bidang datar dan lengkung serta penerapannya untuk menjelaskan proses penglihatan manusia, mata serangga, dan prinsip kerja alat optik ', 0, '2020-08-12 11:01:17', '2020-08-12 11:01:17'),
(110, 15, 11, 35, '3.1', ' Menerapkan konsep pengukuran berbagai besaran dengan menggunakan satuan standar (baku) ', 60, '2020-08-13 01:04:55', '2020-08-13 01:05:02'),
(111, 15, 11, 35, '3.2', 'Mengklasifikasikan makhluk hidup dan benda berdasarkan karakteristik yang diamati ', 65, '2020-08-13 01:04:55', '2020-08-13 01:05:10'),
(112, 15, 11, 35, '3.3', 'Menjelaskan konsep campuran dan zat tunggal (unsur dan senyawa), sifat fisika dan kimia, perubahan fisika dan kimia dalam kehidupan sehari-hari ', 60, '2020-08-13 01:04:55', '2020-08-13 01:05:22'),
(113, 15, 11, 35, '3.4', 'Menganalisis konsep suhu, pemuaian, kalor, perpindahan kalor, dan penerapannya dalam kehidupan sehari-hari termasuk mekanisme menjaga kestabilan suhu tubuh pada manusia dan hewan', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(114, 15, 11, 35, '3.5', 'Menganalisis konsep energi, berbagai sumber energi, dan perubahan bentuk energi dalam kehidupan sehari-hari termasuk fotosintesis ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(115, 15, 11, 35, '3.6', 'Mengidentifikasi sistem organisasi kehidupan mulai dari tingkat sel sampai organisme dan komposisi utama penyusun sel', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(116, 15, 11, 35, '3.7', 'Menganalisis interaksi antara makhluk hidup dan lingkungannya serta dinamika populasi akibat interaksi tersebut ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(117, 15, 11, 35, '3.8', ' Menganalisis terjadinya pencemaran lingkungan dan dampaknya bagi ekosistem ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(118, 15, 11, 35, '3.9', 'Menganalisis perubahan iklim dan dampaknya bagi ekosistem ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(119, 15, 11, 35, '3.10', 'Menjelaskan lapisan bumi, gunung api, gempa bumi, dan tindakan pengurangan resiko sebelum, pada saat, dan pasca bencana sesuai ancaman bencana di daerahnya ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(120, 15, 11, 35, '3.11', 'Menganalisis sistem tata surya, rotasi dan revolusi bumi, rotasi dan revolusi bulan, serta dampaknya bagi kehidupan di bumi ', 0, '2020-08-13 01:04:55', '2020-08-13 01:04:55'),
(124, 15, 12, 70, '3.1', 'Mengidentifikasi unsur-unsur  teks berita  (membanggakan dan memotivasi) yang didengar dan dibaca', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(125, 15, 12, 70, '3.2', 'Menelaah struktur dan kebahasaan teks berita (membanggakan dan memotivasi)  yang didengar dan dibaca ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(126, 15, 12, 70, '3.3', ' Mengidentifikasi informasi teks iklan, slogan, atau poster (yang membuat bangga dan memotivasi) dari berbagai sumber yang dibaca dan didengar ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(127, 15, 12, 70, '3.4', ' Menelaah pola penyajian dan kebahasaan teks iklan, slogan, atau poster (yang membuat bangga dan memotivasi) dari berbagai sumber yang dibaca dan didengar ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(128, 15, 12, 70, '3.5', ' Mengidentifikasi informasi teks eksposisi berupa artikel ilmiah populer dari koran/majalah) yang didengar dan dibaca ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(129, 15, 12, 70, '3.6', ' Mengidentifikasi struktur, unsur kebahasaan, dan aspek lisandalamteks eksposisi  artikel ilmiah populer (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) yang diperdengarkan atau dibaca', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(130, 15, 12, 70, '3.7', 'Mengidentifikasi unsur-unsur pembangun teks puisi yang diperdengarkan atau dibaca ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(131, 15, 12, 70, '3.8', 'Menelaah unsur-unsur pembangun teks puisi (perjuangan, lingkungan hidup, kondisi sosial, dan lain-lain) yang diperdengarkan atau dibaca \r\n ', 0, '2020-08-29 00:54:26', '2020-08-29 00:54:26'),
(132, 15, 12, 70, '3.9', 'Mengidentifikasi informasi dari teks ekplanasi berupa paparan kejadian suatu fenomena alam yang diperdengarkan atau dibaca ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(133, 15, 12, 70, '3.10', 'Menelaah teks ekplanasi berupa paparan kejadian suatu fenomena alam yang diperdengarkan atau dibaca \r\n ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(134, 15, 12, 70, '3.11', ' Mengidentifikasi  informasi pada teks  ulasan tentang kualitas karya (film, cerpen, puisi, novel, dan karya seni daerah) yang dibaca atau diperdengarkan ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(135, 15, 12, 70, '3.12', 'Menelaah struktur  dan kebahasaan teks ulasan (film, cerpen, puisi, novel, dan karya seni daerah) yang diperdengarkan dan dibaca \r\n ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(136, 15, 12, 70, '3.13', 'Mengidentifikasi jenis  saran, ajakan, arahan, dan pertimbangan tentang berbagai hal positif atas permasalahan aktual dari teks persuasi (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya) yang didengar dan dibaca ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(137, 15, 12, 70, '3.14', ' Menelaah struktur dan kebahasaan teks persuasi yang berupa saran, ajakan, dan pertimbangan tentang berbagai permasalahan aktual (lingkungan hidup, kondisi sosial, dan/atau keragaman budaya, dll) dari berbagai sumber yang didengar dan dibaca ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(138, 15, 12, 70, '3.15', 'Mengidentifikasi unsur-unsur  drama (tradisional dan moderen) yang disajikan dalam bentuk pentas atau naskah ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(139, 15, 12, 70, '3.16', 'Menelaah karakteristik unsur dan kaidah kebahasaan dalam teks drama yang berbentuk naskah atau pentas ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(140, 15, 12, 70, '3.17', 'Menggali dan menemukan informasi dari buku fiksi dan nonfiksi yang dibaca ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(141, 15, 12, 70, '3.18', 'Menelaah unsur  buku fiksi dan nonfiksi yang dibaca ', 0, '2020-08-29 00:54:27', '2020-08-29 00:54:27'),
(142, 17, 19, 33, '3.2', 'sdgs', 70, '2020-08-29 06:38:34', '2020-08-29 06:38:34'),
(143, 14, 13, 28, '3.1', 'mengetahui gambar dan bentuk tiga dimensi', 0, '2020-11-15 07:14:01', '2020-11-15 07:14:01'),
(144, 14, 13, 28, '3.2', 'mengetahui tanda tempo dan tinggi rendah nada ', 0, '2020-11-15 07:14:02', '2020-11-15 07:14:02'),
(145, 14, 13, 28, '3.3', 'mengetahui gerak tari kreasi daerah ', 0, '2020-11-15 07:14:02', '2020-11-15 07:14:02'),
(146, 14, 13, 28, '3.4', 'mengetahui karya seni rupa teknik tempel ', 0, '2020-11-15 07:14:02', '2020-11-15 07:14:02'),
(148, 17, 20, 34, '3.1', 'test kd androis', 50, '2021-01-23 09:59:14', '2021-01-23 09:59:14');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi_kesehatan`
--

CREATE TABLE `kondisi_kesehatan` (
  `id_kondisi` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `aspek_fisik` varchar(200) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kondisi_kesehatan`
--

INSERT INTO `kondisi_kesehatan` (`id_kondisi`, `id_sekolah`, `id_siswa`, `aspek_fisik`, `keterangan`, `created_at`, `updated_at`, `id_kelas`, `id_semester`) VALUES
(4, 14, 36, 'Mata', 'test\r\n', '2020-09-02 09:17:27', '2020-09-02 09:17:37', 9, 2),
(5, 17, 44, 'Mata', 'rabun magrib', '2020-12-16 23:59:04', '2020-12-16 23:59:04', 19, 7);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id_mapel` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nama_mata_pelajaran` varchar(200) NOT NULL,
  `jurusan` varchar(200) NOT NULL,
  `kelompok` enum('A','B','C') NOT NULL,
  `kkm_pengetahuan` double NOT NULL,
  `kkm_keterampilan` double NOT NULL,
  `tahun_ajaran` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id_mapel`, `id_sekolah`, `id_kelas`, `nama_mata_pelajaran`, `jurusan`, `kelompok`, `kkm_pengetahuan`, `kkm_keterampilan`, `tahun_ajaran`, `created_at`, `updated_at`) VALUES
(24, 14, 9, 'bahasa indonesia', '', 'A', 69, 64, '2020', '2020-07-21 12:32:47', '2020-08-29 00:40:54'),
(25, 14, 9, 'Matematika', '', 'A', 33, 62, '2020', '2020-07-21 13:58:30', '2021-02-28 07:55:26'),
(26, 14, 18, 'bahasa indonesia', '', 'A', 60, 64, '2020', '2020-07-21 17:08:38', '2020-08-08 07:09:03'),
(27, 14, 15, 'ipa', '', 'A', 70, 60, '2020', '2020-07-28 09:19:57', '2020-08-08 09:34:10'),
(28, 14, 13, 'Seni Budaya dan Prakarya', '', 'B', 60, 65, '2020', '2020-07-30 13:28:33', '2020-11-14 09:40:45'),
(31, 15, 11, 'Bahasa Indonesia', '', 'A', 60, 70, '2020', '2020-08-10 06:15:00', '2020-08-13 00:33:21'),
(32, 15, 11, 'fisika', '', 'A', 0, 0, '2020', '2020-08-10 06:15:14', '2020-08-10 06:15:14'),
(33, 17, 19, 'kimia', 'IPA', 'A', 65, 65, '2020', '2020-08-10 11:03:28', '2020-08-29 06:38:34'),
(34, 17, 20, 'fisika', 'IPA', 'A', 50, 70, '2020', '2020-08-10 11:03:39', '2021-01-23 09:59:14'),
(35, 15, 11, 'IPA', '', 'A', 17, 0, '2020', '2020-08-12 00:40:17', '2020-08-13 01:05:23'),
(36, 15, 12, 'IPA', '', 'A', 0, 61, '2019', '2020-08-12 10:59:54', '2020-08-23 11:16:12'),
(51, 15, 11, 'agama', '', 'A', 0, 0, '2020', '2020-08-17 21:09:04', '2020-08-17 21:09:04'),
(52, 15, 21, 'agama', '', 'A', 0, 0, '2019', '2020-08-17 21:09:04', '2020-08-29 15:40:39'),
(53, 15, 12, 'kimia', '', 'A', 0, 0, '2019', '2020-08-17 21:09:46', '2020-08-17 21:09:46'),
(64, 17, 19, 'bahasa indonesia', 'IPA', 'A', 0, 0, '2020', '2020-08-18 20:26:47', '2020-08-18 20:26:47'),
(65, 17, 22, 'bahasa indonesia', 'IPA', 'A', 0, 0, '2020', '2020-08-18 20:26:47', '2020-08-18 20:26:47'),
(66, 15, 11, 'IPS', '', 'A', 0, 0, '2020', '2020-08-18 20:29:34', '2020-08-18 20:29:34'),
(70, 15, 12, 'Bahasa Indonesia', '', 'A', 0, 0, '2019', '2020-08-29 00:48:55', '2020-08-29 00:48:55'),
(121, 17, 19, 'agama', 'IPA', 'C', 0, 60, '2020', '2021-01-01 07:39:34', '2021-01-01 07:39:55'),
(122, 17, 19, 'agama', 'IPA', 'C', 0, 0, '2020', '2021-01-01 07:39:34', '2021-01-29 08:59:13'),
(123, 17, 19, 'biologi', 'IPA', 'B', 0, 60, '2020', '2021-01-01 07:41:36', '2021-01-01 07:41:48'),
(125, 17, 19, 'seni budaya', 'IPA', 'B', 0, 60, '2020', '2021-01-01 07:47:36', '2021-01-01 07:47:46'),
(126, 17, 22, 'seni budaya', 'IPA', 'B', 0, 0, '2020', '2021-01-01 07:47:36', '2021-01-01 07:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `mengajar`
--

CREATE TABLE `mengajar` (
  `id_mengajar` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mengajar`
--

INSERT INTO `mengajar` (`id_mengajar`, `id_sekolah`, `id_kelas`, `id_guru`, `id_mapel`, `status`, `created_at`, `updated_at`) VALUES
(17, 14, 9, 91, 25, 'Aktif', '2020-07-25 11:52:15', '2020-07-25 11:52:15'),
(18, 14, 10, 92, 26, 'Aktif', '2020-07-25 12:57:00', '2020-11-05 09:03:37'),
(19, 14, 15, 94, 27, 'Aktif', '2020-07-28 09:23:06', '2021-02-19 23:59:33'),
(20, 14, 13, 91, 28, 'Tidak Aktif', '2020-07-30 13:50:57', '2021-02-21 07:44:18'),
(22, 15, 11, 93, 31, 'Aktif', '2020-08-10 06:15:25', '2020-08-10 06:15:25'),
(23, 17, 19, 99, 33, 'Aktif', '2020-08-10 11:16:30', '2020-08-10 11:16:30'),
(24, 17, 20, 98, 34, 'Aktif', '2020-08-10 11:16:39', '2020-08-20 19:54:30'),
(25, 15, 12, 95, 36, 'Aktif', '2020-08-12 11:00:36', '2020-08-12 11:00:36'),
(26, 15, 11, 100, 35, 'Aktif', '2020-08-13 00:56:52', '2020-08-29 15:14:38'),
(27, 17, 19, 99, 33, 'Aktif', '2020-08-23 23:06:25', '2021-02-20 14:08:17'),
(28, 17, 19, 112, 64, 'Aktif', '2020-08-29 04:39:22', '2020-08-29 04:47:49'),
(29, 15, 11, 95, 35, 'Aktif', '2020-12-28 11:11:12', '2020-12-28 11:11:12'),
(30, 15, 12, 100, 70, 'Aktif', '2021-01-27 12:37:14', '2021-01-27 12:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_akhir`
--

CREATE TABLE `nilai_akhir` (
  `id_nilai` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_nilai_kd` int(11) NOT NULL,
  `nilai_pengetahuan` float NOT NULL,
  `nilai_keterampilan` float NOT NULL,
  `predikat_pengetahuan` enum('A','B','C','D') NOT NULL,
  `predikat_keterampilan` enum('A','B','C','D') NOT NULL,
  `deskripsi_pengetahuan` text NOT NULL,
  `deskripsi_keterampilan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_akhir`
--

INSERT INTO `nilai_akhir` (`id_nilai`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_mapel`, `id_semester`, `id_nilai_kd`, `nilai_pengetahuan`, `nilai_keterampilan`, `predikat_pengetahuan`, `predikat_keterampilan`, `deskripsi_pengetahuan`, `deskripsi_keterampilan`, `created_at`, `updated_at`) VALUES
(108, 14, 71, 9, 24, 2, 78, 63, 90, 'D', 'A', '<b>gundul</b> Kurang dalam Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', '<b>gundul</b> Sangat baik dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-01-28 06:08:41', '2021-02-28 07:37:45'),
(111, 14, 35, 9, 24, 2, 122, 0, 96, 'A', 'A', '', '<b>Agusniawan</b> Sangat baik dalam Mempraktikkan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara meletakkan buku, jarak antara mata dan buku, gerakan tangan atas-bawah, kiri-kanan, latihan pelenturan gerakan tangan dengan gerakan menulis di udara/pasir/ meja, melemaskan jari dengan mewarnai, menjiplak, menggambar, membuat garis tegak, miring, lurus, dan lengkung, menjiplak berbagai bentuk gambar, lingkaran, dan bentuk huruf di tempat bercahaya terang) dengan benar  Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-01-30 08:36:17', '2021-02-28 13:17:27'),
(112, 14, 42, 13, 28, 2, 123, 0, 68, 'A', 'C', '', '<b>amirhan</b> Cukup dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar  Dan perlu bimbingan dalam menggambar dan membentuk tiga dimensi', '2021-02-04 12:35:15', '2021-02-09 10:17:14'),
(114, 14, 61, 9, 24, 2, 254, 59, 64, 'D', 'C', '<b>ramadela</b> Kurang dalam Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan  Dan perlu bimbingan dalam Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', '<b>ramadela</b> Cukup dalam Mempraktikkan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara meletakkan buku, jarak antara mata dan buku, gerakan tangan atas-bawah, kiri-kanan, latihan pelenturan gerakan tangan dengan gerakan menulis di udara/pasir/ meja, melemaskan jari dengan mewarnai, menjiplak, menggambar, membuat garis tegak, miring, lurus, dan lengkung, menjiplak berbagai bentuk gambar, lingkaran, dan bentuk huruf di tempat bercahaya terang) dengan benar  Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-06 22:43:02', '2021-02-26 08:22:28'),
(116, 17, 44, 19, 33, 7, 124, 60, 83, 'D', 'B', '<b>ambun</b> Kurang dalam fgdhdfn', '<b>ambun</b> Baik dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar  Dan perlu bimbingan dalam kedua', '2021-02-08 08:52:09', '2021-02-13 07:33:11'),
(118, 15, 43, 11, 35, 3, 24, 38, 0, 'C', 'A', '<b>rido</b> Cukup dalam Mengklasifikasikan makhluk hidup dan benda berdasarkan karakteristik yang diamatinya berkali', '', '2021-02-09 06:43:44', '2021-02-14 07:58:11'),
(119, 14, 71, 9, 25, 2, 127, 0, 70, 'A', 'C', '', '<b>gundul</b> Cukup dalam Menyajikan bilangan cacah sampai dengan 99 yang bersesuaian dengan banyak anggota kumpulan objek yang disajikan ', '2021-02-09 07:33:56', '2021-02-09 07:33:56'),
(120, 14, 71, 9, 31, 2, 128, 0, 45, 'A', 'D', '', '<b>gundul</b> Kurang dalam sdfh', '2021-02-09 12:22:54', '2021-02-09 12:22:54'),
(121, 17, 51, 19, 33, 7, 129, 0, 43, 'A', 'D', '', '<b>omar hisam al arabi</b> Kurang dalam kedua Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-09 12:35:10', '2021-02-13 06:01:39'),
(122, 15, 43, 11, 31, 3, 131, 0, 63, 'A', 'D', '', '<b>rido</b> Kurang dalam sdfh Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-09 12:50:33', '2021-02-09 12:50:45'),
(123, 17, 45, 20, 34, 8, 132, 0, 70, 'A', 'C', '', '<b>riski</b> Cukup dalam testt', '2021-02-12 12:23:45', '2021-02-17 15:00:37'),
(124, 14, 36, 9, 24, 2, 255, 64, 0, 'D', 'A', '<b>Novia</b> Kurang dalam Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan ', '', '2021-02-19 23:15:11', '2021-02-19 23:15:35'),
(125, 14, 61, 9, 25, 2, 266, 60, 0, 'B', 'A', '<b>ramadela</b> Baik dalam  Membandingkan dua bilangan sampai dua angka dengan menggunakan kumpulan bendabenda konkret ', '', '2021-02-22 09:50:09', '2021-02-22 09:50:09'),
(126, 15, 46, 12, 36, 4, 135, 0, 60, 'A', 'D', '', '<b>surya pro</b> Kurang dalam  Menyajikan karya tentang berbagai gangguan pada sistem gerak, serta upaya menjaga kesehatan sistem gerak manusia', '2021-03-01 07:12:47', '2021-03-01 07:17:24'),
(127, 15, 46, 12, 31, 4, 28, 33, 0, 'D', 'A', '<b>surya pro</b> Kurang dalam dgfiuu', '', '2021-03-01 08:59:44', '2021-03-01 08:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_akhir_kkm_satuan_pendidikan`
--

CREATE TABLE `nilai_akhir_kkm_satuan_pendidikan` (
  `id_nilai_akhir` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_nilai_kd` int(11) NOT NULL,
  `nilai_pengetahuan` float NOT NULL,
  `nilai_keterampilan` float NOT NULL,
  `predikat_pengetahuan` enum('A','B','C','D') NOT NULL,
  `predikat_keterampilan` enum('A','B','C','D') NOT NULL,
  `deskripsi_pengetahuan` text NOT NULL,
  `deskripsi_keterampilan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_akhir_kkm_satuan_pendidikan`
--

INSERT INTO `nilai_akhir_kkm_satuan_pendidikan` (`id_nilai_akhir`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `id_mapel`, `id_nilai_kd`, `nilai_pengetahuan`, `nilai_keterampilan`, `predikat_pengetahuan`, `predikat_keterampilan`, `deskripsi_pengetahuan`, `deskripsi_keterampilan`, `created_at`, `updated_at`) VALUES
(84, 14, 71, 9, 2, 24, 78, 63, 45, 'D', 'D', '<b>gundul</b> Kurang dalam Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar ', '<b>gundul</b> Kurang dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-12 10:07:25', '2021-02-12 10:07:25'),
(85, 14, 35, 9, 2, 24, 122, 0, 100, 'D', 'A', '', '<b>awan</b> Sangat baik dalam Mempraktikkan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara meletakkan buku, jarak antara mata dan buku, gerakan tangan atas-bawah, kiri-kanan, latihan pelenturan gerakan tangan dengan gerakan menulis di udara/pasir/ meja, melemaskan jari dengan mewarnai, menjiplak, menggambar, membuat garis tegak, miring, lurus, dan lengkung, menjiplak berbagai bentuk gambar, lingkaran, dan bentuk huruf di tempat bercahaya terang) dengan benar  Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-12 10:22:07', '2021-02-12 10:22:07'),
(86, 14, 42, 13, 2, 28, 123, 0, 68, 'D', 'C', '', '<b>amirhan</b> Cukup dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar  Dan perlu bimbingan dalam menggambar dan membentuk tiga dimensi', '2021-02-12 10:22:07', '2021-02-12 10:22:07'),
(87, 14, 61, 9, 2, 24, 254, 43, 0, 'D', 'D', '<b>ramadela</b> Kurang dalam Menjelaskan  kegiatan persiapan membaca permulaan (cara duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, cara membalik halaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang, dan etika membaca buku) dengan cara yang benar  Dan perlu bimbingan dalam Mengemuka-kan kegiatan persiapan menulis permulaan (cara duduk, cara memegang pensil, cara menggerakkan pensil, cara meletakkan buku, jarak antara mata dan buku, pemilihan tempat dengan cahaya yang terang) yang benar secara lisan ', '', '2021-02-12 10:22:07', '2021-02-12 10:22:07'),
(88, 14, 71, 9, 2, 25, 127, 0, 70, 'D', 'C', '', '<b>gundul</b> Cukup dalam Menyajikan bilangan cacah sampai dengan 99 yang bersesuaian dengan banyak anggota kumpulan objek yang disajikan ', '2021-02-12 10:22:07', '2021-02-12 10:22:07'),
(89, 14, 71, 9, 2, 31, 128, 0, 45, 'D', 'D', '', '<b>gundul</b> Kurang dalam sdfh', '2021-02-12 10:22:08', '2021-02-12 10:22:08'),
(94, 17, 44, 19, 7, 33, 124, 60, 83, 'D', 'B', '<b>ambun</b> Kurang dalam fgdhdfn', '<b>ambun</b> Baik dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar  Dan perlu bimbingan dalam kedua', '2021-02-12 12:13:08', '2021-02-14 08:08:43'),
(96, 17, 51, 19, 7, 33, 129, 0, 43, 'D', 'D', '', '<b>dini</b> Kurang dalam kedua Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-12 12:26:22', '2021-02-17 14:58:22'),
(97, 17, 45, 20, 8, 34, 132, 0, 70, 'D', 'C', '', '<b>riski</b> Cukup dalam testt', '2021-02-13 23:16:26', '2021-02-20 14:07:47'),
(98, 15, 43, 11, 3, 35, 24, 38, 0, 'D', 'D', '<b>rido</b> Kurang dalam Mengklasifikasikan makhluk hidup dan benda berdasarkan karakteristik yang diamati nya', '', '2021-02-14 08:00:37', '2021-02-14 08:01:03'),
(99, 15, 43, 11, 3, 31, 131, 0, 63, 'D', 'D', '', '<b>rido</b> Kurang dalam sdfh Dan perlu bimbingan dalam Mempraktikkan  kegiatan persiapan membaca permulaan (duduk wajar dan baik, jarak antara mata dan buku, cara memegang buku, \r\nhalaman buku, gerakan mata dari kiri ke kanan, memilih tempat dengan cahaya yang  terang) dengan benar ', '2021-02-14 08:00:38', '2021-02-14 08:00:38'),
(100, 15, 46, 12, 4, 36, 135, 0, 60, 'D', 'D', '', '<b>surya pro</b> Kurang dalam  Menyajikan karya tentang berbagai gangguan pada sistem gerak, serta upaya menjaga kesehatan sistem gerak manusia', '2021-03-01 07:33:45', '2021-03-01 07:33:45');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kd_keterampilan`
--

CREATE TABLE `nilai_kd_keterampilan` (
  `id_nilai_kd_keterampilan` int(11) NOT NULL,
  `id_nilai_harian_keterampilan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd_keterampilan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `skor` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_kd_keterampilan`
--

INSERT INTO `nilai_kd_keterampilan` (`id_nilai_kd_keterampilan`, `id_nilai_harian_keterampilan`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `id_kd_keterampilan`, `id_semester`, `skor`, `created_at`, `updated_at`) VALUES
(92, 243, 14, 9, 36, 24, 78, 2, 75, '2020-08-25 08:36:31', '2020-08-25 08:37:40'),
(93, 245, 14, 9, 36, 24, 79, 2, 90, '2020-08-25 08:39:30', '2020-08-25 08:39:30'),
(94, 246, 14, 9, 36, 25, 84, 2, 50, '2020-08-28 16:00:10', '2020-08-28 16:00:10'),
(95, 247, 14, 9, 61, 24, 78, 2, 60, '2020-08-30 08:42:44', '2020-08-30 08:42:44'),
(96, 248, 14, 9, 36, 25, 83, 2, 75, '2020-08-30 08:43:42', '2020-10-25 06:52:13'),
(97, 249, 14, 13, 42, 28, 172, 2, 55, '2020-11-14 09:42:31', '2021-02-09 10:17:14'),
(114, 302, 14, 9, 35, 24, 78, 2, 90, '2021-01-20 04:57:13', '2021-02-28 13:17:27'),
(121, 304, 14, 9, 71, 24, 78, 2, 90, '2021-01-28 06:08:41', '2021-02-28 07:37:45'),
(122, 310, 14, 9, 35, 24, 79, 2, 101, '2021-01-30 08:36:16', '2021-01-30 08:36:16'),
(123, 313, 14, 13, 42, 28, 78, 2, 80, '2021-02-04 12:35:14', '2021-02-04 12:35:14'),
(124, 25, 17, 19, 44, 33, 128, 7, 80, '2021-02-08 08:52:09', '2021-02-08 08:53:17'),
(125, 26, 17, 19, 44, 33, 78, 7, 85, '2021-02-08 09:21:21', '2021-02-09 10:00:09'),
(126, 29, 15, 11, 43, 31, 126, 3, 70, '2021-02-08 09:49:07', '2021-02-09 06:39:27'),
(127, 314, 14, 9, 71, 25, 83, 2, 70, '2021-02-09 07:33:55', '2021-02-09 07:33:55'),
(128, 31, 14, 9, 71, 31, 126, 2, 45, '2021-02-09 12:22:53', '2021-02-09 12:22:53'),
(129, 27, 17, 19, 51, 33, 128, 7, 60, '2021-02-09 12:35:09', '2021-02-09 12:35:09'),
(130, 28, 17, 19, 51, 33, 78, 7, 25, '2021-02-09 12:39:55', '2021-02-13 06:01:39'),
(131, 33, 15, 11, 43, 31, 78, 3, 55, '2021-02-09 12:50:33', '2021-02-09 12:50:33'),
(132, 29, 17, 20, 45, 34, 152, 8, 70, '2021-02-12 12:23:45', '2021-02-17 06:52:54'),
(133, 318, 14, 9, 61, 24, 79, 2, 68, '2021-02-14 06:50:10', '2021-02-14 06:50:10'),
(135, 36, 15, 12, 46, 36, 140, 4, 60, '2021-03-01 07:12:47', '2021-03-01 07:17:24');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kd_pengetahuan`
--

CREATE TABLE `nilai_kd_pengetahuan` (
  `id_nilai_kd_pengetahuan` int(11) NOT NULL,
  `id_nilai_harian_pengetahuan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nph` float NOT NULL,
  `npts` float NOT NULL,
  `npas` float NOT NULL,
  `nilai_kd` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_kd_pengetahuan`
--

INSERT INTO `nilai_kd_pengetahuan` (`id_nilai_kd_pengetahuan`, `id_nilai_harian_pengetahuan`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `id_kd_pengetahuan`, `id_semester`, `nph`, `npts`, `npas`, `nilai_kd`, `created_at`, `updated_at`) VALUES
(233, 79, 15, 12, 46, 36, 98, 4, 60, 65, 0, 46, '2020-08-21 00:58:00', '2020-08-29 15:49:51'),
(234, 81, 15, 12, 46, 36, 99, 4, 90, 0, 0, 45, '2020-08-21 00:58:45', '2020-08-29 15:49:54'),
(235, 82, 15, 11, 43, 31, 79, 3, 72, 0, 0, 35, '2020-08-21 01:01:35', '2021-01-12 13:57:30'),
(236, 3, 17, 19, 44, 33, 81, 7, 90, 40, 0, 55, '2020-08-21 01:12:55', '2021-01-01 23:05:56'),
(241, 4, 17, 19, 51, 33, 81, 7, 75, 0, 0, 0, '2020-08-23 22:39:29', '2020-08-25 07:33:02'),
(247, 83, 15, 12, 46, 70, 124, 4, 90, 0, 0, 45, '2020-08-29 01:00:19', '2020-08-29 17:00:06'),
(254, 381, 14, 9, 61, 24, 33, 2, 42, 40, 0, 48, '2020-08-30 08:39:01', '2021-02-14 06:45:54'),
(255, 380, 14, 9, 36, 24, 34, 2, 90, 75, 0, 64, '2020-08-30 08:39:04', '2021-02-19 23:15:34'),
(256, 379, 14, 9, 61, 24, 34, 2, 70, 50, 90, 70, '2020-08-30 08:39:07', '2021-02-22 09:49:27'),
(258, 383, 14, 9, 35, 24, 34, 2, 75, 0, 0, 0, '2020-09-02 07:14:03', '2020-09-16 04:15:20'),
(259, 385, 14, 9, 36, 25, 55, 2, 95, 98, 0, 72, '2020-10-31 12:55:11', '2020-10-31 12:57:20'),
(260, 84, 15, 11, 43, 35, 111, 3, 72, 0, 0, 0, '2020-12-28 11:13:59', '2020-12-28 11:13:59'),
(262, 386, 14, 9, 71, 25, 54, 2, 75, 0, 0, 0, '2021-01-12 08:11:26', '2021-02-28 07:16:46'),
(264, 403, 14, 9, 35, 24, 33, 2, 47, 0, 0, 0, '2021-01-19 07:22:57', '2021-02-26 08:08:07'),
(265, 409, 14, 9, 71, 24, 33, 2, 100, 50, 0, 63, '2021-01-30 08:45:22', '2021-02-09 07:08:21'),
(266, 384, 14, 9, 61, 25, 55, 2, 85, 70, 0, 60, '2021-02-08 03:52:55', '2021-02-22 09:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kd_pengetahuan_akhir_sekolah_menengah`
--

CREATE TABLE `nilai_kd_pengetahuan_akhir_sekolah_menengah` (
  `id_nilai_kd_pengetahuan_akhir` int(11) NOT NULL,
  `id_nilai_pengetahuan_sekolah_menengah` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nilai_kd` float NOT NULL,
  `npts` float NOT NULL,
  `npas` float NOT NULL,
  `nilai_akhir_kd` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_kd_pengetahuan_akhir_sekolah_menengah`
--

INSERT INTO `nilai_kd_pengetahuan_akhir_sekolah_menengah` (`id_nilai_kd_pengetahuan_akhir`, `id_nilai_pengetahuan_sekolah_menengah`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `id_semester`, `nilai_kd`, `npts`, `npas`, `nilai_akhir_kd`, `created_at`, `updated_at`) VALUES
(20, 27, 17, 19, 44, 33, 7, 63, 70, 0, 60, '2021-02-08 04:27:01', '2021-02-14 09:29:37'),
(24, 35, 15, 11, 43, 35, 3, 75, 10, 0, 38, '2021-02-08 06:39:02', '2021-02-12 23:46:20'),
(25, 36, 15, 11, 43, 31, 3, 65, 0, 0, 0, '2021-02-08 06:39:16', '2021-02-09 12:45:28'),
(26, 38, 17, 19, 51, 33, 7, 60, 0, 0, 0, '2021-02-09 12:31:45', '2021-02-13 05:58:43'),
(27, 41, 17, 20, 45, 34, 8, 98, 0, 0, 0, '2021-02-21 06:46:39', '2021-02-21 06:46:39'),
(28, 42, 15, 12, 46, 31, 4, 65, 0, 0, 33, '2021-03-01 07:24:12', '2021-03-01 08:59:43'),
(29, 43, 15, 12, 46, 36, 4, 50, 0, 0, 0, '2021-03-01 07:26:55', '2021-03-01 07:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kd_pengetahuan_sekolah_menengah`
--

CREATE TABLE `nilai_kd_pengetahuan_sekolah_menengah` (
  `id_nilai_pengetahuan_sekolah_menengah` int(11) NOT NULL,
  `id_nilai_harian_pengetahuan` int(11) NOT NULL,
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nph` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_kd_pengetahuan_sekolah_menengah`
--

INSERT INTO `nilai_kd_pengetahuan_sekolah_menengah` (`id_nilai_pengetahuan_sekolah_menengah`, `id_nilai_harian_pengetahuan`, `id_kd_pengetahuan`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `id_semester`, `nph`, `created_at`, `updated_at`) VALUES
(27, 23, 81, 17, 19, 44, 33, 7, 75, '2021-02-08 04:27:00', '2021-02-14 09:29:37'),
(35, 100, 111, 15, 11, 43, 35, 3, 75, '2021-02-08 06:39:02', '2021-02-12 23:46:20'),
(36, 99, 80, 15, 11, 43, 31, 3, 70, '2021-02-08 06:39:16', '2021-02-09 12:45:28'),
(37, 25, 142, 17, 19, 44, 33, 7, 50, '2021-02-08 08:49:10', '2021-02-13 23:09:02'),
(38, 26, 33, 17, 19, 51, 33, 7, 50, '2021-02-09 12:31:44', '2021-02-09 12:31:44'),
(39, 27, 142, 17, 19, 51, 33, 7, 70, '2021-02-09 12:38:49', '2021-02-13 05:58:42'),
(40, 101, 33, 15, 11, 43, 31, 3, 60, '2021-02-09 12:44:48', '2021-02-09 12:44:48'),
(41, 28, 148, 17, 20, 45, 34, 8, 98, '2021-02-21 06:46:38', '2021-02-21 06:46:38'),
(42, 104, 80, 15, 12, 46, 31, 4, 65, '2021-03-01 07:24:12', '2021-03-01 07:24:12'),
(43, 105, 98, 15, 12, 46, 36, 4, 50, '2021-03-01 07:26:54', '2021-03-01 07:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `post_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `id_sekolah`, `isi`, `gambar`, `created_at`, `updated_at`, `post_by`) VALUES
(1, 14, 'mohon para guru dan siswa untuk melengkapi sendiri data atau profil dirinya', 'IMG_21767667649035.jpeg', '2020-08-02 06:49:09', '2020-08-07 13:56:31', 142),
(2, 14, 'libur corona di perpanjang sampai 2025\r\n\r\n', 'cycling-delh.jpg', '2020-08-02 07:03:05', '2020-08-04 18:25:53', 142),
(3, 14, 'besok libur corona, tapi tetap belajar secara online libur seumur hidup', 'el.png', '2020-08-02 09:36:52', '2021-03-01 06:36:44', 142),
(4, 17, 'besok jokowi datang, tapi boong\r\n', 'FB_IMG_14445685678634618.jpg', '2020-08-10 12:47:27', '2020-10-12 09:50:12', 163),
(5, 15, 'libur corona di perpanjang sampai 2022', 'FB_IMG_1436115365228.jpg', '2020-08-29 15:09:51', '2020-08-29 15:46:42', 144),
(6, 17, 'besok tidak boleh datang terlambat\r\n', '', '2020-10-26 07:45:08', '2020-10-26 07:45:08', 163),
(8, 17, 'lagi gabut nich', '', '2020-12-16 10:42:43', '2020-12-16 10:42:43', 163),
(10, 15, 'diwaijbkan semua siswa dan majelis guru memakai batik pada hari kamis', '', '2020-12-28 12:37:28', '2020-12-28 12:37:28', 144);

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `prestasi` varchar(200) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `id_sekolah`, `id_siswa`, `prestasi`, `keterangan`, `created_at`, `updated_at`, `id_kelas`, `id_semester`) VALUES
(5, 14, 34, 'Olimpiade Matematika', 'test', '2020-09-02 09:11:37', '2020-09-02 09:11:37', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rangking`
--

CREATE TABLE `rangking` (
  `id_rangking` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nilai_rata_rata_pengetahuan` double NOT NULL,
  `nilai_rata_rata_keterampilan` double NOT NULL,
  `total` double NOT NULL,
  `rangking` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rangking`
--

INSERT INTO `rangking` (`id_rangking`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_semester`, `nilai_rata_rata_pengetahuan`, `nilai_rata_rata_keterampilan`, `total`, `rangking`) VALUES
(26, 15, 12, 46, 4, 45.5, 35, 81, 1),
(28, 17, 19, 44, 7, 55, 80, 60, 2),
(29, 14, 9, 36, 2, 31.5, 35, 99, 2),
(30, 14, 9, 61, 2, 47, 30, 77, 2),
(31, 14, 13, 42, 2, 0, 50, 50, 1),
(32, 15, 11, 43, 3, 35, 0, 35, 0),
(33, 17, 19, 51, 7, 58, 0, 58, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rombongan_belajar`
--

CREATE TABLE `rombongan_belajar` (
  `id_rombel` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `tahun_ajaran` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `status` enum('Aktif','Tidak Aktif','Lulus') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rombongan_belajar`
--

INSERT INTO `rombongan_belajar` (`id_rombel`, `id_sekolah`, `id_kelas`, `id_semester`, `tahun_ajaran`, `id_guru`, `id_siswa`, `status`, `created_at`, `updated_at`) VALUES
(29, 14, 15, 1, 2020, 94, 37, 'Tidak Aktif', '2020-07-27 12:50:56', '2020-08-16 06:33:03'),
(33, 14, 10, 6, 2020, 92, 39, 'Aktif', '2020-07-27 12:55:56', '2020-07-27 12:55:56'),
(37, 14, 10, 6, 2020, 92, 33, 'Aktif', '2020-07-27 12:57:52', '2020-07-27 12:57:52'),
(38, 14, 10, 6, 2020, 92, 34, 'Aktif', '2020-07-27 12:59:07', '2020-07-27 12:59:07'),
(41, 14, 13, 2, 2020, 94, 40, 'Aktif', '2020-07-27 13:11:03', '2020-07-27 13:11:03'),
(51, 14, 13, 2, 2020, 94, 42, 'Aktif', '2020-08-05 09:46:30', '2020-08-05 09:50:33'),
(56, 14, 16, 1, 2020, 91, 37, 'Tidak Aktif', '2020-08-08 17:42:23', '2020-08-16 06:33:03'),
(57, 14, 16, 1, 2020, 91, 37, 'Tidak Aktif', '2020-08-08 17:42:23', '2020-08-16 06:33:03'),
(58, 14, 16, 1, 2020, 91, 38, 'Aktif', '2020-08-08 17:59:27', '2020-08-08 17:59:27'),
(59, 17, 20, 8, 2020, 99, 45, 'Tidak Aktif', '2020-08-10 12:38:12', '2020-11-30 03:52:58'),
(60, 17, 19, 7, 2020, 98, 44, 'Tidak Aktif', '2020-08-10 12:39:13', '2021-01-25 10:20:54'),
(101, 14, 9, 2, 2020, 91, 36, 'Tidak Aktif', '2020-08-11 18:51:49', '2020-10-18 07:34:20'),
(102, 14, 9, 2, 2020, 91, 35, 'Tidak Aktif', '2020-08-11 18:52:09', '2020-10-18 07:34:32'),
(103, 14, 9, 2, 2020, 91, 36, 'Tidak Aktif', '2020-08-11 18:52:39', '2020-10-15 04:00:24'),
(104, 14, 9, 2, 2020, 91, 35, 'Aktif', '2020-08-11 18:52:39', '2020-08-11 18:52:39'),
(105, 15, 11, 3, 2020, 93, 43, 'Aktif', '2020-08-11 19:01:31', '2020-08-11 19:01:31'),
(106, 15, 12, 4, 2019, 95, 46, 'Tidak Aktif', '2020-08-11 19:07:08', '2020-08-12 09:33:04'),
(107, 15, 11, 4, 2019, 95, 46, 'Tidak Aktif', '2020-08-12 09:33:04', '2020-08-12 09:33:16'),
(108, 15, 12, 4, 2019, 95, 46, 'Aktif', '2020-08-12 09:33:16', '2020-08-12 09:33:16'),
(109, 14, 15, 1, 2020, 91, 37, 'Tidak Aktif', '2020-08-16 06:33:03', '2020-08-16 06:56:18'),
(110, 14, 15, 1, 2020, 94, 37, 'Aktif', '2020-08-16 06:57:42', '2020-08-16 06:57:42'),
(111, 17, 19, 7, 2020, 98, 51, 'Tidak Aktif', '2020-08-18 21:56:28', '2021-01-25 10:22:35'),
(112, 14, 9, 2, 2020, 91, 61, 'Aktif', '2020-08-25 20:10:45', '2020-08-25 20:10:45'),
(127, 14, 9, 2, 2020, 91, 36, 'Tidak Aktif', '2020-10-15 04:22:31', '2020-10-31 13:00:56'),
(128, 14, 9, 2, 2020, 91, 36, 'Tidak Aktif', '2020-10-31 13:00:56', '2020-10-31 13:01:21'),
(129, 14, 9, 2, 2020, 91, 36, 'Tidak Aktif', '2020-10-31 13:01:22', '2021-01-04 07:20:04'),
(130, 14, 9, 2, 2020, 91, 71, 'Aktif', '2020-11-15 08:53:43', '2020-11-15 08:53:43'),
(131, 17, 19, 7, 2020, 98, 45, 'Tidak Aktif', '2020-11-30 03:52:58', '2020-11-30 03:54:19'),
(132, 17, 19, 7, 2020, 98, 45, 'Tidak Aktif', '2020-11-30 03:54:19', '2020-11-30 03:54:36'),
(133, 17, 20, 8, 2020, 99, 45, 'Aktif', '2020-11-30 03:54:36', '2020-11-30 03:54:36'),
(134, 14, 10, 2, 2020, 92, 36, 'Aktif', '2021-01-04 07:20:04', '2021-01-04 07:20:04'),
(135, 15, 12, 4, 2019, 95, 81, 'Aktif', '2021-01-05 11:54:05', '2021-01-05 11:54:05'),
(136, 17, 19, 7, 2020, 98, 51, 'Lulus', '2021-01-25 10:22:35', '2021-01-25 10:22:35'),
(137, 17, 19, 7, 2020, 98, 44, 'Aktif', '2021-01-25 10:27:08', '2021-01-25 10:27:08'),
(138, 17, 19, 7, 2020, 98, 51, 'Lulus', '2021-01-31 06:48:44', '2021-01-31 06:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id_saran` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `saran` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `saran`, `created_at`, `updated_at`) VALUES
(4, 15, 46, 12, 4, 'jangan suka ngomel', '2020-12-02 12:16:40', '2020-12-02 12:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `sd_nilai_harian_keterampilan`
--

CREATE TABLE `sd_nilai_harian_keterampilan` (
  `id_nilai_harian_keterampilan` int(11) NOT NULL,
  `id_kd_keterampilan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `jenis_penilaian` enum('Produk','Praktik','Portofolio','Proyek') NOT NULL,
  `nilai` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sd_nilai_harian_keterampilan`
--

INSERT INTO `sd_nilai_harian_keterampilan` (`id_nilai_harian_keterampilan`, `id_kd_keterampilan`, `id_sekolah`, `id_kelas`, `id_semester`, `id_mapel`, `id_siswa`, `id_tema`, `jenis_penilaian`, `nilai`, `created_by`, `created_at`, `updated_at`) VALUES
(321, 78, 14, 9, 2, 24, 71, 17, 'Portofolio', 90, 14, '2021-02-28 07:36:10', '2021-02-28 07:37:45'),
(322, 78, 14, 9, 2, 24, 35, 18, 'Produk', 20, 20, '2021-02-28 07:47:17', '2021-02-28 07:47:17'),
(323, 78, 14, 9, 2, 24, 35, 0, 'Produk', 80, 20, '2021-02-28 13:17:27', '2021-02-28 13:17:27');

-- --------------------------------------------------------

--
-- Table structure for table `sd_nilai_harian_pengetahuan`
--

CREATE TABLE `sd_nilai_harian_pengetahuan` (
  `id_nilai_harian_pengetahuan` int(11) NOT NULL,
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_tema` int(11) DEFAULT NULL,
  `id_subtema` int(11) DEFAULT NULL,
  `nama_penilaian` varchar(200) NOT NULL,
  `nilai` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sd_nilai_harian_pengetahuan`
--

INSERT INTO `sd_nilai_harian_pengetahuan` (`id_nilai_harian_pengetahuan`, `id_kd_pengetahuan`, `id_semester`, `id_mapel`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_tema`, `id_subtema`, `nama_penilaian`, `nilai`, `created_at`, `updated_at`) VALUES
(379, 34, 2, 24, 14, 9, 61, 21, 22, '', 70, '2020-08-30 08:27:41', '2021-02-09 07:10:22'),
(380, 34, 2, 24, 14, 9, 36, 21, 22, 'Latihan 1', 90, '2020-08-30 08:28:12', '2021-02-19 23:34:56'),
(381, 33, 2, 24, 14, 9, 61, 20, 20, 'Pr', 30, '2020-08-30 08:29:03', '2021-02-19 23:37:53'),
(383, 34, 2, 24, 14, 9, 35, 21, 22, '', 75, '2020-09-02 07:14:03', '2020-09-16 04:15:20'),
(384, 55, 2, 25, 14, 9, 61, 23, 24, '', 85, '2020-10-24 07:51:37', '2021-02-08 03:52:54'),
(385, 55, 2, 25, 14, 9, 36, 23, 24, '', 95, '2020-10-31 12:55:11', '2020-10-31 12:55:11'),
(386, 54, 2, 25, 14, 9, 71, 22, 23, '', 80, '2021-01-12 08:11:26', '2021-01-12 08:11:26'),
(403, 33, 2, 24, 14, 9, 35, 20, 20, '', 80, '2021-01-19 07:22:56', '2021-01-19 07:22:56'),
(404, 33, 2, 24, 14, 9, 35, 20, 20, '', 70, '2021-01-19 07:23:27', '2021-01-19 07:23:27'),
(405, 33, 2, 24, 14, 9, 35, 20, 20, '', 80, '2021-01-19 10:16:14', '2021-01-19 10:16:14'),
(406, 33, 2, 24, 14, 9, 61, 20, 20, '', 76, '2021-01-20 22:58:41', '2021-01-20 22:58:41'),
(407, 33, 2, 24, 14, 9, 35, 20, 20, '', 35, '2021-01-26 06:45:04', '2021-01-26 06:45:04'),
(408, 54, 2, 25, 14, 9, 71, 22, 23, '', 76, '2021-01-28 06:07:37', '2021-01-28 06:07:37'),
(409, 33, 2, 24, 14, 9, 71, 20, 20, '', 100, '2021-01-30 08:45:20', '2021-01-30 08:45:20'),
(410, 33, 2, 24, 14, 9, 35, 20, 20, '', 50, '2021-02-09 12:54:29', '2021-02-09 12:54:29'),
(411, 33, 2, 24, 14, 9, 61, 20, 20, '', 20, '2021-02-14 06:45:54', '2021-02-14 06:45:54'),
(412, 33, 2, 24, 14, 9, 35, 20, 20, '', 50, '2021-02-19 23:40:19', '2021-02-19 23:40:19'),
(413, 33, 2, 25, 14, 9, 35, 21, 20, '', 60, '2021-02-19 23:41:32', '2021-02-19 23:41:32'),
(414, 33, 2, 25, 14, 9, 35, 21, 20, 'Test', 60, '2021-02-19 23:42:33', '2021-02-19 23:42:33'),
(415, 33, 2, 24, 14, 9, 35, 20, 20, 'Hhg', 45, '2021-02-26 08:04:31', '2021-02-26 08:04:31'),
(416, 33, 2, 24, 14, 9, 35, 20, 20, 'Ggf', 10, '2021-02-26 08:05:19', '2021-02-26 08:05:19'),
(417, 33, 2, 24, 14, 9, 35, 20, 20, 'Ggf', 10, '2021-02-26 08:07:18', '2021-02-26 08:07:18'),
(418, 33, 2, 24, 14, 9, 35, 20, 20, 'Ggf', 10, '2021-02-26 08:08:07', '2021-02-26 08:08:07'),
(419, 54, 2, 25, 14, 9, 71, NULL, NULL, 'coba gundul', 70, '2021-02-28 07:16:46', '2021-02-28 07:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `id_sekolah` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_sekolah` varchar(200) NOT NULL,
  `nama_kepala_sekolah` varchar(200) NOT NULL,
  `nip_kepsek` varchar(200) NOT NULL,
  `jenjang_pendidikan` enum('SD','SMP','SMA') NOT NULL,
  `npsn` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` text NOT NULL,
  `kecamatan` varchar(200) NOT NULL,
  `kabupaten` varchar(200) NOT NULL,
  `provinsi` varchar(200) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`id_sekolah`, `id_user`, `nama_sekolah`, `nama_kepala_sekolah`, `nip_kepsek`, `jenjang_pendidikan`, `npsn`, `alamat`, `kelurahan`, `kecamatan`, `kabupaten`, `provinsi`, `telepon`, `foto`, `created_at`, `updated_at`) VALUES
(14, 142, 'SD N 009 KUBU BABUSSALAM', 'Khoidir Khonofi', '684753', 'SD', '3464564', 'KUBU ', 'TELUK NILAP METROPOLITAN CITY', 'KUBU BABUSSALAM', 'ROKAN HILIR`', 'RIAU', '3465-4363-4634', '2018-01-15.jpg', '2020-07-06 10:40:03', '2021-01-17 07:18:11'),
(15, 144, 'SMPN 02 KUBU BABUSSALAM', 'rian tobul', '684753', 'SMP', '346', 'KUBU ', 'TELUK NILAP', 'KUBU BABUSSALAM', 'ROKAN HILIR`', 'RIAU46yegrd', '3465-4363-4634', 'IMG_15469533701410.jpeg', '2020-07-06 11:57:54', '2020-12-14 07:18:17'),
(17, 163, 'SMA N 1 KUBU BABUSSALAM', 'RAHMAT AZHARI (rambo)', '88696856757', 'SMA', '675677', 'jl hangtuah, teluk nilap kecamatan kubu', 'TELUK NILAP', 'KUBU BABUSSALAM', 'ROKAN HILIR`', 'Riau', '9808-7675-6567', '10174857_1417941801823273_6848443027786325256_n.jpg', '2020-08-10 10:57:05', '2021-01-11 11:58:57'),
(19, 225, 'SMA N 1 BANGKINANG', 'Nopela Amanda', '46343463436', 'SMA', '457745745', 'jl jendral  sudirman', 'air tiris', 'kuok', 'kampar', 'RIAU', '0989-7687-6569', '', '2020-09-02 02:01:14', '2021-01-12 07:33:16'),
(20, 227, 'SMP N 1 JOJOL', 'abdi anugerah', '374853876', 'SMP', '325235', 'jojol sungai pinang', 'sungai pinang', 'KUBU BABUSSALAM', 'ROKAN HILIR`', 'RIAU', '4346-4645-6456', 'maket.jpg', '2020-10-11 07:09:34', '2020-12-15 07:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `tahun_ajaran` varchar(15) NOT NULL,
  `semester` int(11) NOT NULL,
  `periode_aktif` enum('Aktif','Tidak Aktif') NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_semester`, `id_sekolah`, `tahun_ajaran`, `semester`, `periode_aktif`, `tanggal_mulai`, `tanggal_selesai`, `created_at`, `updated_at`) VALUES
(1, 14, '2021', 2, 'Aktif', '2020-07-09', '2020-07-16', '2020-07-06 11:15:15', '2020-07-19 13:46:52'),
(2, 14, '2020', 1, 'Aktif', '2020-07-01', '2020-08-06', '2020-07-06 11:18:31', '2020-08-04 01:47:14'),
(3, 15, '2019', 5, 'Aktif', '2020-07-08', '2020-07-30', '2020-07-06 12:01:18', '2020-12-02 12:18:49'),
(4, 15, '2019', 2, 'Aktif', '2020-07-08', '2020-07-14', '2020-07-06 12:43:46', '2020-07-06 12:43:46'),
(5, 15, '2000', 8, 'Aktif', '2020-07-07', '2020-07-16', '2020-07-06 13:14:55', '2020-07-06 13:14:55'),
(6, 14, '2021', 1, 'Tidak Aktif', '2020-07-10', '2020-07-25', '2020-07-07 05:54:44', '2020-11-03 06:10:52'),
(7, 17, '2020', 1, 'Aktif', '2020-08-10', '2020-09-19', '2020-08-10 11:02:06', '2020-08-10 11:02:06'),
(8, 17, '2020', 2, 'Aktif', '2020-08-10', '2020-11-20', '2020-08-10 11:02:25', '2020-10-27 11:18:34'),
(11, 17, '2022', 3, 'Tidak Aktif', '2020-10-27', '2020-12-20', '2020-10-27 11:16:44', '2020-10-27 11:18:44');

-- --------------------------------------------------------

--
-- Table structure for table `sikap_akhir`
--

CREATE TABLE `sikap_akhir` (
  `id_sikap` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sikap_sosial` int(11) NOT NULL,
  `id_sikap_spiritual` int(11) NOT NULL,
  `keterangan_sosial` text NOT NULL,
  `keterangan_spiritual` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sikap_akhir`
--

INSERT INTO `sikap_akhir` (`id_sikap`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `id_sikap_sosial`, `id_sikap_spiritual`, `keterangan_sosial`, `keterangan_spiritual`, `created_at`, `updated_at`) VALUES
(92, 14, 33, 10, 6, 80, 0, 'Baik dalam Peduli dan telah mampu meningkatkan Peduli', '', '2021-01-04 09:42:22', '2021-02-09 14:09:44'),
(93, 14, 39, 10, 6, 81, 0, 'Baik dalam Disiplin dan telah mampu meningkatkan Disiplin', '', '2021-01-04 09:43:31', '2021-01-04 09:44:52'),
(94, 14, 36, 10, 2, 0, 51, 'Baik dalam Berdoa sebelum dan sesudah melakukan kegiatan dan telah mampu meningkatkan Berdoa sebelum dan sesudah melakukan kegiatan', 'Baik dalam Berprilaku Syukur', '2021-01-04 09:51:36', '2021-02-09 14:02:55'),
(95, 14, 38, 16, 1, 0, 49, 'Baik dalam Berprilaku Syukur dan telah mampu meningkatkan Toleransi dalam beragama', 'Baik dalam Berdoa sebelum dan sesudah melakukan kegiatan dan telah mampu meningkatkan Toleransi dalam beragama', '2021-01-04 12:07:14', '2021-02-09 14:18:47'),
(103, 15, 43, 11, 3, 97, 0, 'Baik dalam Tanggung Jawab sendiri', '', '2021-02-12 06:50:34', '2021-02-12 06:53:47'),
(112, 17, 45, 20, 8, 0, 0, 'sadakj', 'fgdfg', '2021-02-17 14:46:00', '2021-02-17 14:47:10'),
(113, 17, 44, 19, 7, 0, 0, 'rajin bolos', 'u6ujyj', '2021-02-17 14:46:37', '2021-02-17 14:46:37'),
(114, 15, 46, 12, 4, 99, 0, 'Baik dalam Tanggung Jawab', '', '2021-03-01 07:07:53', '2021-03-01 07:07:53');

-- --------------------------------------------------------

--
-- Table structure for table `sikap_sosial`
--

CREATE TABLE `sikap_sosial` (
  `id_sikap_sosial` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `butir_sikap` enum('Jujur','Disiplin','Tanggung Jawab','Santun','Peduli','Percaya Diri') NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sikap_sosial`
--

INSERT INTO `sikap_sosial` (`id_sikap_sosial`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `butir_sikap`, `nilai`, `created_at`, `updated_at`) VALUES
(80, 14, 33, 10, 6, 'Peduli', 85, '2021-01-04 09:42:22', '2021-02-09 14:09:44'),
(82, 17, 45, 20, 8, 'Disiplin', 60, '2021-01-13 11:14:41', '2021-02-10 14:42:43'),
(96, 14, 61, 9, 2, 'Tanggung Jawab', 70, '2021-02-12 06:17:29', '2021-02-28 13:19:58'),
(97, 15, 43, 11, 3, 'Tanggung Jawab', 60, '2021-02-12 06:50:33', '2021-02-12 06:50:33'),
(98, 17, 51, 19, 7, 'Tanggung Jawab', 50, '2021-02-15 08:13:44', '2021-02-15 08:13:44'),
(99, 15, 46, 12, 4, 'Tanggung Jawab', 70, '2021-03-01 07:06:24', '2021-03-01 07:06:55');

-- --------------------------------------------------------

--
-- Table structure for table `sikap_spiritual`
--

CREATE TABLE `sikap_spiritual` (
  `id_sikap_spiritual` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `butir_sikap` enum('Ketaatan Beribadah','Berprilaku Syukur','Berdoa sebelum dan sesudah melakukan kegiatan','Toleransi dalam beragama') NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sikap_spiritual`
--

INSERT INTO `sikap_spiritual` (`id_sikap_spiritual`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `butir_sikap`, `nilai`, `created_at`, `updated_at`) VALUES
(49, 14, 38, 16, 1, 'Berdoa sebelum dan sesudah melakukan kegiatan', 85, '2020-08-24 21:15:45', '2021-02-09 14:18:47'),
(50, 14, 38, 16, 1, 'Toleransi dalam beragama', 78, '2020-08-24 21:23:21', '2021-02-06 11:39:46'),
(51, 14, 36, 10, 2, 'Berdoa sebelum dan sesudah melakukan kegiatan', 60, '2020-08-24 21:23:51', '2021-02-09 14:02:55'),
(58, 17, 45, 20, 8, 'Toleransi dalam beragama', 90, '2021-01-20 06:55:47', '2021-02-09 14:19:28'),
(60, 14, 61, 9, 2, 'Berprilaku Syukur', 80, '2021-01-22 12:53:34', '2021-02-12 06:21:25'),
(61, 14, 71, 9, 2, 'Berdoa sebelum dan sesudah melakukan kegiatan', 80, '2021-01-22 14:25:08', '2021-02-09 14:15:55'),
(62, 14, 61, 9, 2, 'Berdoa sebelum dan sesudah melakukan kegiatan', 60, '2021-02-12 06:28:08', '2021-02-12 06:28:08'),
(63, 17, 51, 19, 7, 'Berprilaku Syukur', 60, '2021-02-15 08:15:39', '2021-02-15 08:15:39');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `nis` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `agama` enum('Islam','Kristen','Katolik','Hindu','Budha') NOT NULL,
  `foto` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `nama_ayah` varchar(200) NOT NULL,
  `nama_ibu` varchar(200) NOT NULL,
  `pekerjaan_ayah` varchar(200) NOT NULL,
  `pekerjaan_ibu` varchar(200) NOT NULL,
  `no_hp_orang_tua` varchar(200) NOT NULL,
  `chat_id_telegram` varchar(200) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_user`, `id_sekolah`, `id_guru`, `id_kelas`, `id_semester`, `nis`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `foto`, `alamat`, `nama_ayah`, `nama_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `no_hp_orang_tua`, `chat_id_telegram`, `status`, `created_at`, `updated_at`) VALUES
(33, 148, 14, 92, 10, 6, '43767', 'dani', 'kubu', '2020-07-15', 'Laki-laki', 'Islam', '', 'jhjgjh', 'jjhhkj', 'jlguyyt', 'ytddy', 'hjguyfuk', '081287654321', '', 'Aktif', '2020-07-25 14:04:41', '2020-08-06 00:54:11'),
(34, 149, 14, 92, 10, 1, '3466', 'latip', 'fjkjhk', '2020-07-22', 'Laki-laki', 'Islam', '', 'hgug', 'uftfufuy', 'giuiugiugi', 'gigiugiu', 'iuhiugo', '', '', 'Aktif', '2020-07-25 14:05:34', '2020-07-27 12:59:07'),
(35, 150, 14, 91, 9, 2, '6567575788', 'Agusniawan', 'hgjg', '2020-07-15', 'Laki-laki', 'Islam', 'DSC_7219.jpg', 'huigi', 'iigi', 'gugi', 'ugiu', 'iuh', '417879016', '1400581778', 'Aktif', '2020-07-25 14:06:10', '2021-02-23 07:08:07'),
(36, 153, 14, 92, 10, 2, '3468687', 'Novia', 'hgghio', '2020-07-17', 'Perempuan', 'Islam', 'DSC_7221.jpg', 'guyfufiu', 'hkgigyf', 'fuyfigiug', 'giugi', 'fytt', '', '', 'Aktif', '2020-07-25 14:07:00', '2021-01-04 07:20:04'),
(37, 154, 14, 94, 15, 1, '3465465', 'ayu', 'hggy', '2020-07-15', 'Perempuan', 'Islam', 'photo6233402194531559994.jpg', 'hgy', 'fudytd', 'ug;oh', 'oguytyduy', 'uyfiu', '', '', 'Aktif', '2020-07-25 14:08:08', '2020-09-20 11:07:02'),
(38, 155, 14, 91, 16, 1, '345675', 'ali musni', 'jiuhiugu', '2020-07-14', 'Laki-laki', 'Islam', 'IMG-20180721-WA0009.jpg', 'hiugif', 'uudufuigi', 'giuuyfuyu', 'iugugluigliu', 'kjgiug', '0813344657768', '', 'Aktif', '2020-07-25 14:08:51', '2020-09-20 11:07:59'),
(39, 156, 14, 92, 10, 6, '4366798', 'yati', 'hgjhkj', '2020-07-18', 'Perempuan', 'Islam', 'cycling-delh.jpg', 'jkhigiy', 'fuyufuy', 'yfifiufiu', 'uiugiugiugiu', 'guftd', '', '', 'Aktif', '2020-07-25 14:10:13', '2020-08-07 13:05:01'),
(40, 157, 14, 94, 13, 2, '3765647', 'dedi', 'sdfs', '1900-11-21', 'Laki-laki', 'Islam', '250px-Colonne_distillazione.jpg', 'guyf', 'ydtrd', 'yfgih', 'ytfgiu', 'utfr', '', '', 'Aktif', '2020-07-27 13:05:49', '2020-07-28 12:17:45'),
(42, 159, 14, 94, 13, 2, '121423547457', 'amirhan', 'gdgd', '2020-08-11', 'Laki-laki', 'Islam', 'IMG-20180717-WA0023.jpg', 'sszc', 'f', 'gugyg', 'ygg', 'gj', '', '', 'Aktif', '2020-08-02 11:14:44', '2020-09-20 10:50:22'),
(43, 162, 15, 93, 11, 3, '5336346', 'rido', 'kubu', '1900-11-27', 'Laki-laki', 'Islam', '', 'sfsdf', 'afsf', 'sdfsd', 'sgsdg', 'sdgsdg', 'sressg', '417879016', 'Aktif', '2020-08-10 06:16:05', '2020-12-28 12:41:58'),
(44, 166, 17, 98, 19, 7, '45647465', 'ambun', 'kubu', '1900-11-27', 'Perempuan', 'Islam', 'FB_IMG_14445685678634618.jpg', 'shgjhfy', 'anto', 'imar', 'grosir', 'irt', '08193678424', '417879016', 'Aktif', '2020-08-10 11:05:14', '2021-02-26 12:37:28'),
(45, 167, 17, 99, 20, 8, '65765765', 'riski', 'kubu', '1900-11-14', '', 'Islam', 'images-4.jpg', 'sdfsdf', 'dauh', 'ani', 'tani', 'irt', '0998', '', 'Aktif', '2020-08-10 12:33:20', '2021-02-17 08:42:05'),
(46, 168, 15, 95, 12, 4, '3653786', 'surya pro', 'Pekanbaru', '1900-11-10', 'Perempuan', 'Islam', 'IMG20190608123335.jpg', 'entah', 'hjg', 'fuyfuydty', 'dytdtdku', 'tudkukfyf', '0900979886', '', 'Aktif', '2020-08-11 19:03:08', '2021-02-02 07:39:27'),
(51, 184, 17, 98, 19, 7, '3467', 'dini', 'duri', '1900-11-13', '', 'Islam', '', 'duri', 'dg', 'yfytdtysdy', 'dytdjukiu', 'higkg', '758769968', '356017493', 'Tidak Aktif', '2020-08-18 21:05:02', '2021-02-17 08:56:21'),
(61, 208, 14, 91, 9, 2, '67688998', 'ramadela', 'kbu', '1900-10-31', 'Perempuan', 'Islam', 'IMG_20200413_104750.jpg', 'Teluk Nilap Kecamatan Kubu', 'acun', 'ima', 'swasta', 'irt', '', '294931514', 'Aktif', '2020-08-25 20:09:47', '2021-02-28 13:27:29'),
(71, 237, 14, 91, 9, 2, '57474', 'gundul', 'kuvukiland', '1901-07-25', 'Laki-laki', 'Islam', 'IMG20171231204730.jpg', 'kubu', 'ijan', 'ilin', 'tani', 'irt', '912794146', '4545', 'Aktif', '2020-11-15 08:52:41', '2021-01-13 05:22:36'),
(81, 259, 15, 95, 12, 4, '33634634', 'abdul', 'duri', '1900-11-12', 'Laki-laki', 'Islam', '', 'pekanbaru', 'ilaf', 'surya', 'las', 'sosialita', '989789687', 'Gjj57775', 'Aktif', '2021-01-05 11:52:37', '2021-02-16 08:29:47'),
(145, 339, 14, 92, 10, NULL, '', '', '', '0000-00-00', 'Laki-laki', 'Islam', '', '', '', '', '', '', '', '', 'Aktif', '2021-03-01 06:42:24', '2021-03-01 06:42:24'),
(146, 340, 15, 95, 12, 4, '', '', '', '0000-00-00', 'Laki-laki', 'Islam', '', '', '', '', '', '', '', '', 'Aktif', '2021-03-01 06:51:14', '2021-03-01 09:33:55');

-- --------------------------------------------------------

--
-- Table structure for table `sma_nilai_harian_keterampilan`
--

CREATE TABLE `sma_nilai_harian_keterampilan` (
  `id_nilai_harian_keterampilan` int(11) NOT NULL,
  `id_kd_keterampilan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama_penilaian` enum('Proyek','Produk','Praktek','Portofolio') NOT NULL,
  `nilai` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sma_nilai_harian_keterampilan`
--

INSERT INTO `sma_nilai_harian_keterampilan` (`id_nilai_harian_keterampilan`, `id_kd_keterampilan`, `id_semester`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `nama_penilaian`, `nilai`, `created_at`, `updated_at`) VALUES
(25, 128, 7, 17, 19, 44, 33, 'Produk', 80, '2021-02-08 08:52:09', '2021-02-08 08:53:17'),
(26, 78, 7, 17, 19, 44, 33, 'Proyek', 85, '2021-02-08 09:21:20', '2021-02-09 10:00:09'),
(27, 128, 7, 17, 19, 51, 33, 'Portofolio', 60, '2021-02-09 12:35:09', '2021-02-09 12:35:09'),
(28, 78, 7, 17, 19, 51, 33, 'Portofolio', 25, '2021-02-09 12:39:55', '2021-02-13 06:01:38'),
(29, 152, 8, 17, 20, 45, 34, 'Proyek', 70, '2021-02-12 12:23:45', '2021-02-17 06:52:53'),
(30, 128, 7, 17, 19, 44, 33, 'Proyek', 80, '2021-02-15 07:49:28', '2021-02-15 07:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `sma_nilai_harian_pengetahuan`
--

CREATE TABLE `sma_nilai_harian_pengetahuan` (
  `id_nilai_harian_pengetahuan` int(11) NOT NULL,
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `nilai` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama_penilaian` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sma_nilai_harian_pengetahuan`
--

INSERT INTO `sma_nilai_harian_pengetahuan` (`id_nilai_harian_pengetahuan`, `id_kd_pengetahuan`, `id_semester`, `id_sekolah`, `nilai`, `created_at`, `updated_at`, `id_kelas`, `id_siswa`, `id_mapel`, `nama_penilaian`) VALUES
(23, 81, 7, 17, 75, '2021-02-08 04:27:00', '2021-02-14 09:29:36', 19, 44, 33, 'Test android'),
(25, 142, 7, 17, 50, '2021-02-08 08:49:10', '2021-02-13 23:09:02', 19, 44, 33, 'harian 1'),
(26, 33, 7, 17, 50, '2021-02-09 12:31:44', '2021-02-13 00:03:56', 19, 51, 33, 'Coba update'),
(27, 142, 7, 17, 70, '2021-02-09 12:38:48', '2021-02-15 07:52:59', 19, 51, 33, 'Nyontek'),
(28, 148, 8, 17, 98, '2021-02-21 06:46:38', '2021-02-21 06:46:38', 20, 45, 34, 'Debug');

-- --------------------------------------------------------

--
-- Table structure for table `smp_nilai_harian_keterampilan`
--

CREATE TABLE `smp_nilai_harian_keterampilan` (
  `id_nilai_harian_keterampilan` int(11) NOT NULL,
  `id_kd_keterampilan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama_penilaian` enum('Produk','Praktek','Proyek','Portofolio') NOT NULL,
  `nilai` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `smp_nilai_harian_keterampilan`
--

INSERT INTO `smp_nilai_harian_keterampilan` (`id_nilai_harian_keterampilan`, `id_kd_keterampilan`, `id_semester`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `nama_penilaian`, `nilai`, `created_at`, `updated_at`) VALUES
(29, 126, 3, 15, 11, 43, 31, 'Proyek', 70, '2021-02-08 09:49:07', '2021-02-09 06:39:26'),
(32, 126, 3, 15, 11, 43, 31, 'Proyek', 20, '2021-02-09 12:49:35', '2021-02-09 12:49:35'),
(33, 78, 3, 15, 11, 43, 31, 'Praktek', 55, '2021-02-09 12:50:33', '2021-02-09 12:50:33'),
(34, 78, 3, 15, 11, 43, 31, 'Praktek', 55, '2021-02-09 12:50:45', '2021-02-09 12:50:45'),
(35, 78, 3, 15, 11, 43, 31, 'Praktek', 55, '2021-02-09 12:50:50', '2021-02-09 12:50:50'),
(36, 140, 4, 15, 12, 46, 36, 'Proyek', 50, '2021-03-01 07:12:47', '2021-03-01 07:12:47'),
(37, 140, 4, 15, 12, 46, 36, 'Produk', 70, '2021-03-01 07:15:40', '2021-03-01 07:17:23');

-- --------------------------------------------------------

--
-- Table structure for table `smp_nilai_harian_pengetahuan`
--

CREATE TABLE `smp_nilai_harian_pengetahuan` (
  `id_nilai_harian_pengetahuan` int(11) NOT NULL,
  `id_kd_pengetahuan` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama_penilaian` varchar(200) NOT NULL,
  `nilai` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `smp_nilai_harian_pengetahuan`
--

INSERT INTO `smp_nilai_harian_pengetahuan` (`id_nilai_harian_pengetahuan`, `id_kd_pengetahuan`, `id_semester`, `id_sekolah`, `id_kelas`, `id_siswa`, `id_mapel`, `nama_penilaian`, `nilai`, `created_at`, `updated_at`) VALUES
(98, 80, 3, 15, 11, 43, 31, 'harian', 80, '2021-02-08 05:57:35', '2021-02-08 05:57:35'),
(99, 80, 3, 15, 11, 43, 31, 'harian1', 40, '2021-02-08 05:58:01', '2021-02-09 07:02:16'),
(100, 111, 3, 15, 11, 43, 35, 'harian7 ', 75, '2021-02-08 05:58:39', '2021-02-12 23:46:19'),
(101, 33, 3, 15, 11, 43, 31, 'Gggu', 60, '2021-02-09 12:44:47', '2021-02-09 12:44:47'),
(104, 80, 4, 15, 12, 46, 31, 'test postman', 65, '2021-03-01 07:24:12', '2021-03-01 07:24:12'),
(105, 98, 4, 15, 12, 46, 36, 'Fgf', 50, '2021-03-01 07:26:54', '2021-03-01 07:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `subtema`
--

CREATE TABLE `subtema` (
  `id_subtema` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `subtema` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subtema`
--

INSERT INTO `subtema` (`id_subtema`, `id_sekolah`, `id_kelas`, `id_mapel`, `id_kd`, `id_tema`, `subtema`, `created_at`, `updated_at`) VALUES
(20, 14, 9, 24, 33, 20, 'subtema 1', '2020-08-08 01:34:29', '2020-08-08 01:34:29'),
(21, 14, 9, 24, 33, 20, 'subtema 2', '2020-08-08 01:34:38', '2020-08-08 01:34:38'),
(22, 14, 9, 24, 34, 21, 'subtema 1', '2020-08-08 06:51:53', '2020-08-08 06:51:53'),
(23, 14, 9, 25, 54, 22, 'subtema 1', '2020-08-11 16:14:17', '2020-08-11 16:14:17'),
(24, 14, 9, 25, 55, 23, 'subtema 2', '2020-08-11 16:24:03', '2020-08-11 16:24:03');

-- --------------------------------------------------------

--
-- Table structure for table `teknik_penilaian_keterampilan`
--

CREATE TABLE `teknik_penilaian_keterampilan` (
  `id` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `jenis_penilaian` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teknik_penilaian_keterampilan`
--

INSERT INTO `teknik_penilaian_keterampilan` (`id`, `id_sekolah`, `id_kelas`, `id_mapel`, `id_kd`, `id_tema`, `jenis_penilaian`, `created_at`, `updated_at`) VALUES
(27, 14, 9, 24, 78, 17, 'Praktik', '2020-08-08 06:12:36', '2020-08-08 06:12:36'),
(28, 14, 9, 24, 78, 17, 'Proyek', '2020-08-08 13:26:34', '2020-08-08 13:26:34'),
(29, 14, 9, 24, 78, 18, 'Produk', '2020-08-08 13:26:44', '2020-08-08 13:26:44'),
(30, 14, 9, 24, 79, 19, 'Proyek', '2020-08-08 13:29:27', '2020-08-08 13:29:27'),
(31, 14, 9, 24, 79, 19, 'Produk', '2020-08-08 13:29:39', '2020-08-08 13:29:39'),
(32, 14, 9, 24, 79, 20, 'Proyek', '2020-08-08 13:29:48', '2020-08-08 13:29:48'),
(33, 14, 9, 24, 79, 20, 'Portofolio', '2020-08-08 13:29:57', '2020-08-08 13:29:57'),
(34, 14, 9, 25, 83, 21, 'Proyek', '2020-08-08 13:30:22', '2020-08-08 13:30:22'),
(35, 14, 9, 25, 83, 21, 'Portofolio', '2020-08-08 13:30:51', '2020-08-08 13:30:51'),
(36, 14, 9, 25, 84, 23, 'Produk', '2020-08-08 13:31:29', '2020-08-08 13:31:29'),
(37, 14, 9, 25, 84, 22, 'Praktik', '2020-08-08 13:31:37', '2020-08-08 13:31:37'),
(38, 14, 9, 24, 88, 24, 'Proyek', '2020-08-08 13:44:08', '2020-08-08 13:44:08'),
(39, 14, 13, 28, 172, 26, 'Praktik', '2020-11-14 09:41:45', '2020-11-14 09:41:45');

-- --------------------------------------------------------

--
-- Table structure for table `telegram`
--

CREATE TABLE `telegram` (
  `id_telegram` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `api_telegram` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `telegram`
--

INSERT INTO `telegram` (`id_telegram`, `id_sekolah`, `api_telegram`, `created_at`, `updated_at`) VALUES
(3, 14, '1365032411:AAHaiEbdpSfBZOSAd356Dm83_LOcpyNZUxI', '2020-12-25 00:39:01', '2020-12-25 00:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `tema` varchar(200) NOT NULL,
  `judul` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tema`
--

INSERT INTO `tema` (`id_tema`, `id_sekolah`, `id_kelas`, `id_mapel`, `id_kd`, `tema`, `judul`, `created_at`, `updated_at`) VALUES
(20, 14, 9, 24, 33, 'tema 1', '', '2020-08-08 01:33:50', '2020-08-08 01:33:50'),
(21, 14, 9, 24, 33, 'tema 1', '', '2020-08-08 06:51:42', '2021-01-14 10:39:36'),
(22, 14, 9, 25, 54, 'tema 1', '', '2020-08-11 16:13:48', '2020-08-11 16:13:48'),
(23, 14, 9, 25, 55, 'tema 1', '', '2020-08-11 16:23:56', '2020-08-11 16:23:56'),
(24, 14, 9, 24, 35, 'tema 1', '', '2020-09-01 01:04:15', '2020-09-01 01:04:15'),
(25, 14, 9, 24, 35, 'tema 4', 'memahami puisi cinta', '2021-01-01 07:04:53', '2021-01-01 07:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `tema_keterampilan`
--

CREATE TABLE `tema_keterampilan` (
  `id_tema` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `tema` varchar(200) NOT NULL,
  `judul` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tema_keterampilan`
--

INSERT INTO `tema_keterampilan` (`id_tema`, `id_sekolah`, `id_kelas`, `id_mapel`, `id_kd`, `tema`, `judul`, `created_at`, `updated_at`) VALUES
(17, 14, 9, 24, 78, 'tema 1', '', '2020-08-08 06:12:24', '2020-08-08 06:12:24'),
(18, 14, 9, 24, 78, 'tema 2', '', '2020-08-08 13:26:20', '2020-08-08 13:26:20'),
(19, 14, 9, 24, 79, 'tema 2', '', '2020-08-08 13:27:38', '2020-08-08 13:27:38'),
(20, 14, 9, 24, 79, 'tema 1', '', '2020-08-08 13:29:11', '2020-08-08 13:29:11'),
(21, 14, 9, 25, 83, 'tema 1', '', '2020-08-08 13:30:14', '2020-08-08 13:30:14'),
(22, 14, 9, 25, 84, 'tema 1', '', '2020-08-08 13:31:13', '2020-08-08 13:31:13'),
(23, 14, 9, 25, 84, 'tema 2', '', '2020-08-08 13:31:20', '2020-08-08 13:31:20'),
(24, 14, 9, 24, 88, 'tema 1', '', '2020-08-08 13:44:01', '2020-08-08 13:44:01'),
(25, 14, 9, 24, 78, 'tema 3', '', '2020-09-01 01:08:18', '2020-09-01 01:08:18'),
(26, 14, 13, 28, 172, 'tema 1', '', '2020-11-14 09:41:37', '2020-11-14 09:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `nilai` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tinggi_badan`
--

CREATE TABLE `tinggi_badan` (
  `id_tinggi_badan` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `aspek_penilaian` enum('Tinggi Badan','Berat Badan') NOT NULL,
  `nilai` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tinggi_badan`
--

INSERT INTO `tinggi_badan` (`id_tinggi_badan`, `id_sekolah`, `id_siswa`, `id_kelas`, `id_semester`, `aspek_penilaian`, `nilai`, `created_at`, `updated_at`) VALUES
(9, 14, 35, 9, 2, 'Tinggi Badan', '140 cm', '2020-11-15 08:25:13', '2020-11-15 08:25:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` enum('Administrator','Guru','Siswa','Wali Kelas') NOT NULL,
  `authKey` varchar(200) NOT NULL,
  `accessToken` varchar(200) NOT NULL,
  `status` enum('Tidak Aktif','Aktif') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `username`, `password`, `role`, `authKey`, `accessToken`, `status`, `created_at`, `updated_at`) VALUES
(142, 'choi@gmail.com', 'choi', 'choi', 'Administrator', 'G-Ht-LZMkibosLi_mnqWJoAyozImnhyz', 'accessToken', 'Aktif', '2020-07-06 10:40:03', '2020-07-06 10:40:03'),
(143, 'eka@gmail.com', 'eka', 'eka', 'Guru', 'JXv7wr9zS8YAb035zQvNMyB_Hmc0rkAc', 'accessToken', 'Aktif', '2020-07-06 11:02:07', '2021-02-28 15:23:05'),
(144, 'rian@gmail.com', 'rian', 'rian', 'Administrator', 'O3LHhjkF57oHPen9nDazsT4_lhhTiJUR', 'accessToken', 'Aktif', '2020-07-06 11:57:54', '2020-07-06 11:57:54'),
(145, 'diki@gmail.com', 'diki', 'diki', 'Guru', 'mhEiLEE84OE8JCw3xJowPi2-27tLQW77', 'accessToken', 'Aktif', '2020-07-06 12:04:44', '2020-10-11 08:56:31'),
(146, 'awal@gmail.com', 'awal', 'awal', 'Guru', 'E3XM5ECZMAO_g3UJBLMHtEUh5AYtC8wT', 'accessToken', 'Aktif', '2020-07-06 14:21:50', '2020-07-06 14:21:51'),
(147, 'Daniuwuk@gmail.comedit', 'dani', 'dani', 'Siswa', 'uJ3Gn0VjShD9ybk2EJarTRjEty0IZLyb', 'accessToken', 'Aktif', '2020-07-07 03:10:19', '2021-02-06 15:58:07'),
(148, 'Dani wak denah', 'Dani', 'Dani', 'Siswa', 'bzwh8wWxRgcjwvBoFcdramNR-Gnkes7X', 'accessToken', 'Aktif', '2020-07-07 03:29:41', '2021-02-06 15:57:47'),
(149, 'Latip toung', 'Latip', 'latip', 'Siswa', 'dfhedhdexyhxdrhndfh', 'access', 'Aktif', '2020-07-07 09:23:53', '2021-02-02 11:55:25'),
(150, 'awan@gmail.com', 'awan', 'awan', 'Siswa', 'ymlAMeO_AeihUlp4UBK70nSaw9CXDGBd', 'accessToken', 'Aktif', '2020-07-07 09:44:04', '2020-07-07 09:44:10'),
(151, 'rano@gmail.com', 'rano', 'rano', 'Guru', 'dMJeM5bmgCkW9ppl-Bj_BtNii3yAWb0x', 'accessToken', 'Aktif', '2020-07-08 04:19:03', '2021-02-16 07:05:26'),
(153, 'novia@gmail.com', 'novia', 'novia', 'Siswa', 'cyfPODL8g7JkdIEls9jkTOeqgFlTLc1J', 'accessToken', 'Aktif', '2020-07-15 09:43:03', '2020-07-15 09:44:28'),
(154, 'ayu@gmail.om', 'ayu', 'ayu', 'Siswa', 'HtJgK_0qeUDvHxtOr9pRO_yHS4dAlOPr', 'accessToken', 'Aktif', '2020-07-17 10:42:37', '2020-10-24 06:11:46'),
(155, 'ali@gmail.com', 'ali', 'ali', 'Siswa', '1yE7Bq5BUSUZat64Rt3n9v298meX7_6Y', 'accessToken', 'Aktif', '2020-07-25 10:14:06', '2021-01-02 08:12:24'),
(156, 'yati@gmail.com', 'yati', 'yati', 'Siswa', 'cJgfrvfkWJU_icvX3oeehlXJ-lbzK8DW', 'accessToken', 'Aktif', '2020-07-25 13:05:45', '2021-01-29 06:59:35'),
(157, 'dedi@gmail.com', 'dedi', 'dedi', 'Siswa', 'E6_KLxuHylcLspJixovXBWzHYpB1z4s6', 'accessToken', 'Aktif', '2020-07-27 13:05:49', '2020-11-26 05:44:04'),
(159, 'amir@gmailcom', 'amir', 'amir', 'Siswa', 'j5L9ulN--4vypkq2vYtsPyFvzc43tMwD', 'accessToken', 'Aktif', '2020-08-02 06:46:06', '2020-12-02 09:41:40'),
(160, 'suci@gmail.com', 'suci padila', 'suci', 'Guru', 'CuN-c4jyt0ifZ88DT9xPfbWyMSZ285yV', 'accessToken', 'Aktif', '2020-08-04 07:21:19', '2020-09-02 01:37:24'),
(161, 'deli', 'deli', 'deli', 'Guru', 'MgJ9r1Dp-hNqH-ywRrP-vre7qlscIx1Z', 'accessToken', 'Tidak Aktif', '2020-08-04 19:19:08', '2021-02-18 14:15:02'),
(162, 'rido', 'rido', 'rido', 'Siswa', 'ts4MZ-RUrDQMRa4d7GPMq9bFtDmqqhDv', 'accessToken', 'Aktif', '2020-08-10 06:16:05', '2020-12-04 07:18:34'),
(163, 'rahmat@gmail.com', 'rahmat', 'rahmat', 'Administrator', 'aVq0v2x3Lt6-AuJlEE1B3qVTIA3MfKhn', 'accessToken', 'Aktif', '2020-08-10 10:57:05', '2020-08-10 10:57:05'),
(164, 'deni', 'deni', 'deni', 'Guru', 'tm6M1B9HcFKTg8egMte5L4_JHAimPYm-', 'accessToken', 'Aktif', '2020-08-10 10:58:55', '2020-08-10 11:00:14'),
(165, 'fira', 'fira', 'fira', 'Guru', 'tOYOP2DY2UhEDLVO-jxgeiMwvYLjFIDn', 'accessToken', 'Aktif', '2020-08-10 10:59:28', '2020-08-10 11:00:12'),
(166, 'ambun', 'ambun', 'ambun', 'Siswa', 'R6nbukYqWpo8YLaAmfYx69VlEUVi3w4J', 'accessToken', 'Aktif', '2020-08-10 11:05:14', '2020-11-30 03:52:17'),
(167, 'siis', 'siis', 'siis', 'Siswa', 'kMefKcA1blJTtvuYraO5h8L615lG7kAA', 'accessToken', 'Aktif', '2020-08-10 12:33:20', '2021-02-15 16:12:43'),
(168, 'surya', 'surya', 'surya', 'Siswa', '_j1wQDXlLFtSApPcOTEwgl6xomyeKP2z', 'accessToken', 'Aktif', '2020-08-11 19:03:08', '2020-12-28 12:39:16'),
(169, 'mimin@gmail.com', 'mimin', 'mimin', 'Guru', '-VB3rMb8CyVVQwgIr3WOoQMlRV-jgABy', 'accessToken', 'Aktif', '2020-08-13 00:55:06', '2020-08-13 00:55:28'),
(175, 'anto', 'anto', 'anto', 'Guru', 'KdqhqopgD9Cec_FUGRiDSu1umoj0uQhE', 'accessToken', 'Aktif', '2020-08-16 12:23:29', '2021-02-05 09:33:04'),
(177, 'fauzi', 'fauzi', 'fauzi', 'Guru', 's0ivn7oYamgitW-nVjPr-DFgHYABp6wv', 'accessToken', 'Aktif', '2020-08-16 12:28:14', '2020-08-16 12:28:14'),
(180, 'rohim', 'rohim', 'rohim', 'Guru', '4qZUs9m1TAeMcmMZ1QZdTgZdLdvCMhja', 'accessToken', 'Aktif', '2020-08-17 21:23:17', '2021-01-11 11:21:28'),
(182, 'fesfs', 'sela', 'sela', 'Siswa', 'joCHigJ0IGIra1ai3TkQmlqKd4o1RJgg', 'accessToken', 'Aktif', '2020-08-18 21:03:38', '2020-08-18 21:03:38'),
(183, 'omar', 'omar', 'omar', 'Siswa', 'UdgF4XPeaZil30Sv1yrNHVY2a8PLWEnm', 'accessToken', 'Aktif', '2020-08-18 21:04:30', '2020-08-18 21:04:30'),
(184, 'asfa', 'oamr', 'omar', 'Siswa', '-uNzVYhgRg3QXgy6XvO0KcXVXAjkPkHq', 'accessToken', 'Aktif', '2020-08-18 21:05:02', '2020-11-30 03:48:27'),
(185, 'choi@gmail.com', 'nana', 'sfs', 'Siswa', 'i-jeDh2qY6sOhW1xcthsKAtigIZRMuh-', 'accessToken', 'Aktif', '2020-08-18 21:07:17', '2020-08-18 21:07:17'),
(186, 'komar', 'komar', 'komar', 'Siswa', 'yMMU1zamyMljwwPi0bBxEs3bGAfjLU0Y', 'accessToken', 'Aktif', '2020-08-23 09:20:18', '2020-08-23 09:20:18'),
(190, 'oca', 'oca', 'oca', 'Guru', 'uY-qMdjVKlciRb5TvjwcYXEOqEx5esaH', 'accessToken', 'Aktif', '2020-08-23 09:33:10', '2020-08-23 10:00:20'),
(208, 'dela', 'dela', 'dela', 'Siswa', '44aup103uLiD6S1n-R7X1d37wRumo7Sz', 'accessToken', 'Aktif', '2020-08-25 20:09:47', '2020-08-25 20:09:47'),
(215, 'daud', 'daud', 'daud', 'Guru', 'zCZl2rI2xhmriv-6i28c5w7FebeICL5F', 'accessToken', 'Aktif', '2020-08-29 04:26:54', '2020-08-29 04:27:42'),
(216, 'evi', 'evi', 'evi', 'Guru', 'X0XZZZ5aKT83EtKAkTRY8L-4h7W5N8i8', 'accessToken', 'Aktif', '2020-08-29 04:43:39', '2021-02-22 13:49:39'),
(217, 'tika', 'tika', 'tika', 'Guru', 'NrXQbwt62xiVtzkHGs6_a9UuzdqvcZfB', 'accessToken', 'Aktif', '2020-08-29 13:27:24', '2020-08-29 15:16:50'),
(225, 'sekolah@gmail.com', 'ela', 'ela', 'Administrator', 'v4uSFu-mW1ZwMWMRqb7i0wY1bQCEC0Da', 'accessToken', 'Aktif', '2020-09-02 02:01:14', '2020-09-02 02:01:14'),
(227, 'email@gmail.com', 'abdi', 'abdi', 'Administrator', 'xXt8ZavTHcC4hNX279szkcbLBC0tNQYK', 'accessToken', 'Aktif', '2020-10-11 07:09:34', '2020-10-11 07:09:34'),
(237, 'gundul', 'gundul', 'gundul', 'Siswa', 'Vv1vEeIpJOrstFlwt4Vtu2tYp0zsqB5k', 'accessToken', 'Aktif', '2020-11-15 08:52:41', '2020-12-02 09:47:13'),
(259, 'abdul', 'adul', 'abdul', 'Siswa', '2KhoLopmXo6XU6o6GFs7DpE1WokpLyge', 'accessToken', 'Aktif', '2021-01-05 11:52:37', '2021-02-09 09:32:34'),
(270, 'Ice', 'Ice', 'Icd', 'Guru', 'S3oPCztq7XuQGEL2x7Zq3oXjzZ7bdfzH', 'accessToken', 'Aktif', '2021-02-10 10:27:33', '2021-02-15 06:07:21'),
(272, 'Dita', 'Dita', 'dita', 'Guru', 'ikkfO9UUtVmzirVyCHX72nLYsFvkNMZd', 'accessToken', 'Tidak Aktif', '2021-02-10 11:53:03', '2021-02-10 11:53:03'),
(316, 'bvjhgfhj', 'drtdrt', 'wwr', 'Guru', 'O9tr7hUeIE_trB3X8neYMOIBEZuEHk8A', 'accessToken', 'Tidak Aktif', '2021-02-26 09:31:36', '2021-02-26 09:31:42'),
(331, 'Bh xbd', 'Ffd', 'Dhdh', 'Guru', '8XZdekkotsHMx_MQiLSu8FZTGAzejlCW', 'accessToken', 'Tidak Aktif', '2021-02-27 10:51:23', '2021-02-27 10:51:23'),
(339, 'Lukkka', 'Lukika', 'lukika', 'Siswa', 'T4Q_zy2ei75JoIOBs4Gd2dDANwAkzkkS', 'accessToken', 'Aktif', '2021-03-01 06:42:24', '2021-03-01 06:43:13'),
(340, 'Bran', 'Bram', 'Bram', 'Siswa', 'jGAlRSpYYcdzGQAg6yLk5TntyMu7y7-q', 'accessToken', 'Aktif', '2021-03-01 06:51:14', '2021-03-01 09:33:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bobot_penilaian`
--
ALTER TABLE `bobot_penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `esktrakurikuler`
--
ALTER TABLE `esktrakurikuler`
  ADD PRIMARY KEY (`id_ekstrakurikuler`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indexes for table `jurnal_sikap`
--
ALTER TABLE `jurnal_sikap`
  ADD PRIMARY KEY (`id_jurnal_sikap`);

--
-- Indexes for table `kd_keterampilan`
--
ALTER TABLE `kd_keterampilan`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `kd_pengetahuan`
--
ALTER TABLE `kd_pengetahuan`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `kehadiran`
--
ALTER TABLE `kehadiran`
  ADD PRIMARY KEY (`id_kehadiran`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indexes for table `kkm_satuan_pendidikan`
--
ALTER TABLE `kkm_satuan_pendidikan`
  ADD PRIMARY KEY (`id_kkm`);

--
-- Indexes for table `kompetensi_dasar_keterampilan`
--
ALTER TABLE `kompetensi_dasar_keterampilan`
  ADD PRIMARY KEY (`id_kd_keterampilan`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_sekolah` (`id_sekolah`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `kompetensi_dasar_pengetahuan`
--
ALTER TABLE `kompetensi_dasar_pengetahuan`
  ADD PRIMARY KEY (`id_kd_pengetahuan`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_sekolah` (`id_sekolah`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `kondisi_kesehatan`
--
ALTER TABLE `kondisi_kesehatan`
  ADD PRIMARY KEY (`id_kondisi`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id_mapel`),
  ADD KEY `id_sekolah` (`id_sekolah`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `mengajar`
--
ALTER TABLE `mengajar`
  ADD PRIMARY KEY (`id_mengajar`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indexes for table `nilai_akhir`
--
ALTER TABLE `nilai_akhir`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_nilai_kd` (`id_nilai_kd`);

--
-- Indexes for table `nilai_akhir_kkm_satuan_pendidikan`
--
ALTER TABLE `nilai_akhir_kkm_satuan_pendidikan`
  ADD PRIMARY KEY (`id_nilai_akhir`);

--
-- Indexes for table `nilai_kd_keterampilan`
--
ALTER TABLE `nilai_kd_keterampilan`
  ADD PRIMARY KEY (`id_nilai_kd_keterampilan`),
  ADD KEY `id_nilai_harian_keterampilan` (`id_nilai_harian_keterampilan`);

--
-- Indexes for table `nilai_kd_pengetahuan`
--
ALTER TABLE `nilai_kd_pengetahuan`
  ADD PRIMARY KEY (`id_nilai_kd_pengetahuan`),
  ADD KEY `id_nilai_harian_pengetahuan` (`id_nilai_harian_pengetahuan`);

--
-- Indexes for table `nilai_kd_pengetahuan_akhir_sekolah_menengah`
--
ALTER TABLE `nilai_kd_pengetahuan_akhir_sekolah_menengah`
  ADD PRIMARY KEY (`id_nilai_kd_pengetahuan_akhir`);

--
-- Indexes for table `nilai_kd_pengetahuan_sekolah_menengah`
--
ALTER TABLE `nilai_kd_pengetahuan_sekolah_menengah`
  ADD PRIMARY KEY (`id_nilai_pengetahuan_sekolah_menengah`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`);

--
-- Indexes for table `rangking`
--
ALTER TABLE `rangking`
  ADD PRIMARY KEY (`id_rangking`);

--
-- Indexes for table `rombongan_belajar`
--
ALTER TABLE `rombongan_belajar`
  ADD PRIMARY KEY (`id_rombel`),
  ADD KEY `id_sekolah` (`id_sekolah`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id_saran`);

--
-- Indexes for table `sd_nilai_harian_keterampilan`
--
ALTER TABLE `sd_nilai_harian_keterampilan`
  ADD PRIMARY KEY (`id_nilai_harian_keterampilan`),
  ADD KEY `id_kd_keterampilan` (`id_kd_keterampilan`),
  ADD KEY `jenis_penilaian` (`jenis_penilaian`);

--
-- Indexes for table `sd_nilai_harian_pengetahuan`
--
ALTER TABLE `sd_nilai_harian_pengetahuan`
  ADD PRIMARY KEY (`id_nilai_harian_pengetahuan`),
  ADD KEY `id_kd_pengetahuan` (`id_kd_pengetahuan`),
  ADD KEY `id_tema` (`id_tema`),
  ADD KEY `id_subtema` (`id_subtema`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indexes for table `sikap_akhir`
--
ALTER TABLE `sikap_akhir`
  ADD PRIMARY KEY (`id_sikap`);

--
-- Indexes for table `sikap_sosial`
--
ALTER TABLE `sikap_sosial`
  ADD PRIMARY KEY (`id_sikap_sosial`);

--
-- Indexes for table `sikap_spiritual`
--
ALTER TABLE `sikap_spiritual`
  ADD PRIMARY KEY (`id_sikap_spiritual`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_sekolah` (`id_sekolah`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `sma_nilai_harian_keterampilan`
--
ALTER TABLE `sma_nilai_harian_keterampilan`
  ADD PRIMARY KEY (`id_nilai_harian_keterampilan`),
  ADD KEY `id_kd_keterampilan` (`id_kd_keterampilan`);

--
-- Indexes for table `sma_nilai_harian_pengetahuan`
--
ALTER TABLE `sma_nilai_harian_pengetahuan`
  ADD PRIMARY KEY (`id_nilai_harian_pengetahuan`),
  ADD KEY `id_kd_pengetahuan` (`id_kd_pengetahuan`);

--
-- Indexes for table `smp_nilai_harian_keterampilan`
--
ALTER TABLE `smp_nilai_harian_keterampilan`
  ADD PRIMARY KEY (`id_nilai_harian_keterampilan`),
  ADD KEY `id_kd_keterampilan` (`id_kd_keterampilan`);

--
-- Indexes for table `smp_nilai_harian_pengetahuan`
--
ALTER TABLE `smp_nilai_harian_pengetahuan`
  ADD PRIMARY KEY (`id_nilai_harian_pengetahuan`),
  ADD KEY `id_kd_pengetahuan` (`id_kd_pengetahuan`);

--
-- Indexes for table `subtema`
--
ALTER TABLE `subtema`
  ADD PRIMARY KEY (`id_subtema`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indexes for table `teknik_penilaian_keterampilan`
--
ALTER TABLE `teknik_penilaian_keterampilan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indexes for table `telegram`
--
ALTER TABLE `telegram`
  ADD PRIMARY KEY (`id_telegram`);

--
-- Indexes for table `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`);

--
-- Indexes for table `tema_keterampilan`
--
ALTER TABLE `tema_keterampilan`
  ADD PRIMARY KEY (`id_tema`),
  ADD KEY `id_kd` (`id_kd`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tinggi_badan`
--
ALTER TABLE `tinggi_badan`
  ADD PRIMARY KEY (`id_tinggi_badan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bobot_penilaian`
--
ALTER TABLE `bobot_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `esktrakurikuler`
--
ALTER TABLE `esktrakurikuler`
  MODIFY `id_ekstrakurikuler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `jurnal_sikap`
--
ALTER TABLE `jurnal_sikap`
  MODIFY `id_jurnal_sikap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `kd_keterampilan`
--
ALTER TABLE `kd_keterampilan`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=686;

--
-- AUTO_INCREMENT for table `kd_pengetahuan`
--
ALTER TABLE `kd_pengetahuan`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=659;

--
-- AUTO_INCREMENT for table `kehadiran`
--
ALTER TABLE `kehadiran`
  MODIFY `id_kehadiran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `kkm_satuan_pendidikan`
--
ALTER TABLE `kkm_satuan_pendidikan`
  MODIFY `id_kkm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `kompetensi_dasar_keterampilan`
--
ALTER TABLE `kompetensi_dasar_keterampilan`
  MODIFY `id_kd_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `kompetensi_dasar_pengetahuan`
--
ALTER TABLE `kompetensi_dasar_pengetahuan`
  MODIFY `id_kd_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `kondisi_kesehatan`
--
ALTER TABLE `kondisi_kesehatan`
  MODIFY `id_kondisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `mengajar`
--
ALTER TABLE `mengajar`
  MODIFY `id_mengajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `nilai_akhir`
--
ALTER TABLE `nilai_akhir`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `nilai_akhir_kkm_satuan_pendidikan`
--
ALTER TABLE `nilai_akhir_kkm_satuan_pendidikan`
  MODIFY `id_nilai_akhir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `nilai_kd_keterampilan`
--
ALTER TABLE `nilai_kd_keterampilan`
  MODIFY `id_nilai_kd_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `nilai_kd_pengetahuan`
--
ALTER TABLE `nilai_kd_pengetahuan`
  MODIFY `id_nilai_kd_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `nilai_kd_pengetahuan_akhir_sekolah_menengah`
--
ALTER TABLE `nilai_kd_pengetahuan_akhir_sekolah_menengah`
  MODIFY `id_nilai_kd_pengetahuan_akhir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `nilai_kd_pengetahuan_sekolah_menengah`
--
ALTER TABLE `nilai_kd_pengetahuan_sekolah_menengah`
  MODIFY `id_nilai_pengetahuan_sekolah_menengah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rangking`
--
ALTER TABLE `rangking`
  MODIFY `id_rangking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `rombongan_belajar`
--
ALTER TABLE `rombongan_belajar`
  MODIFY `id_rombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id_saran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sd_nilai_harian_keterampilan`
--
ALTER TABLE `sd_nilai_harian_keterampilan`
  MODIFY `id_nilai_harian_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;

--
-- AUTO_INCREMENT for table `sd_nilai_harian_pengetahuan`
--
ALTER TABLE `sd_nilai_harian_pengetahuan`
  MODIFY `id_nilai_harian_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=420;

--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `id_sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sikap_akhir`
--
ALTER TABLE `sikap_akhir`
  MODIFY `id_sikap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `sikap_sosial`
--
ALTER TABLE `sikap_sosial`
  MODIFY `id_sikap_sosial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `sikap_spiritual`
--
ALTER TABLE `sikap_spiritual`
  MODIFY `id_sikap_spiritual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `sma_nilai_harian_keterampilan`
--
ALTER TABLE `sma_nilai_harian_keterampilan`
  MODIFY `id_nilai_harian_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `sma_nilai_harian_pengetahuan`
--
ALTER TABLE `sma_nilai_harian_pengetahuan`
  MODIFY `id_nilai_harian_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `smp_nilai_harian_keterampilan`
--
ALTER TABLE `smp_nilai_harian_keterampilan`
  MODIFY `id_nilai_harian_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `smp_nilai_harian_pengetahuan`
--
ALTER TABLE `smp_nilai_harian_pengetahuan`
  MODIFY `id_nilai_harian_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `subtema`
--
ALTER TABLE `subtema`
  MODIFY `id_subtema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `teknik_penilaian_keterampilan`
--
ALTER TABLE `teknik_penilaian_keterampilan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `telegram`
--
ALTER TABLE `telegram`
  MODIFY `id_telegram` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tema_keterampilan`
--
ALTER TABLE `tema_keterampilan`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tinggi_badan`
--
ALTER TABLE `tinggi_badan`
  MODIFY `id_tinggi_badan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `guru_ibfk_2` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`);

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`),
  ADD CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`),
  ADD CONSTRAINT `kelas_ibfk_3` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`);

--
-- Constraints for table `kompetensi_dasar_keterampilan`
--
ALTER TABLE `kompetensi_dasar_keterampilan`
  ADD CONSTRAINT `kompetensi_dasar_keterampilan_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `kompetensi_dasar_keterampilan_ibfk_2` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`),
  ADD CONSTRAINT `kompetensi_dasar_keterampilan_ibfk_3` FOREIGN KEY (`id_mapel`) REFERENCES `mata_pelajaran` (`id_mapel`);

--
-- Constraints for table `kompetensi_dasar_pengetahuan`
--
ALTER TABLE `kompetensi_dasar_pengetahuan`
  ADD CONSTRAINT `kompetensi_dasar_pengetahuan_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `kompetensi_dasar_pengetahuan_ibfk_2` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`),
  ADD CONSTRAINT `kompetensi_dasar_pengetahuan_ibfk_3` FOREIGN KEY (`id_mapel`) REFERENCES `mata_pelajaran` (`id_mapel`);

--
-- Constraints for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD CONSTRAINT `mata_pelajaran_ibfk_1` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`);

--
-- Constraints for table `mengajar`
--
ALTER TABLE `mengajar`
  ADD CONSTRAINT `mengajar_ibfk_1` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`),
  ADD CONSTRAINT `mengajar_ibfk_3` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`),
  ADD CONSTRAINT `mengajar_ibfk_4` FOREIGN KEY (`id_mapel`) REFERENCES `mata_pelajaran` (`id_mapel`);

--
-- Constraints for table `rombongan_belajar`
--
ALTER TABLE `rombongan_belajar`
  ADD CONSTRAINT `rombongan_belajar_ibfk_1` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`),
  ADD CONSTRAINT `rombongan_belajar_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `rombongan_belajar_ibfk_3` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id_siswa`),
  ADD CONSTRAINT `rombongan_belajar_ibfk_4` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`);

--
-- Constraints for table `sd_nilai_harian_keterampilan`
--
ALTER TABLE `sd_nilai_harian_keterampilan`
  ADD CONSTRAINT `sd_nilai_harian_keterampilan_ibfk_1` FOREIGN KEY (`id_kd_keterampilan`) REFERENCES `kompetensi_dasar_keterampilan` (`id_kd_keterampilan`);

--
-- Constraints for table `sd_nilai_harian_pengetahuan`
--
ALTER TABLE `sd_nilai_harian_pengetahuan`
  ADD CONSTRAINT `sd_nilai_harian_pengetahuan_ibfk_1` FOREIGN KEY (`id_kd_pengetahuan`) REFERENCES `kompetensi_dasar_pengetahuan` (`id_kd_pengetahuan`),
  ADD CONSTRAINT `sd_nilai_harian_pengetahuan_ibfk_2` FOREIGN KEY (`id_tema`) REFERENCES `tema` (`id_tema`);

--
-- Constraints for table `semester`
--
ALTER TABLE `semester`
  ADD CONSTRAINT `semester_ibfk_1` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`);

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_sekolah`) REFERENCES `sekolah` (`id_sekolah`);

--
-- Constraints for table `sma_nilai_harian_keterampilan`
--
ALTER TABLE `sma_nilai_harian_keterampilan`
  ADD CONSTRAINT `sma_nilai_harian_keterampilan_ibfk_1` FOREIGN KEY (`id_kd_keterampilan`) REFERENCES `kompetensi_dasar_keterampilan` (`id_kd_keterampilan`);

--
-- Constraints for table `sma_nilai_harian_pengetahuan`
--
ALTER TABLE `sma_nilai_harian_pengetahuan`
  ADD CONSTRAINT `sma_nilai_harian_pengetahuan_ibfk_1` FOREIGN KEY (`id_kd_pengetahuan`) REFERENCES `kompetensi_dasar_pengetahuan` (`id_kd_pengetahuan`);

--
-- Constraints for table `smp_nilai_harian_keterampilan`
--
ALTER TABLE `smp_nilai_harian_keterampilan`
  ADD CONSTRAINT `smp_nilai_harian_keterampilan_ibfk_1` FOREIGN KEY (`id_kd_keterampilan`) REFERENCES `kompetensi_dasar_keterampilan` (`id_kd_keterampilan`);

--
-- Constraints for table `smp_nilai_harian_pengetahuan`
--
ALTER TABLE `smp_nilai_harian_pengetahuan`
  ADD CONSTRAINT `smp_nilai_harian_pengetahuan_ibfk_1` FOREIGN KEY (`id_kd_pengetahuan`) REFERENCES `kompetensi_dasar_pengetahuan` (`id_kd_pengetahuan`);

--
-- Constraints for table `subtema`
--
ALTER TABLE `subtema`
  ADD CONSTRAINT `subtema_ibfk_1` FOREIGN KEY (`id_tema`) REFERENCES `tema` (`id_tema`);

--
-- Constraints for table `teknik_penilaian_keterampilan`
--
ALTER TABLE `teknik_penilaian_keterampilan`
  ADD CONSTRAINT `teknik_penilaian_keterampilan_ibfk_1` FOREIGN KEY (`id_tema`) REFERENCES `tema_keterampilan` (`id_tema`);

--
-- Constraints for table `tema_keterampilan`
--
ALTER TABLE `tema_keterampilan`
  ADD CONSTRAINT `tema_keterampilan_ibfk_1` FOREIGN KEY (`id_kd`) REFERENCES `kompetensi_dasar_keterampilan` (`id_kd_keterampilan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
