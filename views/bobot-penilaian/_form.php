<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Kelas;
use app\models\Sekolah;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $model app\models\BobotPenilaian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bobot-penilaian-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    
    if(Yii::$app->user->identity->role=="Administrator"){
    ?>
      <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?>
        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
    <?php } ?>
    <?= $form->field($model, 'bobot_nph')->textInput(['required' => 'required', 'type' => 'number']) ?>

    <?= $form->field($model, 'bobot_npts')->textInput(['required' => 'required', 'type' => 'number']) ?>

    <?= $form->field($model, 'bobot_npas')->textInput(['required' => 'required', 'type' => 'number']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
