<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BobotPenilaianSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bobot-penilaian-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_sekolah') ?>

    <?= $form->field($model, 'bobot_nph') ?>

    <?= $form->field($model, 'bobot_npts') ?>

    <?= $form->field($model, 'bobot_npas') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
