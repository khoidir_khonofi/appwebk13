<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BobotPenilaian */

$this->title = 'Create Bobot Penilaian';
$this->params['breadcrumbs'][] = ['label' => 'Bobot Penilaians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bobot-penilaian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
