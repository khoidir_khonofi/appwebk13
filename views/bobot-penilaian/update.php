<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BobotPenilaian */

$this->title = 'Update Bobot Penilaian: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bobot Penilaians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bobot-penilaian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
