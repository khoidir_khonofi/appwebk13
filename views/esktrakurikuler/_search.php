<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EsktrakurikulerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="esktrakurikuler-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_ekstrakurikuler') ?>

    <?= $form->field($model, 'id_sekolah') ?>

    <?= $form->field($model, 'id_siswa') ?>

    <?= $form->field($model, 'kegiatan_ekstrakurikuler') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
