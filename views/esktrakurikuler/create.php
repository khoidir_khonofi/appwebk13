<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Esktrakurikuler */

$this->title = 'Create Esktrakurikuler';
$this->params['breadcrumbs'][] = ['label' => 'Esktrakurikulers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="esktrakurikuler-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
