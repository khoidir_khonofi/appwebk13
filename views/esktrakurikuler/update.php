<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Esktrakurikuler */

$this->title = 'Update Esktrakurikuler: ' . $model->id_ekstrakurikuler;
$this->params['breadcrumbs'][] = ['label' => 'Esktrakurikulers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ekstrakurikuler, 'url' => ['view', 'id' => $model->id_ekstrakurikuler]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="esktrakurikuler-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
