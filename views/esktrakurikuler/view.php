<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Esktrakurikuler */

$this->title = $model->id_ekstrakurikuler;
$this->params['breadcrumbs'][] = ['label' => 'Esktrakurikulers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="esktrakurikuler-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_ekstrakurikuler',
            'id_sekolah',
            'id_siswa',
            'kegiatan_ekstrakurikuler',
            'keterangan:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
