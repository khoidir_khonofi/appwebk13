<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use app\models\Sekolah;
use app\models\Users;
use app\models\Guru;
use app\models\MataPelajaran;

/* @var $this yii\web\View */
/* @var $model app\models\Guru */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guru-form">

    <?php $form = ActiveForm::begin(); ?>

    

         <?= $form->field($model, 'id_user')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Users::find()->where('role="Guru" && status="Aktif"')->all(),'id_user', 'username'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih User ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
      

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true, 'placeholder' => 'Nip']) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama'])?>

    <?= $form->field($model, 'alamat', [
        'hintType' => ActiveField::HINT_SPECIAL,
        'hintSettings' => ['placement' => 'right', 'onLabelClick' => true, 'onLabelHover' => false]
            ])->textArea([
                'id' => 'address-input', 
                'placeholder' => 'Enter address...', 
                'rows' => 2
            ])->hint('Enter address in 4 lines. First 2 lines must contain the street details and next 2 lines the city, zip, and country detail.')
    ?>

     <?= $form->field($model, 'jenis_kelamin')->widget(Select2::classname(),[
            'data' => [ 'Laki-Laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Jenis Kelamin ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
    


    <?= $form->field($model, 'jabatan')->textInput(['placeholder' => 'Jabatan']) ?>
     <?= $form->field($model, 'fotoguru')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
        ]); ?> 
    
    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
