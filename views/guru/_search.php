<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GuruSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guru-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'nip')?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'nama') ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'alamat') ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'jabatan') ?>
    </div>
    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
