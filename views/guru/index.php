<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guru';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    <?php if(Yii::$app->user->identity->role=="Administrator"){ ?>
            <p>
            <button class="btn btn-info" onClick="return alert('Tambah guru melalui menu user')">Tambah Guru</button>
                    </p>
        <?php }else{ ?>
                <p>.</p>
         <?php } ?>
   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->foto != NULL) {
                         return Html::img('foto_guru/'.$model->foto, [
                        'width' => '50px', 
                        'height' => '50px',
                        'value' => Url::to([
                            'guru/lihatfoto',
                            'id' => $model->id_guru
                        ]),
                        'title' => '',
                        'class' => 'showModalButton',
                        'style' => 'border-radius:100%;'
                    ]);
                    }else{
                        return "";
                    }
                   
                }
            ],
           
            'nip',
            [
                'attribute' => 'nama',
                'label' => 'Nama Lengkap',
                'format' => 'html',
                'value' => function($model){
                   // $guru = app\models\User::find()->where(['id_user' => $model->id_user])->one(['id_user']);
                    // $url = Yii::$app->urlManager->createUrl([''])
                    return "<span style='text-transform:capitalize;'>".$model->nama."</span>";
                }
            ],
           // 'alamat:html',
            'jenis_kelamin',
            'jabatan',
             
            [
                'attribute' => 'nama',
                'label' => 'Jumlah Siswa',
                'format' => 'html',
                'value' => function($model){
                   $siswa = Siswa::find()->where(['id_guru' => $model->id_guru])->all();
                   return count($siswa);
                }
            ],

            
            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                        if(Yii::$app->user->identity->role=="Administrator"){
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_guru]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_guru]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_guru], [
                            'class' => 'btn btn-danger',
                            'title' => 'Delete',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                            
                        ]) ;
                            return $a." ".$b." ".$c;

                        }elseif(Yii::$app->user->identity->role=="Guru"){
                            $view = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_guru]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $update = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_guru]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            return $view." ".$update;
                        }else{
                            $lihat = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_guru]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            return $lihat;
                        } 
                    }
                ],
                

        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
       
        Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari Guru'
        ]),
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>

<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>