<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\Guru;
use app\models\Siswa;
/* @var $this yii\web\View */
/* @var $model app\models\Guru */

$this->title = $model->id_guru;
$this->params['breadcrumbs'][] = ['label' => 'Gurus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="guru-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_guru',
            [
                'attribute' => 'id_user',
                'label' => 'Username',
                'format' => 'html',
                'value' => function($model){
                    $user = User::find()->where(['id_user' => $model->id_user])->one()['id_user'];
                    $link = Yii::$app->urlManager->createUrl(['users/lihat', 'id' => $user]);
                    return '<a href="'.$link.'>'.$model->idUser->username.'</a>';
                   // return '<a href="'.$url.'">'.$model->idServices->jenis_service.'</a>';
                }
            ],
            'id_user',
            'id_sekolah',
             [
                'attribute' => 'nama',
                'label' => 'Jumlah Siswa',
                'format' => 'html',
                'value' => function($model){
                   $siswa = Siswa::find()->where(['id_guru' => $model->id_guru])->all();
                   return count($siswa);
                }
            ],
            [
                'attribute' => 'id_sekolah',
                'label' => 'Nama Sekolah',
                'value' => function($model){
                    return $model->tblsekolah->nama_sekolah;
                }
            ],
            'nip',
            'nama',
            [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                   return Html::img('foto_guru/'.$model->foto, ['width' => '50px', 'height' => '50px']);
                }
            ],
            'alamat:ntext',
            'jenis_kelamin',
            'jabatan',
        ],
    ]) ?>

</div>
