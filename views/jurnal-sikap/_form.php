<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\JurnalSikap;
use app\models\Sekolah;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
/* @var $this yii\web\View */
/* @var $model app\models\JurnalSikap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jurnal-sikap-form">

    <?php $form = ActiveForm::begin();?>

    <?php   if(Yii::$app->user->identity->role=="Administrator"){ 
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
    <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
                    'onchange'=>'
                       $.post( "index.php?r=jurnal-sikap/siswa&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...','id' => 'smt',
                    'onchange'=>'
                       $.post( "index.php?r=jurnal-sikap/semester&id='.'"+$(this).val(), function( data ){
                       $("select#x").html(data);
                      });'
                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                     <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...','id' => 'x' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?>

     <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
                    'onchange'=>'
                       $.post( "index.php?r=jurnal-sikap/siswa&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_guru' => $guru->id_guru])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ,'id' => 'smt',
                    'onchange'=>'
                       $.post( "index.php?r=jurnal-sikap/semester&id='.'"+$(this).val(), function( data ){
                       $("select#x").html(data);
                      });'
                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                     <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...','id' => 'x' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php } ?>

     <?= $form->field($model, 'sikap')->widget(Select2::classname(),[
            'data' => [ 
                'Sosial' => 'Sosial', 
                'Spiritual' => 'Spiritual',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Sikap ...',
                    'onchange'=>'
                       $.post( "index.php?r=jurnal-sikap/sikap&sikap='.'"+$(this).val(), function( data ){
                       $("select#sikapselect").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

        <?= $form->field($model, 'butir_sikap')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(JurnalSikap::find()->all(),'butir_sikap', 'butir_sikap'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih ...','id' => 'sikapselect' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Butir Sikap') ?>
    


    <?= $form->field($model, 'catatan_perilaku')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'penilaian')->widget(Select2::classname(),[
            'data' => [ 'Sangat Baik' => 'Sangat Baik', 'Baik' => 'Baik', 'Perlu Bimbingan' => 'Perlu Bimbingan'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Penilaian ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Penilaian') ?>

    <?= $form->field($model, 'tindak_lanjut')->textarea(['rows' => 2]) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
