<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $model app\models\JurnalSikapSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jurnal-sikap-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<?php   if(Yii::$app->user->identity->role=="Administrator"){ 
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_guru' => $guru->id_guru])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

    <?php } ?>

     <?= $form->field($model, 'sikap')->widget(Select2::classname(),[
            'data' => [ 
                'Sosial' => 'Sosial', 
                'Spiritual' => 'Spiritual',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Sikap ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <?= $form->field($model, 'catatan_perilaku')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'butir_sikap')->textInput() ?>

    <?= $form->field($model, 'penilaian')->widget(Select2::classname(),[
            'data' => [ 'Sangat Baik' => 'Sangat Baik', 'Perlu Bimbingan' => 'Perlu Bimbingan'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Penilaian ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Penilaian') ?>

    <?= $form->field($model, 'tindak_lanjut')->textarea(['rows' => 2]) ?>

    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
