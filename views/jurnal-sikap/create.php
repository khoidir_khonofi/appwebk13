<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JurnalSikap */

$this->title = 'Create Jurnal Sikap';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Sikaps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-sikap-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
