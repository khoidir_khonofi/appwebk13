<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JurnalSikap */

$this->title = 'Update Jurnal Sikap: ' . $model->id_jurnal_sikap;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Sikaps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jurnal_sikap, 'url' => ['view', 'id' => $model->id_jurnal_sikap]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jurnal-sikap-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
