<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KdKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kd-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kelas')->textInput(['value' => 6]) ?>

    <?= $form->field($model, 'no_kd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'judul')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mata_pelajaran')->textInput(['value' => 'Pendidikan Agama Kristen dan BP']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
