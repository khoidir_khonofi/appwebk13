<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KdKeterampilanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kd-keterampilan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_kd') ?>

    <?= $form->field($model, 'kelas') ?>

    <?= $form->field($model, 'no_kd') ?>

    <?= $form->field($model, 'judul') ?>

    <?= $form->field($model, 'mata_pelajaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
