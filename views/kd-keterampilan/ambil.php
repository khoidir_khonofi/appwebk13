<?php


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\KdKeterampilan;
/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kompetensi-dasar-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>

    
   
     

     <?= $form->field($model, 'mata_pelajaran')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KdKeterampilan::find()->all(),'mata_pelajaran', 'mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         <?= $form->field($model, 'kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KdKeterampilan::find()->all(),'kelas', 'kelas'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
