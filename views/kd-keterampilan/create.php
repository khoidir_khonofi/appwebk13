<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KdKeterampilan */

$this->title = 'Create Kd Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Kd Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kd-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
