<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KdKeterampilan */

$this->title = 'Update Kd Keterampilan: ' . $model->id_kd;
$this->params['breadcrumbs'][] = ['label' => 'Kd Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kd, 'url' => ['view', 'id' => $model->id_kd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kd-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
