<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KdPengetahuan */

$this->title = 'Create Kd Pengetahuan';
$this->params['breadcrumbs'][] = ['label' => 'Kd Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kd-pengetahuan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
