<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KdPengetahuan */

$this->title = 'Update Kd Pengetahuan: ' . $model->id_kd;
$this->params['breadcrumbs'][] = ['label' => 'Kd Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kd, 'url' => ['view', 'id' => $model->id_kd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kd-pengetahuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
