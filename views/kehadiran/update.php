<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kehadiran */

$this->title = 'Update Kehadiran: ' . $model->id_kehadiran;
$this->params['breadcrumbs'][] = ['label' => 'Kehadirans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kehadiran, 'url' => ['view', 'id' => $model->id_kehadiran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kehadiran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
