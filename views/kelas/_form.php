<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin();?>
 
      
   <?php
    $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
   ?>


  <?= $form->field($model, 'id_guru')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Guru::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_guru', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Wali Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Wali Kelas') ?>

   <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['periode_aktif' => 'Aktif'])->all(),'id_semester', 'semester'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Semester ...',
                    'onchange'=>'
                       $.post( "index.php?r=kelas/semester&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Semester') ?>

         <?= $form->field($model, 'tahun_ajaran')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['periode_aktif' => 'Aktif'])->all(),'tahun_ajaran', 'tahun_ajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Tahun Ajaran ...', 'id' => 'smt' ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Tahun Ajaran') ?>


   <?php if ($sekolah->jenjang_pendidikan == "SD") { ?>
       <?= $form->field($model, 'tingkat_kelas')->widget(Select2::classname(),[
            'data' => [ '1' => '1','2' => '2', '3' => '3', '4' => '4', '5' => '5' ,'6' => '6'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
  <?php } elseif ($sekolah->jenjang_pendidikan == "SMP") { ?>
        <?= $form->field($model, 'tingkat_kelas')->widget(Select2::classname(),[
            'data' => [ '7' => 'VII','8' => 'VIII', '9' => 'IX'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
  <?php }else{ ?>
        <?= $form->field($model, 'tingkat_kelas')->widget(Select2::classname(),[
            'data' => [ '10' => 'X','11' => 'XI', '12' => 'XII'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
  <?php } ?>


    <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 'Aktif' => 'Aktif','Tidak Aktif' => 'Tidak Aktif'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true])->label('Nama Kelas') ?>

  
   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
