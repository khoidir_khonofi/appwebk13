<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */

$this->title = $model->id_kelas;
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kelas-view">

    <h1><?= Html::encode($this->title) ?></h1>

 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_sekolah',
            'id_kelas',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->guru->nama;
                }
            ],
           [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            
            'tingkat_kelas',
            'nama',
            'tahun_ajaran',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
