<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Guru;
use app\models\Kelas;
use app\models\Sekolah;

/* @var $this yii\web\View */
/* @var $model app\models\KkmSatuanPendidikan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kkm-satuan-pendidikan-form">

    <?php $form = ActiveForm::begin(); ?>
<?php
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    
?>
    
    <?php   if(Yii::$app->user->identity->role=="Administrator"){ ?>

     <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
        ?>
        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_kelas' => $kelas->id_kelas])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
    <?php } ?>

    <?= $form->field($model, 'kkm')->textInput() ?>

     <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 
                'Aktif' => 'Aktif', 
                'Tidak Aktif' => 'Tidak Aktif',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
