<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KkmSatuanPendidikan */

$this->title = 'Create Kkm Satuan Pendidikan';
$this->params['breadcrumbs'][] = ['label' => 'Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kkm-satuan-pendidikan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
