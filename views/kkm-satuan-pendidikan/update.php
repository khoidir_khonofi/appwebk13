<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KkmSatuanPendidikan */

$this->title = 'Update Kkm Satuan Pendidikan: ' . $model->id_kkm;
$this->params['breadcrumbs'][] = ['label' => 'Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kkm, 'url' => ['view', 'id' => $model->id_kkm]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kkm-satuan-pendidikan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
