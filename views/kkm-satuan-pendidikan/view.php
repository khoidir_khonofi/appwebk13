<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KkmSatuanPendidikan */

$this->title = $model->id_kkm;
$this->params['breadcrumbs'][] = ['label' => 'Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kkm-satuan-pendidikan-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kkm',
            'id_sekolah',
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas. " ".$model->kelas->nama;
                }
            ],
            'kkm',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
