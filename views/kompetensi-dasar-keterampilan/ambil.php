<?php


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\KdKeterampilan;
use app\models\KompetensiDasarKeterampilan;
use app\models\MataPelajaran;
use app\models\Kelas;
use app\models\Sekolah;
use app\models\Guru;
/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kompetensi-dasar-keterampilan-form">

    <?php $form = ActiveForm::begin();
    $sekolah = Sekolah::find()->where(['id_user' =>Yii::$app->user->identity->id_user])->one();
    
    if(Yii::$app->user->identity->role=="Administrator"){
     ?>
 <?= $form->field($kd, 'id_kelas')->widget(Select2::classname(),[
            'data' =>  ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
            'onchange'=>'
                           $.post( "index.php?r=kompetensi-dasar-keterampilan/mapel&id='.'"+$(this).val(), function( data ){
                           $("select#mapel").html(data);
                          });'
                        ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

     <?= $form->field($kd, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

        

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' =>Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
        ?>
        <?= $form->field($kd, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $kelas->id_guru])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
            'onchange'=>'
                           $.post( "index.php?r=kompetensi-dasar-keterampilan/mapel&id='.'"+$(this).val(), function( data ){
                           $("select#mapel").html(data);
                          });'
                        ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>
         <?= $form->field($kd, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...','id' => 'mapel'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         
    <?php } ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
