<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */

$this->title = 'Create Kompetensi Dasar Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kompetensi-dasar-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
