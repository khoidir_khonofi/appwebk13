<?php


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kompetensi-dasar-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>

  

    <?= $form->field($model, 'no_kd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'judul')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'kkm')->textInput(['maxlength' => true, 'type' => 'number']) ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
