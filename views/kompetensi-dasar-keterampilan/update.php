<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */

$this->title = 'Update Kompetensi Dasar Keterampilan: ' . $model->id_kd_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kd_keterampilan, 'url' => ['view', 'id' => $model->id_kd_keterampilan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kompetensi-dasar-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
