<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarKeterampilan */

$this->title = $model->id_kd_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kompetensi-dasar-keterampilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kd_keterampilan',
            'id_sekolah',
            'id_kelas',
            'id_mapel',
            'no_kd',
            'judul:ntext',
            'kkm',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
