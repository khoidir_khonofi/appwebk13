<?php


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarPengetahuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kompetensi-dasar-pengetahuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    
<?php
        $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
        if (Yii::$app->user->identity->role=="Administrator") {
    ?>

    


      <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>
        <?php }else{ 
             $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
             $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            ?>

      <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>
        <?php } ?>

    <?= $form->field($model, 'no_kd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'judul')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'kkm')->textInput(['maxlength' => true]) ?>


    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
