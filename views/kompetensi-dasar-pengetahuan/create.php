<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarPengetahuan */

$this->title = 'Create Kompetensi Dasar Pengetahuan';
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kompetensi-dasar-pengetahuan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
