<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\KompetensiDasarKeterampilan;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KD Pengetahuan';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    <?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){ ?>
            <p>
                <?= Html::button('Tambah KD Pengetahuan',
                            ['value' => Url::to(['create']),
                            'title' => 'Bisa Melalui Menu Mata Pelajaran', 'class' => 'showModalButton btn btn-info']); ?>

                <?= Html::button('Ambil Referensi KD',[
                        'value' => Url::to(['ambil']), 
                        'class' => 'showModalButton btn btn-warning',
                        'title' => ''
                    ]); ?>

                <?php 
                if(Yii::$app->user->identity->role=="Administrator"){
                    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $kelas = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->one();
                    $kd = KompetensiDasarKeterampilan::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                }else{
                     $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                    $kd = KompetensiDasarKeterampilan::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                }
               ?>
                <?= Html::button('Lihat Nama KD',[
                    'value' => Url::to(['lihatkade', 'id' => $kd->kelas->tingkat_kelas]), 
                    'class' => 'showModalButton btn btn-success',
                    'title' => ''
                ]); 
                ?>
            </p>
        <?php }else{ ?>
                <p>.</p>
         <?php } ?>
   
<?php

$form = ActiveForm::begin();

    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id_kd_pengetahuan',
            //'id_sekolah',
           
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
             
            'no_kd',
           // 'judul:ntext',
            'kkm',
           
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_kd_pengetahuan]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_kd_pengetahuan]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_kd_pengetahuan], [
                            'class' => 'btn btn-danger',
                            'title' => 'Delete',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                            
                            ]) ;
                            $d = Html::button('+ Tema',
                                ['value' => Url::to(['tema/tambah','id'=>$model->id_kd_pengetahuan]),
                                'title' => '', 'class' => 'showModalButton btn btn-primary']);
                            $e = Html::a('<span class="fa fa-eye"></span> Tema',
                                ['tema/lihat','id'=>$model->id_kd_pengetahuan],[
                                'title' => '', 'class' => 'btn btn-info']);
                            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

                            if (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SD") {
                                return $a." ".$b." ".$c." ".$d." ".$e;
                            }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SD") {
                                return $a." ".$b." ".$c." ".$d." ".$e;
                            }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMP") {
                                return $a." ".$b." ".$c;
                            }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMP") {
                                return $a." ".$b." ".$c;
                            }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMA") {
                                return $a." ".$b." ".$c;
                            }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMA") {
                                return $a." ".$b." ".$c;
                            }else{
                                return $a;
                               
                            

                        
                    }
                            }
                ],
                

        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'name' => 'hapus', 
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
       
        Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari KD'
        ]). ' '.

        Html::a('<span class="fa fa-trash"> Delete All</span>',['hapus'],['class' => 'btn btn-default', 'data-confirm' => 'Are you sure you want to delete this item?', 'data-method' => 'POST', 'style' => 'margin-left:5px;']). ''.
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px;'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);

ActiveForm::end();
?>
</div>
</div>
</div>
</div>
</section>

<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>