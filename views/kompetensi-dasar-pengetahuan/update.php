<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarPengetahuan */

$this->title = 'Update Kompetensi Dasar Pengetahuan: ' . $model->id_kd_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kd_pengetahuan, 'url' => ['view', 'id' => $model->id_kd_pengetahuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kompetensi-dasar-pengetahuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
