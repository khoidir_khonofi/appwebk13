<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KompetensiDasarPengetahuan */

$this->title = $model->id_kd_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Kompetensi Dasar Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kompetensi-dasar-pengetahuan-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kd_pengetahuan',
            'id_sekolah',
          
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            'no_kd',
            'judul:ntext',
            'kkm',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
