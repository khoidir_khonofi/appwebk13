<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KondisiKesehatan */

$this->title = 'Create Kondisi Kesehatan';
$this->params['breadcrumbs'][] = ['label' => 'Kondisi Kesehatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kondisi-kesehatan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
