<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KondisiKesehatan */

$this->title = 'Update Kondisi Kesehatan: ' . $model->id_kondisi;
$this->params['breadcrumbs'][] = ['label' => 'Kondisi Kesehatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kondisi, 'url' => ['view', 'id' => $model->id_kondisi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kondisi-kesehatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
