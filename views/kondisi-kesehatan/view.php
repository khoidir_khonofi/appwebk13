<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KondisiKesehatan */

$this->title = $model->id_kondisi;
$this->params['breadcrumbs'][] = ['label' => 'Kondisi Kesehatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kondisi-kesehatan-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kondisi',
            'id_sekolah',
            'id_siswa',
            'aspek_fisik',
            'keterangan:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
