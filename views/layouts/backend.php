<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Siswa;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Users;
use app\assets\BackendAsset;
BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="Aset/dist/js/jquery-2.2.1.min.js"></script>
    <?php $this->head() ?>
</head>
<style>
  #test:hover{
      color : red;
  }
</style>

<script>
$(document).ready(function(){
$(".preloader").fadeOut();
})
</script>
<body>
<?php $this->beginBody() ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo" style="background: linear-gradient(to left, #19878c, #3d4b5d)">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>13</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" id="test"><b>SMART - </b>K13</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background: linear-gradient(to left, #1bcaff, #4e4e4e); ">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->

          <?php
          
          if (Yii::$app->user->identity->role=="Administrator") {
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $model = Users::find()->joinWith('guru')->where(['guru.id_sekolah' => $sekolah->id_sekolah])->andWhere(['status' => 'Tidak Aktif'])->all(); 
          ?>
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?= count($model) ?></span>
            </a>
            <ul class="dropdown-menu">
              <?php if (count($model) > 0) { ?>
                <li class="header">You have <?= count($model) ?> notifications</li>
              <?php }else{ ?>
                <li class="header">You dont have notifications</li>
              <?php } ?>
              
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="<?= Yii::$app->urlManager->createUrl(['users']) ?>">
                      <i class="fa fa-users text-aqua"></i> <?= count($model) ?> new members joined
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <?php }elseif (Yii::$app->user->identity->role=="Guru") { 
             $guru = Guru::findOne(['id_user' => Yii::$app->user->identity->id_user]);
             $model = Users::find()->joinWith('siswa')->where(['siswa.id_guru' => $guru->id_guru])->AndWhere(['user.status' => 'Tidak Aktif'])->all();   
          ?>
              <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?= count($model) ?></span>
            </a>
            <ul class="dropdown-menu">
            <?php if (count($model) > 0) { ?>
                <li class="header">You have <?= count($model) ?> notifications</li>
              <?php }else{ ?>
                <li class="header">You dont have notifications</li>
              <?php } ?>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="<?= Yii::$app->urlManager->createUrl(['users']) ?>">
                      <i class="fa fa-users text-aqua"></i> <?= count($model) ?> new members joined
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <?php } ?>
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <?php
          $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
          $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
          $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
          ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php 
          

          if(Yii::$app->user->identity->role=="Administrator"){ 

            if($sekolah->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive user-image" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_sekolah/<?= $sekolah->foto ?>" class="profile-user-img img-responsive user-image" alt="User Image">
            <?php } ?>

           
            <?php }elseif(Yii::$app->user->identity->role=="Guru"){

            if($guru->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive user-image" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_guru/<?= $guru->foto ?>" class="profile-user-img img-responsive user-image" alt="User Image" title="<?= $guru->nama ?>">
            <?php } ?>

            <?php }else{

            if($siswa->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive user-image" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_siswa/<?= $siswa->foto ?>" class="profile-user-img img-responsive user-image" alt="User Image" title="<?= $siswa->nama ?>">
            <?php } ?>

        <?php }?>
              <span class="hidden-xs" style="text-transform: capitalize;"><?= Yii::$app->user->identity->username ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                 <?php 
          

          if(Yii::$app->user->identity->role=="Administrator"){ 

            if($sekolah->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_sekolah/<?= $sekolah->foto ?>" class="img-circle" alt="User Image">
            <?php } ?>

           
            <?php }elseif(Yii::$app->user->identity->role=="Guru"){

            if($guru->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_guru/<?= $guru->foto ?>" class="img-circle" alt="User Image" title="<?= $guru->nama ?>">
            <?php } ?>

            <?php }else{

            if($siswa->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_siswa/<?= $siswa->foto ?>" class="img-circle" alt="User Image" title="<?= $siswa->nama ?>">
            <?php } ?>

        <?php }?>

                <p style="text-transform: capitalize;">
                 <?= Yii::$app->user->identity->username ?>
                  <small>Member since <?= Yii::$app->user->identity->created_at ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?= Yii::$app->urlManager->createUrl(['site/logout']) ?>" onclick = "return confirm('Are you sure to Logout?')" class="btn btn-info btn-flat" >Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="<?= Yii::$app->urlManager->createUrl(['site/logout']) ?>"  onclick="return confirm('Are you sure to logout?');"><i class="glyphicon glyphicon-log-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div value="<?= Url::to(['site/profil']) ?>" title="profil" class="pull-left image  showModalButton" >
          <?php 
          $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
          $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
          $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

          if(Yii::$app->user->identity->role=="Administrator"){ 

            if($sekolah->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_sekolah/<?= $sekolah->foto ?>" class="profile-user-img img-responsive img-circle" alt="User Image">
            <?php } ?>

           
            <?php }elseif(Yii::$app->user->identity->role=="Guru"){

            if($guru->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_guru/<?= $guru->foto ?>" class="profile-user-img img-responsive img-circle" alt="User Image" title="<?= $guru->nama ?>">
            <?php } ?>

            <?php }else{

            if($siswa->foto==NULL){ ?>
              <img src="Aset/dist/img/user2-160x160.jpg" class="profile-user-img img-responsive img-circle" alt="User Image" >
            <?php }else{ ?>
               <img src="foto_siswa/<?= $siswa->foto ?>" class="profile-user-img img-responsive img-circle" alt="User Image" title="<?= $siswa->nama ?>">
            <?php } ?>

        <?php }?>
        </div>
        <div class="pull-left info">
          <p style="text-transform: capitalize;"><?= Yii::$app->user->identity->username ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::$app->user->identity->role ?></a>
        </div>
      </div>

      

      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="index.php"><i class="fa fa-home"></i> <span>Home</span></a></li>
<?php if(Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SD"){ ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['semester'])?>"><i class="fa fa-circle-o"></i> Semester</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
           
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Data Penilaian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['tema'])?>"><i class="fa fa-edit"></i> <span> Tema Pengetahuan</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['tema-keterampilan'])?>"><i class="fa fa-edit"></i> <span> Tema Keterampilan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['subtema'])?>"><i class="fa fa-edit"></i> <span> Subtema Pengetahuan</span></a></li>
          

          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span>Grafik</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['pengumuman'])?>"><i class="fa fa-edit"></i> <span> Pengumuman</span></a></li>


<?php }elseif(Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SD"){ ?>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Data Master</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                  
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                 
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

               <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
               <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Data Penilaian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['tema'])?>"><i class="fa fa-edit"></i> <span> Tema Pengetahuan</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['tema-keterampilan'])?>"><i class="fa fa-edit"></i> <span> Tema Keterampilan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['subtema'])?>"><i class="fa fa-edit"></i> <span> Subtema Pengetahuan</span></a></li>
         

          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span>Grafik</span></a></li>



<?php }elseif(Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMP"){ ?>
              <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['semester'])?>"><i class="fa fa-circle-o"></i> Semester</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
           
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan Akhir</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span>
          Grafik</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['pengumuman'])?>"><i class="fa fa-edit"></i> <span> Pengumuman</span></a></li>
<?php }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMP") { ?>
      <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Data Master</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                  
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                 
                </ul>
              </li>

               <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

               <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
               <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
             <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan Akhir</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span> Grafik</span></a></li>

<?php }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMA") { ?>
   <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['semester'])?>"><i class="fa fa-circle-o"></i> Semester</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan Akhir</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span> Grafik</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['pengumuman'])?>"><i class="fa fa-edit"></i> <span> Pengumuman</span></a></li>

<?php }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMA") { ?>

  <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Data Master</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                  
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Siswa</a></li>
                   <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
                 
                </ul>
              </li>
               <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

               <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mengajar'])?>"><i class="fa fa-edit"></i> <span>Mengajar</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['bobot-penilaian'])?>"><i class="fa fa-table"></i> <span>Bobot Penilaian</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
             <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan Akhir</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
       <li><a href="<?= Yii::$app->urlManager->createUrl(['rangking'])?>"><i class="fa fa-edit"></i> <span> Rangking</span></a></li>

          <li><a href="<?= Yii::$app->urlManager->createUrl(['telegram'])?>"><i class="fa fa-send"></i> <span> Telegram</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span> Grafik</span></a></li>

<?php }elseif(Yii::$app->user->identity->role=="Siswa" AND $siswa->tblsekolah->jenjang_pendidikan=="SD"){ ?>

     <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
             <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
           
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sd-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span>  Grafik</span></a></li>


<?php }elseif (Yii::$app->user->identity->role=="Siswa" AND $siswa->tblsekolah->jenjang_pendidikan=="SMP") { ?>
  <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
             <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
           
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['smp-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span> Grafik</span></a></li>
<?php }else { ?>
  <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Data Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['guru'])?>"><i class="fa fa-circle-o"></i> Guru</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['users'])?>"><i class="fa fa-circle-o"></i> User</a></li>
                
                 <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa'])?>"><i class="fa fa-circle-o"></i> Profil</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kelas'])?>"><i class="fa fa-circle-o"></i> Kelas</a></li>
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Rombongan Belajar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class=""><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar'])?>"><i class="fa fa-circle-o"></i> Naik Kelas</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['rombongan-belajar/sudahlulus'])?>"><i class="fa fa-circle-o"></i> Lulus</a></li>
                  
                 
                </ul>
              </li>

             <li><a href="<?= Yii::$app->urlManager->createUrl(['sekolah'])?>"><i class="fa fa-pie-chart"></i> <span>Sekolah</span></a></li>
             <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['mata-pelajaran'])?>"><i class="fa fa-table"></i> <span>Mata Pelajaran</span></a></li>
           
            <li><a href="<?= Yii::$app->urlManager->createUrl(['kkm-satuan-pendidikan'])?>"><i class="fa fa-table"></i> <span>KKM Satuan Pendidikan</span></a></li>

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-pie-chart"></i>
                  <span>Kompetensi Dasar</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-pengetahuan'])?>"><i class="fa fa-circle-o"></i> KD Pengetahuan</a></li>
                  <li><a href="<?= Yii::$app->urlManager->createUrl(['kompetensi-dasar-keterampilan'])?>"><i class="fa fa-circle-o"></i> KD Keterampilan</a></li>
                  
                </ul>
            </li>

       

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Perkembangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
          <li><a href="<?= Yii::$app->urlManager->createUrl(['prestasi'])?>"><i class="fa fa-edit"></i> <span> Prestasi</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['kehadiran'])?>"><i class="fa fa-edit"></i> <span> Kehadiran</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['kondisi-kesehatan'])?>"><i class="fa fa-edit"></i> <span> Kondisi kesehatan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['esktrakurikuler'])?>"><i class="fa fa-edit"></i> <span> Ekstrakurikuler</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['tinggi-badan'])?>"><i class="fa fa-edit"></i> <span> Tinggi Badan</span></a></li>
           <li><a href="<?= Yii::$app->urlManager->createUrl(['saran'])?>"><i class="fa fa-edit"></i> <span> Saran</span></a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Penilaian Sikap</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-sosial'])?>"><i class="fa fa-circle-o"></i> Sikap Sosial</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-spiritual'])?>"><i class="fa fa-circle-o"></i> Sikap Spiritual</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sikap-akhir'])?>"><i class="fa fa-circle-o"></i> Sikap Akhir</a></li>
          </ul>
        </li>

        <li><a href="<?= Yii::$app->urlManager->createUrl(['jurnal-sikap'])?>"><i class="fa fa-table"></i> <span>Jurnal Sikap</span></a></li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai Harian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['sma-nilai-harian-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Nilai KD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-keterampilan'])?>"><i class="fa fa-circle-o"></i> Nilai Keterampilan</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan'])?>"><i class="fa fa-circle-o"></i> Nilai Pengetahuan</a></li>
          </ul>
        </li>

       <li><a href="<?= Yii::$app->urlManager->createUrl(['nilai-akhir'])?>"><i class="fa fa-edit"></i> <span> Nilai Akhir</span></a></li>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['siswa/rekapnilai'])?>"><i class="fa fa-edit"></i> <span> Grafik</span></a></li>
<?php } ?>

          

            

       
        <li class="header">LABELS</li>
        <?php if(!Yii::$app->user->isGuest){ ?>
          <li><a href="<?= Yii::$app->urlManager->createUrl(['site/logout']) ?>" onclick = "return confirm('Are you sure want tol logout?')"><i class="fa fa-sign-out text-red"></i> <span>Logout(<?= Yii::$app->user->identity->username ?>)</span></a></li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

      <div class="preloader">
            <div class="loading">
              <img src="poi.gif" width="80">
              <p>Please wait...</p>
            </div>
      </div>
     <div class="content-wrapper" style="background: #eaf6f9;">

        <div class="row" style="padding-top: 5px; margin-right: 15px; margin-left: 15px;">
          <?= Breadcrumbs::widget([
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
          <?= Alert::widget() ?>
        </div>
        
      <?php 
        require_once('alert.php');
        echo $content 
      ?>

    </div>

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019-2020 <a href="#">Khoidir Khonofi</a>.</strong> All rights
    reserved.
  </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<style type="text/css">
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background-color: #fff;
}
.preloader .loading {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%,-50%);
  font: 14px arial;
}
</style>


<script type="text/javascript">
  $(function(){
//get the click of modal button to create / update item
//we get the button by class not by ID because you can only have one id on a page and you can
//have multiple classes therefore you can have multiple open modal buttons on a page all with or without
//the same link.
//we use on so the dom element can be called again if they are nested, otherwise when we load the content once it kills the dom element and wont let you load anther modal on click without a page refresh
$(document).on('click', '.showModalButton', function(){
//check if the modal is open. if it's open just reload content not whole modal
//also this allows you to nest buttons inside of modals to reload the content it is in
//the if else are intentionally separated instead of put into a function to get the
//button since it is using a class not an #id so there are many of them and we need
//to ensure we get the right button and content.
if ($('#modal').data('bs.modal').isShown) {
$('#modal').find('#modalContent')
.load($(this).attr('value'));
//dynamiclly set the header for the modal
document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
} else {
//if modal isn't open; open it and load content
$('#modal').modal('show')
.find('#modalContent')
.load($(this).attr('value'));
//dynamiclly set the header for the modal
document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
}
});
});
</script>


