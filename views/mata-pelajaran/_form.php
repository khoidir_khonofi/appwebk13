<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;


/* @var $this yii\web\View */
/* @var $model app\models\MataPelajaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mata-pelajaran-form">

    <?php $form = ActiveForm::begin(); ?>
<?php
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    
?>
    


    <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->groupBy(['tingkat_kelas'])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'tingkat_kelas'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
            'onchange'=>'
                           $.post( "index.php?r=mata-pelajaran/thnajaran&id='.'"+$(this).val(), function( data ){
                           $("select#tahun").html(data);
                          });'
                        ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

        <?= $form->field($model, 'tahun_ajaran')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'tahun_ajaran', 'tahun_ajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Tahun Ajaran ...', 'id' => 'tahun'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Tahun Ajaran') ?>

    <?= $form->field($model, 'nama_mata_pelajaran')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'kelompok')->widget(Select2::classname(),[
                'data' => [ 
                    'A' => 'A (Wajib)', 
                    'B' => 'B (Tidak Wajib)',
                    'C' => 'C (Peminatan)',
                ],
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih ...', 'required' => 'required'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ]) ?>

    <?php if($sekolah->jenjang_pendidikan =="SMA"){ ?>
        <?= $form->field($model, 'jurusan')->widget(Select2::classname(),[
                'data' => [ 
                    'IPA' => 'IPA', 
                    'IPS' => 'IPS',
                ],
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih ...', 'required' => 'required'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ]) ?>
    <?php  } ?>
   


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
