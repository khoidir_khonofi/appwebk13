<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MataPelajaran */

$this->title = $model->id_mapel;
$this->params['breadcrumbs'][] = ['label' => 'Mata Pelajarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mata-pelajaran-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_mapel',
            'id_sekolah',
            
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            'nama_mata_pelajaran',
            'kelompok',
            'kkm_pengetahuan',
            'kkm_keterampilan',
            'tahun_ajaran',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
<div class="kkm" style="width: 100%; height: 100px;">
    <div class="interval" style="margin-bottom: 15px; float: left; margin-left: 10px;">
        <p>Interval Pengetahuan = <?= $model->kkm_pengetahuan ?></p>
        <?php
        $interval = (100-$model->kkm_pengetahuan)/3;
            $a = 100-$interval;
            $b = $a-$interval;
            $c = $b-$interval;
            echo "<span class='label label-success'>A</span> = ".round($a) ." - 100</br>";
            echo "<span class='label label-info'>B</span> = ".round($b)." - ".round($a-1). "</br>";
            echo "<span class='label label-warning'>C</span> = ".round($c)." - ".round($b-1). "</br>";
            echo "<span class='label label-danger'>D</span> = <".$model->kkm_pengetahuan;
        ?>
    </div>

    <div class="interval" style="margin-bottom: 15px; float: left; margin-left: 140px;">
        <p>Interval Keterampilan = <?= $model->kkm_keterampilan?></p>
        <?php
        $interval = (100-$model->kkm_keterampilan)/3;
            $a = 100-$interval;
            $b = $a-$interval;
            $c = $b-$interval;
            echo "<span class='label label-success'>A</span> = ".round($a) ." - 100</br>";
            echo "<span class='label label-info'>B</span> = ".round($b)." - ".round($a-1). "</br>";
            echo "<span class='label label-warning'>C</span> = ".round($c)." - ".round($b-1). "</br>";
            echo "<span class='label label-danger'>D</span> = <".$model->kkm_keterampilan;
        ?>
    </div>
</div>
