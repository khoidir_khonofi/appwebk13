<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\RombonganBelajar;


/* @var $this yii\web\View */
/* @var $model app\models\Mengajar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mengajar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $user = User::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $sekolah = Sekolah::find()->where(['id_user' => $user->id_user])->one();
    ?>

        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
            'onchange'=>'
                           $.post( "index.php?r=mengajar/mapel&id='.'"+$(this).val(), function( data ){
                           $("select#mp").html(data);
                          });'
                        ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

         <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mp'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>


      <?= $form->field($model, 'id_guru')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Guru::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_guru', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Guru ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Guru') ?>
    <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 
                'Aktif' => 'Aktif', 
                'Tidak Aktif' => 'Tidak Aktif',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
     

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
