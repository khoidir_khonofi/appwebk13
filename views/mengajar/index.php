<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;
use app\models\Sekolah;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Mengajar';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
        <?php if(Yii::$app->user->identity->role=="Administrator"){ ?>
            <p>
            <?= Html::button('Tambah Mengajar',
                        ['value' => Url::to(['create']),
                        'title' => '', 'class' => 'showModalButton btn btn-info']); ?>
                    </p>
        <?php }else{ ?>
                <p>.</p>
         <?php } ?>
           
        <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,

     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id_mengajar',
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
             [
                'attribute' => 'id_guru',
                'label' => 'Guru',
                'value' => function($model){
                    return $model->guru->nama;
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->matapelajaran->nama_mata_pelajaran;
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'html',
                'value' => function($model){
                    if($model->status == "Aktif"){
                        return "<span class='label label-success'>".$model->status."</span>";
                    }else{
                        return "<span class='label label-danger'>".$model->status."</span>";
                    }
                   
                }
            ],
            //'tahun_ajaran',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                        
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_mengajar]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_mengajar]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_mengajar], [
                                    'class' => 'btn btn-danger',
                                    'title' => 'Delete',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                    
                                    ]) ;
                            $d =  Html::a('Lihat Siswa', ['lihatsiswa', 'id' => $model->id_kelas], ['class' => 'btn btn-info']);
                            $e =  '<button type="button" class="btn btn-default btn-block" style="width:100px;float:left; margin-right:5px;">Lihat Siswa</button>';
                            $sma = ButtonDropdown::widget([
                                'label' => 'Lihat Nilai Harian Siswa',
                                'dropdown' => [
                                    'items' => [
                                        ['label' => 'Pengetahuan',
                                            'url' => ['sma-nilai-harian-pengetahuan/lihatnilaisiswapeng','id'=>$model->id_kelas],
                                        ],
                                        ['label' => 'Keterampilan',
                                            'url' => ['sma-nilai-harian-keterampilan/lihatnilaisiswaktr','id'=>$model->id_kelas],
                                        ],
                                        ],
                                    ],
                                
                            ]);
                            $smp = ButtonDropdown::widget([
                                'label' => 'Lihat Nilai Harian Siswa',
                                'dropdown' => [
                                    'items' => [
                                        ['label' => 'Pengetahuan',
                                            'url' => ['smp-nilai-harian-pengetahuan/lihatnilaisiswapeng','id'=>$model->id_kelas],
                                        ],
                                        ['label' => 'Keterampilan',
                                            'url' => ['smp-nilai-harian-keterampilan/lihatnilaisiswaktr','id'=>$model->id_kelas],
                                        ],
                                        ],
                                    ],
                                
                            ]);
                            $sd = ButtonDropdown::widget([
                                'label' => 'Lihat Nilai Harian Siswa',
                                'dropdown' => [
                                    'items' => [
                                        ['label' => 'Pengetahuan',
                                            'url' => ['sd-nilai-harian-pengetahuan/lihatnilaisiswapeng','id'=>$model->id_kelas],
                                        ],
                                        ['label' => 'Keterampilan',
                                            'url' => ['sd-nilai-harian-keterampilan/lihatnilaisiswaktr','id'=>$model->id_kelas],
                                        ],
                                        ],
                                    ],
                                
                            ]);
                            $sikap =  ButtonDropdown::widget([
                                'label' => 'Lihat Sikap Siswa',
                                'dropdown' => [
                                    'items' => [
                                        ['label' => 'Sosial',
                                            'url' => ['sikap-sosial/lihatsikap','id'=> $model->id_kelas],
                                        ],
                                        ['label' => 'Spiritual',
                                            'url' => ['sikap-spiritual/lihatsikap','id'=> $model->id_kelas],
                                        ],
                                        ],
                                    ],
                                
                            ]);
                            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            if(Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan == "SMA"){
                                if($model->status=="Aktif"){
                                    return $a." ".$b." ".$c." ".$d." ".$sma." ".$sikap;
                                }else{
                                    return $a." ".$b." ".$c." ".$e;
                                }
                            }elseif(Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan == "SMA"){
                                if($model->status=="Aktif"){
                                    return $a." ".$d." ".$sma." ".$sikap;
                                }else{
                                    return $a."  ".$e;
                                }
                            }elseif(Yii::$app->user->identity->role=="Administrator" AND        $sekolah->jenjang_pendidikan == "SMP"){
                                if($model->status=="Aktif"){
                                    return $a." ".$b." ".$c." ".$d." ".$smp." ".$sikap;
                                }else{
                                    return $a." ".$b." ".$c." ".$e;
                                }
                            }elseif(Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan == "SMP"){
                                if($model->status=="Aktif"){
                                    return $a."  ".$d." ".$smp." ".$sikap;
                                }else{
                                    return $a."  ".$e;
                                }
                            }elseif (Yii::$app->user->identity->role=="Administrator" AND        $sekolah->jenjang_pendidikan == "SD") {
                                if($model->status=="Aktif"){
                                    return $a." ".$b." ".$c." ".$d." ".$sd." ".$sikap;
                                }else{
                                    return $a." ".$b." ".$c." ".$e;
                                }
                            }elseif(Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan == "SD"){
                                    if($model->status=="Aktif"){
                                        return $a."  ".$d." ".$sd." ".$sikap;
                                    }else{
                                        return $a."  ".$e;
                                    }
                                
                            }else{
                                if($model->status=="Aktif"){
                                    return $a."  ".$d;
                                }else{
                                    return $a."  ".$e;
                                }
                                
                            }
                            
                        
                            
                        }
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
          Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari']).' '.
         
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px;'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => false,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'resizableColumns'=>true,
    'resizeStorageKey'=>Yii::$app->user->identity->id_user . '-' . date("m"),
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>