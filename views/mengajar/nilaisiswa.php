<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Sekolah;
use app\models\Siswa;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penilaian Siswa';
$this->params['breadcrumbs'][] = $this->title;
?> 
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h1><?= Html::encode($this->title) ?></h1>
                      
 <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Pengetahuan</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Keterampilan</a></li>
                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
            <div class="tab-content">
    <div class="tab-pane active" id="tab_1">

   
    <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_siswa',
             [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->foto != NULL) {
                         return Html::img('foto_siswa/'.$model->foto, [
                        'width' => '50px', 
                        'height' => '50px',
                        'value' => Url::to([
                            'mengajar/lihatfoto',
                            'id' => $model->id_siswa
                        ]),
                        'title' => '',
                        'class' => 'showModalButton',
                        'style' => 'border-radius:50%'
                    ]);
                    }else{
                        return "";
                    }
                   
                }
            ],
             'nama',
              'nis',
            'jenis_kelamin',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->tblguru->nama;
                }
            ],
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],

           
           

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                      
                             $e = Html::button('<span class="fa fa-edit"></span> Nilai Pengetahuan',
                                ['value' => Url::to(['sd-nilai-harian-pengetahuan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-warning']);
                            $d = Html::button('<span class="fa fa-edit"></span> Sikap Sosial',
                                ['value' => Url::to(['sikap-sosial/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-primary']);
                            $f = Html::button('<span class="fa fa-edit"></span> Nilai Pengetahuan',
                                ['value' => Url::to(['smp-nilai-harian-pengetahuan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-warning']);
                            $g = Html::button('<span class="fa fa-edit"></span> Nilai Pengetahuan',
                                ['value' => Url::to(['sma-nilai-harian-pengetahuan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-warning']);


                           $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                           $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

                           if (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SD") {
                                    return $e." ".$d;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SD") {
                                    return $e." ".$d;
                           }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMP") {
                                    return $f." ".$d;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMP") {
                                    return $f." ".$d;
                           }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMA") {
                                    return $g." ".$d;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMA") {
                                    return $g." ".$d;
                           }else{
                                    return $a;
                           }
                         
                            
                        }
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
        'toolbar' =>  [
             ['content'=>
            //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
             ],
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
       // 'floatHeaderOptions' => ['top' => $scrollingTop],
        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO
        ],
    ]);
    ?>
    </div>
<div class="tab-pane" id="tab_2">
   <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_siswa',
             [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->foto != NULL) {
                         return Html::img('foto_siswa/'.$model->foto, [
                        'width' => '50px', 
                        'height' => '50px',
                        'value' => Url::to([
                            'mengajar/lihatfoto',
                            'id' => $model->id_siswa
                        ]),
                        'title' => '',
                        'class' => 'showModalButton',
                        'style' => 'border-radius:50%'
                    ]);
                    }else{
                        return "";
                    }
                   
                }
            ],
           
             'nama',
              'nis',
            'jenis_kelamin',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->tblguru->nama;
                }
            ],
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
              

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                      
                          
                            $d = Html::button('<span class="fa fa-edit"></span> Nilai Keterampilan',
                                ['value' => Url::to(['sd-nilai-harian-keterampilan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-primary']);
                            $e = Html::button('<span class="fa fa-edit"></span> Sikap Spiritual',
                                ['value' => Url::to(['sikap-spiritual/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-warning']);
                            $f = Html::button('<span class="fa fa-edit"></span> Nilai Keterampilan',
                                ['value' => Url::to(['smp-nilai-harian-keterampilan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-primary']);
                            $g = Html::button('<span class="fa fa-edit"></span> Nilai Keterampilan',
                                ['value' => Url::to(['sma-nilai-harian-keterampilan/create' ,'id' => $model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-primary']);
                         
                            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

                           if (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SD") {
                                    return $e. " ".$d;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SD") {
                                    return $e. " ".$d;
                           }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMP") {
                                    return $f." ".$e;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMP") {
                                    return $f." ".$e;
                           }elseif (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SMA") {
                                    return $g. " ".$e;
                           }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SMA") {
                                    return $g. " ".$e;
                           }else{
                                    return $a;
                           }
                        } 
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
        'toolbar' =>  [
             ['content'=>
            //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
             ],
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
       // 'floatHeaderOptions' => ['top' => $scrollingTop],
        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO
        ],
    ]);
    ?>
    </div>

</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>