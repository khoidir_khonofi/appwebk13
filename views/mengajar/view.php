<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mengajar */

$this->title = $model->id_mengajar;
$this->params['breadcrumbs'][] = ['label' => 'Mengajars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mengajar-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_mengajar',
           [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;;
                }
            ],
             [
                'attribute' => 'id_guru',
                'label' => 'Guru',
                'value' => function($model){
                    return $model->guru->nama;
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->matapelajaran->nama_mata_pelajaran;
                }
            ],
            'status',
            
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
