<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhirKkmSatuanPendidikan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-akhir-kkm-satuan-pendidikan-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'deskripsi_pengetahuan')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'deskripsi_keterampilan')->textarea(['rows' => 6]) ?>

    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
