<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhirKkmSatuanPendidikan */

$this->title = 'Create Nilai Akhir Kkm Satuan Pendidikan';
$this->params['breadcrumbs'][] = ['label' => 'Nilai Akhir Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-akhir-kkm-satuan-pendidikan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
