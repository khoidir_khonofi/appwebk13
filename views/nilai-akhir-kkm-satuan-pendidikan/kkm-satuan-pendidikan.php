<?php
use app\models\NilaiAkhirKkmSatuanPendidikan;
use app\models\KkmsatuanPendidikan;
use yii\helpers\Html;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Mengajar;
use app\models\Kelas;
$this->title="Nilai Akhir KKM Satuan Pendidikan";
?>
  <link rel="stylesheet" href="Aset/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="Aset/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<section class="content">
      <div class="row">
        <div class="col-xs-12">
  <div class="box">
            <div class="box-header">
              
              <?php 
              if(Yii::$app->user->identity->role=="Guru"){
                $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                $kkm = KkmsatuanPendidikan::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                  ?>
                  <h3 class="box-title">Data Nilai Akhir KKM Satuan Pendidikan</h3><br/>
                  <h3 class="box-title">KKM = <?= $kkm->kkm ?></h3>
              <?php }else{ ?>
                <h3 class="box-title">Data Nilai Akhir KKM Satuan Pendidikan</h3><br/>
              <?php } ?>
            </div>
             
            <!-- /.box-header -->
            <div class="box-body">
              <?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){ ?>
                    <div class="form-group" style="margin-bottom:10px;">
                        <?= Html::a('<span class="fa fa-print"></span> Cetak Rapor',['pilihsiswa'],
                              ['title' => '', 'class' => 'btn btn-success', 'target' => '_blank'
                        ]);?>
                        <?= Html::a('Deskripsi Manual',['index'],
                              ['title' => '', 'class' => 'btn btn-info', 'target' => '_blank'
                        ]);?>
                    </div>
              <?php } ?>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th style="background-color: #e0e6ea;">No</th>
                    <th style="background-color: #e0e6ea;">Mata Pelajaran</th>
                    <th style="background-color: #e0e6ea;">Nilai</th>
                    <th style="background-color: #e0e6ea;">Predikat</th>
                    <th style="background-color: #e0e6ea;">Deskripsi</th>
                    <th style="background-color: #e0e6ea;">Nilai</th>
                    <th style="background-color: #e0e6ea;">Predikat</th>
                    <th style="background-color: #e0e6ea;">Deskripsi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    if (Yii::$app->user->identity->role=="Administrator") {
                        $test = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all();
                    }else{
                        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                        $mengajar = Mengajar::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                        $test = NilaiAkhirKkmSatuanPendidikan::find()->where(['id_kelas' => $mengajar->id_kelas])->all();
                    }
                  
                  $no = 1; 
                  foreach ($test as $key => $value) { ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $value->mapel->nama_mata_pelajaran ?></td>
                      <td><?= $value->nilai_pengetahuan ?></td>
                      <td><?= $value->predikat_pengetahuan ?></td>
                      <td><?= $value->deskripsi_pengetahuan ?></td>
                      <td><?= $value->nilai_keterampilan ?></td>
                      <td><?= $value->predikat_keterampilan ?></td>
                      <td><?= $value->deskripsi_keterampilan ?></td>
                    </tr>
                 <?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script src="Aset/bower_components/jquery/dist/jquery.min.js"></script>
  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>