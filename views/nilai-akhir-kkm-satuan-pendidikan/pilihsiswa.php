<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Sekolah;
use app\models\MataPelajaran;
use yii\helpers\Url;
use app\models\Siswa;
use app\models\NilaiAkhir;
use app\models\Guru;
use app\models\Kelas;
use app\models\Mengajar;
/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Pilih Siswa';
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            
            <!-- /.box-header -->
            <div class="box-body">

    <?php $form = ActiveForm::begin([
        'action'=> ['printraportkkmsatuanpendidikan'],
        'method' => 'GET',
    ]);  
    
    
    if(Yii::$app->user->identity->role=="Administrator"){
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
        <label>Pilih Siswa</label>
        <?= Select2::widget([
            'name' => 'id_siswa',
            'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
            'disabled' => false
        ]);?>
        
    <?php }else{
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
        ?>
        <label>Pilih Siswa</label>
        <?= Select2::widget([
            'name' => 'id_siswa',
            'data' => ArrayHelper::map(Siswa::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_siswa', 'nama'),
            'disabled' => false
        ]);?>
    <?php } ?>
        
    <div class="form-group" style="float: right; margin-top: 5px;">
        <input type="submit" class="btn btn-success" value="Cetak">
    </div>
   <?php ActiveForm::end(); ?> 

</div>
</div>
</div>
</div>
</section>