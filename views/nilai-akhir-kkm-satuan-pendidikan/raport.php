<?php
use app\models\Sekolah;
use app\models\KkmSatuanPendidikan;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdKeterampilan;

$this->title="Raport KKM Satuan Pendidikan";
?>


<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h2 style="text-align: center;">Rapor Peserta Didik</h2>
<div class="page_break" style="page-break-after: always;">
	<table>
		<?php 
		$sekolah = Sekolah::find()->where(['id_sekolah' => $pesertadidik->id_sekolah])->all();
		foreach ($sekolah as $key => $nilai) { ?>
		<tr>
			<td>Nama Sekolah</td>
			<td>:</td>
			<td><?= $nilai['nama_sekolah'] ?></td>
		</tr>
		<tr>
			<td>NPSN</td>
			<td>:</td>
			<td><?= $nilai['npsn'] ?></td>
		</tr>
		<tr>
			<td>alamat</td>
			<td>:</td>
			<td><?= $nilai['alamat'] ?></td>
		</tr>
		<tr>
			<td>Kelurahan</td>
			<td>:</td>
			<td><?= $nilai['kelurahan'] ?></td>
		</tr>
		<tr>
			<td>Kecamatan</td>
			<td>:</td>
			<td><?= $nilai['kecamatan'] ?></td>
		</tr>
		<tr>
			<td>Kabupaten</td>
			<td>:</td>
			<td><?= $nilai['kabupaten'] ?></td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>:</td>
			<td><?= $nilai['provinsi'] ?></td>
		</tr>
		<tr>
			<td>Telepon</td>
			<td>:</td>
			<td><?= $nilai['telepon'] ?></td>
		</tr>
		<?php } ?>
	</table>
	</div>


	<div class="page_break" style="page-break-after: always;">
	<h3 style="text-align: center;">Identitas Peserta Didik</h3>
	<table style="font-size: 16px;" width="100%">
		<?php foreach ($siswa as $key => $val) { ?>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><?= $val['nama'] ?></td>
			</tr>
			<tr>
				<td>Nis</td>
				<td>:</td>
				<td><?= $val['nis'] ?></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><?= $val['tempat_lahir'] ?></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><?= $val['tanggal_lahir'] ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td><?= $val['jenis_kelamin'] ?></td>
			</tr>
			<tr>
				<td>Agama</td>
				<td>:</td>
				<td><?= $val['agama'] ?></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><?= $val['alamat'] ?></td>
			</tr>
			<tr>
				<td>Nama Ayah</td>
				<td>:</td>
				<td><?= $val['nama_ayah'] ?></td>
			</tr>
			<tr>
				<td>Nama Ibu</td>
				<td>:</td>
				<td><?= $val['nama_ibu'] ?></td>
			</tr>
			<tr>
				<td>Pekerjaan Ayah</td>
				<td>:</td>
				<td><?= $val['pekerjaan_ayah'] ?></td>
			</tr>
			<tr>
				<td>Pekerjaan Ibu</td>
				<td>:</td>
				<td><?= $val['pekerjaan_ibu'] ?></td>
			</tr>
			
		<?php }?>
	</table>
	<br><br><br><br>
	<div class="foto" style="float:right; width:400px;">
		<img src="foto_siswa/<?= $val['foto'] ?>" style="height: 150px; width:100px; float:left;">

		<div class="ttd" style="float: left;">
			<p style="text-align: right;">Kepala Sekolah</p>
			<p style="text-align: right;"><u><?= $pesertadidik->tblsekolah->nama_kepala_sekolah ?></u></p>
			<p style="text-align: right;">NIP : <?= $pesertadidik->tblsekolah->nip_kepsek ?></p>
		</div>
	</div>
</div>


<div class="page_break" style="page-break-after: always;">
	<h3 style="text-align: center; font-size: 18px;">RAPOR PESERTA DIDIK DAN PROFIL PESERTA DIDIK</h3>
	<table>
		<tr>
			<td>Nama Peserta Didik</td>
			<td>:</td>
			<td><?= $pesertadidik->nama ?></td>
			<td><td><td>
			<td>Kelas</td>
			<td>:</td>
			<td><?= $pesertadidik->kelas->tingkat_kelas ?></td>
		</tr>
		<tr>
			<td>NISN/NIS</td>
			<td>:</td>
			<td><?= $pesertadidik->nis ?></td>
			<td><td><td>
			<td>Tahun Ajaran</td>
			<td>:</td>
			<td><?= $pesertadidik->kelas->tahun_ajaran ?></td>
		</tr>
		<tr>
			<td>Nama Sekolah</td>
			<td>:</td>
			<td><?= $pesertadidik->tblsekolah->nama_sekolah ?></td>
			<td><td><td>
			<td>Semester</td>
			<td>:</td>
			<td><?= $pesertadidik->semester->semester ?></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>:</td>
			<td><?= $pesertadidik->tblsekolah->alamat ?></td>
		</tr>
	</table>

	<div class="sikap">
	<h4>A. Sikap</h4>
		<table border="1" cellspacing="0" width="100%">
			<tr>
				<th colspan="2" style="text-align: center; height: 40px; background-color: #e0e6ea;">Deskripsi</th>
			</tr>
			<?php foreach ($nilaisikap as $key => $sikap) {?>
				<tr>
					<td style="height:200px;">Sosial</td>
					<td style="height:200px;"><?= $sikap->keterangan_sosial ?></td>
				</tr>
				<tr>
					<td style="height:200px;">Spiritual</td>
					<td style="height:200px;"><?= $sikap->keterangan_spiritual ?></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>

<?php  if($nilai->jenjang_pendidikan == 'SD' || $nilai->jenjang_pendidikan == 'SMP') { ?>
<div class="nilai" style="page-break-after: always;">
	<h4>B. Pengetahuan dan Keterampilan</h4>
	<h5>KKM = <?= $kkm->kkm ?></h5>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th rowspan="2" style="text-align: center;">No</th>
			<th rowspan="2" style="text-align: center;">Mata Pelajaran</th>
			<th colspan="3" style="text-align: center;">Pengetahuan</th>
			<th colspan="3" style="text-align: center;">Keterampilan</th>
		</tr>
		<tr>
			<th style="text-align: center;">Nilai</th>
			<th style="text-align: center;">Predikat</th>
			<th style="text-align: center;">Deskripsi</th>
			<th style="text-align: center;">Nilai</th>
			<th style="text-align: center;">Predikat</th>
			<th style="text-align: center;">Deskripsi</th>
		</tr>
		<tr>
			<th colspan="8">Kelompok A</th>
		</tr>

		<?php 

			$no = 1; 
			foreach ($modelA as $key => $nilaia) { 
		?>
		
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $nilaia->mapel->nama_mata_pelajaran ?></td>
				<td style="text-align: center;"><?= $nilaia->nilai_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->predikat_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->deskripsi_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->nilai_keterampilan ?></td>
				<td style="text-align: center;"><?= $nilaia->predikat_keterampilan ?></td>
				<td><?= $nilaia->deskripsi_keterampilan ?></td>
			</tr>
		<?php } ?>

		<tr>
			<th colspan="8">Kelompok B</th>
		</tr>
		<?php 

			$no = 1; 
			foreach ($modelB as $key => $nilaib) { 
		?>
		
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $nilaib->mapel->nama_mata_pelajaran ?></td>
				<td style="text-align: center;"><?= $nilaib->nilai_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->predikat_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->deskripsi_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->nilai_keterampilan ?></td>
				<td style="text-align: center;"><?= $nilaib->predikat_keterampilan ?></td>
				<td><?= $nilaib->deskripsi_keterampilan ?></td>
			</tr>
		<?php } ?>
			
	</table>
</div>

<?php }else{ ?>
	<div class="nilai" style="page-break-after: always;">
	<h4>B. Pengetahuan dan Keterampilan</h4>
	<h5>KKM = <?= $kkm->kkm ?></h5>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th rowspan="2" style="text-align: center;">No</th>
			<th rowspan="2" style="text-align: center;">Mata Pelajaran</th>
			<th colspan="3" style="text-align: center;">Pengetahuan</th>
			<th colspan="3" style="text-align: center;">Keterampilan</th>
		</tr>
		<tr>
			<th style="text-align: center;">Nilai</th>
			<th style="text-align: center;">Predikat</th>
			<th style="text-align: center;">Deskripsi</th>
			<th style="text-align: center;">Nilai</th>
			<th style="text-align: center;">Predikat</th>
			<th style="text-align: center;">Deskripsi</th>
		</tr>
		<tr>
			<th colspan="8">Kelompok A</th>
		</tr>

		<?php 

			$no = 1; 
			foreach ($modelA as $key => $nilaia) { 
		?>
		
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $nilaia->mapel->nama_mata_pelajaran ?></td>
				<td style="text-align: center;"><?= $nilaia->nilai_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->predikat_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->deskripsi_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaia->nilai_keterampilan ?></td>
				<td style="text-align: center;"><?= $nilaia->predikat_keterampilan ?></td>
				<td><?= $nilaia->deskripsi_keterampilan ?></td>
			</tr>
		<?php } ?>

		<tr>
			<th colspan="8">Kelompok B</th>
		</tr>
		<?php 

			$no = 1; 
			foreach ($modelB as $key => $nilaib) { 
		?>
		
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $nilaib->mapel->nama_mata_pelajaran ?></td>
				<td style="text-align: center;"><?= $nilaib->nilai_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->predikat_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->deskripsi_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaib->nilai_keterampilan ?></td>
				<td style="text-align: center;"><?= $nilaib->predikat_keterampilan ?></td>
				<td><?= $nilaib->deskripsi_keterampilan ?></td>
			</tr>
		<?php } ?>

		<tr>
			<th colspan="8">Kelompok C</th>
		</tr>

		<?php 

			$no = 1; 
			foreach ($modelC as $key => $nilaic) { 
		?>
		
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $nilaic->mapel->nama_mata_pelajaran ?></td>
				<td style="text-align: center;"><?= $nilaic->nilai_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaic->predikat_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaic->deskripsi_pengetahuan ?></td>
				<td style="text-align: center;"><?= $nilaic->nilai_keterampilan ?></td>
				<td style="text-align: center;"><?= $nilaic->predikat_keterampilan ?></td>
				<td><?= $nilaic->deskripsi_keterampilan ?></td>
			</tr>
		<?php } ?>
			
	</table>
</div>
<?php } ?>

<div class="Ekstrakurikuler" style="page-break-after: always;">
	<h4>C. Ekstrakurikuler</h4>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">No</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Kegiatan Ekstrakurikuler</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Keterangan</th>
		</tr>
		<?php $no =1; foreach ($ekskul as $key => $ekstra) { ?>
			<tr>
				<td style="text-align: center; height: 35px;"><?= $no++ ?></td>
				<td style="height: 35px;"><?= $ekstra->kegiatan_ekstrakurikuler ?></td>
				<td style="height: 35px;"><?= $ekstra->keterangan ?></td>	
			</tr>
		<?php } ?>
	</table>
	
	<h4>D. Saran</h4>
	<table border="1" cellspacing="0" width="100%">
		<?php foreach ($saran as $key => $sar) { ?>
			<tr>
				<td><?= $sar->saran ?></td>
			</tr>
		<?php } ?>
	</table>

	<h4>E. Tinggi dan Berat Badan</h4>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">No</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Aspek Fisik</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Nilai</th>
		</tr>
		<?php $no=1; foreach ($tinggibadan as $key => $tinggi) { ?>
			<tr>
				<td style="text-align: center; height: 35px;"><?= $no++ ?></td>
				<td style="height: 35px;"><?= $tinggi->aspek_penilaian ?></td>
				<td style="height: 35px;"><?= $tinggi->nilai ?></td>
			</tr>
		<?php } ?>
	</table>

	<h4>F. Kondisi Kesehatan</h4>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">No</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Aspek Fisik</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Keterangan</th>
		</tr>
		<?php $no=1; foreach ($kondisikesehatan as $key => $kondisi) { ?>
			<tr>
				<td style="text-align: center; height: 35px;"><?= $no++ ?></td>
				<td style="height: 35px;"><?= $kondisi->aspek_fisik ?></td>
				<td style="height: 35px;"><?= $kondisi->keterangan ?></td>
			</tr>
		<?php } ?>
	</table>

	<h4>G . Prestasi</h4>
	<table border="1" cellspacing="0" width="100%">
		<tr>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">No</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Prestasi</th>
			<th style="text-align: center; height: 40px; background-color: #e0e6ea;">Keterangan</th>
		</tr>
		<?php $no=1; foreach ($prestasi as $key => $pres) { ?>
			<tr>
				<td style="text-align: center; height: 35px;"><?= $no++ ?></td>
				<td style="height: 35px;"><?= $pres->prestasi ?></td>
				<td style="height: 35px;"><?= $pres->keterangan ?></td>
			</tr>
		<?php } ?>
	</table>

	<h4>H. Ketidakhadiran</h4>
	<table border="1" cellspacing="0" width="100%">
		
		<?php foreach ($kehadiran as $key => $absen) { ?>
			<tr>
				<td style="height: 30px;"><?= $absen->alasan ?></td>
				<td style="height: 30px;"><?= $absen->keterangan ?></td>
			</tr>
		<?php } ?>
	</table>
	<br><br>
	<?php 
		if ($pesertadidik->semester->semester == 2) {?>
				Keputusan:<br>
				Berdasarkan pencapaian seluruh kompetensi,
				peserta didik dinyatakan:<br>
				Naik/Tinggal*) kelas ……… ( ………………… )<br>
				*) Coret yang tidak perlu

	<?php } ?>
<br><br><br><br><br>
	<div class="ket" style="width: 100%;">
		<div class="ortu" style="width: 33%; float: left; margin: 1px;">
			<p style="text-align: left;">Mengetahui:
			Orang Tua/ Wali</p>
			<br><br><br>
			<p style="text-align: left; text-transform: uppercase;"><b><?= $pesertadidik->nama_ayah ?></b></p>
		</div>
		<div class="s" style=" width: 33%; float: left; margin: 1px;">
			<p style="text-align: center;">Mengetahui:
			Kepala Sekolah</p>
			<br><br><br>
			<?php foreach ($sekolah as $key => $s) {?>
				<p style="text-align: center;"><u><b><?= $s->nama_kepala_sekolah ?></b></u><br><?= $s->nip_kepsek ?></p>
			<?php } ?>
		</div>
		<div class="guru" style=" width: 33%; float: left; margin: 1px;">
			<p style="text-align: right;">Sekolah:
			Wali Kelas</p>
			<br><br><br>
			<p style="text-align: right; text-transform: uppercase;"><u><b><?= $pesertadidik->tblguru->nama ?></b></u><br><?= $pesertadidik->tblguru->nip ?></p>
		</div>	
	</div>
</body>
</html>