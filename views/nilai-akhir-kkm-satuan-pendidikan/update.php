<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhirKkmSatuanPendidikan */

$this->title = 'Update Nilai Akhir Kkm Satuan Pendidikan: ' . $model->id_nilai_akhir;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Akhir Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_akhir, 'url' => ['view', 'id' => $model->id_nilai_akhir]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-akhir-kkm-satuan-pendidikan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
