<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhirKkmSatuanPendidikan */

$this->title = $model->id_nilai_akhir;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Akhir Kkm Satuan Pendidikans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="nilai-akhir-kkm-satuan-pendidikan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_nilai_akhir], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_nilai_akhir], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_akhir',
            'id_sekolah',
            'id_siswa',
            'id_kelas',
            'id_semester',
            'id_mapel',
            'id_nilai_kd',
            'nilai_pengetahuan',
            'nilai_keterampilan',
            'predikat_pengetahuan',
            'predikat_keterampilan',
            'deskripsi_pengetahuan:ntext',
            'deskripsi_keterampilan:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
