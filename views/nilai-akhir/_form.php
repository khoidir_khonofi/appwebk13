<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-akhir-form">

    <?php $form = ActiveForm::begin(); ?>

   


    <?= $form->field($model, 'deskripsi_pengetahuan')->textarea(['rows' => 4]) ?>
    <?= $form->field($model, 'deskripsi_keterampilan')->textarea(['rows' => 4]) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
