<?php

use app\models\Kelas;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Guru;
use app\models\MataPelajaran;
use app\models\Sekolah;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-kd-pengetahuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); 
    
   
    
    ?>

    

    <?php   
        if(Yii::$app->user->identity->role=="Administrator"){ 
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
			    <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
			                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
			                        'language' => 'de',
			                        'options' => ['placeholder' => 'Pilih Siswa ...' ],
			                        'pluginOptions' => [
			                            'allowClear' => true,
			                            
			                        ],
			                    ])->label('Siswa') ?>

                <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
                    'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Mapel ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                ])->label('Pilih Mapel') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
    ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
                    'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Mapel ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                ])->label('Pilih Mapel') ?>

    <?php } ?>


    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
