<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdKeterampilan;
use app\models\KompetensiDasarPengetahuan;
use kartik\form\ActiveForm;
use app\models\KompetensiDasarKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\MataPelajaran;
use app\models\Kelas;
use app\models\BobotPenilaian;
use app\models\KkmSatuanPendidikan;
use app\models\Mengajar;
use app\models\Sekolah; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai Akhir (Multi KKM)';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    <?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){ ?>
            <p>
                <?= Html::a('<span class="fa fa-print"></span> Cetak Raport',['pilihsiswa'],
                        ['title' => '', 'class' => 'btn btn-info', 'target' => '_blank'
                    ]);
                    ?>
                <?= Html::a('<span class="fa fa-print"></span> Rangking',['rangking/create'],
                        ['title' => '', 'class' => 'btn btn-warning', 'target' => '_blank'
                    ]);
                    ?>
                <?= Html::button('Cetak Rapor',
                                    ['value' => Url::to(['nilai-akhir/pilihsiswa']),
                                    'title' => '', 'class' => 'showModalButton btn btn-info']); 
                ?>
                <?php
                if(Yii::$app->user->identity->role=="Administrator"){
                    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $kelas = Kelas::find()->where(['id_sekolah' => $sekolah->id_sekolah])->one();
                    $bobot = KkmSatuanPendidikan::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                }else{
                    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                    $mengajar = Mengajar::find()->where(['id_guru' => $guru->id_guru])->one();
                    $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
                    
                    if ($kelas) {
                        $bobot = KkmSatuanPendidikan::find()->where(['id_kelas' => $kelas->id_kelas])->one();
                    }elseif ($mengajar){
                        $mapel = Mengajar::find()->where(['id_mapel' => $mengajar->id_mapel])->one();
                        $bobot = KkmSatuanPendidikan::find()->where(['id_kelas' => $mapel->id_kelas])->one();
                    }
                }
                if($bobot){
                    if(Yii::$app->user->identity->role=="Administrator"){
                        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                        if($sekolah->jenjang_pendidikan == 'SD'){ ?>
                            <?= Html::a('<span class="fa fa-print"></span> KKM Satuan Pendidikan',['nilai-akhir-kkm-satuan-pendidikan/create'],
                            ['title' => '', 'class' => 'btn btn-success', 'data-confirm' => 'Nilai akhir ini menggunakan kkm satuan pendidikan','target' => '_blank'
                            ]);
                            ?>
                        <?php } else{ ?>
                            <?= Html::a('<span class="fa fa-print"></span> KKM Satuan Pendidikan',['nilai-akhir-kkm-satuan-pendidikan/createsekolahmenengah'],
                            ['title' => '', 'class' => 'btn btn-success', 'data-confirm' => 'Nilai akhir ini menggunakan kkm satuan pendidikan','target' => '_blank'
                            ]);
                            ?>
                        <?php } }else{ 
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            if($guru->tblsekolah->jenjang_pendidikan == 'SD'){ ?>
                                <?= Html::a('<span class="fa fa-print"></span> KKM Satuan Pendidikan',['nilai-akhir-kkm-satuan-pendidikan/create'],
                                ['title' => '', 'class' => 'btn btn-success', 'data-confirm' => 'Nilai akhir ini menggunakan kkm satuan pendidikan','target' => '_blank'
                                ]);
                                ?>
                        <?php } else{ ?>
                                <?= Html::a('<span class="fa fa-print"></span> KKM Satuan Pendidikan',['nilai-akhir-kkm-satuan-pendidikan/createsekolahmenengah'],
                                ['title' => '', 'class' => 'btn btn-success', 'data-confirm' => 'Nilai akhir ini menggunakan kkm satuan pendidikan','target' => '_blank'
                                ]);
                                ?>
                        <?php }
                        }
                ?>
                   
                <?php }else{ ?>
                    <button class="btn btn-default" onClick="return alert('KKM satuan pendidikan belum dibuat')">KKM Satuan Pendidikan</button>
                <?php } ?>               
            </p>
    <?php }else{ ?>
            <p>
                
            </p>
    <?php } ?>  

    <?php $form = ActiveForm::begin(); ?>
    
    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_nilai',
            //'id_sekolah',
            // [
            //     'attribute' => 'id_siswa',
            //     'label' => 'Siswa',
            //     'value' => function($model){
            //         return $model->siswa->nama;
            //     }
            // ],
           [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],


            //'id_semester',
           //'id_nilai_kd',
            [
                'attribute' => 'nilai_pengetahuan',
                'label' => 'Nilai',
                'value' => function($model){
                    return $model->nilai_pengetahuan;
                }
            ],
            [
                'attribute' => 'predikat_pengetahuan',
                'label' => 'Predikat',
                'value' => function($model){
                    return $model->predikat_pengetahuan;
                }
            ],
            [
                'attribute' => 'deskripsi_pengetahuan',
                'label' => 'Deskripsi Pengetahuan',
                'format' => 'html',
                'value' => function($model){
                   return $model->deskripsi_pengetahuan;                    
                }
            ],
           [
                'attribute' => 'nilai_keterampilan',
                'label' => 'Nilai',
                'value' => function($model){
                    return $model->nilai_keterampilan;
                }
            ],
           [
                'attribute' => 'predikat_keterampilan',
                'label' => 'Predikat',
                'value' => function($model){
                    return $model->predikat_keterampilan;
                }
            ],
           [
                'attribute' => 'deskripsi_keterampilan',
                'label' => 'Deskripsi Keterampilan',
                'format' => 'html',
                'value' => function($model){
                    return $model->deskripsi_keterampilan;
                   
                }
            ],
           
            ['class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
            ],
            'buttons' => [
                  'view' => function($url,$model){
                    
                        $a = Html::button('<span class="fa fa-eye"></span>',
                            ['value' => Url::to(['view','id'=>$model->id_nilai]),
                            'title' => '', 'class' => 'showModalButton btn btn-info']);
                        $b = Html::button('<span class="fa fa-pencil"></span> Deskripsi Manual',
                        ['value' => Url::to(['update','id'=>$model->id_nilai]),
                        'title' => '', 'class' => 'showModalButton btn btn-info']);
                        $c = "<button class='btn btn-default btn-block' onClick='return alert('Deskripsi masih kosong')'><span class='fa fa-pencil'></span> Deskripsi Manual</button>";
                       
                       
                        if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru" ){
                            if ($model->deskripsi_pengetahuan == NULL || $model->deskripsi_keterampilan == NULL) {
                                return $c;
                            }else{
                                return $b;
                            }
                            
                        }else{
                            return $a;
                        }
                        
                    
                        
                    }
            ],
            

    
    ],

    [
                'class' => 'kartik\grid\CheckboxColumn',
                'header' => Html::a('<span class="fa fa-trash"></span>', ['deleteall'],['data-method' => 'POST',  'data-confirm' => 'Are you sure you want to delete this item?' ,'class' => 'btn  btn-danger']),
                'name' => 'hapus',
            ],
        
        
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
                 Html::button('Search', [
                'value' => Url::to(['search']), 
                'class' => 'showModalButton btn btn-default',
                'style' => 'float:left',
        ]).''.
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px;'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
ActiveForm::end();
?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>


