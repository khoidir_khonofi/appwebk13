<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdKeterampilan;
use app\models\KompetensiDasarPengetahuan;
use app\models\NilaiAkhir;
use app\models\KompetensiDasarKeterampilan;
use app\models\SdNilaiHarianPengetahuan;
use app\models\SdNilaiHarianKeterampilan;
use app\models\MataPelajaran;
use app\models\Kelas;
use app\models\BobotPenilaian;
use app\models\KkmSatuanPendidikan;
use app\models\Sekolah; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rangking';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    
    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_nilai',
            //'id_sekolah',
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'id_siswa',
                'label' => 'Nilai Rata-Rata',
                'value' => function($model){
                    $nilai = NilaiAkhir::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->all();
                    $hitung = 0;
                    foreach ($nilai as $key => $value) {
                        $row = count($nilai);
                        $hitung += $value->nilai_pengetahuan;
                        $hasil = $hitung/$row;
                    }return round($hasil);
                }
            ],
           
     
        
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
                 Html::button('Search', [
                'value' => Url::to(['search']), 
                'class' => 'showModalButton btn btn-default',
                'style' => 'float:left',
                'title' => 'Cari Guru'
        ]),
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_DEFAULT
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>


