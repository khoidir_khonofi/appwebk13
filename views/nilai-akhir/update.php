<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhir */

$this->title = 'Update Nilai Akhir: ' . $model->id_nilai;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Akhirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai, 'url' => ['view', 'id' => $model->id_nilai]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-akhir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
