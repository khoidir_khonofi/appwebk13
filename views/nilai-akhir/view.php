<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiAkhir */

$this->title = $model->id_nilai;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Akhirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="nilai-akhir-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai',
            'id_nilai_kd',
            'id_sekolah',
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
           [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
           [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            'nilai_pengetahuan',
            'predikat_pengetahuan',
            'deskripsi_pengetahuan:ntext',
            'nilai_keterampilan',
            'predikat_keterampilan',
            'deskripsi_keterampilan:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
