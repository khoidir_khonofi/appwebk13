<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-kd-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'skor')->textInput(['readOnly' => true, 'type' => 'hidden'])->label(false) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
