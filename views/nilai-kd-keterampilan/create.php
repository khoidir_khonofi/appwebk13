<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdKeterampilan */

$this->title = 'Create Nilai Kd Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-kd-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
