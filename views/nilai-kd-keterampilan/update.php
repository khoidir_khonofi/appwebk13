<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdKeterampilan */

$this->title = 'Update Nilai Kd Keterampilan: ' . $model->id_nilai_kd_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_kd_keterampilan, 'url' => ['view', 'id' => $model->id_nilai_kd_keterampilan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-kd-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
