<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdKeterampilan */

$this->title = $model->id_nilai_kd_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="nilai-kd-keterampilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

  

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_kd_keterampilan',
            'id_nilai_harian_keterampilan',
            'id_sekolah',
             [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
           [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            //'id_semester',
            
            [
                'attribute' => 'id_kd_keterampilan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdketerampilan->no_kd." ".$model->kdketerampilan->judul;
                }
            ],
            [
                'attribute' => 'skor',
                'label' => 'Skor',
                'value' => function($model){
                    return $model->skor;
                }  
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
