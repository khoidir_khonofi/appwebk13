<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanAkhirSekolahMenengah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-kd-pengetahuan-akhir-sekolah-menengah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'npts')->textInput() ?>

    <?= $form->field($model, 'npas')->textInput() ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
