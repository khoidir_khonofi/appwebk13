<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanAkhirSekolahMenengah */

$this->title = 'Create Nilai Kd Pengetahuan Akhir Sekolah Menengah';
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuan Akhir Sekolah Menengahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-kd-pengetahuan-akhir-sekolah-menengah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
