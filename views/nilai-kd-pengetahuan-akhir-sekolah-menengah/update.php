<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanAkhirSekolahMenengah */

$this->title = 'Update Nilai Kd Pengetahuan Akhir Sekolah Menengah: ' . $model->id_nilai_kd_pengetahuan_akhir;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuan Akhir Sekolah Menengahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_kd_pengetahuan_akhir, 'url' => ['view', 'id' => $model->id_nilai_kd_pengetahuan_akhir]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-kd-pengetahuan-akhir-sekolah-menengah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
