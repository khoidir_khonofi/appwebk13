<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanSekolahMenengah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-kd-pengetahuan-sekolah-menengah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_nilai_harian_pengetahuan')->textInput() ?>

    <?= $form->field($model, 'id_kd_pengetahuan')->textInput() ?>

    <?= $form->field($model, 'id_sekolah')->textInput() ?>

    <?= $form->field($model, 'id_kelas')->textInput() ?>

    <?= $form->field($model, 'id_siswa')->textInput() ?>

    <?= $form->field($model, 'id_mapel')->textInput() ?>

    <?= $form->field($model, 'id_semester')->textInput() ?>

    <?= $form->field($model, 'nph')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
