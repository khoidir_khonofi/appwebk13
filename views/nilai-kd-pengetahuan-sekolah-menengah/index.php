<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\BobotPenilaian;
use app\models\Guru;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\SmaNilaiHarianPengetahuan;
use app\models\SmpNilaiHarianPengetahuan;
use app\models\NilaiKdPengetahuan;
use app\models\NilaiKdPengetahuanAkhir;
use app\models\NilaiAkhir;
use app\models\KompetensiDasarPengetahuan;
use app\models\NilaiKdPengetahuanAkhirSekolahMenengah;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai KD Pengetahuan';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    <?php $form = ActiveForm::begin(); ?>
        <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    $link = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_siswa' => $model->id_siswa])->one()['id_siswa'];
                    $url = Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-akhir-sekolah-menengah/lihatsiswa', 'id' => $link]);
                    $cek = NilaiKdPengetahuanAkhirSekolahMenengah::find()->where(['id_siswa' => $model->id_siswa, 'id_mapel' => $model->id_mapel])->one();
                    if($cek){
                        return "<a href='".$url."'>".$model->siswa->nama."</a>";
                    }else{
                        return $model->siswa->nama;
                    }
                    
                }
            ],
           [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            //'id_semester',
            
            [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdpengetahuan->no_kd;
                }
            ],
            [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KKM/KD',
                'value' => function($model){
                    $kd = KompetensiDasarPengetahuan::find()->where(['id_kd_pengetahuan' => $model->id_kd_pengetahuan])->one();
                    return $kd->kkm;
                }
            ],
            'nph',
            
            
           
           
            //'created_at',
            //'updated_at',

             ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                        
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_nilai_pengetahuan_sekolah_menengah]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $b = Html::button('<span class="fa fa-pencil"></span> Npts dan Npas',
                                ['value' => Url::to(['update','id'=>$model->id_nilai_pengetahuan_sekolah_menengah]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_nilai_pengetahuan_sekolah_menengah], [
                                    'class' => 'btn btn-danger',
                                    'title' => 'Delete',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                    
                                    ]) ;
                           
                            if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru" ){
                                return $a." ".$c;
                            }else{
                                return $a;
                            }
                            
                        
                            
                        }
                ],
                

        
        ],
        [
            'class' => 'kartik\grid\checkboxColumn',
            'name' => 'hapussemua',
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
                 Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari'
        ]).' '.
                  Html::a('<span class="fa fa-trash"></span>',['hapus'],['class' => 'btn btn-default', 'data-confirm' => 'Are you sure you want to delete this item?', 'data-method' => 'POST', 'style' => 'margin-left:5px']).' '.
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>

