<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanSekolahMenengah */

$this->title = 'Update Nilai Kd Pengetahuan Sekolah Menengah: ' . $model->id_nilai_pengetahuan_sekolah_menengah;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuan Sekolah Menengahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_pengetahuan_sekolah_menengah, 'url' => ['view', 'id' => $model->id_nilai_pengetahuan_sekolah_menengah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-kd-pengetahuan-sekolah-menengah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
