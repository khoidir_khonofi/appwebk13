<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuanSekolahMenengah */

$this->title = $model->id_nilai_pengetahuan_sekolah_menengah;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuan Sekolah Menengahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="nilai-kd-pengetahuan-sekolah-menengah-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_pengetahuan_sekolah_menengah',
            'id_nilai_harian_pengetahuan',
           [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdpengetahuan->judul;
                }
            ],
            'id_sekolah',
           [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
           [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            'nph',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
