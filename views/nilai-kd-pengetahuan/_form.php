<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-kd-pengetahuan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'npts')->textInput()->label('Penilaian Tengah Semester') ?>

    <?= $form->field($model, 'npas')->textInput()->label('Penilaian Akhir Semester') ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
