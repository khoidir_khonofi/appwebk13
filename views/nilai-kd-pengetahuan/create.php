<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuan */

$this->title = 'Create Nilai Kd Pengetahuan';
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-kd-pengetahuan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
