<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiKdPengetahuan */

$this->title = 'Update Nilai Kd Pengetahuan: ' . $model->id_nilai_kd_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Kd Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_kd_pengetahuan, 'url' => ['view', 'id' => $model->id_nilai_kd_pengetahuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nilai-kd-pengetahuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
