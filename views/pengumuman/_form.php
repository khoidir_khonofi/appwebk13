<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\models\MataPelajaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mata-pelajaran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'isi')->textarea(['rows' => 6, 'class' => 'textarea']) ?>

    <?= $form->field($model, 'gambarpengumuman')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
        ]); ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
