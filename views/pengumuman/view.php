<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pengumuman */

$this->title = $model->id_pengumuman;
$this->params['breadcrumbs'][] = ['label' => 'Pengumumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pengumuman-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pengumuman',
            'id_sekolah',
            'isi:ntext',
            [
                'attribute' => 'gambar',
                'label' => 'Gambar',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img('gambar_pengumuman/'.$model->gambar, ['width'=>'150px', 'height' => '150px']);
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
