<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rangking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rangking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_sekolah')->textInput() ?>

    <?= $form->field($model, 'id_kelas')->textInput() ?>

    <?= $form->field($model, 'id_siswa')->textInput() ?>

    <?= $form->field($model, 'id_semester')->textInput() ?>

    <?= $form->field($model, 'nilai_rata_rata')->textInput() ?>

    <?= $form->field($model, 'rangking')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
