<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RangkingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rangking-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rangking') ?>

    <?= $form->field($model, 'id_sekolah') ?>

    <?= $form->field($model, 'id_kelas') ?>

    <?= $form->field($model, 'id_siswa') ?>

    <?= $form->field($model, 'id_semester') ?>

    <?php // echo $form->field($model, 'nilai_rata_rata') ?>

    <?php // echo $form->field($model, 'rangking') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
