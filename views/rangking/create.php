<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rangking */

$this->title = 'Create Rangking';
$this->params['breadcrumbs'][] = ['label' => 'Rangkings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rangking-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
