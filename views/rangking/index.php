<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use app\models\Guru;
use app\models\Siswa;
use app\models\Rangking;
use yii\bootstrap\ButtonDropdown;
use app\models\Sekolah;
use app\models\NilaiAkhir;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rangking';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
        
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
   
   <?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){ ?>
            <p>
            <?= Html::a('<span class="fa fa-repeat"></span> Update Rangking',['rangking/create'],
                       ['class' => 'btn btn-info']); ?>
                    </p>
        <?php }else{ ?>
                <p>.</p>
         <?php } ?>
   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas. " ".$model->kelas->nama;
                }
            ],
            
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            'nilai_rata_rata_pengetahuan',
            'nilai_rata_rata_keterampilan',
            'total',
            [
                'attribute' => 'rangking',
                'label' => 'Rangking',
                'value' => function($model, $key, $index ){
                    // $juara = Rangking::find()->where(['id_siswa' => $model->id_siswa, 'id_semester' => $model->id_semester])->all();
                    // $hit = 1;
                    // foreach ($juara as $key => $value) {
                    //     return $hit++;
                    // }
                    return $index+1;  
                    
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
            ],
            'buttons' => [
                  'view' => function($url,$model){
                    
                        $a = Html::button('<span class="fa fa-eye"></span>',
                            ['value' => Url::to(['view','id'=>$model->id_rangking]),
                            'title' => '', 'class' => 'showModalButton btn btn-info']);
                        $b = Html::button('<span class="fa fa-pencil"></span>',
                            ['value' => Url::to(['update','id'=>$model->id_rangking]),
                            'title' => '', 'class' => 'showModalButton btn btn-success']);
                        $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_rangking], [
                                'class' => 'btn btn-danger',
                                'title' => 'Delete',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                                
                                ]) ;
                       
                        if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru" ){
                            return $a." ".$c;
                        }else{
                            return $a;
                        }
                        
                    
                        
                    }
            ],
            

    
    ],
    ],
'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
'beforeHeader'=>[
    [
        // 'columns'=>[
        //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
        //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
        //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
        // ],
        'options'=>['class'=>'skip-export'] // remove this row from export
    ]
],
'toolbar' =>  [
     ['content'=>
             Html::button('Search', [
        'value' => Url::to(['search']), 
        'class' => 'showModalButton btn btn-info',
        'style' => 'float:left',
        'title' => 'Cari'
    ]).''.
    //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
    Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px'])
     ],
    '{export}',
    '{toggleData}'
],
'pjax' => true,
'bordered' => true,
'striped' => true,
'condensed' => true,
'responsive' => true,
'hover' => true,
'floatHeader' => true,
// 'floatHeaderOptions' => ['top' => $scrollingTop],
'showPageSummary' => true,
'panel' => [
    'type' => GridView::TYPE_INFO
],
]);
?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>
