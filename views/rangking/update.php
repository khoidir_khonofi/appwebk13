<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rangking */

$this->title = 'Update Rangking: ' . $model->id_rangking;
$this->params['breadcrumbs'][] = ['label' => 'Rangkings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rangking, 'url' => ['view', 'id' => $model->id_rangking]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rangking-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
