<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rangking */

$this->title = $model->id_rangking;
$this->params['breadcrumbs'][] = ['label' => 'Rangkings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rangking-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_rangking',
            'id_sekolah',
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas. " ".$model->kelas->nama;
                }
            ],
            
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            'nilai_rata_rata_pengetahuan',
            'nilai_rata_rata_keterampilan',
            'total',
            'rangking',
        ],
    ]) ?>

</div>
