<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $model app\models\RombonganBelajar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rombongan-belajar-form">

    <?php $form = ActiveForm::begin(); ?>


     <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 
                'Aktif' => 'Aktif', 
                'Tidak Aktif' => 'Tidak Aktif',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih  ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
