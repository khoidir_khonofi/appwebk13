<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Guru;
use app\models\Kelas;
use app\models\Siswa;
use app\models\Sekolah;
/* @var $this yii\web\View */
/* @var $model app\models\RombonganBelajarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rombongan-belajar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    
    
    if(Yii::$app->user->identity->role=="Administrator"){
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>


     <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

    <?= $form->field($model, 'id_guru')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Guru::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_guru', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Guru ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Guru') ?>

        <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Siswa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Siswa') ?>


    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?> 
             <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Kelas') ?>

    <?= $form->field($model, 'id_guru')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Guru::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_guru', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Guru ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Guru') ?>

        <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_siswa', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Siswa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Siswa') ?>

    <?php } ?>
    <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 
                'Aktif' => 'Aktif', 
                'Tidak Aktif' => 'Tidak Aktif',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
