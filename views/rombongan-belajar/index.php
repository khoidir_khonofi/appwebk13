<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Kelas;
use app\models\Siswa;
use app\models\BobotPenilaian;
use app\models\RombonganBelajar;
use yii\widgets\ActiveForm;
use app\models\Sekolah;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rombongan Belajar';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::beginForm(['aksi-naik-kelas'], 'post', ['enctype' => 'multipart/form-data']);?>

<?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){?>

    <div class="row" style="margin-left: 4px; margin-right: 4px;">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                   
                        <?php if(Yii::$app->user->identity->role=="Administrator"){
                            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                        ?>
                            <label>Naik Ke Kelas / Tinggal di Kelas</label>
                            <?= Select2::widget([
                                    'name' => 'id_kelas',
                                    'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
                                    'disabled' => false
                                ]);?>
                        <?php } else { 
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                        ?>
                            <label>Naik Ke Kelas / Tinggal di Kelas</label>
                            <?= Select2::widget([
                                    'name' => 'id_kelas',
                                    'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $guru->id_sekolah])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
                                    'disabled' => false
                                ]);?>
                        <?php } ?>

                        <div class="form-group" style="float: right; margin-top: 10px;">
                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                        </div>

                </div>
            </div>
        </div>
    </div>
<?php }else { ?>
        <p></p>
<?php } ?>


<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
                <h2><?= Html::encode($this->title) ;?></h2>
<?php
$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
     
    [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    $url = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one()['id_siswa'];
                    $link = Yii::$app->urlManager->createUrl(['siswa/detailsiswa','id' => $url]);
                    return "<a href='".$link."'>".$model->siswa->nama."</a>";
                }
            ],
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'format' => 'html',
                'value' => function($model){
                    $url = RombonganBelajar::find()->where(['id_kelas' => $model->id_kelas])->one()['id_kelas'];
                    $link = Yii::$app->urlManager->createUrl(['rombongan-belajar/lihatkelas','id' => $url]);
                    return "<a href='".$link."'>".$model->kelas->tingkat_kelas." ".$model->kelas->nama."</a>";
                }
            ],
             [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            'tahun_ajaran',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->guru->nama;
                }
            ],
           // 'status',
          
    
    [
        //'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'status', 
        'vAlign' => 'middle',
        'value' => function($model){
            return $model->status;
        }
    ],
     ['class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
            ],
            'buttons' => [
                  'view' => function($url,$model){
                    
                        $a = Html::button('<span class="fa fa-eye"></span>',
                            ['value' => Url::to(['view','id'=>$model->id_rombel]),
                            'title' => '', 'class' => 'showModalButton btn btn-info']);
                        $b = Html::button('<span class="fa fa-pencil"></span>',
                            ['value' => Url::to(['update','id'=>$model->id_rombel]),
                            'title' => '', 'class' => 'showModalButton btn btn-success']);
                        $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_rombel], [
                                'class' => 'btn btn-danger',
                                'title' => 'Delete',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                                
                                ]) ;
                       
                        if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru" ){
                            return $a." ".$c;
                        }else{
                            return $a;
                        }
                        
                    
                        
                    }
            ],
            

    
    ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => true,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) {  return '#';  },
    //     'viewOptions'=>[
    //         'title'=>'View', 
    //         'data-toggle'=>'tooltip',
    //     ],
    //     'updateOptions'=>['title'=>'Update', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['title'=>'Delete', 'data-toggle'=>'tooltip'], 
    // ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
    ]
];
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => $gridColumns,        
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
                Html::button('Search', [
                    'value' => Url::to(['search']), 
                    'class' => 'showModalButton btn btn-default',
                    'style' => 'float:left; margin-right:5px;',
                ]).''.
                Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>

<?= Html::endForm();?>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>
