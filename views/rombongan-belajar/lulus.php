<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use kartik\form\ActiveForm;
use app\models\Guru;
use app\models\Siswa;
use app\models\Rangking;
use yii\bootstrap\ButtonDropdown;
use app\models\Sekolah;
use app\models\NilaiAkhir;
use app\models\RombonganBelajar;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rombongan Belajar';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
        
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
   <?php $form = ActiveForm::begin(); ?>
   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    $url = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one()['id_siswa'];
                    $link = Yii::$app->urlManager->createUrl(['siswa/detailsiswa','id' => $url]);
                    return "<a href='".$link."'>".$model->siswa->nama."</a>";
                }
            ],
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'format' => 'html',
                'value' => function($model){
                    $url = RombonganBelajar::find()->where(['id_kelas' => $model->id_kelas])->one()['id_kelas'];
                    $link = Yii::$app->urlManager->createUrl(['rombongan-belajar/lihatkelas','id' => $url]);
                    return "<a href='".$link."'>".$model->kelas->tingkat_kelas." ".$model->kelas->nama."</a>";
                }
            ],
             [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            'tahun_ajaran',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->guru->nama;
                }
            ],
           // 'status',
          
    
    [
        //'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'status', 
        'vAlign' => 'middle',
        'value' => function($model){
            return $model->status;
        }
    ],


            ['class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
            ],
            'buttons' => [
                  'view' => function($url,$model){
                    
                        $a = Html::button('<span class="fa fa-eye"></span>',
                            ['value' => Url::to(['view','id'=>$model->id_rombel]),
                            'title' => '', 'class' => 'showModalButton btn btn-info']);
                        $b = Html::button('<span class="fa fa-pencil"></span>',
                            ['value' => Url::to(['update','id'=>$model->id_rombel]),
                            'title' => '', 'class' => 'showModalButton btn btn-success']);
                        $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_rombel], [
                                'class' => 'btn btn-danger',
                                'title' => 'Delete',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                                
                                ]) ;
                       
                        if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru" ){
                            return $a." ".$c;
                        }else{
                            return $a;
                        }
                        
                    
                        
                    }
            ],
            

    
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'name' => 'test',
    ]
    ],
'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
'beforeHeader'=>[
    [
        // 'columns'=>[
        //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
        //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
        //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
        // ],
        'options'=>['class'=>'skip-export'] // remove this row from export
    ]
],
'toolbar' =>  [
     ['content'=>
                Html::button('Search', [
                    'value' => Url::to(['search']), 
                    'class' => 'showModalButton btn btn-default',
                    'style' => 'float:left; margin-right:5px;',
                ]).''.
                 Html::a('<span class="fa fa-trash"> Lulus</span>',['choi'],['class' => 'btn btn-default', 'data-confirm' => 'Are you sure you want to delete this item?', 'data-method' => 'POST', 'style' => 'margin-left:5px; margin-right:5px']). ''.
                Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
     ],
    '{export}',
    '{toggleData}'
],
'pjax' => false,
'bordered' => true,
'striped' => true,
'condensed' => true,
'responsive' => true,
'hover' => true,
'floatHeader' => true,
// 'floatHeaderOptions' => ['top' => $scrollingTop],
'showPageSummary' => true,
'panel' => [
    'type' => GridView::TYPE_INFO
],
]);
?>
 <?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>
