<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Sekolah;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Guru;
use app\models\Kelas;

/* @var $this yii\web\View */
/* @var $model app\models\RombonganBelajar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rombongan-belajar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->role=="Administrator"){
    	$sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
		<?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
	            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
	            'language' => 'de',
	            'options' => ['placeholder' => 'Pilih Kelas ...' ],
	            'pluginOptions' => [
	                'allowClear' => true,
	                
	            ],
	        ])->label('Kelas') ?>

		

	<?php }else { 
		 $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
	?>
		<?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
	            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
	            'language' => 'de',
	            'options' => ['placeholder' => 'Pilih Kelas ...'],
	            'pluginOptions' => [
	                'allowClear' => true,
	                
	            ],
	        ])->label('Kelas') ?>
	<?php } ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
