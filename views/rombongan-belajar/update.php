<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RombonganBelajar */

$this->title = 'Update Rombongan Belajar: ' . $model->id_rombel;
$this->params['breadcrumbs'][] = ['label' => 'Rombongan Belajars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rombel, 'url' => ['view', 'id' => $model->id_rombel]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rombongan-belajar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
