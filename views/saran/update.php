<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Saran */

$this->title = 'Update Saran: ' . $model->id_saran;
$this->params['breadcrumbs'][] = ['label' => 'Sarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_saran, 'url' => ['view', 'id' => $model->id_saran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="saran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
