<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */

$this->title = 'Create Sd Nilai Harian Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Sd Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sd-nilai-harian-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
