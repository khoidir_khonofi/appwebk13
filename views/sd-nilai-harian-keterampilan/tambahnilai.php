<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\TemaKeterampilan;
use app\models\TeknikPenilaianKeterampilan;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sd-nilai-harian-pengetahuan-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        
        //$v = $model->=nama_mata_pelajaran;

        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
        $siswa = Siswa::find()->where(['id_siswa' => $idsiswa])->one();
    ?>


    <?php if(Yii::$app->user->identity->role=="Administrator"){ ?>

        <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...', 'required' => 'required', 'id' => 'datamapel',
                        'onchange'=>'
                           $.post( "index.php?r=sd-nilai-harian-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_keterampilan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_keterampilan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'required' => 'required', 'id'=> 'kade',
                    'onchange'=>'
                       $.post( "index.php?r=sd-nilai-harian-keterampilan/tema&id='.'"+$(this).val(), function( data ){
                       $("select#choi").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
             <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...','required' => 'required', 'id'=>'datamapel',
                        'onchange'=>'
                           $.post( "index.php?r=sd-nilai-harian-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_keterampilan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_kd_keterampilan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'required' => 'required', 'id'=> 'kade',
                    'onchange'=>'
                       $.post( "index.php?r=sd-nilai-harian-keterampilan/tema&id='.'"+$(this).val(), function( data ){
                       $("select#choi").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>
    <?php } ?>

    <?php
        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
    ?>
    <?= $form->field($model, 'id_siswa')->textInput(['value' => $idsiswa ,'type' => 'hidden'])->label(false) ?>
    <div style="display: none;" id="datatema">
    <?= $form->field($model,  'id_tema')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(TemaKeterampilan::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_tema', 'tema'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tema ...', 'required' => 'required', 'id' =>'choi',
                    'onchange'=>'
                       $.post( "index.php?r=sd-nilai-harian-keterampilan/teknik&id='.'"+$(this).val(), function( data ){
                       $("select#themes").html(data);
                      });'
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Tema') ?>
    </div>

             <?= $form->field($model, 'jenis_penilaian')->widget(Select2::classname(),[
            'data' => [ 
                'Proyek' => 'Proyek',
                'Produk' => 'Produk', 
                'Praktek' => 'Praktek',
                'Portofolio' => 'Portofolio',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Jenis Penilaian ...', 'required' => 'required'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Jenis Penilaian') ?>

    <?= $form->field($model, 'nilai')->textInput(['required' => 'required']) ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $ (document).ready(function(){
        $('#datamapel').change(function(){
          //alert($('#datamapel option:selected').text());
          if ($('#datamapel option:selected').text() == 'Matematika') {
            $('#datatema').hide('slow');
          }else{
            $('#datatema').show('slow');
          }
        })
    })
</script>
