<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */

$this->title = $model->id_nilai_harian_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Sd Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sd-nilai-harian-keterampilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_harian_keterampilan',
            'id_sekolah',
             [
                'attribute' => 'id_kd_keterampilan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdKeterampilan->judul;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kdKeterampilan->kelas->tingkat_kelas." ".$model->kdKeterampilan->kelas->nama;
                }
            ],
            
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
             [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
            [
                'attribute' => 'jenis_penilaian',
                'label' => 'Jenis Penilaian',
                'value' => function($model){
                    return $model->jenis_penilaian;
                }
            ],
            'nilai',
            'created_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
