<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianPengetahuan */

$this->title = 'Update Sd Nilai Harian Pengetahuan: ' . $model->id_nilai_harian_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Sd Nilai Harian Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_harian_pengetahuan, 'url' => ['view', 'id' => $model->id_nilai_harian_pengetahuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sd-nilai-harian-pengetahuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
