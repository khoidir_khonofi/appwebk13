<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianPengetahuan */

$this->title = $model->id_nilai_harian_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Sd Nilai Harian Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sd-nilai-harian-pengetahuan-view">

    <h1><?= Html::encode($this->title) ?></h1>

  

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_harian_pengetahuan',
             'id_sekolah',
             [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdPengetahuan->judul;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kdPengetahuan->kelas->tingkat_kelas." ".$model->kdPengetahuan->kelas->nama;
                }
            ],
           
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
             [
                'attribute' => 'id_tema',
                'label' => 'tema',
                'value' => function($model){
                    return $model->tema->tema;
                }
            ],
           // 'id_sekolah',
           
            
            [
                'attribute' => 'id_subtema',
                'label' => 'Subtema',
                'value' => function($model){
                    return $model->subtema->subtema;
                }
            ],
            'nama_penilaian',
            'nilai',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
