<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\form\ActiveField;
/* @var $this yii\web\View */
/* @var $model app\models\Sekolah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sekolah-form">

    <?php $form = ActiveForm::begin([
        'action' => ['update']
    ]); ?>

    <?= $form->field($model, 'nama_sekolah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'npsn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_kepala_sekolah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nip_kepsek')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'jenjang_pendidikan')->widget(Select2::classname(),[
            'data' => [ 'SD' => 'SD', 'SMP' => 'SMP', 'SMA' => 'SMA', ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih sekolah'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?= $form->field($model, 'alamat', [
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'right', 'onLabelClick' => true, 'onLabelHover' => false]
        ])->textArea([
            'id' => 'address-input', 
            'placeholder' => 'Enter address...', 
            'rows' => 2
        ])->hint('Enter address in 4 lines. First 2 lines must contain the street details and next 2 lines the city, zip, and country detail.') ?>

    <?= $form->field($model, 'kelurahan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kabupaten')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provinsi')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'telepon', [
    'feedbackIcon' => [
        'prefix' => 'fas fa-',
        'default' => 'mobile-alt',
        'success' => 'check-circle',
        'error' => 'exclamation-circle',
    ]
    ])->widget('yii\widgets\MaskedInput', [
        'mask' => '9999-9999-9999'
    ]);?>

    <?= $form->field($model, 'fotosekolah')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
        ])->label('Foto Sekolah'); ?> 
    
    <?php
    if (Yii::$app->user->identity->role=="Administrator") {?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php }else{ ?>
        -
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
