<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Sekolah;
use app\models\User;
use yii\helpers\Url;
use app\models\Guru;
use app\models\Siswa;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SekolahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Sekolah';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->user->identity->role=="Administrator") {
    $user = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $model = Sekolah::find()->where(['id_sekolah' => $user->id_sekolah])->one();
    $siswa = Siswa::find()->where(['id_sekolah' => $model->id_sekolah])->all();
    $jmlguru = Guru::find()->where(['id_sekolah' => $model->id_sekolah])->all();
}elseif (Yii::$app->user->identity->role=="Guru") {
    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $model = Sekolah::find()->where(['id_sekolah' => $guru->id_sekolah])->one();
    $siswa = Siswa::find()->where(['id_sekolah' => $model->id_sekolah])->all();
    $jmlguru = Guru::find()->where(['id_sekolah' => $model->id_sekolah])->all();
}else{
    $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $model = Sekolah::find()->where(['id_sekolah' => $siswa->id_sekolah])->one();
    $siswa = Siswa::find()->where(['id_sekolah' => $model->id_sekolah])->all();
    $jmlguru = Guru::find()->where(['id_sekolah' => $model->id_sekolah])->all();
}


?>
<section class="content">
<div class="row">
  <div class="col-md-4">

    <!-- Profile Image -->
    <div class="box box-info">
      <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="foto_sekolah/<?= $model->foto ?>" alt="User profile picture">
        <h4 class="profile-username text-center"><?= $model->nama_sekolah ?></h4>

        <p class="text-muted text-center"><?= $model->nama_kepala_sekolah ?></p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Npsn</b> <a class="pull-right"><?= $model->npsn ?></a>
          </li>
          <li class="list-group-item">
            <b>Nip Kepsek</b> <a class="pull-right"><?= $model->nip_kepsek ?></a>
          </li>
          <li class="list-group-item">
            <b>Email Sekolah</b> <a class="pull-right"><?= $model->idUser->email ?></a>
          </li>
          <li class="list-group-item">
            <b>Jenjang Pendidikan</b> <a class="pull-right"><?= $model->jenjang_pendidikan ?></a>
          </li>
          <li class="list-group-item">
            <b>Jumlah Siswa</b> <a class="pull-right"><?= count($siswa) ?></a>
          </li>
          <li class="list-group-item">
            <b>Jumlah Guru</b> <a class="pull-right"><?= count($jmlguru) ?></a>
          </li>
        </ul>

        <button class="showModalButton btn btn-info" value="<?= Yii::$app->urlManager->createUrl(['sekolah/view', 'id' => $model->id_sekolah]) ?>" style="width:100%;" title=""><b>View</b></button>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->
   
  </div>
  <!-- /.col -->
  <div class="col-md-8">
  <div class="box box-info">
    <div class="nav-tabs-custom">
     
      <div class="tab-content">
       
        <div class="active tab-pane">
        <?php  echo $this->render('update', ['model' => $model]); ?>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

</section>

<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>