<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sekolah */

$this->params['breadcrumbs'][] = ['label' => 'Sekolah', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sekolah, 'url' => ['view', 'id' => $model->id_sekolah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sekolah-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
