<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sekolah */

$this->params['breadcrumbs'][] = ['label' => 'Sekolahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sekolah-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_sekolah',
            'id_user',
            'nama_sekolah',
            'nip_kepsek',
            'npsn',
            'nama_kepala_sekolah',
            'jenjang_pendidikan',
            'alamat:ntext',
            'kelurahan:ntext',
            'kecamatan',
            'kabupaten',
            'provinsi',
            'telepon',
             [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img('foto_sekolah/'.$model->foto, ['width' => '50px', 'height' => '50px']);
                }
            ],
        ],
    ]) ?>

</div>
