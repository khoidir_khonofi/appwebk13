<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Semester */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="semester-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'tahun_ajaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'semester')->textInput() ?>
    <?= $form->field($model, 'periode_aktif')->widget(Select2::classname(),[
            'data' => [ 'Aktif' => 'Aktif', 'Tidak Aktif' => 'Tidak Aktif'],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Periode'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?= $form->field($model, 'tanggal_mulai')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'tanggal_selesai')->textInput(['type' => 'date']) ?>

   
    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
