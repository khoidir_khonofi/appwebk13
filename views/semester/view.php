<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Semester */

$this->title = $model->id_semester;
$this->params['breadcrumbs'][] = ['label' => 'Semesters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="semester-view">

    <h1><?= Html::encode($this->title) ?></h1>

  

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_semester',
            'id_sekolah',
            'tahun_ajaran',
            'semester',
            'periode_aktif',
            'tanggal_mulai',
            'tanggal_selesai',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
