<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Siswa;
use app\models\Guru;
use app\models\Sekolah;
use app\models\Semester;

/* @var $this yii\web\View */
/* @var $model app\models\SikapAkhirSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikap-akhir-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<?php   if(Yii::$app->user->identity->role=="Administrator"){ 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();   
?>
                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ,
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-sosial/semester&id='.'"+$(this).val(), function( data ){
                                       $("select#smt").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_guru' => $guru->id_guru])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...',
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-sosial/semester&id='.'"+$(this).val(), function( data ){
                                       $("select#smt").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php } ?>

    <?= $form->field($model, 'keterangan_sosial')->textarea(['rows' => 2])->label('Deskripsi Sosial') ?>
    <?= $form->field($model, 'keterangan_spiritual')->textarea(['rows' => 2])->label('Deskripsi Spiritual') ?>

    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
