<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapAkhir */

$this->title = 'Create Sikap Akhir';
$this->params['breadcrumbs'][] = ['label' => 'Sikap Akhirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikap-akhir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
