<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapAkhir */

$this->title = 'Update Sikap Akhir: ' . $model->id_sikap;
$this->params['breadcrumbs'][] = ['label' => 'Sikap Akhirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sikap, 'url' => ['view', 'id' => $model->id_sikap]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sikap-akhir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
