<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SikapAkhir */

$this->title = $model->id_sikap;
$this->params['breadcrumbs'][] = ['label' => 'Sikap Akhirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sikap-akhir-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_sikap',
            'id_sekolah',
            // 'id_sekolah',
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    return $model->siswa->nama;
                }
           ],
           [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'format' => 'html',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas.''.$model->kelas->nama;
                }
           ],
          
           [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'format' => 'html',
                'value' => function($model){
                    return $model->semester->semester;
                }
           ],
          
            // 'id_sikap_sosial',
            // 'id_sikap_spiritual',
           'keterangan_sosial:ntext',
            'keterangan_spiritual:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
