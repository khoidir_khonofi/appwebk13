<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */

$this->title = 'Create Sikap Sosial';
$this->params['breadcrumbs'][] = ['label' => 'Sikap Sosials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikap-sosial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
