<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikap-sosial-form">

    <?php $form = ActiveForm::begin(); ?>



     <?= $form->field($model, 'butir_sikap')->widget(Select2::classname(),[
            'data' => [ 
            	'Jujur' => 'Jujur', 
            	'Disiplin' => 'Disiplin',
            	'Tanggung Jawab' => 'Tanggung Jawab',
            	'Santun' => 'Santun',
            	'Peduli' => 'Peduli',
            	'Percaya Diri' => 'Percaya Diri',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Butir Sikap ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <?= $form->field($model, 'nilai')->textInput(['type' => 'number', 'required' => 'required']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
