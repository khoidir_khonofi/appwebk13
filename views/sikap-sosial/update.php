<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */

$this->title = 'Update Sikap Sosial: ' . $model->id_sikap_sosial;
$this->params['breadcrumbs'][] = ['label' => 'Sikap Sosials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sikap_sosial, 'url' => ['view', 'id' => $model->id_sikap_sosial]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sikap-sosial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
