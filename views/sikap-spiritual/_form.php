<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\Kelas;
use app\models\Guru;
use app\models\Semester;
/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikap-spiritual-form">
 <?php $form = ActiveForm::begin();?>

    <?php   if(Yii::$app->user->identity->role=="Administrator"){ 
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>

            <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
                        'data' =>  ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Kelas ...',
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-spiritual/kelas&id='.'"+$(this).val(), function( data ){
                                       $("select#klas").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Kelas') ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' , 'id'=> 'klas',
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-spiritual/semester&id='.'"+$(this).val(), function( data ){
                                       $("select#smt").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                    <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>

                <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
                        'data' =>  ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->all(),'id_kelas', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Kelas ...',
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-spiritual/kelas&id='.'"+$(this).val(), function( data ){
                                       $("select#klas").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Kelas') ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_guru' => $guru->id_guru])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' , 'id' => 'klas',
                        'onchange'=>'
                                       $.post( "index.php?r=sikap-spiritual/semester&id='.'"+$(this).val(), function( data ){
                                       $("select#smt").html(data);
                                      });'
                                    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

                    <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>

    <?php } ?>

     <?= $form->field($model, 'butir_sikap')->widget(Select2::classname(),[
            'data' => [ 
                'Ketaatan Beribadah' => 'Ketaatan Beribadah', 
                'Berprilaku Syukur' => 'Berprilaku Syukur',
                'Berdoa sebelum dan sesudah melakukan kegiatan' => 'Berdoa sebelum dan sesudah melakukan kegiatan',
                'Toleransi dalam beragama' => 'Toleransi dalam beragama',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Butir Sikap ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <?= $form->field($model, 'nilai')->textInput(['type' => 'number', 'required' => 'required']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
