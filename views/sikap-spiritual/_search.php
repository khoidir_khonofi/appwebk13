<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSpiritualSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikap-spiritual-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_sikap_spiritual') ?>

    <?= $form->field($model, 'id_sekolah') ?>

    <?= $form->field($model, 'id_siswa') ?>

    <?= $form->field($model, 'butir_sikap') ?>

    <?= $form->field($model, 'nilai') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
