<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSpiritual */

$this->title = 'Create Sikap Spiritual';
$this->params['breadcrumbs'][] = ['label' => 'Sikap Spirituals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikap-spiritual-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
