<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikap-spiritual-form">

    <?php $form = ActiveForm::begin(); ?>



     <?= $form->field($model, 'butir_sikap')->widget(Select2::classname(),[
            'data' => [ 
            	'Ketaatan Beribadah' => 'Ketaatan Beribadah', 
            	'Berprilaku Syukur' => 'Berprilaku Syukur',
            	'Berdoa sebelum dan sesudah melakukan kegiatan' => 'Berdoa sebelum dan sesudah melakukan kegiatan',
            	'Toleransi dalam beragama' => 'Toleransi dalam beragama',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Butir Sikap ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <?= $form->field($model, 'nilai')->textInput(['type' => 'number', 'required' => 'required']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
