<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SikapSpiritual */

$this->title = 'Update Sikap Spiritual: ' . $model->id_sikap_spiritual;
$this->params['breadcrumbs'][] = ['label' => 'Sikap Spirituals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sikap_spiritual, 'url' => ['view', 'id' => $model->id_sikap_spiritual]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sikap-spiritual-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
