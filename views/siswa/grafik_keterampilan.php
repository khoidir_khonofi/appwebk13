<?php
 
   use dosamigos\highcharts\HighCharts;

   //use miloschuman\highcharts\Highcharts;
   /* @var $this yii\web\View */

   $this->title = 'Grafik Nilai Keterampilan';
   $jml = count($nilai);
   foreach($nilai as $values){	
      $a[0]= ($values['id_mapel']);
      $c[]= ($values['id_mapel']);
      $b[]= array('type'=> 'column', 'name' =>$values->kdketerampilan['no_kd']." ".$values->mapel['nama_mata_pelajaran'], 'data' => array((int)$values['skor']));
   }?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
   <?php
   echo
   Highcharts::widget([
      'clientOptions' => [
         'chart'=>[
            'type'=>'chart'
         ],
         'title' => ['text' => 'Nilai Harian per KD Keterampilan '.$model->nama],
         'xAxis' => [
            'categories' => ['Perolehan Nilai']
         ],
         'yAxis' => [
            'title' => ['text' => 'Nilai']
         ],
         'series' => $b
      ]
   ]); ?>
</div>
</div>
</div>
</div>
</section>