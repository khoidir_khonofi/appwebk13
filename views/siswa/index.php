<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Sekolah;
use app\models\Guru;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa';
$this->params['breadcrumbs'][] = $this->title;
?> 
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){ ?>
                        <p>
                            <button class="btn btn-info" onClick="return alert('Tambah siswa melalui menu user')">Tambah Siswa</button>
                        </p>
                    <?php }else{ ?>
                        <p>.</p>
                    <?php } ?>
                        
   
    <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_siswa',
            [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->foto != NULL) {
                         return Html::img('foto_siswa/'.$model->foto, [
                        'width' => '35x', 
                        'height' => '35px',
                        'value' => Url::to([
                            'mengajar/lihatfoto',
                            'id' => $model->id_siswa
                        ]),
                        'title' => '',
                        'class' => 'showModalButton',
                        'style' => 'border-radius:50%, border-style:solid, border-color:red'
                    ]);
                    }else{
                        return "";
                    }
                   
                }
            ],
             'nama',
            
              'nis',
            'jenis_kelamin',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->tblguru->nama;
                }
            ],
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            // [
            //     'attribute' => 'id_semester',
            //     'label' => 'Semester',
            //     'value' => function($model){
            //         return $model->semester->semester;
            //     }
            // ],
            'status',
            
           

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                      
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-info', 'style' => 'border-radius:100%;']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-success', 'style' => 'border-radius:100%;']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_siswa], [
                            'class' => 'btn btn-danger', 'style' => 'border-radius:100%;',
                            'title' => 'Delete',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                            
                            ]) ;
                            if (Yii::$app->user->identity->role == 'Administrator' || Yii::$app->user->identity->role == 'Guru') {
                                return $a." ".$b." ".$c;
                            }else{
                                return $a.' '.$b;
                            }
                            
                        }
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
        'toolbar' =>  [
             ['content'=>
             Html::button('Search', [
                'value' => Url::to(['search']), 
                'class' => 'showModalButton btn btn-default',
                'style' => 'float:left',
            ]).''.
            //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px;'])
             ],
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
       // 'floatHeaderOptions' => ['top' => $scrollingTop],
        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO
        ],
    ]);
    ?>

</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>

