<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Sekolah;
use app\models\MataPelajaran;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([
        'action'=> ['siswa/rekapnilaiketerampilan', 'id' => $model->id_siswa],
        'method' => 'GET',
    ]); ?>

        <?= Select2::widget([
            'name' => 'id_mapel',
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $model->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'disabled' => false
        ]);
        ?>
        
        
    <div class="form-group" style="float: right;">
        <input type="submit" class="btn btn-success" value="lihat">
    </div>
   <?php ActiveForm::end(); ?> 


  
       

    


</div>
