<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use app\models\Sekolah;
use app\models\Siswa;
use app\models\Guru;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa';
$this->params['breadcrumbs'][] = $this->title;
?>
 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
                

    <h1><?= Html::encode($this->title) ?></h1>

   
      <?php if(Yii::$app->user->identity->role=="Guru"){ ?>
            <p>
                <?= Html::button('Tambah Siswa', [
                    'value' => Url::to(['create']),
                    'title' => '', 
                    'class' => 'showModalButton btn btn-info'
                    ]);
                ?>
            </p>
        <?php }else{ ?>
                <p>.</p>
         <?php } ?>

   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             'nama',
              'nis',
            'jenis_kelamin',
            [
                'attribute' => 'id_guru',
                'label' => 'Wali Kelas',
                'value' => function($model){
                    return $model->tblguru->nama;
                }
            ],
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                            
                            $d = Html::a('Rekap Nilai Keterampilan', ['pilihmapel','id' => $model->id_siswa], ['class' => 'btn btn-info hvr-float-shadow']) ;
                            
                            $e = Html::button('Rekap Nilai Pengetahuan', [
                                    'value' => Url::to(['pilihmapel', 'id' => $model->id_siswa]),
                                    'title' => '', 
                                    'class' => 'showModalButton btn btn-warning'
                                    ]);
                            $g = Html::a('Rekap Nilai Keterampilan', ['pilihmapelsekolahmenengah','id' => $model->id_siswa], ['class' => 'btn btn-info']) ;
                            $j = Html::a('Rekap Nilai Pengetahuan', ['pilihmapelsekolahmenengah','id' => $model->id_siswa], ['class' => 'btn btn-primary']) ;

                            $h = ButtonDropdown::widget([
                                'label' => 'Lihat Grafik',
                                'dropdown' => [
                                    'items' => [
                                        ['label' => 'Pengetahuan',
                                            'url' => ['grafik','id'=>$model->id_siswa],
                                        ],
                                        ['label' => 'Keterampilan',
                                            'url' => ['grafikktr','id'=>$model->id_siswa],
                                        ],
                                        ],
                                    ],
                                
                            ]);
                            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

                            if (Yii::$app->user->identity->role=="Administrator" AND $sekolah->jenjang_pendidikan=="SD") {
                                return $h;
                            }elseif (Yii::$app->user->identity->role=="Guru" AND $guru->tblsekolah->jenjang_pendidikan=="SD") {
                                return $h;
                            }elseif(Yii::$app->user->identity->role=="Siswa" AND $siswa->tblsekolah->jenjang_pendidikan=="SD"){
                                return $h;
                            }else{
                                return $g." ".$j." ".$h;
                            }
                           
                        

                            
                        }
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>