<?php
use app\models\Kelas;
use app\models\SdNilaiHarianKeterampilan;
$this->title="Rekap Nilai Keterampilan";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Rekap Nilai Keterampilan</title>
</head>
<body>
	 <section class="content">
      <div class="row">
        <div class="col-xs-12">
 <div class="box">
                 
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr style="text-align: center;">
                  <th rowspan="3" style="text-align: center;">No</th>
                  <th rowspan="3" style="text-align: center;">KD</th>
                  <th colspan ="<?= count($jenis) ?>" style="text-align: center;">Penilaian Harian</th>
                <tr>
                  <?php foreach ($tema as $key => $t) { ?>
                    <th colspan="<?= count($jenispenilaian) ?>" style="text-align: center;"><?= $t->id_tema ?></th>
                  <?php } ?>
                </tr>
                <tr>
                  <?php foreach ($jenis as $key => $js) { ?>
                    <th style="text-align: center;"><?= $js->teknik->jenis_penilaian ?></th>
                  <?php } ?>
                </tr>
               
                </thead>

                <tbody>
                   
                </tbody>
                  
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
</div>
</div>
</section>

</body>
</html>