<?php
use app\models\Kelas;
use app\models\SdNilaiHarianKeterampilan;
$this->title="Rekap Nilai Keterampilan";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Rekap Nilai Keterampilan</title>
</head>
<body>
	 <section class="content">
      <div class="row">
        <div class="col-xs-12">
 <div class="box">
                 
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr style="text-align: center;">
                  <th rowspan="2" style="text-align: center;">No</th>
                  <th rowspan="2" style="text-align: center;">KD</th>
                  <th colspan ="3" style="text-align: center;">Penilaian Harian</th>
                </tr>
                <tr>
                  <?php foreach ($th as $key => $value) { ?>
                    <th><?= $value->nama_penilaian ?></th>
                  <?php } ?>
                </tr>
                <tr>
                  <?php $no = 1;foreach ($nilai as $key => $x) { ?>
                    <td><?= $no++ ?></td>
                    <td><?= $x->kdPengetahuan->no_kd ?></td>
                    <td><?= $x->nilai ?></td>
                  <?php } ?>
                </tr>
                </thead>

                <tbody>
                   
                </tbody>
                  
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
</div>
</div>
</section>

</body>
</html>