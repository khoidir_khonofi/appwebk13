<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Kelas;
use app\models\Sekolah;
use app\models\Saran;
use app\models\Guru;

$this->title = 'Telegram';

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">

<?php $form = ActiveForm::begin([
    'action' => ['siswa/telegram'],
    'method' => 'GET'
]); ?>
<?php   if(Yii::$app->user->identity->role=="Administrator"){
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
<?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih Kelas') ?>
<?php }else{ 
    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
?> 
        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_guru' => $guru->id_guru])->andWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih Kelas') ?>
<?php } ?>

<div class="form-group" style="float:right;">
    <?= Html::a('Telegram', ['telegramnilai', 'id' => $model->id_kelas], ['class' => 'btn btn-info']) ?>

    <?= Html::a('Telegram Foto', ['telegramfoto', 'id' => $model->id_kelas], ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</section>