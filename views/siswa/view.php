<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Siswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="siswa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_user',
                'label' => 'Id User',
                'value' => function($model){
                    return $model->idUser->id_user;
                }
            ],
            'id_siswa',
            [
                'attribute' => 'username',
                'label' => 'Username',
                'value' => function($model){
                    return $model->idUser->username;
                }  
             ],
           
            'nis',
            'nama',
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
             [
                'attribute' => 'foto',
                'label' => 'Foto Siswa',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img('foto_siswa/'.$model->foto, ['width' => '150px', 'height' => '150px', 'border-radius' => '15px']);
                }
            ],
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'agama',
            'alamat:ntext',
            'nama_ayah',
            'nama_ibu',
            'pekerjaan_ayah',
            'pekerjaan_ibu',
            'no_hp_orang_tua',
            'chat_id_telegram',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
