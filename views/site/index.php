<?php
use app\models\Sekolah;
use app\models\Guru;
use app\models\User;
use app\models\Pengumuman;
use app\models\Siswa;
/* @var $this yii\web\View */

$this->title = 'App Smart-K13';
?>
<div class="site-index" style="background-color: white;">

    <div class="jumbotron" style="background-color: white;">
        <h1>SMART-K13</h1>
        
        <p class="lead">Aplikasi Kurikulum Tahun 2013 Berbasis Web Berdasarkan Permendikbud No 24 Tahun 2016</p>
        <?php 
        $skl = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

          if(Yii::$app->user->identity->role=="Administrator"){ ?>
            <p class="lead"><?= $skl->nama_sekolah ?></p>
          <?php }elseif(Yii::$app->user->identity->role=="Guru"){ ?>
            <p class="lead"><?= $guru->tblsekolah->nama_sekolah ?></p>
          <?php } else { ?>
            <p class="lead"><?= $siswa->tblsekolah->nama_sekolah ?></p>
          <?php } ?>
          <p><?= date('d - m - Y'); ?></p>
        <p><a class="btn btn-lg btn-info" href="<?= Yii::$app->urlManager->createUrl(['web']) ?>">Lets start with Choidir</a></p>
    </div>

       
 <?php
    if(Yii::$app->user->identity->role=="Administrator"){
        $test = Sekolah::find()->where(['id_user' => Yii::$app->user->identity])->all();
        if (count($test) != 0) { 
            echo "";
      
        }else{ ?>
            <center>
          <div class="alert alert-info alert-dismissible" style="width: 50%;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Info!</h4>
               Lengkapi Data Sekolah Anda Sekarang <a href="<?= Yii::$app->urlManager->createUrl(['sekolah']) ?>"> Disini </a>
              </div>
        </center>

      <?php  } 

  }else{
        echo "";
  }?>
 

   

  
<div class="row">
  <h3 style="text-align: center; font-family: sans-serif;">Pengumuman</h3>
        <div class="col-md-12" >
          <!-- Box Comment -->
            <?php
            $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
            if (Yii::$app->user->identity->role=="Administrator") {
              $p = Pengumuman::find()->where(['id_sekolah' => $sekolah->id_sekolah])->orderBy(['id_pengumuman' => SORT_DESC])->all();
            }elseif (Yii::$app->user->identity->role=="Guru") {
               $p = Pengumuman::find()->where(['id_sekolah' => $guru->id_sekolah])->orderBy(['id_pengumuman' => SORT_DESC])->all();
            }else{
               $p = Pengumuman::find()->where(['id_sekolah' => $siswa->id_sekolah])->orderBy(['id_pengumuman' => SORT_DESC])->all();
            }
              
              foreach ($p as $key => $value) {
            ?>
          <div class="box box-widget" style="background-color: #f6f7f7; float: left; width: 31%; margin: 10px;">
            <div class="box-header with-border" style="background-color: #e5eeff;">
              <div class="user-block">

                      <img class="img-responsive pad" src="foto_sekolah/<?= $value->sekolah->foto ?>" alt="Photo"style="border-radius: 100%; height: 60px; width: 60px;">

                <span class="username"><a href="#">Admin Sekolah</a></span>
                <span class="description">Shared at - <?= date('d F Y, h:i:s A', strtotime($value->created_at)) ?></span>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($value->gambar != NULL){ ?>
                  <img class="img-responsive pad" src="gambar_pengumuman/<?= $value['gambar'] ?>" alt="Photo" >
              <?php }else{ ?>
                  
              <?php } ?>

              <p><?= $value['isi'] ?></p>
            </div>
         
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
            <?php } ?>
          <!-- /.box -->
        </div>
      </div>
      
   
</div>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>