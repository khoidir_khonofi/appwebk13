<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use app\models\Service;
use app\assets\BackendAsset;

BackendAsset::register($this);
?>
<?php 
$this->title = 'Login';
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<body class="content">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php" style="color:white;"><b>Selamat Datang Di</b> SMART-K13</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="border-radius: 5px;">
  <p class="login-box-msg">Sign in to start your session</p>
   <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'type' => ActiveForm::TYPE_VERTICAL,
       'fieldConfig' => ['autoPlaceholder' => true],
       
    ]); ?>

       <?= $form->field($model, 'username',[
        'feedbackIcon' => [
            'default' => 'user',
            'success' => 'user-plus',
            'error' => 'user-times',
            'defaultOptions' => ['class' => 'text-warning'],
        ],
    ])->textInput(['maxlength' => true, 'placeholder' => 'Enter Username']) ?>

        <?= $form->field($model, 'password',[
        'feedbackIcon' => [
            'default' => 'lock',
            'success' => 'lock-plus',
            'error' => 'lock-times',
            'defaultOptions' => ['class' => 'text-warning'],
        ],
    ])->passwordInput() ?>

       
        <div class="form-group">
            
                <button type="Submit" class="btn btn-info"><i class="fa fa-sign-in"></i> Login</button>
                <button type="Reset" class="btn btn-success"><i class="fa fa-reset"></i> Reset</button>
         
        </div>

    <?php ActiveForm::end(); ?>
       <p><a href="" onclick = "return alert('OTW!!!!!!!! BESOK BUAT');">Lupa Password ?</a></p>
        <p>Belum Punya Akun ? <a href="<?= Yii::$app->urlManager->createUrl(['site/register']) ?>">Daftar Disini</p></a>
       

           
   
    <!-- /.social-auth-links -->

   

  </div>
  <!-- /.login-box-body -->
</div>

<!-- /.login-box -->
<script type="text/javascript">
  function pindah(url)
  {
    window.location = url;
  }
</script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
</body>


<style type="text/css">
  body{
  background-color: #f1c40f;
  -webkit-animation: color 5s ease-in  0s infinite alternate running;
  -moz-animation: color 5s linear  0s infinite alternate running;
  animation: color 5s linear  0s infinite alternate running;
}

/* Animasi + Prefix untuk browser */
@-webkit-keyframes color {
    0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
@-moz-keyframes color {
     0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
@keyframes color {
  0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
</style>

