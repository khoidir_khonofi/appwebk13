<?php
use app\models\User;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Siswa;



?>
<center>
<div class="row" style="margin-left: 150px;">
        <div class="col-md-8">

          <!-- Profile Image -->
          <div class="box box-primary" style="background: #e8eff1;">
            <div class="box-body box-profile">
              <?php 
              $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
              $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
              $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

              if(Yii::$app->user->identity->role=="Administrator"){ ?>
                        <img class="profile-user-img img-responsive img-circle" src="foto_sekolah/<?= $sekolah->foto ?>" alt="User profile picture">
                       
                        <h3 class="profile-username text-center"><?=   Yii::$app->user->identity->username ?></h3>

                        <p class="text-muted text-center"><?= Yii::$app->user->identity->role ?>(<?= Yii::$app->user->identity->id_user ?>)</p>

                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item" style="padding-right: 5px;">
                            <b>Join</b> <a class="pull-right"><?= Yii::$app->user->identity->created_at ?></a>
                          </li>
                          <li class="list-group-item" style="padding-right: 5px;">
                            <b>last Update</b> <a class="pull-right"><?= Yii::$app->user->identity->updated_at ?></a>
                          </li>
                        </ul>
                <?php }elseif(Yii::$app->user->identity->role=="Guru"){ ?>
                        <img class="profile-user-img img-responsive img-circle" src="foto_guru/<?= $guru->foto ?>" alt="User profile picture">
                             
                              <h3 class="profile-username text-center"><?= $guru->nama ?></h3>

                              <p class="text-muted text-center"><?= Yii::$app->user->identity->role ?>(<?= $guru->nip ?>)</p>

                              <ul class="list-group list-group-unbordered">
                                <li class="list-group-item" style="padding-right: 5px;">
                                  <b>Join</b> <a class="pull-right"><?= Yii::$app->user->identity->created_at ?></a>
                                </li>
                                <li class="list-group-item" style="padding-right: 5px;">
                                  <b>last Update</b> <a class="pull-right"><?= Yii::$app->user->identity->updated_at ?></a>
                                </li>
                              </ul>

                <?php }else{ ?>
                       <img class="profile-user-img img-responsive img-circle" src="foto_siswa/<?= $siswa->foto ?>" alt="User profile picture">
                             
                              <h3 class="profile-username text-center"><?= $siswa->nama ?></h3>

                              <p class="text-muted text-center"><?= Yii::$app->user->identity->role ?>(<?= $siswa->nis ?>)</p>

                              <ul class="list-group list-group-unbordered">
                                <li class="list-group-item" style="padding-right: 5px;">
                                  <b>Join</b> <a class="pull-right"><?= Yii::$app->user->identity->created_at ?></a>
                                </li>
                                <li class="list-group-item" style="padding-right: 5px;">
                                  <b>last Update</b> <a class="pull-right"><?= Yii::$app->user->identity->updated_at ?></a>
                                </li>
                              </ul>
                <?php } ?>

              <a href="#" class="btn btn-info btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
</center>