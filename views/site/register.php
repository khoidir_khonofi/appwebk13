<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use app\models\User;
use app\models\Sekolah;
use kartik\select2\Select2;
use app\models\Kelas;
use app\models\Guru;
use yii\helpers\ArrayHelper;
use app\assets\BackendAsset;

$this->title = 'Register';
BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<body class="content">
<div class="register-box">
  <div class="login-logo">
    <a href="#" style="color:white;"><b>Selamat Datang Di</b> SMART-K13</a>

  </div>
  <!-- /.login-logo -->
  <div class="register-box-body" style="border-radius: 5px;">
    <p class="login-box-msg">Daftarkan akun anda disini</p>

   <?php
    $form = ActiveForm::begin([
        'id' => 'form-signup',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'fieldConfig' => ['autoPlaceholder' => true],
       
    ]); ?>

         <?=  $form->field($model, 'email', [
            'addon' => ['prepend' => ['content'=>'@']]
        ]);?>

        <?= $form->field($model, 'username', [
          'feedbackIcon' => [
              'default' => 'user',
              'success' => 'user-plus',
              'error' => 'user-times',
              'defaultOptions' => ['class'=>'text-warning']
          ]
      ])->textInput(['placeholder'=>'Enter username...']); ?>

        <?= $form->field($model, 'password', [
          'feedbackIcon' => [
              'default' => 'lock',
              'success' => 'user-plus',
              'error' => 'user-times',
              'defaultOptions' => ['class'=>'text-warning']
          ]
      ])->textInput(['placeholder'=>'Enter Password...', 'type' => 'password']); ?>
        
        <?= Select2::widget([
            'model' => $model,
            'attribute' => 'role',
            'data' => ['Administrator' => 'Sekolah', 'Guru' => 'Guru', 'Siswa' => 'Siswa'],
            'options' => ['placeholder' => 'Mendaftar Sebagai...', 'id'=>'status'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        <br>
          <div style="display: none" id="data-sekolah">
               <?= $form->field($guru, 'id_sekolah')->widget(Select2::classname(),[
                  'data' => ArrayHelper::map(Sekolah::find()->all(),'id_sekolah', 'nama_sekolah'),
                  'language' => 'de',
                  'options' => ['placeholder' => 'Pilih Sekolah ...'],
                  'pluginOptions' => [
                      'allowClear' => true,
                      
                  ],
              ]) ?>
          </div>

          <div style="display: none; margin-bottom: 10px;" id="jenjangpendidikan">
              <?= Select2::widget([
                  'model' => $sekolah,
                  'attribute' => 'jenjang_pendidikan',
                  'data' => ['SD' => 'SD', 'SMP' => 'SMP', 'SMA' => 'SMA'],
                  'options' => ['placeholder' => 'Jenjang Pendidikan...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]); ?>
          </div>


        <div style="display: none" id="nonpsn">
              <?= $form->field($sekolah, 'npsn')->textInput(['maxlength' => true]) ?>
        </div>

          <div style="display: none" id="data-sekolahsiswa">
               <?= $form->field($siswa, 'id_sekolah')->widget(Select2::classname(),[
                  'data' => ArrayHelper::map(Sekolah::find()->all(),'id_sekolah', 'nama_sekolah'),
                  'language' => 'de',
                  'options' => [
                    'placeholder' => 'Pilih Sekolah ...',
                    'onchange'=>'
                       $.post( "index.php?r=site/kelas&id='.'"+$(this).val(), function( data ){
                       $("select#test").html(data);
                      });'
                    ],
                  'pluginOptions' => [
                      'allowClear' => true,
                      
                  ],
              ]) ?>
          </div>

          <div style="display: none" id="kelassiswa">
              <?= $form->field($siswa, 'id_kelas')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->all(),'id_kelas', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Kelas ...','id' => 'test',
                 'onchange'=>'
                       $.post( "index.php?r=site/guru&id='.'"+$(this).val(), function( data ){
                       $("select#cikgu").html(data);
                      });'
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
          </div>


        

          <div style="display: none" id="data-gurusiswa">
             <?= $form->field($siswa, 'id_guru')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Guru::find()->all(),'id_guru', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Guru ...','id' => 'cikgu'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
          </div>
      
        <div class="form-group" style=" margin-left: 1px;">
            
               <?= Html::submitButton('Daftar', ['class' => 'btn btn-info']) ?>
               <button type="Reset" class="btn btn-success">Reset</button>
         
        </div>

    <?php ActiveForm::end(); ?>
   
    <!-- /.social-auth-links -->
    <p>Back to <a href="<?= Yii::$app->urlManager->createUrl(['site/login']) ?>">Login</a></p>

   

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
</body>


<style type="text/css">
  body{
  background-color: #f1c40f;
  -webkit-animation: color 5s ease-in  0s infinite alternate running;
  -moz-animation: color 5s linear  0s infinite alternate running;
  animation: color 5s linear  0s infinite alternate running;
}

/* Animasi + Prefix untuk browser */
@-webkit-keyframes color {
    0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
@-moz-keyframes color {
     0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
@keyframes color {
  0% { background-color: #f1c40f; }
    32% { background-color: #e74c3c; }
    55% { background-color: #9b59b6; }
    76% { background-color: #16a085; }
    100% { background-color: #2ecc71; }
}
</style>

<script type="text/javascript">
  
$( document ).ready(function() {
  $('#status').change(function(){
    if($(this).val()=="Guru"){
      $('#nonpsn').hide('slow');
      $('#jenjangpendidikan').hide('slow');
      $('#data-sekolahsiswa').hide('slow');
      $('#data-gurusiswa').hide('slow');
      $('#data-sekolah').show('slow');
      $('#kelassiswa').hide('slow');

    }else if($(this).val()=="Siswa"){
      $('#nonpsn').hide('slow');
      $('#jenjangpendidikan').hide('slow');
      $('#data-sekolah').hide('slow');
      $('#data-sekolahsiswa').show('slow');
      $('#data-gurusiswa').show('slow');
      $('#kelassiswa').show('slow');

    }else if($(this).val()=="Administrator"){
      $('#jenjangpendidikan').show('slow');
      $('#nonpsn').show('slow');
      $('#data-sekolah').hide('slow');
      $('#data-gurusiswa').hide('slow');
      $('#data-sekolahsiswa').hide('slow');
      $('#kelassiswa').hide('slow');
      
    }else{
      $('#nonpsn').hide('slow');
      $('#data-sekolah').hide('slow');
      $('#jenjangpendidikan').hide('slow');
      $('#data-gurusiswa').hide('slow');
      $('#data-sekolahsiswa').hide('slow');
      $('#kelassiswa').hide('slow');
    }
  })
});

</script>