<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\TemaKeterampilan;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TeknikPenilaianKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sd-nilai-harian-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
    ?>


    <?= $form->field($model, 'id_siswa')->textInput(['value' => $siswa->id_siswa, 'type' => 'hidden'])->label(false) ?>
  

    <?= $form->field($model, 'nilai')->textInput() ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
