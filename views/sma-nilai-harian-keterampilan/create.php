<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmaNilaiHarianKeterampilan */

$this->title = 'Create Sma Nilai Harian Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Sma Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sma-nilai-harian-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
