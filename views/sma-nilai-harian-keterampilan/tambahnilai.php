<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sd-nilai-harian-pengetahuan-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        //$v = $model->=nama_mata_pelajaran;

        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
        $siswa = Siswa::find()->where(['id_siswa' => $idsiswa])->one();
    ?>


    <?php if(Yii::$app->user->identity->role=="Administrator"){ ?>

        <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...', 'required' => 'required',
                        'onchange'=>'
                           $.post( "index.php?r=sma-nilai-harian-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_keterampilan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_keterampilan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'required' => 'required', 'id'=> 'kade'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>

    <?php }else{ ?>
             <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...','required' => 'required',
                        'onchange'=>'
                           $.post( "index.php?r=sma-nilai-harian-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_keterampilan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_kd_keterampilan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'required' => 'required', 'id'=> 'kade'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>
    <?php } ?>

    <?php
        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
    ?>
    <?= $form->field($model, 'id_siswa')->textInput(['value' => $idsiswa ,'type' => 'hidden'])->label(false) ?>

    <?= $form->field($model, 'nama_penilaian')->widget(Select2::classname(),[
            'data' => [ 
                'Proyek' => 'Proyek',
                'Produk' => 'Produk', 
                'Praktek' => 'Praktek',
                'Portofolio' => 'Portofolio',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Jenis Penilaian ...', 'required' => 'required'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Jenis Penilaian') ?>

    <?= $form->field($model, 'nilai')->textInput(['required' => 'required']) ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
