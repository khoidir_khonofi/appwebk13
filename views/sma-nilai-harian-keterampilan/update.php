<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmaNilaiHarianKeterampilan */

$this->title = 'Update Sma Nilai Harian Keterampilan: ' . $model->id_nilai_harian_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Sma Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_harian_keterampilan, 'url' => ['view', 'id' => $model->id_nilai_harian_keterampilan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sma-nilai-harian-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
