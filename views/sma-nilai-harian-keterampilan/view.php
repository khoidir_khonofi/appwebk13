<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SmaNilaiHarianKeterampilan */

$this->title = $model->id_nilai_harian_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Sma Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sma-nilai-harian-keterampilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_nilai_harian_keterampilan',
            'id_kd_keterampilan',
            'id_semester',
            'id_sekolah',
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'format' => 'html',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas. " ".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_semester',
                'label' => 'Semester',
                'format' => 'html',
                'value' => function($model){
                    return $model->semester->semester;
                }
            ],
            [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    return $model->siswa->nama;
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            [
                'attribute' => 'id_kd_keterampilan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdKeterampilan->no_kd." ".$model->kdKeterampilan->judul;
                }
            ],
            'nama_penilaian',
            'nilai',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
