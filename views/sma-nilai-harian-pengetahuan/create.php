<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmaNilaiHarianPengetahuan */

$this->title = 'Create Sma Nilai Harian Pengetahuan';
$this->params['breadcrumbs'][] = ['label' => 'Sma Nilai Harian Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sma-nilai-harian-pengetahuan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
