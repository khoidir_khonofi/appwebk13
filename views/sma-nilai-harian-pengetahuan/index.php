<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use app\models\Guru;
use app\models\Siswa;
use app\models\NilaiKdPengetahuanSekolahMenengah;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai Harian Pengetahuan';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
    date_default_timezone_set('Asia/Jakarta');  //tambahan dari agan  AntiCoding :)
    function time_since($original)
    {
    $chunks = array(
        array(60 * 60 * 24 * 365, 'tahun'),
        array(60 * 60 * 24 * 30, 'bulan'),
        array(60 * 60 * 24 * 7, 'minggu'),
        array(60 * 60 * 24, 'hari'),
        array(60 * 60, 'jam'),
        array(60, 'menit'),
    );

    $today = time();
    $since = $today - $original;

    // if ($since > 604800)
    // {
    //   $print = date("M jS", $original);
    //   if ($since > 31536000)
    //   {
    //     $print .= ", " . date("Y", $original);
    //   }
    //   return $print;
    // }

    for ($i = 0, $j = count($chunks); $i < $j; $i++)
    {
        $seconds = $chunks[$i][0];
        $name = $chunks[$i][1];

        if (($count = floor($since / $seconds)) != 0)
        break;
    }

    $print = ($count == 1) ? '1 ' . $name : "$count {$name}";
    return $print . ' yang lalu';
    }
    // echo time_since(strtotime($model->tanggal)); 
?>  
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
    <?php $form = ActiveForm::begin(); ?>
        <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           [
                'attribute' => 'id_siswa',
                'label' => 'Siswa',
                'format' => 'html',
                'value' => function($model){
                    $nilaikd = NilaiKdPengetahuanSekolahMenengah::find()->where(['id_siswa' => $model->id_siswa])->one()['id_siswa'];
                    $url = Yii::$app->urlManager->createUrl(['nilai-kd-pengetahuan-sekolah-menengah/lihatnilai', 'id' => $nilaikd]);

                    return "<a href='".$url."'>".$model->siswa->nama."</a>";
                }
            ],
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdPengetahuan->no_kd;
                }
            ],
           'nama_penilaian',
            'nilai',
            [
                'attribute' => 'created_at',
                'label' => 'Waktu',
                'format' => 'html',
                'value' => function($model){
                    return time_since(strtotime($model->created_at));
                }
            ],

           ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                        
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_nilai_harian_pengetahuan]),
                                'title' => '', 'class' => 'showModalButton btn btn-info']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_nilai_harian_pengetahuan]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_nilai_harian_pengetahuan], [
                                    'class' => 'btn btn-danger',
                                    'title' => 'Delete',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                    
                                    ]) ;
                          
                            if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){
                                return $a." ".$b." ".$c;
                            }else{
                                return $a;
                            }
                            
                        
                            
                        }
                ],
                

        
        ],
        [
            'class' => 'kartik\grid\checkboxColumn',
            'name' => 'hapussemua',
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
         Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari'
         ]). '  '.
         Html::a('<span class="fa fa-trash"></span>',['hapus'],['class' => 'btn btn-danger', 'data-confirm' => 'Are you sure you want to delete this item?', 'data-method' => 'POST', 'style' => 'margin-left:5px']).' '.
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left:5px;'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
</section>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>  