<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Tema;
use app\models\Subtema;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarPengetahuan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sma-nilai-harian-pengetahuan-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        
        //$v = $model->=nama_mata_pelajaran;
    ?>
  <?php
    
        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
        $siswa = Siswa::find()->where(['id_siswa' => $idsiswa])->one();
    ?>
    

    <?php if(Yii::$app->user->identity->role=="Administrator"){ ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...', 'required' => 'required',
                        'onchange'=>'
                           $.post( "index.php?r=sma-nilai-harian-pengetahuan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_pengetahuan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_pengetahuan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'id'=> 'kade', 'required' => 'required'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>

    <?php }else{
    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
             <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...', 'required' => 'required',
                        'onchange'=>'
                           $.post( "index.php?r=sma-nilai-harian-pengetahuan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Mapel') ?>

    <?= $form->field($model,  'id_kd_pengetahuan')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_kd_pengetahuan', 'no_kd'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih KD ...', 'id'=> 'kade', 'required' => 'required'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih KD') ?>
    <?php } ?>

     <?php
        if(isset($_GET['id'])){
          $idsiswa = $_GET['id'];
        } else {
          $idsiswa = '';
        }
    ?>

    <?= $form->field($model, 'id_siswa')->textInput(['value' => $idsiswa, 'type' => 'hidden' ])->label(false) ?>

   
      <?= $form->field($model, 'nama_penilaian')->textInput(['required' => 'required','placeholder' => 'Contoh: Harian 1'])->label('Nama Penilaian'); ?>
             

    <?= $form->field($model, 'nilai')->textInput(['required' => 'required','placeholder' => 'Angka']) ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
