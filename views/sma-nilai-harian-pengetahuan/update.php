<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmaNilaiHarianPengetahuan */

$this->title = 'Update Sma Nilai Harian Pengetahuan: ' . $model->id_nilai_harian_pengetahuan;
$this->params['breadcrumbs'][] = ['label' => 'Sma Nilai Harian Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_harian_pengetahuan, 'url' => ['view', 'id' => $model->id_nilai_harian_pengetahuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sma-nilai-harian-pengetahuan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
