<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\TemaKeterampilan;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;
use app\models\TeknikPenilaianKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sd-nilai-harian-keterampilan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $siswa = Siswa::find()->where(['id_siswa' => $model->id_siswa])->one();
    ?>


            <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...',
                        'onchange'=>'
                           $.post( "index.php?r=smp-nilai-harian-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                           $("select#kade").html(data);
                          });'
                        ],
                'pluginOptions' => [
                    'allowClear' => true,
                
            ],
            ])->label('Pilih Mapel') ?>

            <?= $form->field($model,  'id_kd_keterampilan')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_kelas' => $siswa->id_kelas])->all(),'id_kd_keterampilan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'kade'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

           

              <?= $form->field($model, 'nama_penilaian')->widget(Select2::classname(),[
                    'data' => [ 
                        'Proyek' => 'Proyek',
                        'Produk' => 'Produk', 
                        'Praktek' => 'Praktek',
                        'Portofolio' => 'Portofolio',
                    ],
                    'language' => 'de',
                    'options' => ['placeholder' => 'Pilih Jenis Penilaian ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        
                    ],
                ]) ?>
    
    <?= $form->field($model, 'id_siswa')->textInput(['value' => $siswa->id_siswa, 'type' => 'hidden'])->label(false) ?>
  

    <?= $form->field($model, 'nilai')->textInput() ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
