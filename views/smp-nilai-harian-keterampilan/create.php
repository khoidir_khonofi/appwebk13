<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmpNilaiHarianKeterampilan */

$this->title = 'Create Smp Nilai Harian Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Smp Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smp-nilai-harian-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
