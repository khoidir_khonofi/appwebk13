<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmpNilaiHarianKeterampilan */

$this->title = 'Update Smp Nilai Harian Keterampilan: ' . $model->id_nilai_harian_keterampilan;
$this->params['breadcrumbs'][] = ['label' => 'Smp Nilai Harian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_nilai_harian_keterampilan, 'url' => ['view', 'id' => $model->id_nilai_harian_keterampilan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smp-nilai-harian-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
