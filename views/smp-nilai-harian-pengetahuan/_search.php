<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\TemaKeterampilan;
use app\models\Siswa;
use app\models\KompetensiDasarPengetahuan;
use app\models\TeknikPenilaianKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sd-nilai-harian-keterampilan-form">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'GET'
    ]); ?>

    <?php 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    if (Yii::$app->user->identity->role=="Administrator") { ?>
             <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Siswa ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Siswa') ?>
            <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Mapel ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                
            ],
            ])->label('Pilih Mapel') ?>

            <?= $form->field($model,  'id_kd_pengetahuan')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_pengetahuan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

            <?php }else{
                 $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
                 $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
            ?>
             <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Siswa::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_siswa', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Siswa ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
        ])->label('Pilih Siswa') ?>
            <?= $form->field($model,  'id_mapel')->widget(Select2::classname(),[
                    'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Mapel ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        
                    ],
            ])->label('Pilih Mapel') ?>

            <?= $form->field($model,  'id_kd_pengetahuan')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_kd_pengetahuan', 'judul'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>
            <?php } ?>
  

    <?= $form->field($model, 'nilai')->textInput() ?>

   

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
