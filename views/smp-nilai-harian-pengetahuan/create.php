<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmpNilaiHarianPengetahuan */

$this->title = 'Create Smp Nilai Harian Pengetahuan';
$this->params['breadcrumbs'][] = ['label' => 'Smp Nilai Harian Pengetahuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smp-nilai-harian-pengetahuan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
