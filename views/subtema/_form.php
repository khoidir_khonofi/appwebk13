<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\Tema;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarPengetahuan;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subtema-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->role=="Administrator"){ 
         $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>
        <?= $form->field($model,  'id_kelas')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Kelas ...',
                'onchange'=>'
                               $.post( "index.php?r=tema/mapel&id='.'"+$(this).val(), function( data ){
                               $("select#mapel").html(data);
                              });'
                            ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#test").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

        
        <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_pengetahuan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'test',
                    'onchange'=>'
                       $.post( "index.php?r=subtema/tema&id='.'"+$(this).val(), function( data ){
                       $("select#themes").html(data);
                      });'
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

         <?= $form->field($model,  'id_tema')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Tema::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_tema', 'tema'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tema ...', 'id' => 'themes'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Tema') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();    
    ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#kade").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

          <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_kd_pengetahuan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'kade',
                    'onchange'=>'
                       $.post( "index.php?r=subtema/tema&id='.'"+$(this).val(), function( data ){
                       $("select#themes").html(data);
                      });'
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

         <?= $form->field($model,  'id_tema')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Tema::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_tema', 'tema'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tema ...', 'id' => 'themes'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Tema') ?>
    <?php } ?>

    <?= $form->field($model, 'subtema')->textInput(['maxlength' => true, 'placeholder' => 'Subtema 1']) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
