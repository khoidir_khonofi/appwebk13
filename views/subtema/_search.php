<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubtemaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subtema-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_subtema') ?>

    <?= $form->field($model, 'id_sekolah') ?>

    <?= $form->field($model, 'id_kelas') ?>

    <?= $form->field($model, 'id_mapel') ?>

    <?= $form->field($model, 'id_kd') ?>

    <?php // echo $form->field($model, 'id_tema') ?>

    <?php // echo $form->field($model, 'subtema') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
