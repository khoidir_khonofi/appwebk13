<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */

$this->title = 'Create Subtema';
$this->params['breadcrumbs'][] = ['label' => 'Subtemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subtema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
