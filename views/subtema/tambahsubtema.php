<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subtema-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'subtema')->textInput(['maxlength' => true, 'placeholder' => 'Subtema 1']) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
