<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */

$this->title = 'Update Subtema: ' . $model->id_subtema;
$this->params['breadcrumbs'][] = ['label' => 'Subtemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_subtema, 'url' => ['view', 'id' => $model->id_subtema]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subtema-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
