<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */

$this->title = $model->id_subtema;
$this->params['breadcrumbs'][] = ['label' => 'Subtemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="subtema-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_subtema',
            'id_sekolah',
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas;
                }
            ],
            [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
             [
                'attribute' => 'id_kd',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kd->judul;
                }
            ],
             [
                'attribute' => 'id_tema',
                'label' => 'Tema',
                'value' => function($model){
                    return $model->tema->tema;
                }
            ],
            'subtema',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
