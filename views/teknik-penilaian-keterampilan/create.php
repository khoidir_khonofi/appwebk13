<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeknikPenilaianKeterampilan */

$this->title = 'Create Teknik Penilaian Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Teknik Penilaian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teknik-penilaian-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
