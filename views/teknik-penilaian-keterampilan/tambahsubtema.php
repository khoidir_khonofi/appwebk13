<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\TemaKeterampilan;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\Subtema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subtema-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'jenis_penilaian')->textInput(['maxlength' => true, 'placeholder' => 'Contoh: Proyek']) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
