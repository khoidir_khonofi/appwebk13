<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TeknikPenilaianKeterampilan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Teknik Penilaian Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="teknik-penilaian-keterampilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_sekolah',
            'id_kelas',
            'id_mapel',
            'id_kd',
            'jenis_penilaian',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
