<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\SikapSosial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telegram-form">

     <?php $form = ActiveForm::begin([
        'action' => ['create'],
        'method' => 'POST'
     ]); ?>

    
    <?= $form->field($model, 'api_telegram')->textInput() ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
