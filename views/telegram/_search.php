<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Sekolah;
use kartik\date\DatePicker;
use app\models\Users;
use app\models\Kelas;
use app\models\Semester;
use app\models\Siswa;
use app\models\Guru;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\SiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<?php   if(Yii::$app->user->identity->role=="Administrator"){
        $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>

             <?= $form->field($model, 'id_guru')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Guru::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_guru', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Wali Kelas ...',
                    'onchange'=>'
                       $.post( "index.php?r=siswa/kelas&id='.'"+$(this).val(), function( data ){
                       $("select#clas").html(data);
                      });'
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Wali Kelas') ?>

            <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...','id' =>'clas',
                    'onchange'=>'
                       $.post( "index.php?r=siswa/semester&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->andWhere(['periode_aktif' => 'Aktif'])->all(),'id_semester', 'semester'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Semester') ?>

    <?php }elseif(Yii::$app->user->identity->role=="Guru"){
         $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>

        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
                    'onchange'=>'
                       $.post( "index.php?r=siswa/semester&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $guru->id_sekolah])->andWhere(['periode_aktif' => 'Aktif'])->all(),'id_semester', 'semester'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Semester') ?>

    <?php }else{ 
                $siswa = Siswa::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();

    ?>
         <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $siswa->id_sekolah])->all(),'id_kelas', 'nama'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Kelas ...',
                    'onchange'=>'
                       $.post( "index.php?r=siswa/semester&id='.'"+$(this).val(), function( data ){
                       $("select#smt").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $siswa->id_sekolah])->andWhere(['periode_aktif' => 'Aktif'])->all(),'id_semester', 'semester'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Semester') ?>
    <?php } ?>

    <?= $form->field($model, 'nis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group" style="float:right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
