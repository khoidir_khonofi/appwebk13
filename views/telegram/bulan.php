<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KotaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Berita Terbit';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kota-index">
    <?php
    if(isset($_GET['tahun']) AND isset($_GET['bulan'])){
    $tahun = $_GET['tahun'];
     $bulan = $_GET['bulan'];
     
     } else { 
        $tahun = date('Y');
         $bulan = date('m');
         } ?>
    <h1><?= Html::encode($this->title) ?> Periode <?= $bulan ?>/<?= $tahun ?></h1>
    <?php
    $start = date($tahun.'-'.$bulan.'-01');
    $end = date($tahun.'-'.$bulan.'-t');
    $count = date('t',strtotime(date($tahun.'-'.$bulan.'t')));
    ?>
    <p>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['laporan/pdf']) ?>&tahun=<?= $tahun; ?>&bulan=<?= $bulan ?>" class="btn btn-danger btn-circle pull-right" target="_blank" style="margin-left: 10px">
            <i class="fa fa-print"> </i> CETAK
        </a>
    </p>
    <p>
        <button type="button" class="btn btn-info btn-circle pull-right" data-toggle="modal" data-target="#printmodel">
            <i class="fa fa-search"></i> CEK DATA
        </button>
    </p>

    <div class="modal fade " id="printmodel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-center" id="exampleModalLabel">Print Laporan Berita</h3>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <form action="?r=laporan/index" method="get">
                            <input name="r" type="hidden" value="laporan/index">
                           
                            <div class="col-lg-4">
                                <select class="form-control" name="tahun">
                                    <option value="0">Pilih Tahun</option>
                                    <?php
                                  //  $thn = app\models\Berita::find()->groupby()->all()

                                    for($i = date('Y'); $i >= 2000; $i--){
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                             <div class="col-lg-4">
                                <select class="form-control" name="bulan">
                                    <option value="0">Pilih Bulan</option>
                                    <?php
                                    $bulan = [
                                        1 => 'Januari',
                                        2 => 'Februari',
                                        3 => 'Maret',
                                        4 => 'April',
                                        5 => 'Mei',
                                        6 => 'Juni',
                                        7 => 'Juli',
                                        8 => 'Agustus',
                                        9 => 'September',
                                        10 => 'Oktober',
                                        11 => 'November',
                                        12 => 'Desember',
                                    ];
                                    for($i = 1; $i <= 12; $i++){
                                        echo "<option value='".$i."'>".$bulan[$i]."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                

                                 <button type="submit" class="btn btn-block btn-info btn-circle"><i class="fa fa-print"></i> Sort</button>

       
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($_GET['tahun']) AND isset($_GET['bulan'])){
        $tahun = $_GET['tahun']; 
     $bulan = $_GET['bulan'];
      
  } else { 
    $tahun = date('Y'); 
    $bulan = date('m'); } ?>
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Judul Berita</th>
                <th>Status Berita</th>
                <th>Tanggal Berita</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>
        <?php

        $no = 1;
       
            //$kecamatan = \app\models\Kecamatan::find()->where(['kecamatanKotaID' => $d['kotaID']])->all();
       
            if($bulan+1 > 12){
                $bulan2 = 1;
                $tahun2 = $tahun+1;
            } else {
                $tahun2 = $tahun;
                $bulan2 = $bulan+1;
            }
            $user = backend\models\User::find()->all();
            foreach ($user as $key => $value) {
                # code...
                $a[]= $value['userID'];
                  
                  }
                  $array = implode(",",$a);
                  $bencana = \app\models\Berita::find()->where('id_wartawan IN ('.$array.')')->andWhere(["between",'tanggal_berita',$tahun.'-'.$bulan.'-01',$tahun2.'-'.$bulan2.'-00'])->orderBy(['tanggal_berita' => SORT_DESC])->all();

                  foreach ($bencana as $key => $f) {
                      # code...
                   $nama = backend\models\User::find()->where(['userID' => $f['id_wartawan']])->all();
                   foreach ($nama as $key => $test) {
                       # code...
                   $cek = count($bencana);

                   
                    echo "<tr>";
                    if($cek != 0 ){

                        echo "<td>" . $no++ . "</td>";
                        echo "<td>" .$test['nama']. "</td>";
                        echo "<td>" . $f['judul'] . "</td>";
                        if($f['status_berita'] == "Terbit"){
                        echo "<td>" . "<span class='label label-info'>".$f['status_berita'] . "</span></td>";
                        }elseif($f['status_berita'] == "Headline"){
                             echo "<td>" . "<span class='label label-success'>".$f['status_berita'] . "</span></td>";
                            }else{
                                 echo "<td>" . "<span class='label label-warning'>".$f['status_berita'] . "</span></td>";
                            }
                        echo "<td>" . date('d M Y', strtotime($f['tanggal_berita'])) . "</td>";
                        echo "<td>"."<a href='".Yii::$app->urlManager->createUrl(['berita/view', 'id' => $f['id_berita']])."'>"."<u>Detail Berita</u>". "</td>";
                      
                    }else{
                        echo "<td>"."test"."</td>";
                        echo    "<script>
                                alert('data kosong');
                                window.location=('index.php?r=laporan');
                                </script>";
                    }
                 echo "</tr>";
            
        }
        }
        ?>
        </tbody>
    </table>

    </div>
