<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telegram */

$this->title = 'Create Telegram';
$this->params['breadcrumbs'][] = ['label' => 'Telegrams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
