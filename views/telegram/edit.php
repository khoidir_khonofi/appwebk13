<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use app\models\Sekolah;
use app\models\Telegram;
use app\models\Guru;
use yii\helpers\ArrayHelper;
use app\models\Kelas;
use app\models\Siswa;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(); ?>


<?= $form->field($model, 'chat_id_telegram')->textInput(['placeholder' => 'Contoh : 234564345']) ?>

<div class="form-group" style="float:right; margin-top: 5px;">
     <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

  
</div>

<?php ActiveForm::end(); ?>