<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\form\ActiveField;
use kartik\select2\Select2;
use app\models\Sekolah;
use app\models\Telegram;
use app\models\Guru;
use yii\helpers\ArrayHelper;
use app\models\Kelas;
use app\models\Siswa;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TelegramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Telegram';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
   

   <?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'foto',
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function($model){
                    if ($model->foto != NULL) {
                         return Html::img('foto_siswa/'.$model->foto, [
                        'width' => '40px', 
                        'height' => '40px',
                        'value' => Url::to([
                            'mengajar/lihatfoto',
                            'id' => $model->id_siswa
                        ]),
                        'title' => '',
                        'class' => 'showModalButton',
                        'style' => 'border-radius:100%'
                    ]);
                    }else{
                        return "";
                    }
                   
                }
            ],
             'nama',
            
              'nis',
            
             [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas." ".$model->kelas->nama;
                }
            ],
            // [
            //     'attribute' => 'id_semester',
            //     'label' => 'Semester',
            //     'value' => function($model){
            //         return $model->semester->semester;
            //     }
            // ],
            [
                'attribute' => 'chat_id_telegram',
                'label' => 'Id Chat Telegram',
                'value' => function($model){
                    if ($model != NULL) {
                       return $model->chat_id_telegram;
                    }else{
                        return "-";
                    }
                    
                }
            ],

           ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                    

                            $c = Html::a('<span class="fa fa-send"></span> Nilai Pengetahuan', ['telegrampengetahuan', 'id' => $model->id_siswa], [
                                'class' => 'btn btn-primary',
                                'title' => 'Kirim',
                                'target' => '_blank', 
                            ]);

                            $a = Html::a('<span class="fa fa-send"></span> Nilai Keterampilan', ['telegramketerampilan', 'id' => $model->id_siswa], [
                                'class' => 'btn btn-primary',
                                'title' => 'Kirim',
                                'target' => '_blank',
                            
                            ]) ;
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['editid','id'=>$model->id_siswa]),
                                'title' => '', 'class' => 'showModalButton btn btn-success']);
                        
                            return $b.' '.$c.' '.$a;

                       
                    }
                ],
                

        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
                 Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left',
            'title' => 'Cari'
        ]).''.
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'style' => 'margin-left: 5px'])
         ],
        // '{export}',
        // '{toggleData}'
    ],
    'pjax' => false,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>


<?php
    yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-default',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
    ]);
    echo "<div id='modalContent'></div>";
     echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
    yii\bootstrap\Modal::end();
?>