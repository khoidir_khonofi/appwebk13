<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use app\models\Guru;
use app\models\Siswa;
use app\models\NilaiKdPengetahuan;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuruSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai Harian Pengetahuan';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h2><?= Html::encode($this->title) ;?></h2>
<?php
    date_default_timezone_set('Asia/Jakarta');  //tambahan dari agan  AntiCoding :)
    function time_since($original)
    {
    $chunks = array(
        array(60 * 60 * 24 * 365, 'tahun'),
        array(60 * 60 * 24 * 30, 'bulan'),
        array(60 * 60 * 24 * 7, 'minggu'),
        array(60 * 60 * 24, 'hari'),
        array(60 * 60, 'jam'),
        array(60, 'menit'),
    );

    $today = time();
    $since = $today - $original;


    for ($i = 0, $j = count($chunks); $i < $j; $i++)
    {
        $seconds = $chunks[$i][0];
        $name = $chunks[$i][1];

        if (($count = floor($since / $seconds)) != 0)
        break;
    }

    $print = ($count == 1) ? '1 ' . $name : "$count {$name}";
    return $print . ' yang lalu';
    }
    // echo time_since(strtotime($model->tanggal)); 
?>
<p>Nama : <?= $model->nama ?></p>
<p>Kelas : <?= $model->kelas->tingkat_kelas.''.$model->kelas->nama ?></p>
<p>Semester : <?= $model->semester->semester ?></p>
<?php $form = ActiveForm::begin(); ?>

<?php if ($model->tblsekolah->jenjang_pendidikan == 'SD') { ?>
     <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdPengetahuan->no_kd;
                }
            ],
             [
                'attribute' => 'id_tema',
                'label' => 'tema',
                'value' => function($model){
                    return $model->tema->tema;
                }
            ],
           // 'id_sekolah',
           
            
            [
                'attribute' => 'id_subtema',
                'label' => 'Subtema',
                'value' => function($model){
                    return $model->subtema->subtema;
                }
            ],
            'nilai',
            [
                'attribute' => 'created_at',
                'label' => 'Waktu',
                'format' => 'html',
                'value' => function($model){
                    return time_since(strtotime($model->created_at));
                }
            ],
        [
            'class' => 'kartik\grid\checkboxColumn',
            'name' => 'hapus',
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'pjax' => false,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
<?php }else{ ?>

 <?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
             [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
            [
                'attribute' => 'id_kd_pengetahuan',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kdPengetahuan->no_kd;
                }
            ],
            'nama_penilaian',
            'nilai',
            [
                'attribute' => 'created_at',
                'label' => 'Waktu',
                'format' => 'html',
                'value' => function($model){
                    return time_since(strtotime($model->created_at));
                }
            ],
        [
            'class' => 'kartik\grid\checkboxColumn',
            'name' => 'hapus',
        ],
        ],
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'beforeHeader'=>[
            [
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
       
        'pjax' => false,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
       // 'floatHeaderOptions' => ['top' => $scrollingTop],
        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO
        ],
]);
?>

<?php } ?>
       
 <?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
</section>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>