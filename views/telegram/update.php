<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telegram */

$this->title = 'Update Telegram: ' . $model->id_telegram;
$this->params['breadcrumbs'][] = ['label' => 'Telegrams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_telegram, 'url' => ['view', 'id' => $model->id_telegram]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telegram-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
