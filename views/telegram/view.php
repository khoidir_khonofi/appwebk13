<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Telegram */

$this->title = $model->id_telegram;
$this->params['breadcrumbs'][] = ['label' => 'Telegrams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telegram-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_telegram',
            [
                'attribute' => 'id_sekolah',
                'label' => 'Sekolah',
                'format' => 'html',
                'value' => function($model){
                    return $model->sekolah->nama_sekolah;
                }
            ],
            'api_telegram',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
