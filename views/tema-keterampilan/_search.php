<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\TemaKeterampilanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tema-keterampilan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<?php if(Yii::$app->user->identity->role=="Administrator"){ 
         $sekolah = Sekolah::find()->where(['id_user' =>Yii::$app->user->identity->id_user])->one();
    ?>
        <?= $form->field($model,  'id_kelas')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Kelas ...',
                'onchange'=>'
                               $.post( "index.php?r=tema-keterampilan/mapel&id='.'"+$(this).val(), function( data ){
                               $("select#mapel").html(data);
                              });'
                            ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#test").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_keterampilan', 'judul'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'test'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

    <?php }else{ 
        $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
        $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();        
    ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema-keterampilan/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#kade").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarKeterampilan::find()->where(['id_sekolah' => $guru->id_sekolah])->all(),'id_kd_keterampilan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'kade'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>
    <?php } ?>


    <?= $form->field($model, 'tema')->textInput(['maxlength' => true, 'placeholder' => 'Contoh : Tema 1']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
