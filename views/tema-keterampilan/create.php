<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TemaKeterampilan */

$this->title = 'Create Tema Keterampilan';
$this->params['breadcrumbs'][] = ['label' => 'Tema Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tema-keterampilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
