<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarKeterampilan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tema-form">

    <?php $form = ActiveForm::begin(); ?>
    


    <?= $form->field($model, 'tema')->textInput(['maxlength' => true, 'placeholder' => 'Contoh : Tema 1','required' => 'required']) ?>
     <?= $form->field($model, 'judul')->textArea(['maxlength' => true]) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
