<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TemaKeterampilan */

$this->title = 'Update Tema Keterampilan: ' . $model->id_tema;
$this->params['breadcrumbs'][] = ['label' => 'Tema Keterampilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tema, 'url' => ['view', 'id' => $model->id_tema]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tema-keterampilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
