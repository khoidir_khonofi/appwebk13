<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Sekolah;
use app\models\User;
use app\models\Semester;
use app\models\Guru;
use app\models\Kelas;
use app\models\MataPelajaran;
use app\models\Siswa;
use app\models\KompetensiDasarPengetahuan;

/* @var $this yii\web\View */
/* @var $model app\models\SdNilaiHarianKeterampilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tema-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->role=="Administrator"){ 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();    
    ?>
        <?= $form->field($model,  'id_kelas')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Kelas ...',
                'onchange'=>'
                               $.post( "index.php?r=tema/mapel&id='.'"+$(this).val(), function( data ){
                               $("select#mapel").html(data);
                              });'
                            ],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih Kelas') ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#kd").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kd_pengetahuan', 'judul'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...', 'id' => 'kd'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>

    <?php }else{
         $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
         $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();    
    ?>

        <?= $form->field($model, 'id_mapel')->widget(Select2::classname(),[
            'data' => ArrayHelper::map(MataPelajaran::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_mapel', 'nama_mata_pelajaran'),
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Mata Pelajaran ...', 'id' => 'mapel',
                    'onchange'=>'
                       $.post( "index.php?r=tema/kdktr&id='.'"+$(this).val(), function( data ){
                       $("select#kade").html(data);
                      });'
                    ],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ])->label('Mata Pelajaran') ?>

         <?= $form->field($model,  'id_kd')->widget(Select2::classname(),[
                'data' => ArrayHelper::map(KompetensiDasarPengetahuan::find()->all(),'id_kd_pengetahuan', 'no_kd'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih KD ...' ,'id' => 'kade'],
                'pluginOptions' => [
                    'allowClear' => true,
                    
                ],
            ])->label('Pilih KD') ?>
    <?php } ?>


    <?= $form->field($model, 'tema')->textInput(['maxlength' => true, 'placeholder' => 'Contoh : Tema 1']) ?>
     <?= $form->field($model, 'judul')->textArea(['maxlength' => true]) ?>


    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
