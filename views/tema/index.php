<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Subtema;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tema Pengetahuan';
$this->params['breadcrumbs'][] = $this->title;
?>
 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
                

    <h1><?= Html::encode($this->title) ?></h1>

   
            <p>
                <?= Html::button('Tambah Tema', [
                    'value' => Url::to(['create']),
                    'title' => '', 
                    'class' => 'showModalButton btn btn-info'
                    ]);
                ?>
           </p>

   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tema',
            //'id_sekolah',
            [
                'attribute' => 'id_kelas',
                'label' => 'Kelas',
                'value' => function($model){
                    return $model->kelas->tingkat_kelas."".$model->kelas->nama;
                }
            ],
            [
                'attribute' => 'id_mapel',
                'label' => 'Mata Pelajaran',
                'value' => function($model){
                    return $model->mapel->nama_mata_pelajaran;
                }
            ],
             [
                'attribute' => 'id_kd',
                'label' => 'KD',
                'value' => function($model){
                    return $model->kd->no_kd;
                }
            ],
             [
                'attribute' => 'tema',
                'label' => 'Tema',
                'format' => 'html',
                'value' => function($model){
                    $link  = Subtema::find()->where(['id_tema' => $model->id_tema])->one()['id_tema'];
                    $url = Yii::$app->urlManager->createUrl(['subtema/lihat','id' => $link]);
                    return '<a href="'.$url.'">'.$model->tema.'</a>';
                }
            ],
            //'created_at',
            //'updated_at',

             ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                       
                            $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_tema]),
                                'title' => '', 'class' => 'showModalButton btn btn-info', 'style' => 'border-radius:100%;']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['update','id'=>$model->id_tema]),
                                'title' => '', 'class' => 'showModalButton btn btn-success', 'style' => 'border-radius:100%;']);
                             $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_tema], [
                                    'class' => 'btn btn-danger', 'style' => 'border-radius:100%;',
                                    'title' => 'Delete',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                            
                                ]) ;
                              $d = Html::button('Tambah Subtema',
                                ['value' => Url::to(['subtema/tambahsubtema','id'=>$model->id_tema]),
                                'title' => '', 'class' => 'showModalButton btn btn-warning']);
                           if(Yii::$app->user->identity->role=="Administrator" || Yii::$app->user->identity->role=="Guru"){
                             return $a." ".$b." ".$c." ".$d;
                         }else{
                            return $a;
                         }
                           
                        
                            
                        }
                ],
                

        
        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
         Html::button('Search', [
            'value' => Url::to(['search']), 
            'class' => 'showModalButton btn btn-default',
            'style' => 'float:left; margin-right:5px;',
        ]).''.
         
        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        Html::a('<span class="fa fa-repeat"></span>', ['index'], ['data-pjax'=>0, 'class' => 'btn btn-default'])
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>



<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>