<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Guru;
use app\models\Kelas;
use app\models\Semester;
use app\models\Sekolah;
/* @var $this yii\web\View */
/* @var $model app\models\TinggiBadan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tinggi-badan-form">

    <?php $form = ActiveForm::begin(); 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
   

    ?>
    <?php   if(Yii::$app->user->identity->role=="Administrator"){ ?>
        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),[
			                        'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->AndWhere(['status' => 'Aktif'])->all(),'id_kelas', 'nama'),
			                        'language' => 'de',
			                        'options' => ['placeholder' => 'Pilih Kelas ...', 'id' => 'kls',
                                    'onchange'=>'
                                                   $.post( "index.php?r=esktrakurikuler/siswa&id='.'"+$(this).val(), function( data ){
                                                   $("select#ssw").html(data);
                                                  });'
                                                ],
			                        'pluginOptions' => [
			                            'allowClear' => true,
			                            
			                        ],
			                    ])->label('Kelas') ?>

                            <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
			                        'data' => ArrayHelper::map(Siswa::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_siswa', 'nama'),
			                        'language' => 'de',
			                        'options' => ['placeholder' => 'Pilih Siswa ...'  , 'id' => 'ssw',
                                    'onchange'=>'
                                                   $.post( "index.php?r=esktrakurikuler/semester&id='.'"+$(this).val(), function( data ){
                                                   $("select#smt").html(data);
                                                  });'
                                                ],
			                        'pluginOptions' => [
			                            'allowClear' => true,
			                            
			                        ],
			                    ])->label('Siswa') ?>

                <?= $form->field($model, 'id_semester')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Semester::find()->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_semester', 'semester'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Semester ...', 'id' => 'smt'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Semester') ?>
    <?php }else{ 
     $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
     $kelas = Kelas::find()->where(['id_guru' => $guru->id_guru])->one();
    ?>

                <?= $form->field($model, 'id_siswa')->widget(Select2::classname(),[
                        'data' => ArrayHelper::map(Siswa::find()->where(['id_kelas' => $kelas->id_kelas])->all(),'id_siswa', 'nama'),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih Siswa ...' ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            
                        ],
                    ])->label('Siswa') ?>

    <?php } ?>

    <?= $form->field($model, 'aspek_penilaian')->widget(Select2::classname(),[
            'data' => [ 
                'Tinggi Badan' => 'Tinggi Badan', 
                'Berat Badan' => 'Berat Badan',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

        <?= $form->field($model, 'nilai')->textInput(['required' => 'required']) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
