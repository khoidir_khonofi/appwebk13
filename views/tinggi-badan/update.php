<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TinggiBadan */

$this->title = 'Update Tinggi Badan: ' . $model->id_tinggi_badan;
$this->params['breadcrumbs'][] = ['label' => 'Tinggi Badans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tinggi_badan, 'url' => ['view', 'id' => $model->id_tinggi_badan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tinggi-badan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
