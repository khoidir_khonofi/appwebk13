<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TinggiBadan */

$this->title = $model->id_tinggi_badan;
$this->params['breadcrumbs'][] = ['label' => 'Tinggi Badans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tinggi-badan-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_tinggi_badan',
            'id_sekolah',
            'id_siswa',
            'aspek_penilaian',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
