<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password') ?>

     <?= $form->field($model, 'role')->widget(Select2::classname(),[
            'data' => [ 'Administrator' => 'Sekolah','Guru' => 'Guru', 'Siswa' => 'Siswa', ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Level ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>

    <div class="form-group" style="float: right;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
