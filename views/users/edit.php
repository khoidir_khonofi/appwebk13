<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use app\models\Siswa;
use app\models\Sekolah;
use app\models\Guru;
use app\models\Kelas;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form-signup',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'fieldConfig' => ['autoPlaceholder' => true],
       
    ]); 
    $sekolah = Sekolah::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    $guru = Guru::find()->where(['id_user' => Yii::$app->user->identity->id_user])->one();
    ?>

         <?=  $form->field($model, 'email', [
            'addon' => ['prepend' => ['content'=>'@']]
        ]);?>

        <?= $form->field($model, 'username', [
          'feedbackIcon' => [
              'default' => 'user',
              'success' => 'user-plus',
              'error' => 'user-times',
              'defaultOptions' => ['class'=>'text-warning']
          ]
      ])->textInput(['placeholder'=>'Enter username...']); ?>

        <?= $form->field($model, 'password', [
          'feedbackIcon' => [
              'default' => 'lock',
              'success' => 'user-plus',
              'error' => 'user-times',
              'defaultOptions' => ['class'=>'text-warning']
          ]
      ])->textInput(['placeholder'=>'Enter Password...', 'type' => 'password']); ?>
        
      <?php if(Yii::$app->user->identity->role=="Administrator"){  ?>
      
          <?= Select2::widget([
              'model' => $model,
              'attribute' => 'role',
              'data' => ['Guru' => 'Guru', 'Siswa' => 'Siswa'],
              'options' => ['placeholder' => 'Mendaftar Sebagai...', 'id'=>'status'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]); ?>
          <br>

          
            
            <div style="display: none" id="kelassiswa">
                <?= $form->field($siswa, 'id_kelas')->widget(Select2::classname(),[
                  'data' => ArrayHelper::map(Kelas::find()->select(['id_kelas', 'nama' => 'concat(tingkat_kelas, " ", nama)'])->where(['id_sekolah' => $sekolah->id_sekolah])->all(),'id_kelas', 'nama'),
                  'language' => 'de',
                  'options' => ['placeholder' => 'Pilih Kelas ...',
                  'onchange'=>'
                        $.post( "index.php?r=users/guru&id='.'"+$(this).val(), function( data ){
                        $("select#cikgu").html(data);
                        });'
                      ],
                  'pluginOptions' => [
                      'allowClear' => true,
                  ],
              ]) ?>
            </div>

            <div style="display: none" id="data-gurusiswa">
              <?= $form->field($siswa, 'id_guru')->widget(Select2::classname(),[
                  'data' => ArrayHelper::map(Guru::find()->all(),'id_guru', 'nama'),
                  'language' => 'de',
                  'options' => ['placeholder' => 'Pilih Guru ...','id' => 'cikgu'],
                  'pluginOptions' => [
                      'allowClear' => true,
                  ],
              ]) ?>
            </div>
        
      <?php }else{ ?>

                <?= Select2::widget([
                  'model' => $model,
                  'attribute' => 'role',
                  'data' => ['Siswa' => 'Siswa'],
                  'options' => ['placeholder' => 'Mendaftar Sebagai...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
                ]); ?>
                
      <?php } ?>

      <?= $form->field($model, 'status')->widget(Select2::classname(),[
            'data' => [ 
                'Aktif' => 'Aktif', 
                'Tidak Aktif' => 'Tidak Aktif',
            ],
            'language' => 'de',
            'options' => ['placeholder' => 'Pilih Status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                
            ],
        ]) ?>
              
      
        <div class="form-group" style=" float: right;">
            
               <?= Html::submitButton('Daftar', ['class' => 'btn btn-info']) ?>
               <button type="Reset" class="btn btn-success">Reset</button>
         
        </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
  
$( document ).ready(function() {
  $('#status').change(function(){
    if($(this).val()=="Guru"){
      $('#data-gurusiswa').hide('slow');
      $('#kelassiswa').hide('slow');

    }else if($(this).val()=="Siswa"){
      $('#data-gurusiswa').show('slow');
      $('#kelassiswa').show('slow');

    }else{
      $('#data-gurusiswa').hide('slow');
      $('#kelassiswa').hide('slow');
    }
  })
});

</script>