<?php

use yii\helpers\Html;
use app\models\Siswa;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
    <h1><?= Html::encode($this->title) ?></h1>

   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
<?php
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
     'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // [
            //     'attribute' => 'id_user',
            //     'label' => 'Id user',
            //     'format' => 'html',
            //     'value' => function($model){
            //         if($model->status == "Aktif"){
            //             return $model->id_user;
            //         }else{
            //             return '<span style="color:grey">'.$model->id_user."</span>";
            //         }
            //     }
            // ],
            [
                'attribute' => 'email',
                'label' => 'Email',
                'format' => 'html',
                'value' => function($model){
                    if($model->status == "Aktif"){
                        return $model->email;
                    }else{
                        return '<span style="color:grey">'.$model->email."</span>";
                    }
                }
            ],
            [
                'attribute' => 'username',
                'label' => 'Username',
                'format' => 'html',
                'value' => function($model){
                    if($model->status == "Aktif"){
                        $link = Siswa::find()->where(['id_user' => $model->id_user])->one()['id_user'];
                        $url = Yii::$app->urlManager->createUrl(['siswa/siswaview', 'id' => $link]);
                        return '<a href="'.$url.'">'.'<u>'.$model->username.'</u></a>';
                    }else{
                        return '<span style="color:grey">'.$model->username."</span>";
                    }
                }
            ],
            //'password',
            
            [
                'attribute' => 'role',
                'label' => 'Role',
                'format' => 'html',
                'value' => function($model){
                    if ($model->role == 'Administrator') {
                        if($model->status=="Aktif"){
                            return "<span class='label label-info'>Sekolah</span>";
                        }else{
                            return "<span class='label label-default'>Sekolah</span>";
                        }
                        
                    }elseif ($model->role == "Guru") {
                        if ($model->status == "Aktif") {
                            return "<span class='label label-danger'>Guru</span>";
                        }else{
                            return "<span class='label label-default'>Guru</span>";
                        }
                        
                    }else{
                        if ($model->status == "Aktif") {
                            return "<span class='label label-success'>Siswa</span>";
                        }else{
                            return "<span class='label label-default'>Siswa</span>";
                        }
                    }
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'status',
                'format' => 'html',
                'value' => function($model){
                    if($model->status == "Aktif"){
                        return $model->status;
                    }else{
                        return '<span style="color:grey">'.$model->status."</span>";
                    }
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Aksi',
                'format' => 'html',
                'value' => function($model){
                    if(Yii::$app->user->identity->role == "Administrator" || Yii::$app->user->identity->role == "Guru"){
                        if($model->status == "Tidak Aktif"){
                            return Html::a('Aktifkan', ['aktif', 'id' => $model->id_user], [
                                'class' => 'btn btn-info',
                                'title' => 'Konfirmasi',
                                'data-confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                                
                                
                            ]) ;
                        }else{
                            return Html::a('Nonaktifkan', ['tidakaktif', 'id' => $model->id_user], [
                                'class' => 'btn btn-danger',
                                'title' => 'Konfirmasi',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                                
                            ]);
                        } 
                
                }else{
                    return "-";
                }
            }
            ],
            //'role',
            //'authKey',
            //'accessToken',
           // 'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false,
                    'delete' => false,
                ],
                'buttons' => [
                      'view' => function($url,$model){
                        if(Yii::$app->user->identity->role=="Siswa"){
                            $a1 = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_user]),
                                'title' => '', 'class' => 'showModalButton btn btn-info','style' => 'border-radius:100%']);
                           
                            return $a1;
                        }else{
                             $a = Html::button('<span class="fa fa-eye"></span>',
                                ['value' => Url::to(['view','id'=>$model->id_user]),
                                'title' => '', 'class' => 'showModalButton btn btn-info','style' => 'border-radius:100%']);
                            $b = Html::button('<span class="fa fa-pencil"></span>',
                                ['value' => Url::to(['edit','id'=>$model->id_user]),
                                'title' => '', 'class' => 'showModalButton btn btn-success','style' => 'border-radius:100%']);
                            $c = Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id_user], [
                            'class' => 'btn btn-danger','style' => 'border-radius:100%',
                            'title' => 'Delete',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                            
                        ]) ;
                            return $a." ".$b." ".$c;
                            
                        }
                        }
                ],
                

        ],
        ],
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
         ['content'=>
        
          Html::button('Tambah',
                ['value' => Url::to(['create']),
                'title' => 'Tambah Baru User', 'class' => 'showModalButton btn btn-default','style' => 'margin-right:5px;']).'   '.
           Html::button('Search',
                ['value' => Url::to(['search']),
                'title' => 'Cari User', 'class' => 'showModalButton btn btn-default']).' ',

        //     Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
       
         ],
        '{export}',
        '{toggleData}'
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
   // 'floatHeaderOptions' => ['top' => $scrollingTop],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_INFO
    ],
]);
?>
</div>
</div>
</div>
</div>
</section>


<?php
yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-default',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
 echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
yii\bootstrap\Modal::end();
?>