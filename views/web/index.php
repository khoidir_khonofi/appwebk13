<?php
$this->title = 'Web';
?>

<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" href="WebAsset/css/style.minified.css">
    <link rel="stylesheet" href="WebAsset/css/main.css">
</head>
<body>
        <section class="section-full">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-offset-2 col-md-8">
                            <div class="section-title style3 text-center">
                                <h2 class="text-uppercase" style="color: #47c1e6;">SELAMAT DATANG DI <b> SMART-K13</b></h2>
                                <p>APLIKASI  PENILAIAN KURIKULUM TAHUN 2013 BERBASIS WEB BERDASARKAN PERMENDIKBUD NO 24 TAHUN 2016</p>
                                <p>Aplikasi penilaian ini untuk sekolah jenjang pendidikan SD, SMP dan SMA yang berstatus negeri</p>
                                <?php if (Yii::$app->user->isGuest) {?>
                                	<a href="<?= Yii::$app->urlManager->createUrl(['site/login']) ?>" class="btn-large waves-effect btn-sky-blue text-uppercase" style="margin-bottom: 20px;"><i class="fa fa-sign-in"> </i>Login</a>
                                <?php }else{?>
                                	
                                    <a href="<?= Yii::$app->HomeUrl ?>" class="btn-large waves-effect btn-sky-blue text-uppercase" style="margin-bottom: 20px;">Index</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="devider style-2"><span class="sep"></span></div>
                        </div>
                    </div> <!-- end .row -->
                </div> <!-- .container -->
            </section> <!-- end .section-full -->

        <center>
			<a class="store-badge google" href="#"><img src="WebAsset/img/download.png" alt="Play Store Badge"></a>
		</center>
</body>
</html>
<script src="WebAsset/js/scripts.minified.js"></script>
<script src="WebAsset/js/main.js"></script>
<script>
         
